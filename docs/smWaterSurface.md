Simple shader for water surface, as one of shaders in “sm…” series, doesn’t have any options. They’re there for compatibility reasons, but they don’t affect anything, apart from one, *ksAlphaRef*, setting up type of water surface.

Supported types:
- Ocean: `ksAlphaRef = -2`;
- Lake: `ksAlphaRef = -1`;
- Sea: `ksAlphaRef = 0`;
- Pool: `ksAlphaRef = 1`;
- Pond: `ksAlphaRef = 2`.

If you think the way shader looks needs to be changed, or new type should be added, please contact me. I think it would be better if water looked the same across different tracks. Although, quite possibly, I’ll add customizable version later on.

Also, in general, if you’re targeting CSP only, it would be much better to set up water using extension config and “materials_track.ini”.