Shader which is very similar to **ksMultilayer_fresnel_nm**, but with four separate normal textures instead of single one, mapped same as detail textures, and masked with *txMask*. Good for landscapes.

### Tiling fix

Unlike original multilayer shader, this one has proper options for tiling fix, separate for each channel: *tilingFixR*, *tilingFixG*, *tilingFixB* and *tilingFixA*. To enable tiling fix, change according option from `0` to `1`. Please keep in mind tiling fix requires an extra texture sample, do not use it for textures which are too high res.
