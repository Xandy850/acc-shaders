#define EXTRA_CARPAINT_FIELDS_2\
  float extNormalsNoise;\
  float extNormalsNoiseUv;

#define ALTER_NORMAL\
  [branch]\
  if (extNormalsNoise){\
    normalW = normalize(normalW + txNoise.SampleLevel(samLinearSimple, pin.Tex * extNormalsNoiseUv, 0).xyz * extNormalsNoise);\
  }

#include "smCarPaint_ps.fx"