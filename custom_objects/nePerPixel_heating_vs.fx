#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
// alias: nePerPixelReflection_heating_vs

PS_IN_PerPixelPosL main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixelPosL);
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = vin.NormalL.xyz;
  return vout;
}
