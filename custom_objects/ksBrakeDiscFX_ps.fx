#define USE_BRAKE_DISC_MODE
#define USE_BLURRED_NM
#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define DIM_REFLECTTIONS_WITH_SAMPLE_PARAM
#define CALCULATELIGHTING_USETXSPECULAR_AS_REFLECTANCE_MODEL
#define SSLR_FORCE 1
#define IS_ADDITIVE_VAR 0
#define ALLOW_BRAKEDISCFX

#include "include_new/base/_include_ps.fx"
#include "common/brakeDiscFX.hlsl"

float posWithinCircle(float3 delta, float3 wheelNormal) {
  return length(delta - wheelNormal * dot(delta, wheelNormal));
}

float carvedShape(float3 delta, float3 wheelNormal, float3 wheelUp) {  
  float3 p = delta - wheelNormal * dot(delta, wheelNormal);
  float3 mdir0 = normalize(wheelUp);
  float3 mdir1 = normalize(cross(mdir0, wheelNormal));
  float angle = M_PI + atan2(dot(p, mdir0), dot(p, mdir1)) * extCarvedPattern;
  return frac(angle) < fwidth(angle);
}

float3 getHeatColor(float h) {
  float T = h * 5600;
  if (T < 400) return 0;
  float3 f = float3(1, 1.5, 2);
  float3 O = 100 * f * f * f / (exp(f * 19e3 / T) - 1);
  return 3 * O;
}

float anisReflModel(float3 viewDir, float3 lightDir, float3 normalW, 
    float3 dirX, float3 dirY, float x, float y) {
  float3 halfwayVector = normalize(lightDir + viewDir);
  float dotLN = dot(lightDir, normalW);  
  float dotHN = dot(halfwayVector, normalW);
  float dotVN = dot(viewDir, normalW);
  float dotHTAlphaX = dot(halfwayVector, dirX) / x;
  float dotHBAlphaY = dot(halfwayVector, dirY) / y;
  return sqrt(max(0, dotLN / dotVN)) * exp(-2 * (dotHTAlphaX * dotHTAlphaX + dotHBAlphaY * dotHBAlphaY) / (1 + dotHN));
}

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float4 txNormalValue) {
  float3 T = normalize(tangentW);
  float3 B = normalize(bitangentW);
  float4 txNormalBaseValue = txNormal.Sample(samLinearSimple, uv);
  float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
  txNormalValue = lerp(txNormalBaseValue, txNormalBlurValue, blurLevel);
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_BrakeDiscFX pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, 0);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float3 txGlowValue = txGlow.Sample(samLinear, pin.Tex).rgb;
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float4 txNormalValue;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, txNormalValue);

  float3 specularValue = 0;
  float reflectionOcclusion = 1;

  float3 resultGlow = 0;
  float baseWorn = saturate(extWorn);
  // baseWorn = 0.5 + 0.5 * sin(ksGameTime / 1000);
  // baseWorn = 0.5;
  // baseWorn = 1;
  float heatK = saturate(extGlowMult < 0 ? abs(extGlowMult) : glowLevel * extGlowMult);
  // heatK = 0.5 + 0.5 * sin(ksGameTime / 1000);
  // heatK += 0.9;

  // float blurLevel = 0.5 + 0.5 * sin(ksGameTime / 60);

  float3 extWheelNormalW = normalize(pin.WheelNormalW);
  float3 extWheelUpW = normalize(pin.WheelUpW);
  if (dot(pin.NormalW, extWheelNormalW) < 0) extWheelNormalW = -extWheelNormalW;

  #ifdef ALLOW_BRAKEDISCFX
    float3 posRel = pin.PosC - pin.WheelCenterC;
    float withinCircle = posWithinCircle(posRel, extWheelNormalW);

    float holeValue = saturate(txDiffuseValue.a * extAlphaAsMaskMul + extAlphaAsMaskAdd);
    float mapValue = saturate(dot(txDiffuseValue.rgb, 0.33) * extLuminocityAsMaskMul + extLuminocityAsMaskAdd);
    mapValue *= withinCircle < extBrakeDiscRadius - 0.01 ? saturate(length(txNormalValue.xy * 2 - 1) * extNormalAsMaskMul + extNormalAsMaskAdd) : 1;

    float3 dir0 = normalize(cross(extWheelNormalW, posRel));
    float3 dir1 = normalize(cross(extWheelNormalW, dir0));

    float withinDisk = (withinCircle > extBrakeDiscInnerRadius) * pow(dot(normalize(pin.NormalW), extWheelNormalW), 4);
    float glowArea = saturate((withinCircle - extBrakeDiscInnerRadius) / (extBrakeDiscRadius - extBrakeDiscInnerRadius));
    glowArea = 1 - abs(glowArea * (1 + extGlowOffset) - extGlowOffset);
    glowArea = saturate(glowArea * 1.1);

    float wearGradient = glowArea;
    float4 noiseBase = txNoise.Sample(samLinearSimple, extNoiseOffset + pin.Tex * 0.4);
    float4 noiseCircle = txNoise.Sample(samLinear, extNoiseOffset + float2(withinCircle * 37, withinCircle * 13));
    float4 noiseCircleChunk = txNoise.Sample(samLinear, extNoiseOffset + float2(withinCircle * 7, withinCircle * 5));
    float groove = lerp(1, noiseCircle.x, mapValue);
    float grooveLarge = lerp(1, (saturate(remap(noiseCircleChunk.x, 0.45, 0.55, 0, 1))
      + saturate(remap(noiseCircleChunk.y, 0.45, 0.55, 0, 1))) / 2, mapValue);
    float wearK = lerp(baseWorn * 0.4, 1, saturate(wearGradient + baseWorn * 2 - 1 + lerp(-0.4, 0.4, noiseBase.x * noiseBase.y) * baseWorn) * groove);
    float mapShading = lerp(mapValue, 1, pow(1 - wearK, 2) * 0.8);

    float ceramicTexture = lerp(dot(txNoise.Sample(samLinear, pin.Tex * 13).xyz 
       * txNoise.Sample(samLinear, (pin.Tex + 0.5) * 21).xyz, 0.33), 0.1 + dot(noiseCircle, 0.07), blurLevel) * 0.8 + 0.2;
    float ceramic = withinDisk * extCeramicBrakes;
    
    float brushedK = withinCircle > extBrakeDiscInnerRadius ? withinDisk * lerp(1, mapShading, baseWorn) : 0;
    float2 roughness = lerp(0.22, float2(extRoughnessX, extRoughnessY), withinDisk * mapValue);

    wearK = lerp(0, wearK, withinDisk);
    wearK = lerp(groove * baseWorn / 2, 1, wearK);
    float cleanK = 1 - wearK;
    float cleanK2 = pow(cleanK, 2);
    float cleanKS = 1 - pow(wearK, 2);

    float carvedBase = withinDisk * cleanK2 * cleanK2 * mapValue;
    float carved = 0;
    [branch] 
    if (extCarvedPattern) {
      carved = carvedShape(posRel, extWheelNormalW, extWheelUpW) * carvedBase * (1 - blurLevel);
    }

    roughness *= lerp(0.2, 1, cleanK);
    ceramicTexture *= lerp(groove * 0.7 + grooveLarge * 0.3, 1, cleanK);

    [branch] 
    if (extOverrideNormalMap) {
      normalW = lerp(normalW, extWheelNormalW, withinDisk * mapValue);
      normalW = lerp(normalW, 
        normalize(extWheelNormalW * noiseCircle.x + dir0 * (noiseCircle.y - 0.5) + dir1 * (noiseCircle.z - 0.5)),
        brushedK * wearK * extOverrideNormalMap * 0.01);
      normalW = normalize(normalW);
    }

    [branch] 
    if (extOverrideDiffuseMap) {
      float3 baseColor = lerp(lerp(0.05, 0.1, cleanK2), ceramicTexture * 0.6, ceramic) * extOverrideDiffuseMap;
      txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, baseColor * lerp(0.9, 1, mapValue) * holeValue, withinDisk);
    }

    roughness = lerp(roughness, lerp(0, 0.2, ceramicTexture), ceramic * mapValue);
    roughness = lerp(roughness, 0.2, carvedBase * (1 - carved) * 0.3);
    float reflMask = lerp(1, ceramicTexture, ceramic * 0.5) * lerp(1, holeValue, withinDisk);

    // float extDebugMode = 1;

    [branch]
    if (extDebugMode) {
      if (withinCircle < extBrakeDiscInnerRadius) {
        txDiffuseValue.rgb = float3(0, 0, 0.5);
      } else if (withinCircle < extBrakeDiscRadius) {
        txDiffuseValue.rgb = float3(1 - mapValue, 1, mapValue) * holeValue;
      } else {
        txDiffuseValue.rgb = float3(1, 0, 1); 
      }
      if (abs(withinCircle - extBrakeDiscInnerRadius) < 0.002) {
        txDiffuseValue.rgb = float3(3, 0, 0); 
      }
      if (abs(withinCircle - extBrakeDiscRadius) < 0.002) {
        txDiffuseValue.rgb = float3(3, 0, 0); 
      }
    }
    
    float heatL = saturate(heatK * 20) * saturate(heatK * 0.7 + 0.3) * lerp(1, 0.9, noiseBase.z * noiseBase.w * (1 - blurLevel));
    float grooveOffset = mapValue * lerp(0, groove * 0.1 + grooveLarge * 0.4, (1 - heatK * heatK * 0.9)) * saturate(1 - pow(saturate(glowArea * 1.2), 2));
    resultGlow = getHeatColor((heatL + glowArea * 0.35 - 0.35 - grooveOffset) * lerp(lerp(0.6, 1.1, heatK), 1, mapValue)) * holeValue * withinDisk;

    specularValue = anisReflModel(-toCamera, -ksLightDirection.xyz, normalW, dir0, dir1, roughness.x, roughness.y) 
      * lerp(extSpecularWorn, extSpecularBase, cleanK2) * lerp(1, pow(ceramicTexture, 2), ceramic * cleanK);

    float3 reflDir = normalize(reflect(toCamera, normalW));
    float reflDirN = saturate(dot(reflDir, extWheelNormalW));
    reflDir *= extRimHeight / reflDirN;
    float reflectedPos = posWithinCircle(posRel + reflDir, extWheelNormalW);
    float shadeBlur = lerp(lerp(0.9, cleanKS, brushedK), 1.4, ceramic);
    float result = (extRimRadius - reflectedPos) / lerp(1, 10, shadeBlur) + 0.005 * shadeBlur;
    reflectionOcclusion = saturate(result * 150.0);
    // reflectionOcclusion = 1;
    specularValue *= lerp(0.3, 1, reflectionOcclusion) * saturate(dot(normalW, -ksLightDirection.xyz) * 4) * holeValue;
    specularValue = lerp(reflectanceModel(-toCamera, -ksLightDirection.xyz, normalW, ksSpecularEXP), specularValue, withinDisk);
  #else
    float brushedK = 0;
    float cleanK = 1;
    float cleanKS = 1;
    float cleanK2 = 1;
    float mapShading = 0;
    float groove = 0;
    float withinDisk = 0;
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = specularValue;
  L.specularValue = lerp(ksSpecular * txDiffuseValue.a, 1, brushedK * mapShading);
  float3 lighting = calculateLighting(L);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase();
  R.useBias = true;
  R.finalMult = (0.5 + 0.5 * AO_REFLECTION) * lerp(mapShading, 1, cleanK2) * reflMask;
  R.fresnelC = lerp(R.fresnelC, lerp(extF0Worn, extF0Base, cleanK2), withinDisk);
  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 1, withinDisk);
  R.fresnelEXP = lerp(R.fresnelEXP, 5, withinDisk);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, lerp(extEXPWorn, extEXPBase, cleanK), withinDisk);
  R.reflectionSampleParam = reflectionOcclusion;

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  // withReflection = normalW.y * 10 + 1;
  withReflection += resultGlow;
  // withReflection += frac(withinCircle * 20) * 10;
  RETURN(withReflection.rgb, 1);
}
