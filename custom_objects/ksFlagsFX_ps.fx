#define NO_CARPAINT
#define SUPPORTS_AO
#define SUNLIGHT_DOT_FUNC(x) saturate(x * 0.6 + 0.4)
#define STENCIL_VALUE 1
#include "include_new/base/_include_ps.fx"
// alias: ksFlagsFX_tessellation_ps

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.multAO(saturate(normalW.y + 1.2) * saturate(normalW.y * 0.25 + 0.75));
  float3 lighting = L.calculate();
  lighting += shadow 
    * ksLightColor.xyz 
    * pow(txDiffuseValue.xyz, 2) 
    * lerp(0.15, 0.5, pow(saturate(-dot(toCamera, ksLightDirection.xyz)), 4)) 
    * saturate(-dot(toCamera, normalW) * 2)
    * saturate(dot(ksLightDirection.xyz, normalW)) * INPUT_DIFFUSE_K;
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
