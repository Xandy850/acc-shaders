#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

#define gMinTessDistance extMinDisplacementDistance
#define gMaxTessDistance extMaxDisplacementDistance

#define gMinTessFactor extMinTesselationFactor
#define gMaxTessFactor extMaxTesselationFactor

VS_OUT main(VS_IN vin) {
  VS_OUT vout;
  float4 posW, posV;
  toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);

  float d = length(vout.PosC);
  float tess = saturate((gMinTessDistance - d) / (gMinTessDistance - gMaxTessDistance));
  tess *= tess;
  vout.TessFactor = gMinTessFactor + tess * (gMaxTessFactor - gMinTessFactor);

  return vout;
}
