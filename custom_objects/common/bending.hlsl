#include "include/common.hlsl"

struct MovingPoint {
  float3 posParam;
  float mult;
  float3 radiusInv;
  float addition;
  float exp;
  float smooth;
  float2 pad;

  float calculate(float3 posL){
    float d = saturate(1 - length(posL * radiusInv + posParam));
    float a = saturate(d * mult + addition);
    // float t = pow(a, max(exp, 0.1));
    float t = pow(a, exp);
    float s = lerp(t, t * t * (3 - 2 * t), smooth);
    return s;
    // return saturate(s);
  }
};

#define POINTS_COUNT 8

cbuffer _cbBendingParams : register(b5) {
  MovingPoint gMovingPoints[POINTS_COUNT];
}

cbuffer _cbBending : register(b12) {
  float4 gOffsets[POINTS_COUNT];
  float4 gOffsetsPrev[POINTS_COUNT];
}

#define ALTER_POSW alterPosBending
void alterPosBending(float3 posL, bool prevFrame, inout float3 posW);

#define BENDING_FX_IMPL\
  void alterPosBending(float3 posL, bool prevFrame, inout float3 posW){\
    float totalMovement = 1;\
    float3 newPosW = posW;\
    for (int i = 0; i < POINTS_COUNT; ++i){\
      bendingRun(gMovingPoints[i], prevFrame ? gOffsetsPrev[i] : gOffsets[i], posL, newPosW, totalMovement);\
    }\
    posW = lerp(posW, newPosW, totalMovement * 2);\
  }

#define BENDING_FX_IMPL_NONE\
  void alterPosBending(float3 posL, bool prevFrame, inout float3 posW){}

void bendingRun(MovingPoint P, float4 offset, float3 posL, inout float3 posW, inout float totalMovement){
  float weight = P.calculate(posL);
  posW += offset.xyz * weight;
  totalMovement *= lerp(1, offset.w, weight);
}
