#ifdef SUPPORTS_COLORFUL_AO
  #undef SUPPORTS_COLORFUL_AO
  #define SUPPORTS_NORMALS_AO
#endif

#ifdef ALLOW_PERVERTEX_AO
  #define AO_NORMALS centroid float Ao : COLOR;
  #define AO_COLORFUL centroid float3 Ao : COLOR1;
#else
  #define AO_NORMALS 
  #define AO_COLORFUL 
#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS_ITEMS
#else
  #define SHADOWS_COORDS_ITEMS \
    float4 ShadowTex0 : TEXCOORD3;\
    float4 ShadowTex1 : TEXCOORD4;\
    float4 ShadowTex2 : TEXCOORD5;
#endif

#ifdef CREATE_MOTION_BUFFER
  #define MOTION_BUFFER float4 PosCS0 : TEXCOORD10; float4 PosCS1 : TEXCOORD5;
#else 
  #define MOTION_BUFFER
#endif

#ifndef VS_OUT_EXTRAS
  #define VS_OUT_EXTRAS
#endif

struct VS_OUT {
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  #ifdef SUPPORTS_COLORFUL_AO
    AO_COLORFUL
  #else
    AO_NORMALS
  #endif
  float TessFactor : TESS;
  VS_OUT_EXTRAS
};

#ifndef HS_OUT_EXTRAS
  #define HS_OUT_EXTRAS
#endif

struct HS_OUT {
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  #ifdef SUPPORTS_COLORFUL_AO
    AO_COLORFUL
  #else
    AO_NORMALS
  #endif
  HS_OUT_EXTRAS
};

#ifndef DS_OUT_EXTRAS
  #define DS_OUT_EXTRAS
#endif

struct DS_OUT {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  #ifdef SUPPORTS_COLORFUL_AO
    AO_COLORFUL
  #else
    AO_NORMALS
  #endif
  DS_OUT_EXTRAS
};

struct PatchTess {
  float EdgeTess[3] : SV_TessFactor;
  float InsideTess  : SV_InsideTessFactor;
};
