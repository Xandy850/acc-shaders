struct PS_IN_SmDigitalScreen {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  float2 DigitalScreenPos : EXTRAVALUE0;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
};

#ifdef TARGET_PS
  #ifndef MULTIEMISSIVE_MODE
    cbuffer cbEmissiveLight : register(b12) {  
      float2 smScreenScale;
      float smScreenType;
      float smFlipXY;
      
      #ifdef MULTIMAP_MODE
        float smMaskDiffuseAlpha;
        float smMaskMapsAlpha;
        float smMaskNormalAlpha;
        float smMaskArea;
        float smMaskCenterX;
        float smMaskCenterY;
        float smMaskWidthHalf;
        float smMaskHeightHalf;
        float smGlitchMult;
        float3 _smPad0;
      #else
        float smUseTextureCoordinates;
        float3 _smPad0;
      #endif
    }
  #endif

  #define IsTypeTN (smScreenType == 0)
  #define IsTypeIPS (smScreenType == 1)

  #ifdef MULTIMAP_MODE
    float getMask(float2 uv){
      float2 tex = frac(uv);
      float2 smMaskSizeHalf = float2(smMaskWidthHalf, smMaskHeightHalf);
      float2 smMaskCenter = float2(smMaskCenterX, smMaskCenterY);
      float maskArea = smMaskArea ? all(abs(tex - smMaskCenter) < smMaskSizeHalf) : 1;
      float maskDiffuse = smMaskDiffuseAlpha ? txDiffuse.Sample(samLinear, uv).a : 1;
      float maskMaps = smMaskMapsAlpha ? txMaps.Sample(samLinear, uv).a : 1;
      float maskNormal = smMaskNormalAlpha ? txNormal.Sample(GETNORMALW_SAMPLER, uv).a : 1;
      return (smMaskDiffuseAlpha == 2 ? 1 - maskDiffuse : maskDiffuse)
          * (smMaskMapsAlpha == 2 ? 1 - maskMaps : maskMaps)
          * (smMaskNormalAlpha == 2 ? 1 - maskNormal : maskNormal)
          * (smMaskArea == 2 ? 1 - maskArea : maskArea);
    }
  #endif

  struct DigitalScreenData {
    float distanceK;
    float2 cell;
    float3 cellColor;
    float2 screenThing;
    float mask;
  };

  DigitalScreenData digitalScreenInit(inout float2 tex, float2 digitalScreenPos
      #ifdef MULTIEMISSIVE_MODE
      , float mask
      #endif
    ){
    DigitalScreenData D;

    float2 screenScale = smScreenScale;
    if (IsTypeIPS) screenScale.y *= 2;

    #ifdef MULTIMAP_MODE
      const bool smUseTextureCoordinates = true;
      D.mask = getMask(tex);
    #elif defined(MULTIEMISSIVE_MODE)
      const bool smUseTextureCoordinates = true;
      D.mask = mask;
    #else
      D.mask = 1;
    #endif
    
    D.screenThing = smUseTextureCoordinates ? tex * screenScale : digitalScreenPos * screenScale;
    if (smFlipXY) D.screenThing.xy = D.screenThing.yx;
    float screenDDX = abs(ddx(D.screenThing.x)) + abs(ddy(D.screenThing.x));
    D.distanceK = sqrt(saturate(screenDDX * 2));
    // D.distanceK = 0;

    D.cell = frac(D.screenThing);
    D.cellColor = float3(1, D.cell.x > 0.333, D.cell.x > 0.667);
    D.cellColor -= float3(D.cellColor.y, D.cellColor.z, 0);
    if (IsTypeTN) D.cellColor = D.cellColor.bgr;
    if (dot(screenScale, 1) == 0) D.cellColor = 1;
    if (screenDDX < 0.001) D.cellColor = 0;
    if (smUseTextureCoordinates) tex -= D.cell / screenScale * (1 - D.distanceK) * D.mask;

    return D;
  }

  #ifdef USE_GLITCHING
    void glitching(float3 posW, float2 digitalPixel, float2 uv, inout float4 diffuseValue){
      float2 block = floor(digitalPixel / 8);
      float2 uvNoise = block / 64;
      float iTime = ksGameTime / 1e6;
      uvNoise += floor(iTime * float2(1234.0, 3543.0)) * 0.17169;
      uvNoise += floor(posW.xz * 0.27) * 0.13716;
      float line_thresh = pow(frac(iTime * 2236.0453), 2.0) * 0.7;
      float block_thresh = pow(frac(iTime * 1236.0453), 3.0) * 0.2;

      float4 noiseBlock = txNoise.SampleLevel(samPoint, uvNoise, 0);
      float4 noiseLine = txNoise.SampleLevel(samPoint, float2(uvNoise.y, 0.0), 0);
      noiseBlock += txNoise.SampleLevel(samPoint, uvNoise + 0.17, 0);
      noiseLine += txNoise.SampleLevel(samPoint, float2(uvNoise.y + 0.17, 0.0), 0);

      float activeK = (0.2 + pow(saturate(2 
        * sin(ksGameTime * 3.7 / 1e3) * cos(ksGameTime * 4.7 / 1e3) - 0.8), 4)) * smGlitchMult;
      line_thresh *= activeK;
      block_thresh *= activeK;

      // glitch some blocks and lines
      if (noiseBlock.r < block_thresh * 4 || noiseLine.g < line_thresh * 4) {
        float2 dist = (frac(uvNoise) - 0.5) * 0.3;
        diffuseValue.r = txDiffuse.Sample(samLinear, uv + dist * 0.06).r;
        diffuseValue.g = txDiffuse.Sample(samLinear, uv + dist * 0.12).g;
        diffuseValue.b = txDiffuse.Sample(samLinear, uv + dist * 0.08).b;
      }

      if (noiseBlock.g < block_thresh){
        diffuseValue.rgb = diffuseValue.ggg;
      }

      if (noiseBlock.w < block_thresh){
        diffuseValue.rgb *= sqrt(frac(ksGameTime));
      }

      if (noiseLine.b < line_thresh){
        diffuseValue.rgb = float3(0.0, saturate(dot(diffuseValue.rgb, 1)), 0.0);
      }
    }
  #endif

  float3 digitalScreenCalculate(DigitalScreenData D, float3 posW, float2 uv, float3 normalW, float3 toCamera, float4 diffuseValue){
    #if defined(USE_GLITCHING) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)
      [branch]
      if (smGlitchMult > 0){
        glitching(posW, D.screenThing.xy, uv, diffuseValue);
      }
    #endif

    float brightnessK = dot(diffuseValue.rgb, 0.33);
    float monochromeK = pow(saturate(1 - dot(abs(diffuseValue.rgb - brightnessK), 1)), 4);

    float3 screenFix = D.cell.y < (IsTypeTN ? 0.85 : 0.9) ? D.cellColor : 0;  
    // screenFix *= frac(cell.x * 3) > (IsTypeTN ? 0.3 : 0.2);
    // if (IsTypeIPS) screenFix *= frac((cell.x - 0.24) * 9) > 0.2;
    screenFix = lerp(screenFix, 1, D.distanceK);

    float NdotV = pow(abs(dot(-toCamera, normalW)), 0.7);
    if (IsTypeTN){
      screenFix *= NdotV * lerp(1, NdotV * NdotV, monochromeK * saturate(brightnessK));
      screenFix *= lerp(diffuseValue.bgr, diffuseValue.rgb, NdotV * NdotV);
    } else {
      screenFix *= diffuseValue.rgb;
    }

    return screenFix * D.mask;
  }
#endif