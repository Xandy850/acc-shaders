#include "include/common.hlsl"
#define RAINFX_STATIC_OBJECT

#define CB_MATERIAL_EXTRA_4\
	float extDetailScale;\
	float extOcclusionDebug;

#define CUSTOM_STRUCT_FIELDS\
	float2 TexAlt : CUSTOM0;\
	float3 NormalL : CUSTOM1;

cbuffer _cbData : register(b12) {
  float4x4 gCarTransform;
  float4x4 gCarTransformInv;
	float3 gCameraSide;
	float gUseColor;
	float3 gCameraUp;
	float gPad1;
}

#define FILL_VS(vin, vout)\
  vout.TexAlt = vin.TangentPacked.xy;\
  vout.NormalL = mul(vout.NormalW.xyz, (float3x3)gCarTransformInv);

#ifdef TARGET_PS

	float3 sampleEnv(float3 ray, float bias, float dimming);

	void mixLayer(inout float3 resultColor, inout float resultAlpha, float3 layerColor, float layerAlpha){
		float newAlpha = lerp(resultAlpha, 1, layerAlpha);
		resultColor = lerp(resultColor * resultAlpha, layerColor, layerAlpha) / max(0.000001, newAlpha);
		resultAlpha = newAlpha;
	}

	// #ifdef ALLOW_RAINFX // TODO
		#define APPLY_RAIN
	// #endif

	#ifdef APPLY_RAIN
		Texture2D txRainWater : register(t1);
		Texture2D txRainDetail : register(t2);
		Texture2D txRainWiper : register(t3);
		Texture2D txOcclusion : register(t4);
		Texture2D<float> txRainWaterBlurred : register(t5);
	#endif

	struct WindscreenRainData {
		float water;
		float trace;
		float condensation;
		float facingShare;
		float3 normal;
		float2 fakeRefraction;
		float occlusion;
		float refractionOcclusion;
		float dirtBlur;
		float wiperOcclusion;

		bool debugMode;
		float3 debugColor;

		void applyOpaqueMask(float alpha){			
			water *= 1 - alpha;
			trace *= 1 - alpha;
		}
	};

#endif