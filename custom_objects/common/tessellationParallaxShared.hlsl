cbuffer cbTessellationParams : register(b12) {  
  float extMinTesselationFactor;
  float extMaxTesselationFactor;
  float extMinDisplacementDistance;
  float extMaxDisplacementDistance;

  float extHeightScale;
  float extParallaxScale;
  float extParallaxHeightEXP;
  float extDisplacementInvert;

  float extOcclusionValue;
  float3 extPad0;
}

// TODO: Once variables mapping is reworked, remove that crap
// But it should only be, like, a couple of extra instructions
float useEverything(){
  return 0.00001 * dot(
    float4(extMinTesselationFactor, extMaxTesselationFactor, extMinDisplacementDistance, extMaxDisplacementDistance), 
    float4(extHeightScale, extParallaxScale, extParallaxHeightEXP, extDisplacementInvert));
}