
#ifndef ALLOW_CUSTOM_A2C
  #undef USE_CUSTOM_COVERAGE
#endif

#ifdef USE_CUSTOM_COVERAGE
  uint _customA2C(float2 uv, float alpha){
    float threshold = extMSAA == 2 ? 0.25 : 0.125;
    if (extMSAA == 8){
      alpha += dot(frac(uv / 4) > 0.5, 1) == 1 ? 0.0625 : 0;
    }
    alpha -= dot(frac(uv / 2) > 0.5, 1) * threshold;
    clip(alpha - threshold);
    alpha *= extMSAA == 2 ? 1.5 : 1.2;
    return dot(1, uint4(1, 
      extMSAA == 8 ? dot(1, (alpha > float4(1, 3, 5, 7) / 8) * uint4(16, 32, 64, 128)) : 0,
      extMSAA >= 4 ? dot(1, (alpha > float2(2, 6) / 8) * uint2(4, 8)) : 0,
      alpha > 0.5 ? 2 : 0));
  }

  void _clipA2C(float2 posH, inout float alpha, inout uint coverage){
    coverage = _customA2C(posH, alpha);
    alpha = 1;
  }

  #define PS_INPUT_EXTRA , out uint _coverage : SV_Coverage
  #define A2C_ALPHA(x) _clipA2C(pin.PosH.xy, x, _coverage)
#else
  #define PS_INPUT_EXTRA
  #define A2C_ALPHA(x) clip(x - 0.12)
#endif
