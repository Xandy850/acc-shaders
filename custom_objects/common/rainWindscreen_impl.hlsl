#include "include/common.hlsl"

#ifdef TARGET_PS

	float3 sampleEnv(float3 ray, float bias, float dimming){
		float dimmingMult = saturate(remap(ray.y, -1, 0.2, 0, 1));
		return SAMPLE_REFLECTION_FN(ray, bias, false, REFL_SAMPLE_PARAM_DEFAULT) * lerp(1, dimmingMult, dimming);
	}

	WindscreenRainData getWindscreenRainData(float2 uv, float3 normalL, float3 normalW, float3 toCamera, float3 cameraUp){
		float4 layerRain = txRainWater.Sample(samLinear, uv);
		float4 layerRainBlurred = txRainWater.SampleLevel(samLinearSimple, uv, 2.5);
		float layerBlurred = txRainWaterBlurred.SampleLevel(samLinearSimple, uv, 0) / 3;
		float4 layerWiper = txRainWiper.Sample(samLinear, uv);
		float4 layerOcclusion = txOcclusion.SampleLevel(samLinear, uv, 1 - extOcclusionDebug);

		float actualWater = saturate(remap(layerRain.a, 0.45, 0.55, 0, 1));
		float trail = saturate(remap(layerRain.z * lerp(1, 0.8, saturate(layerBlurred * 9 - 2.5)), 0.83, 0.93, 0, 1));
		float water = max(actualWater, trail);// * saturate(1 - layerWiper.a);

		float4 detail = txRainDetail.Sample(samLinearSimple, uv * extDetailScale);
		detail.a *= saturate(4 - 10 * max(layerRainBlurred.z * 0.9, layerRainBlurred.a));
		detail.a *= saturate(1 - layerWiper.y);
		detail.a *= saturate(1 - layerRain.z);
		detail.a *= saturate(layerOcclusion.a * 2 - 1);

		float dif = (detail.a * 2 - lerp(1, 0, saturate(layerBlurred * 9 - 2))) - saturate(1.5 - layerBlurred * 10);
		detail.a = saturate(detail.a * saturate(dif * 3) * 2);

		float2 normalMapValue = (layerRain.xy * 2 - 1) * lerp(0.1, 1, actualWater);
		normalMapValue = lerp(normalMapValue, detail.xy, detail.a * (1 - water));
		water = lerp(water, 1, detail.a);
		water *= saturate(layerOcclusion.a * 10);

		// water = actualWater;

    float facingShare = sqrt(saturate(1 - dot2(normalMapValue)));
		float3 normalX = layerOcclusion.xyz * 2 - 1;
		float3 normalY = normalize(cross(normalL, normalX));
		float3 normalZ = normalL;
		float3 normal = normalize(normalMapValue.x * normalX + normalMapValue.y * normalY + facingShare * normalZ);
		float3 normalDebug = normal;
		normal = mul(normal, (float3x3)gCarTransform);

		float wiperTrace = layerWiper.x;
		wiperTrace = lerp(wiperTrace, 1, lerp(0.6, 0.4, water) * saturate(layerBlurred * 8 - 0.8) * (1 - layerWiper.y));
		wiperTrace = saturate(wiperTrace - lerp(0.2, 0, saturate(2.5 - layerRainBlurred.a * 10) * saturate(1 - layerRain.z * 5)));

		float3 fakeCameraSide = normalize(cross(cameraUp, normalW));
		float3 fakeCameraUp = normalize(cross(normalW, fakeCameraSide));
    float2 fakeRefraction = float2(dot(fakeCameraSide, normal), dot(fakeCameraUp, normal)) * float2(-1, 1);

		WindscreenRainData ret;
    // ret.wiperOcclusion = saturate(layerWiper.w * 5 - 3);
    ret.wiperOcclusion = saturate(1 - layerRain.w * 4 * saturate(1 - layerRain.z * 4));
		ret.water = water;
		ret.trace = wiperTrace;
		ret.condensation = layerWiper.z;
		ret.facingShare = facingShare;
		ret.normal = normal;
		ret.fakeRefraction = fakeRefraction;
		ret.occlusion = (0.5 + 0.5 * layerWiper.w) * saturate(layerWiper.w * 2.5 - 0.5) * layerOcclusion.a;
		ret.refractionOcclusion = 0.95 * (0.5 + 0.5 * layerWiper.w) * layerOcclusion.a;
    ret.dirtBlur = max(layerWiper.x, layerWiper.z);
    // ret.wiperOcclusion = 1;
    ret.debugMode = extOcclusionDebug;
    ret.debugColor = layerOcclusion.xyz;

		// ret.debugColor.x = layerBlurred;
    // ret.debugMode = true;
    // ret.debugColor = float3(layerBlurred, 1 - layerBlurred, 0);

    return ret;
	}

#endif