// Uses txEmissive for emissive color. Optionally, has a mode where
// txEmissive channels instead are used as mask for multi-channel emissive.
// Another option allows to use three different ways of defining multi-emissive
// areas instead of using txEmissive, and combine them in various ways. Whole thing
// allows to set up to four channels, with optional mirroring by local position it
// gets to seven (for surfaces laying on the other side of the mirror, 4th channel will
// use ksEmissive4 instead of ksEmissive3, for example). And another option is mirroring
// based on texture coordinates. Plus, options are available to change how txDiffuse 
// affects resulting color.

// This shader should be enough for external lights, and, in a lot of cases, dashboard lights.
// With all possible ways used, it might be somewhat expensive computationally, however I believe
// this shouldn’t be a problem, as there are no loops or texture samples and little need in
// using all three different modes at once.

// TODO: Move everything related to procedural stuff to another shader which would just generate
// a txEmissive on-fly instead? On one hand, it seems like a more reasonable approach, but maybe
// there are some interesting things to have with changing those proceducal masks dynamically.
// Plus, new txEmissive texture would have to be uncompressed, and potentially have a high resolution
// to match other textures, wouldn’t that be more of an issue performance-wise?

#ifndef EMISSIVE_SLOT
  #define EMISSIVE_SLOT 4
#endif

#include "include/common.hlsl"
TEXTURE2D(txEmissive, EMISSIVE_SLOT);

#ifdef MODE_KUNOS

  #define CB_MATERIAL_EXTRA_3\
    float3 ksEmissive1;\
    float3 ksEmissive2;\
    float3 ksEmissive3;\
    float3 ksEmissive4;\
    float3 ksEmissive5;\
    float3 ksEmissive6;\
    float emChannelsMode;\
    float emMirrorChannel3As4;\
    float emMirrorChannel2As5;\
    float emMirrorChannel1As6;\
    float emSkipDiffuseMap;

#else

  #define CB_MATERIAL_EXTRA_3\
    float emChannelsMode;\
    float3 ksEmissive1;\
    float emMirrorChannel3As4;\
    float3 ksEmissive2;\
    float emMirrorChannel2As5;\
    float3 ksEmissive3;\
    float emMirrorChannel1As6;\
    float3 ksEmissive4;\
    float emSkipDiffuseMap;\
    float3 ksEmissive5;\
    float emProceduralMode;\
    float3 ksEmissive6;
    
#endif


#ifdef USE_BOUNCEBACK
  #undef CB_MATERIAL_EXTRA_4
  #define CB_MATERIAL_EXTRA_4\
    float extBounceBack /* = 2 */;\
    PARAM_CSP(float4, extBounceBackMask);
#endif

cbuffer cbStaticStuff : register(b11) { 
  float3 emMirrorDir;
  float emMirrorOffset;

  #ifdef MODE_KUNOS
    float emAlphaFromDiffuse;
  #else
    float4 emEmissiveArea0;
    float4 emEmissiveArea1;
    float4 emEmissiveArea2;
    float4 emEmissiveArea3;

    float emAlphaFromDiffuse;
    float emMaskColorInputBlur;
    float emMaskResultDiffuseBlur;
    float emMaskResultNormalAlphaBlur;

    float4 emGeoChannel0S;
    float4 emGeoChannel0P;
    float4 emGeoChannel1S;
    float4 emGeoChannel1P;
    float4 emGeoChannel2S;
    float4 emGeoChannel2P;
    float4 emGeoChannel3S;
    float4 emGeoChannel3P;
    
    float4 emMaskColor0C;
    float4 emMaskColor0P;
    float4 emMaskColor1C;
    float4 emMaskColor1P;
    float4 emMaskColor2C;
    float4 emMaskColor2P;
    float4 emMaskColor3C;
    float4 emMaskColor3P;
    float emMaskColor0A;
    float emMaskColor1A;
    float emMaskColor2A;
    float emMaskColor3A;

    float4 emPoly00;
    float4 emPoly01;
    float4 emPoly10;
    float4 emPoly11;
    float4 emPoly20;
    float4 emPoly21;
    float4 emPoly30;
    float4 emPoly31;
    float4 emPoly02;
    float4 emPoly12;
    float4 emPoly22;
    float4 emPoly32;

    float4 emDiffuseAvgColorAsMultiplier0;
    float4 emDiffuseAvgColorAsMultiplier1;
    float4 emDiffuseAvgColorAsMultiplier2;
    float4 emDiffuseAvgColorAsMultiplier3;
    float4 emDiffuseAlphaAsMultiplier0;
    float4 emDiffuseAlphaAsMultiplier1;
    float4 emDiffuseAlphaAsMultiplier2;
    float4 emDiffuseAlphaAsMultiplier3;

    float2 emMirrorUV;
    float emMirrorUVOffset;
  #endif

  #ifdef INLCUDE_DIGITALSCREEN_PARAMS
    float smGlitchMult;
    float4 smScreenEmissiveMask;
    float2 smScreenScale;
    float smScreenType;
    float smFlipXY;
  #endif
}

void subtractNext(inout float4 proc){
  proc.x -= dot(proc.yzw, 2);
  proc.y -= dot(proc.zw, 2);
  proc.z -= proc.w * 2;
  proc = saturate(proc);
}

#define USE_TXEMISSIVE_AS_EMISSIVE
#define TXEMISSIVE_VALUE emissiveMap

#ifdef TARGET_VS
  #include "include_new/base/_include_vs.fx"

  float4 getExtraValue(float3 posL){
    float3 posM = posL;
    if (dot(emMirrorDir, posM) - emMirrorOffset < 0){
      float3 p = emMirrorDir * emMirrorOffset;
      posM = p + reflect(posM - p, emMirrorDir);
    }

    return float4(
      dot(emMirrorDir, 1) ? dot(posL, emMirrorDir) - emMirrorOffset : 1,
      posM);
  }
#else
  #include "include_new/base/_include_ps.fx"
  
  float sdTriangle(in float2 p, in float2 p0, in float2 p1, in float2 p2) {
    float2 e0 = p1 - p0, e1 = p2 - p1, e2 = p0 - p2;
    float2 v0 = p - p0, v1 = p - p1, v2 = p - p2;
    float2 pq0 = v0 - e0 * clamp(dot(v0, e0) / dot(e0, e0), 0.0, 1.0);
    float2 pq1 = v1 - e1 * clamp(dot(v1, e1) / dot(e1, e1), 0.0, 1.0);
    float2 pq2 = v2 - e2 * clamp(dot(v2, e2) / dot(e2, e2), 0.0, 1.0);
    float s = sign(e0.x * e2.y - e0.y * e2.x);
    float2 d = min(min(float2(dot(pq0, pq0), s*(v0.x * e0.y - v0.y * e0.x)),
                     float2(dot(pq1, pq1), s*(v1.x * e1.y - v1.y * e1.x))),
                     float2(dot(pq2, pq2), s*(v2.x * e2.y - v2.y * e2.x)));
    return -sqrt(d.x) * sign(d.y);
  }

  
  float sdPoly(in float2 v[4], in float2 p) {
      float d = dot(p - v[0],p - v[0]);
      float s = 1.0;
      for (int i = 0, j = 4 - 1; i < 4; j = i, i++) {
          float2 e = v[j] - v[i];
          float2 w = p - v[i];
          float2 b = w - e * clamp(dot(w, e) / dot(e, e), 0.0, 1.0);
          d = min(d, dot(b, b));

          bool3 cond = bool3(p.y >= v[i].y, p.y < v[j].y, e.x * w.y > e.y * w.x);
          if (all(cond) || all(!cond)) s *= -1.0;  
      }      
      return s * sqrt(d);
  }

  float procGeo(float2 uv, float4 p0, float4 p1){
    // hit = (uv - center) / halfSize
    // hit = uv / halfSize - center / halfSize
    // p0.xy = 1 / halfSize
    // p0.zw = -center / halfSize
    float2 hit = uv * p0.xy + p0.zw; // gives -1…1 if value within box, 0 for center, linear
    float2 hitAbs = 1 - abs(hit); // gives 1…0 value within box, 1 for center
    return pow(saturate(1 - length(1 - saturate(hitAbs * p1.xy))), max(p1.z, 0.0001));
  }

  float procColor(float4 diffuse, float4 p0, float4 p1){
    float samenessRaw = 1 - length(p0.xyz - diffuse.rgb);
    float samenessNormalized = dot(normalize(p0.xyz), normalize(diffuse.rgb));
    float aligned = lerp(samenessRaw, samenessNormalized, p0.w);
    return saturate(aligned * p1.x - p1.y) * saturate(smoothstep(p1.z - 0.000001, p1.w, diffuse.a));
  }

  float procPoly(float2 uv, float4 p0, float4 p1, float4 p2){
    float2 p[4];
    p[0] = p0.xy;
    p[1] = p0.zw;
    p[2] = p1.xy;
    p[3] = p1.zw;

    float distance = sdPoly(p, uv);
    // float distance = min(sdTriangle(uv, 0, float2(1, 0), 1), sdTriangle(uv, 0, 1, float2(0, 1)));
    // float distance = min(sdTriangle(uv, p0.xy, p0.zw, p1.xy), sdTriangle(uv, p0.xy, p1.xy, p1.zw));
    // return 1;
    // return distance < 0 ? 1 : 0;
    // return saturate(-distance * 100);
    // return pow(saturate(-distance * 100), max(p2.z, 0.0001)) * p2.w;
    if (p2.z < 0) distance = abs(distance);
    return pow(saturate(distance * p2.x - p2.y), max(abs(p2.z), 0.0001));
  }

  float3 getEmissiveValue(float4 diffuseValue, float2 uv, float4 extraValue, out float4 emissiveMap){
    bool isMirrored = extraValue.x < 0;
    emissiveMap = txEmissive.Sample(samLinear, uv);

    #ifndef MODE_KUNOS
      [branch]
      if (emProceduralMode != 0){
        uint flags = uint(round(emProceduralMode));
        bool useEmissiveMap = flags & 1;
        bool useGeometryBounds = flags & 2;
        bool useGeometryBoundsAdditive = flags & 4;
        bool useGeometryBoundsSubtractive = flags & 8;
        bool useColorMasks = flags & 16;
        bool useColorMasksAdditive = flags & 32;
        bool useColorMasksSubtractive = flags & 64;
        bool usePoly = flags & 128;
        bool usePolyAdditive = flags & 256;
        bool usePolySubtractive = flags & 512;
        bool subtractNextGeometry = flags & 1024;
        bool subtractNextColorMasks = flags & 2048;
        bool subtractNextPoly = flags & 4096;
        bool useVertexMask = flags & 8192;
        bool vertexMaskAdditive = flags & 16384;
        bool vertexMaskSubtractive = flags & 32768;
        bool useGeometryBoundsAsMask = flags & 65536;
        float2 uvFrac = (flags & 131072) ? uv : frac(uv);

        if (dot(emMirrorUV, uv) - emMirrorUVOffset < 0){
          float2 p = emMirrorUV * emMirrorUVOffset;
          uv = p + reflect(uv - p, emMirrorUV);
        }

        if (!useEmissiveMap){
          emissiveMap = 1;
        }

        [branch]
        if (useGeometryBounds){
          float4 proc = float4(
            procGeo(uvFrac, emGeoChannel0S, emGeoChannel0P),
            procGeo(uvFrac, emGeoChannel1S, emGeoChannel1P),
            procGeo(uvFrac, emGeoChannel2S, emGeoChannel2P),
            procGeo(uvFrac, emGeoChannel3S, emGeoChannel3P));
          if (subtractNextGeometry){
            subtractNext(proc);
          }

          float4 mix = float4(emGeoChannel0P.w, emGeoChannel1P.w, emGeoChannel2P.w, emGeoChannel3P.w);
          if (useGeometryBoundsAsMask) {
            proc *= mix;
            float mask = max(proc.x, max(proc.y, max(proc.z, proc.w)));
            if (useGeometryBoundsSubtractive) mask = 1 - mask;
            emissiveMap *= mask;
          } else if (useGeometryBoundsAdditive) { 
            emissiveMap += proc * mix;
          } else if (useGeometryBoundsSubtractive) { 
            emissiveMap -= proc * mix;
          } else {
            emissiveMap *= lerp(1, proc, mix);
          }
          emissiveMap = saturate(emissiveMap);
        }

        [branch]
        if (useColorMasks){
          float4 diffuseInput = txDiffuse.SampleBias(samLinear, uv, emMaskColorInputBlur);
          float4 proc = float4(
            procColor(diffuseInput, emMaskColor0C, emMaskColor0P),
            procColor(diffuseInput, emMaskColor1C, emMaskColor1P),
            procColor(diffuseInput, emMaskColor2C, emMaskColor2P),
            procColor(diffuseInput, emMaskColor3C, emMaskColor3P));
          if (subtractNextColorMasks){
            subtractNext(proc);
          }

          float4 mix = float4(emMaskColor0A, emMaskColor1A, emMaskColor2A, emMaskColor3A);
          if (useColorMasksAdditive) { 
            emissiveMap += proc * mix;
          } else if (useColorMasksSubtractive) { 
            emissiveMap -= proc * mix;
          } else {
            emissiveMap *= lerp(1, proc, mix);
          }
          emissiveMap = saturate(emissiveMap);
        }

        [branch]
        if (usePoly){
          float4 proc = float4(
            procPoly(uvFrac, emPoly00, emPoly01, emPoly02),
            procPoly(uvFrac, emPoly10, emPoly11, emPoly12),
            procPoly(uvFrac, emPoly20, emPoly21, emPoly22),
            procPoly(uvFrac, emPoly30, emPoly31, emPoly32));
          if (subtractNextPoly){
            subtractNext(proc);
          }

          float4 mix = float4(emPoly02.w, emPoly12.w, emPoly22.w, emPoly32.w);
          if (usePolyAdditive) { 
            emissiveMap += proc * mix;
          } else if (usePolySubtractive) { 
            emissiveMap -= proc * mix;
          } else {
            emissiveMap *= lerp(1, proc, mix);
          }
          emissiveMap = saturate(emissiveMap);
        }
    
        [branch]
        if (useVertexMask) {
          float dist0 = dot2(extraValue.yzw - emEmissiveArea0.xyz) / max(emEmissiveArea0.w, 0.00001);
          float dist1 = dot2(extraValue.yzw - emEmissiveArea1.xyz) / max(emEmissiveArea1.w, 0.00001);
          float dist2 = dot2(extraValue.yzw - emEmissiveArea2.xyz) / max(emEmissiveArea2.w, 0.00001);
          float dist3 = dot2(extraValue.yzw - emEmissiveArea3.xyz) / max(emEmissiveArea3.w, 0.00001);
          float minValue = min(dist0, min(dist1, min(dist2, dist3)));
          float4 vertexMask = float4(
            dist0 == minValue || emEmissiveArea0.w == 0,
            dist1 == minValue || emEmissiveArea1.w == 0,
            dist2 == minValue || emEmissiveArea2.w == 0,
            dist3 == minValue || emEmissiveArea3.w == 0);

          if (vertexMaskAdditive) {
            emissiveMap += vertexMask;
          } else if (vertexMaskSubtractive) {
            emissiveMap -= vertexMask;
          } else {
            emissiveMap *= vertexMask;
          }
          emissiveMap = saturate(emissiveMap);
        }

        // float4 maskDiffuse = txDiffuse.SampleBias(samLinear, uv, emMaskResultDiffuseBlur);
        // float4 maskNormalAlpha = txNormal.SampleBias(samLinear, uv, emMaskResultNormalAlphaBlur);
        // emissiveMap *= lerp(1, pow(saturate(saturate(dot(maskDiffuse.rgb, 1)) * maskDiffuse.a), 1 + emMaskResultDiffuseBlur), saturate(emMaskResultDiffuseBlur));
        // emissiveMap *= lerp(1, pow(saturate(maskNormalAlpha.a), 1 + emMaskResultNormalAlphaBlur), saturate(emMaskResultNormalAlphaBlur));
      }

      if (dot(emDiffuseAvgColorAsMultiplier3, 1)){
        emissiveMap *= saturate(lerp(1, pow(saturate(dot(diffuseValue.rgb, 0.333) * emDiffuseAvgColorAsMultiplier0 + emDiffuseAvgColorAsMultiplier1), 
          emDiffuseAvgColorAsMultiplier2), emDiffuseAvgColorAsMultiplier3));
      }

      if (dot(emDiffuseAlphaAsMultiplier3, 1)){
        emissiveMap *= saturate(lerp(1, pow(saturate(diffuseValue.a * emDiffuseAlphaAsMultiplier0 + emDiffuseAlphaAsMultiplier1), 
          emDiffuseAlphaAsMultiplier2), abs(emDiffuseAlphaAsMultiplier3)));
      }
    #endif

    float3 result;
    if (emChannelsMode){
      result = emissiveMap.r * ksEmissive
        + emissiveMap.g * (isMirrored && emMirrorChannel1As6 ? ksEmissive6 : ksEmissive1)
        + emissiveMap.b * (isMirrored && emMirrorChannel2As5 ? ksEmissive5 : ksEmissive2)
        + emissiveMap.a * (isMirrored && emMirrorChannel3As4 ? ksEmissive4 : ksEmissive3);
    } else {
      result = ksEmissive * emissiveMap.rgb;
    }

    if (!emSkipDiffuseMap){
      result *= diffuseValue.rgb;
    }

    return result;
  }
#endif
