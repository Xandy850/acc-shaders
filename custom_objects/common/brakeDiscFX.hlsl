cbuffer cbBrakeDiscFX : register(b11) {
  float3 extWheelPos;
  float extNoiseOffset;

  float3 extWheelNormal;
  float extBrakeDiscRadius;

  float3 extWheelUp;
  float extBrakeDiscInnerRadius;

  float extRimRadius;
  float extRimHeight;
  float extGlowMult;
  float extWorn;

  float extAlphaAsMaskMul;
  float extAlphaAsMaskAdd;
  float extLuminocityAsMaskMul;
  float extLuminocityAsMaskAdd;

  float extNormalAsMaskMul;
  float extNormalAsMaskAdd;
  float extF0Base;
  float extF0Worn;
  
  float extEXPBase;
  float extEXPWorn;
  float extCarvedPattern;
  float extCeramicBrakes;

  float extDebugMode;
  float extOverrideNormalMap;
  float extOverrideDiffuseMap;
  float extGlowOffset;

  float extSpecularBase;
  float extSpecularWorn;
  float extRoughnessX;
  float extRoughnessY;
}

struct PS_IN_BrakeDiscFX {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float3 WheelCenterC : POSITION0;
  float3 WheelNormalW : POSITION1;
  float3 WheelUpW : POSITION2;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};