#include "include_new/base/_flags.fx"

#define USE_BOUNCEBACK
#ifdef ALLOW_BOUNCEBACK_LIGHTING
  #define CB_MATERIAL_EXTRA_4\
    float extBounceBack /* = 2 */;
  
  #define LIGHTINGFX_BOUNCEBACK
  #define LIGHTINGFX_BOUNCEBACK_EXP 80

  #define BOUNCEBACKFX_MAPSMASK (extBounceBack < 0 ? 1 - txDiffuseValue.a : 1)
  #define BOUNCEBACKFX_DIFFUSEMASK (extBounceBack < 0 ? 1 - txDiffuseValue.a : 1)
  #define BOUNCEBACKFX(lighting, mask)\
    float3 bounceBackMult = abs(extBounceBack) * txDiffuseValue.rgb * (mask);\
    lighting += calculateBounceBackLighting(toCamera, normalW, txDiffuseValue.rgb, bounceBackMult, shadow, 80);
#else
  #define BOUNCEBACKFX_MAPSMASK 0
  #define BOUNCEBACKFX(lighting, mask) float3 bounceBackMult = 0;
#endif