#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#ifndef WITHOUT_NMDETAIL
  #define GETNORMALW_NORMALIZED_TB
  #define CARPAINT_NMDETAILS
  #define CARPAINT_NMDETAILS_PBR_MODE
#endif
#define GETNORMALW_XYZ_TX
// #define GBUFFER_FORCE_SHADOWS
#define SUPPORTS_AO
#define GBUFF_NORMAL_W_SRC normalWOuter

#define INPUT_AMBIENT_K 1
#define INPUT_SPECULAR_K 1
#define LIGHTINGFX_KSDIFFUSE 0.4
#define LIGHTINGFX_SPECULAR_COLOR lfxSpecularPower
#define LIGHTINGFX_SPECULAR_EXP lfxSpecularEXP
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0

#ifdef USE_CLEARCOAT
  #define EXTRA_CARPAINT_FIELDS_CLEARCOAT\
    float2 pbClearCoatSmoothness;\
    float pbClearCoatIntensity;\
    float pbClearCoatF0;\
    float pbClearCoatNormalShare;
#else
  #define EXTRA_CARPAINT_FIELDS_CLEARCOAT
  #define USE_HILL12_REORIENTATION
  // #define USE_HILL12_REORIENTATION_UDN
#endif

#ifdef USE_CLOTH_SHADING
  #define EXTRA_CARPAINT_FIELDS_CLOTH\
    float3 pbClothSheenColor;\
    float3 pbClothSubsurfaceColor;
#else
  #define EXTRA_CARPAINT_FIELDS_CLOTH
#endif

#ifdef WITHOUT_NMDETAIL
#define EXTRA_CARPAINT_FIELDS_NMDETAIL
#else
#define EXTRA_CARPAINT_FIELDS_NMDETAIL\
  float4 pbNormalDetailPBROccludedColor;\
  float pbNormalDetailInPBRFormat;\
  float3 pbNormalDetailPBRMults;\
  float pbBlurColors;
  float pbRoughnessEXP;
#endif

#define EXTRA_CARPAINT_FIELDS\
  float2 pbOcclusion;\
  float2 pbSmoothness;\
  float2 pbMetalness;\
  float2 pbReflectance;\
  float pbColorSource;\
  float pbOcclusionSource;\
  float pbOcclusionMult;\
  float pbReflectionBlurEnv;\
  EXTRA_CARPAINT_FIELDS_CLOTH\
  EXTRA_CARPAINT_FIELDS_NMDETAIL\
  EXTRA_CARPAINT_FIELDS_CLEARCOAT

#include "include_new/base/_include_ps.fx"
#include "pbr/pbr.hlsl"

#ifdef USE_CLEARCOAT
  #include "pbr/clear_coat.hlsl"
#endif

// Please don’t copy this code as a base for your shader
// It’s all very experimental and WIP, and will be moved to utils_ps.fx as soon as it’s sorted and tested

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW
  // shadow *= saturate(-dot(normalW, ksLightDirection.xyz) * 20);

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  // Some PBR stuff
  float occlusionTexValue = 1;
  if (pbOcclusionSource == 1){
    occlusionTexValue = lerp(max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), 0.05), 1, 0.25);
  } else if (pbOcclusionSource == 2){
    occlusionTexValue = txMapsValue.w;
  }

  if (abs(pbColorSource) == 1){
    txDiffuseValue.xyz /= occlusionTexValue;
  } else if (abs(pbColorSource) == 2){
    txDiffuseValue.xyz = 1;
  } else if (abs(pbColorSource) == 3){
    txDiffuseValue.xyz = luminance(txDiffuseValue.xyz);
  }

  occlusionTexValue = saturate(occlusionTexValue * pbOcclusionMult);
  if (pbColorSource < 0) txDiffuseValue.a = 0;
  txDiffuseValue.xyz *= INPUT_DIFFUSE_K;

  // normals piece  
  #ifdef USE_HILL12_REORIENTATION
    float4 txNormalValue = txNormal.Sample(samLinearSimple, pin.Tex);
  #else    
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
    float4 txNormalValue = 1;
  #endif
  float3 pbrMults = float3(1, 0, 1);
  float baseOcclusion = saturate(lerp(pbOcclusion.x, pbOcclusion.y, occlusionTexValue));
  float baseMetalness = saturate(lerp(pbMetalness.x, pbMetalness.y, txMapsValue.y));
  
  float3 normalWBase = normalW;
  #ifdef WITHOUT_NMDETAIL
    considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
    float pbRoughnessEXP = 1;
  #else
    float3 pbrMultsBase = pbrMults;
    considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW,
      #ifdef USE_HILL12_REORIENTATION
      txNormalValue.xyz, 
      #endif
      pbNormalDetailInPBRFormat, pbBlurColors, pbrMultsBase);
    pbrMults = saturate(lerp(pbrMults, pbrMultsBase, pbNormalDetailPBRMults));
    baseMetalness *= pbrMults.z;
    txDiffuseValue.rgb = saturate(lerp(txDiffuseValue.rgb, pbNormalDetailPBROccludedColor.rgb, pbNormalDetailPBROccludedColor.a * (1 - pbrMultsBase.x)));
  #endif

  float diffLum = dot(txDiffuseValue.rgb, 0.333);
  float saturation = max(txDiffuseValue.r, max(txDiffuseValue.g, txDiffuseValue.b)) - diffLum;
  txDiffuseValue.rgb = lerp(diffLum, txDiffuseValue.rgb, saturate(saturation * 100 - 1));

  float roughnessBaseInner = saturate(1 - lerp(pbSmoothness.x, pbSmoothness.y, txMapsValue.x)) * pow(saturate(1 - pbrMults.y), pbRoughnessEXP);
  #ifdef USE_CLEARCOAT
    float roughnessBaseOuter = lerp(
      roughnessBaseInner,
      saturate(1 - lerp(pbClearCoatSmoothness.x, pbClearCoatSmoothness.y, txMapsValue.x)),
      pbClearCoatIntensity);
  #else
    float roughnessBaseOuter = roughnessBaseInner;
  #endif

  #ifdef USE_CLEARCOAT
    float3 normalWInner = normalize(lerp(normalW, normalWBase, pbClearCoatNormalShare * pbClearCoatIntensity));
    float3 normalWOuter = normalize(lerp(normalW, normalWBase, (1 - pbClearCoatNormalShare) * pbClearCoatIntensity));
  #else
    float3 normalWInner = normalW;
    float3 normalWOuter = normalW;
  #endif

  // lighting
  LightingParamsPBR L;
  L.txDiffuseValue = txDiffuseValue.rgb;
  L.emissiveMult = 1;
  L.shadow = shadow;
  L.occlusion = saturate(min(baseOcclusion, _AO_VAO) * min(extraShadow.y, _AO_SSAO));
  // L.occlusion = min(baseOcclusion, _AO_VAO) * min(extraShadow.y, _AO_SSAO);
  L.reflectionOcclusion = baseOcclusion * EXTRA_SHADOW_REFLECTION * AO_REFLECTION;
  L.roughness = pow(roughnessBaseInner, 2);

  float f0Mult = lerp(pbReflectance.x, pbReflectance.y, txMapsValue.z) * saturate(pbrMults.x);
  #ifdef USE_CLOTH_SHADING
    L.f0 = pbClothSheenColor * f0Mult;
    L.subsurfaceColor = pbClothSubsurfaceColor;
  #else
    L.metalness = baseMetalness;
    L.reflectance = f0Mult;
  #endif

  float reflBlur = getReflBlur(roughnessBaseInner);
  L.reflDir = normalize(reflect(toCamera, normalWInner));
  L.reflDir = normalize(lerp(L.reflDir, normalWInner, roughnessBaseInner));
  L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalWInner, reflBlur, 1 + roughnessBaseInner * 2);

  LightingOutput lightingOutput = calculateLighting(pin.PosC, toCamera, normalWInner, tangentW, bitangentW, L);

  #ifdef USE_CLEARCOAT
    float3 reflDirInner = L.reflDir;
    float reflBlurOuter = getReflBlur(roughnessBaseOuter);
    L.reflDir = normalize(reflect(toCamera, normalWOuter));
    L.reflDir = normalize(lerp(L.reflDir, normalWOuter, roughnessBaseOuter));
    L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalWOuter, reflBlurOuter, 1 + roughnessBaseOuter * 2);

    L.txDiffuseValue = 1;
    L.metalness = 0;
    L.roughness = pow(roughnessBaseOuter, 2);
    L.reflectance = pbClearCoatF0;
    LightingOutput lightingOutputClearCoat = calculateLighting(pin.PosC, toCamera, normalWOuter, tangentW, bitangentW, L, true);

    ClearCoatParams ccP;
    ccP.intensity = pbClearCoatIntensity;
    ccP.f0 = pbClearCoatF0;
    ClearCoat cc = CalculateClearCoat(toCamera, normalWOuter, ccP);
    lightingOutput.diffuse = cc.ProcessDiffuse(lightingOutput.diffuse);
    lightingOutput.specular = cc.ProcessSpecular(lightingOutput.specular, lightingOutputClearCoat.specular);
    float3 reflColorInner = getReflectionAt(reflDirInner, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0);  
    float3 reflFinalInner = lightingOutput.F * reflColorInner;
    float3 reflColorOuter = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlurOuter), true, 0);  
    float3 reflFinalOuter = lightingOutputClearCoat.F * reflColorOuter;
    float3 reflFinal = cc.ProcessSpecular(reflFinalInner, reflFinalOuter);

    // For G-buffer:
    lightingOutput.F = cc.ProcessSpecular(lightingOutput.F, lightingOutputClearCoat.F);
    reflBlur = reflBlurOuter;
  #else
    float3 reflColor = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0);  
    // lightingOutput.F = 1;
    float3 reflFinal = lightingOutput.F * reflColor;
  #endif
  
  float3 lighting = lightingOutput.diffuse + lightingOutput.specular;

  L.txDiffuseValue = txDiffuseValue.rgb;
  #ifndef USE_CLOTH_SHADING
    L.txDiffuseValue.rgb *= occlusionTexValue * lerp(lerp(1, 0.3, L.reflectance), 0.03, L.metalness);
  #endif
  L.txDiffuseValue *= saturate(L.occlusion * 2);
  float lfxSpecularBase = pow(1 - roughnessBaseOuter, 4);
  float lfxSpecularPower = lfxSpecularBase * sqrt(saturate(f0Mult * 4));
  float lfxSpecularEXP = 1 + lfxSpecularBase * 250;
  LIGHTINGFX(lighting);

  float3 withReflection = lighting + reflFinal;
  ReflParams R = (ReflParams)0;
  R.resultBlur = clamp(reflBlur * reflBlur * 7 - 1, 0, 6);
  R.resultPower = 1;
  R.coloredReflectionsColor = lightingOutput.F;
  R.coloredReflections = 1;
  R.resultColor = reflFinal;
  RETURN(withReflection, txNormalValue.a);
}
