#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/refractingCover.hlsl"

PS_IN_RefractingCover main(VS_IN vin) {
  PS_IN_RefractingCover vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);

  float distanceToMirrorPlane = distanceToPlane(extMirrorPlane, posW.xyz);
  vout.IsMirrored = distanceToMirrorPlane < 0;
  vout.PosV = vout.PosC;
  if (vout.IsMirrored){
    vout.PosV -= distanceToMirrorPlane * extMirrorPlane.xyz * 2;
  }

  return vout;
}
