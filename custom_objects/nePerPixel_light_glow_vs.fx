// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/distantGlow.hlsl"

#define PixelScale max(extScreenSize.z, extScreenSize.w)

PS_IN_PerPixelExtra2 main(VS_IN vin) {
  float3 posC = vin.PosL.xyz - ksCameraPosition.xyz;
  float distance = length(posC);
  float3 look = normalize(posC);
  float3 side = normalize(cross(float3(0, 1, 0), posC));
  float3 up = -normalize(cross(side, posC));

  float ambientK = extGlowBrightness;
  float fogRadiusK = saturate(calculateFogNew(posC + normalize(posC) * 1e3) * 2);
  fogRadiusK = pow(saturate((fogRadiusK - 0.4) / 0.6), 0.6);
  float distanceFarGrowK = clamp(distance / 800 - 0.8, 0, 3);
  float distanceFarFadeK = saturate(1.2 - distance / 800);
  distanceFarFadeK = lerp(distanceFarFadeK, distanceFarFadeK * 0.75 + 0.25, saturate(-posC.y / 20 - 0.5));
  float fillRateFix = saturate(distance / 67 - 0.2);

  float3 fogPosC = posC * 0.4;
  float fogK = calculateFogImpl(0, 0, fogPosC);

  float normalConcentration = pow(saturate(length(vin.NormalL.xyz)), 2);
  vin.NormalL.xyz = normalize(vin.NormalL.xyz);

  float2 angle = vsLoadAltTex(vin.TangentPacked);
  float heightMult = lerp(extDistantHeightMult, 1, abs(look.y));
  float halfSize = fogRadiusK * (1 + distanceFarGrowK) * extDistantRadiusMult * fillRateFix;
  vin.PosL.xyz += vin.NormalL.xyz * float3(1, 0.25, 1) * normalConcentration * halfSize / 4;
  vin.PosL.xyz += (up * angle.y * heightMult + side * angle.x) * halfSize;
  vin.PosL.xyz -= look * (2 + distance / 50 + fogK * 20);

  GENERIC_PIECE(PS_IN_PerPixelExtra2);

  #if !defined( ALLOW_VERTEX_ADJUSTMENTS ) || defined( NO_DEPTH_MAP )
    vout = (PS_IN_PerPixelExtra2)0;
    vout.PosH = 10;
    return vout;
  #endif

  float baseFog = vout.Fog;
  vout.Fog = calculateFogImpl(posV, posW, fogPosC);

  float NdotV = dot(-look, vout.NormalW);
  vout.NormalW.xy = angle;
  vout.NormalW.z = 1 / (extDistantSoftK + distance / 30 + fogK * 20);
  vout.Extra.x = extDistantMult * fogRadiusK * saturate(distance / 100 - 0.5) * distanceFarFadeK * ambientK;
  // vout.Extra.y = 15 * (pow(vout.Fog, 2) - pow(baseFog, 2));
  vout.Extra.y = 2 * vout.Fog;
  if (vout.Extra.x <= 0.001 || dot(ksEmissive, 1) < 1){
    vout.PosH = 10;
  }

  return vout;
}
