// #define USE_OLD_CODE
#ifdef USE_OLD_CODE
#include "../recreated/ksPerPixelReflection_ps.fx"
#else

#define USE_PBR
#define CARPAINT_SIMPLE
#define SUPPORTS_AO

#define CARPAINT_NM
#define GETNORMALW_OS_SAMPLER samLinear
#define GETNORMALW_SAMPLER samLinear
#define INPUT_NM_OBJECT_SPACE 0

#define LIGHTINGFX_GLASS_BACKLIT
#define LIGHTINGFX_KSDIFFUSE 1
#define LIGHTINGFX_SPECULAR_COLOR lfxSpecularPower
#define LIGHTINGFX_SPECULAR_EXP lfxSpecularEXP
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0
#define RAINFX_GLASS_BACKLIT

#ifdef USE_PBR

  #ifdef USE_PHOTOELASTICITY_EFFECT
    #define EXTRA_CARPAINT_FIELDS_PHEL\
      float extPhelMult/* = 0.5 */;\
      float extPhelBaseScale/* = 6 */;\
      float extPhelNoiseScale/* = 0.01 */;\
      float extPhelSpotsScale/* = 0.33 */;\
      float extPhelSpotsOffset/* = 0.15 */;\
      float extPhelStripesScale/* = 20 */;
  #else
    #define EXTRA_CARPAINT_FIELDS_PHEL
  #endif

  #define EXTRA_CARPAINT_FIELDS\
    float extIOR;\
    float extThicknessMult;\
    float extThicknessProfileFix;\
    float _blendingMode;\
    float stAmbientSpec;\
    float stAmbientEXP;\
    float extUseNormalMap;\
    float extRefraction;\
    float extRefractionRainbow;\
    float extSaturation;\
    float extRefractionNormalFactor;\
    float extMaskPass;\
    float3 extMaskPassColor;\
    EXTRA_CARPAINT_FIELDS_PHEL
#endif

#ifdef EMISSIVE_MAPPING
  #include "emissiveMapping.hlsl"
#else
  #include "include_new/base/_include_ps.fx"
#endif

#ifdef USE_PBR
  #include "pbr/pbr.hlsl"
  #include "pbr/clear_coat.hlsl"
  #include "common/ambientSpecular.hlsl"
  #define pbReflectionBlurEnv 0
#endif

float estimateV(float x){
  // https://jsfiddle.net/x4fab/txu6amyb/11/
	x *= 0.99;
	return pow(saturate(x - pow(x, 4)), 0.84) * 0.61;
}

#ifdef USE_PHOTOELASTICITY_EFFECT
  float random(in float2 st) {
    return txNoise.SampleLevel(samLinearSimple, st.xy * extPhelNoiseScale, 0).x;
  }

  float noise(float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);
    float2 u = f * f * (3 - 2 * f);
    float v1 = random(i + float2(0, 0));
    float v2 = random(i + float2(1, 0));
    float v3 = random(i + float2(0, 1));
    float v4 = random(i + float2(1, 1));
    return lerp(lerp(v1,v2, u.x), lerp(v3, v4, u.x), u.y);
  }

  float3 birefringence(float3 N, float3 PL, float3 NL, float3 V){  
    float3 L = float3(0, 1, 0);
    float3 H = normalize(L + V);
    float NdotV = saturate(dot(N, V));
    float NdotH = saturate(dot(H, N));

    float S = noise((PL.xz + float2(0.2 * NdotV, 0.5 * NdotH)) * extPhelBaseScale);
    float W = frac(extPhelStripesScale * abs(S));
    float R = floor(extPhelStripesScale * abs(S));
    float3 C = saturate(1 - rainbow(W) * extPhelMult * extPhotoElBoost);
    float U = (R % 4 != 3) * pow(saturate(txNoise.SampleLevel(samLinearSimple, 
      extPhelSpotsOffset + PL.xy * extPhelSpotsScale, 0).x * 2 - 0.2), 3);
    return lerp(1, C, U);
  }
#endif

#ifdef USE_PHOTOELASTICITY_EFFECT
  #define INPUT_TYPE PS_IN_NmPosL
#elif defined(EMISSIVE_MAPPING)
  #define INPUT_TYPE PS_IN_NmExtra4
#else
  #define INPUT_TYPE PS_IN_Nm
#endif

RESULT_TYPE main(INPUT_TYPE pin){
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float inputAlpha = txDiffuseValue.a;
  RAINFX_WET(txDiffuseValue.xyz);

  [branch]
  if (extUseNormalMap) {
    float alpha;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);
    if (extUseNormalMap == 2){
      inputAlpha = alpha;
    }
  }

  float4 txDiffuseValueOrigSaturation = txDiffuseValue;

  #ifdef USE_PBR
    #ifndef MODE_COLORMASK
      txDiffuseValue.rgb = saturate(lerp(luminance(txDiffuseValue.rgb), txDiffuseValue.rgb, extSaturation));

      float4 txDiffuseValueOrig = txDiffuseValue;
      txDiffuseValue.rgb *= abs(ksDiffuse);
    #endif

    float roughnessBase = 0;
    float baseMetalness = 0;

    LightingParamsPBR L;
    L.txDiffuseValue = txDiffuseValue.rgb;
    #ifdef EMISSIVE_MAPPING
      L.emissiveMult = 0;
    #else
      L.emissiveMult = 1;
    #endif
    L.shadow = shadow;
    L.occlusion = extraShadow.y;
    L.reflectionOcclusion = EXTRA_SHADOW_REFLECTION;
    L.roughness = roughnessBase * roughnessBase;
    L.metalness = baseMetalness;
    L.reflectance = fresnelC;
    
    float reflBlur = getReflBlur(roughnessBase);
    L.reflDir = normalize(reflect(toCamera, normalW));
    L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalW, reflBlur);

    LightingOutput lightingOutput = calculateLighting(pin.PosC, toCamera, normalW, 0, 0, L);

    #ifdef EMISSIVE_MAPPING
      float4 emissiveMap;
      lightingOutput.diffuse += getEmissiveValue(txDiffuseValue, pin.Tex, pin.Extra, emissiveMap) * getEmissiveMult();
    #endif

    float specularAmbientMult;
    lightingOutput.specular += getAmbientSpecular(toCamera, normalW, stAmbientSpec, stAmbientEXP, specularAmbientMult);

    #ifdef USE_PHOTOELASTICITY_EFFECT
      float3 photoElasticityMult = birefringence(normalW, pin.PosL, pin.NormalL, -toCamera);
    #else
      float3 photoElasticityMult = 1;
    #endif
    float3 lighting = lightingOutput.diffuse + lightingOutput.specular * photoElasticityMult;

    #ifndef MODE_COLORMASK
      float lfxSpecularPower = 4;
      float lfxSpecularEXP = 1000;
      {
        L.txDiffuseValue = txDiffuseValueOrig.rgb * ksAmbient;
        LIGHTINGFX(lighting);
      }
    #endif

    L.roughness = roughnessBase * roughnessBase;
    float3 reflColor = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0);
    reflColor *= photoElasticityMult;

    float NdotV = saturate(dot(-toCamera, normalW));
    float ior = extIOR;
    float V = estimateV(inputAlpha);
    float thickness = extThicknessMult * V / max(saturate(1 - inputAlpha), 0.0000001);
    float thicknessFix = extThicknessProfileFix / max(0.000001, NdotV) > 0.5;
    lightingOutput.F = saturate(lightingOutput.F + thicknessFix * lightingOutput.F);

    float ccDistance = ClearCoatDistance(toCamera, normalW, ior);
    float alpha = 1 - ClearCoatAttenuation(thickness, ccDistance);
    alpha = lerp(alpha, 1, lightingOutput.F.g);
    alpha = lerp(alpha, 1, specularAmbientMult);
    alpha = saturate(alpha + thicknessFix);
    if (_blendingMode) lightingOutput.F /= max(alpha, 0.001);

    float3 reflFinal = lightingOutput.F * lerp(reflColor, luminance(reflColor), baseMetalness) * getCPLMult(L.reflDir);
    float3 withReflection = lighting + reflFinal;
    ReflParams R = (ReflParams)0;
    R.resultBlur = clamp(reflBlur * reflBlur * 7 - 1, 0, 6);
    R.resultPower = saturate(dot(lightingOutput.F, 0.33)) * L.occlusion * getCPLMult(L.reflDir);
    R.resultColor = reflFinal;
    
    // withReflection = 1;
    // alpha = extIOR;
  #else
    float3 lighting = calculateLighting(pin.PosC, toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, extraShadow.y);

    ReflParams R = getReflParams(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
    R.useBias = true;
    float alpha = 1;
    float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  #endif  

  #ifdef MODE_COLORMASK
    withReflection = txDiffuseValue.rgb / max(txDiffuseValue.r, max(txDiffuseValue.g, max(txDiffuseValue.b, 0.01)));
  #else
    // clip(-1);
  #endif

  // float extRefraction = 0.04;

  RAINFX_WATER_ALPHA(withReflection, alpha);

  [branch]
  if (extRefraction){
    float2 offset = clamp(float2(dot(normalW, normalize(cross(extDirUp, toCamera))), dot(normalW, extDirUp)), -0.3, 0.3);

    float refractionMult = abs(dot(normalW, normalize(pin.NormalW)));
    refractionMult = saturate(refractionMult * extRefractionNormalFactor - (extRefractionNormalFactor - 1));
    refractionMult *= pow(saturate(abs(dot(normalW, toCamera)) * 3 - 0.05), 0.75);
    offset *= lerp(extRefraction, 0.0, refractionMult) * float2(1, 9./16) / length(pin.PosC);

    // float2 ssUV = pin.PosH.xy * extScreenSize.zw;

    #ifdef MODE_GBUFFER
      float4 refracted = 1;
    #elif defined(ALLOW_EXTRA_VISUAL_EFFECTS)
      float2 ssUV = pin.PosH.xy * extScreenSize.zw;
      float4 refracted = txPrevFrame.Sample(samLinear, ssUV + offset);

      [branch]
      if (extRefractionRainbow){
        float4 refracted0 = txPrevFrame.Sample(samLinear, ssUV + offset * extRefractionRainbow);
        float4 refracted2 = txPrevFrame.Sample(samLinear, ssUV + offset / extRefractionRainbow);
        refracted = float4(refracted0.r, refracted.g, refracted2.b, refracted.a);
      }

      [branch]
      if (extMaskPass){
        float3 c = txDiffuseValueOrigSaturation.rgb;
        c = c.rgb / max(c.r, max(c.g, max(c.b, 0.01)));
        c += extMaskPassColor;
        c = max(c, 0.00001);
        c = c / max(c.r, max(c.g, c.b));
        float a = saturate(alpha * extMaskPass);
        refracted.rgb *= lerp(1, c, a);
      }
    #else
      float4 refracted = 0;
    #endif

    refracted.a *= saturate(remap(refractionMult, 0.94, 0.99, 1, 0));
    withReflection = lerp(withReflection, refracted.xyz, refracted.a * (1 - alpha));
    alpha = lerp(alpha, 1, refracted.a);

    // withReflection = abs(dot(normalW, normalize(pin.NormalW)));
    // alpha = 1;
  }

  RETURN(withReflection, alpha);
}

#endif
