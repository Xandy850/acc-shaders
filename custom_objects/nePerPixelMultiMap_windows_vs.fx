#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Windows main(VS_IN vin) {
  PS_IN_Windows vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);

  vout.PosV = vin.PosL.xyz;
  vout.TexV = vin.Tex;

  return vout;
}
