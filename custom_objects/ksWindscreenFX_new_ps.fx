#define NO_CARPAINT
#define NO_SSAO
#define SUPPORTS_BLUR_SHADOWS
#define ALPHATEST_THRESHOLD 0.5
// #define LIGHTINGFX_GLASS_BACKLIT
// #define LIGHTINGFX_EXTRA_FOCUSED

#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_ps.fx"
#ifdef BLUR_SHADOWS
	#include "include_new/ext_windscreenfx/_include_ps.fx"
#endif
#include "common/rainWindscreen_impl.hlsl"

RESULT_TYPE main(PS_IN_PerPixelCustom pin) {
	READ_VECTORS

	float txDiffuseAlpha;
	float resultAlpha;
	float3 resultColor;

	float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
	float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
	float lightValue = saturate(dot(normalize(pin.PosC), -ksLightDirection.xyz) * shadow);
	float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
	float3 resultLight = calculateAmbient(normalW, (float4)1) + diffuse;

	#ifdef BLUR_SHADOWS
		resultAlpha = txDiffuseValue.a < 0.5 
			? (disableTexture ? 0 : txDiffuseValue.a * lightValue) 
			: txDiffuseValue.a;
	#else
		resultAlpha = txDiffuseValue.a < 0.5 ? txDiffuseValue.a * lightValue : txDiffuseValue.a;
	#endif

	resultColor = txDiffuseValue.rgb * resultLight;
	resultColor += calculateEmissive(pin.PosC, txDiffuseValue.rgb, 1);
	txDiffuseAlpha = txDiffuseValue.a;

	bool earlyExit;
	#if defined(MODE_GBUFFER) || defined(MODE_KUNOS)
		earlyExit = true;
	#else
		earlyExit = extMotionStencil == 17;
		[branch]
	#endif
	if (earlyExit){
		clip(resultAlpha - 0.5);
		RETURN_BASE(resultColor, resultAlpha);
	}

	#if defined(APPLY_RAIN) && !defined(MODE_GBUFFER) && !defined(MODE_GBUFFER)
		WindscreenRainData WR = getWindscreenRainData(pin.TexAlt, pin.NormalL, pin.NormalW, toCamera, gCameraUp);
		// WR.applyOpaqueMask(resultAlpha);

    float fresnel = saturate(pow(1 - WR.facingShare, 3)) * 0.5;
    float3 refraction = normalize(lerp(toCamera, -WR.normal, 0.3 * (1 - WR.facingShare)));
    float3 reflection = reflect(toCamera, WR.normal);
    float3 reflectionColor = sampleEnv(lerp(reflection, WR.normal, 0.7), 5, 0);
    float3 refractionColor = sampleEnv(refraction, 3, 0);
    float3 dirtColor = sampleEnv(toCamera, 3, 0);

		#ifdef ALLOW_EXTRA_FEATURES
			if (gUseColor){
				float2 ssUV = pin.PosH.xy * extScreenSize.zw;
				refractionColor = lerp(refractionColor, 
					txPrevFrame.SampleBias(samLinearClamp, ssUV + WR.fakeRefraction * 0.25, -2).rgb, 
					saturate(remap(WR.facingShare, 0.3, 0.4, 0, 1)));
				dirtColor = txPrevFrame.SampleLevel(samLinearClamp, ssUV, 3 + WR.dirtBlur).rgb;
			}
		#endif

		bool useRefraction = true;
		#ifdef NO_REFLECTIONS
			fresnel = lerp(0.2, 1, saturate(1 + dot(toCamera, WR.normal)));
			reflectionColor = getAmbientBaseAt(reflection, 0.35) * saturate(reflection.y * 10);
			dirtColor = getAmbientBaseAt(toCamera, 0.2) * (0.5 + 0.5 * saturate(toCamera.y * 10));
			useRefraction = false;
		#endif

		// dirtColor *= float3(1, 0, 1);
		// refractionColor *= WR.debugColor;

		refractionColor *= WR.refractionOcclusion;
		reflectionColor *= WR.occlusion;
		dirtColor *= WR.occlusion;

		fresnel += lerp(0.2, 0, WR.wiperOcclusion);
		reflectionColor *= lerp(0.5, 1, WR.wiperOcclusion);
		float alphaMult = max(fresnel, WR.wiperOcclusion);
		if (!gUseColor){
			alphaMult *= sqrt(1 - WR.facingShare);
		}

		float3 waterColor = useRefraction ? lerp(refractionColor, reflectionColor, fresnel) : reflectionColor;
		float waterAlpha = saturate(WR.water * 2 - 1) * alphaMult;
		if (!useRefraction){
			waterAlpha *= fresnel;
		}

		float3 rainNormal = WR.normal;
		#ifdef ALLOW_RAINFX
			RainParams RP = (RainParams)0;
		#endif
		normalW = -WR.normal;
		LightingParams L = getLightingParams(pin.PosC, toCamera, WR.normal, 0, shadow, 1);
		L.txDiffuseValue = 0;
		L.txSpecularValue = saturate(WR.water * 2);
		L.txEmissiveValue = 0;
		L.specularExp = 40;
		L.specularValue = 0.5;
		float3 lighting = L.calculate();

		L.specularExp = 8;
		L.specularValue = 2;
  	LIGHTINGFX(lighting);

		waterColor += lighting * WR.occlusion;
		mixLayer(waterColor, waterAlpha, dirtColor, WR.trace);
		mixLayer(waterColor, waterAlpha, resultColor, resultAlpha);

		resultColor = waterColor;
		resultAlpha = waterAlpha;
		mixLayer(resultColor, resultAlpha, dirtColor, WR.condensation);

		if (WR.debugMode) {
			resultColor = WR.debugColor;
			resultAlpha = 0.8;
		}
		
		if (resultColor.g > -1) {
			// resultColor = dirtColor;
			// resultAlpha = 1;
		}

		// resultColor = txWiperShape.Sample(samLinear, pin.TexAlt).w;
		// resultAlpha = 0.5;

		// resultColor = float3(water, 1 - water, 0);
		// resultColor = float3(trail, 1 - trail, 0);
		// resultAlpha = 0.8;
	#endif
	
  #ifdef NO_LIGHTING
    resultColor = 0.0;
  #endif  
	
  RETURN_BASE(resultColor, resultAlpha);
}
