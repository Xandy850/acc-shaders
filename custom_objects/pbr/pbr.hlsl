#define PI 3.14159265
#define BRIGHTNESS_FIX_DIFFUSE 0.4
#define BRIGHTNESS_FIX_SPECULAR 0.4
#define CLEARCOAT_GOOGLE

struct LightingParamsPBR {
  float3 txDiffuseValue;
  float3 emissiveMult;
  float3 specMult;
  float3 reflDir;
  float roughness;
  float occlusion;
  float reflectionOcclusion;
  float shadow;

  #ifdef USE_CLOTH_SHADING
    float3 f0;
    float3 subsurfaceColor;
  #else
    float reflectance;
    float metalness;
  #endif
};

struct LightingOutput {
  float3 diffuse;
  float3 specular;
  float3 F;
};

float F_Schlick(float f0, float f90, float u){
  return f0 + (f90 - f0) * pow(1.f - u, 5.f);
}

float3 F_Schlick(float3 f0, float f90, float u){
  return f0 + (f90 - f0) * pow(1.f - u, 5.f);
}

float3 sqr(float3 x){
  return x * x;
}

float3 F_Fresnel(float3 SpecularColor, float VoH){
  float3 SpecularColorSqrt = sqrt(min(SpecularColor, float3(0.99, 0.99, 0.99)));
  float3 n = (1 + SpecularColorSqrt) / (1 - SpecularColorSqrt);
  float3 g = sqrt(n*n + VoH*VoH - 1);
  return 0.5 * sqr((g - VoH) / (g + VoH)) * (1 + sqr(((g + VoH)*VoH - 1) / ((g - VoH)*VoH + 1)));
}

float V_SmithGGXCorrelated(float NdotL, float NdotV, float alphaG2) {
  float Lambda_GGXV = NdotL * sqrt((-NdotV * alphaG2 + NdotV) * NdotV + alphaG2);
  float Lambda_GGXL = NdotV * sqrt((-NdotL * alphaG2 + NdotL) * NdotL + alphaG2);
  return 0.5 / (Lambda_GGXV + Lambda_GGXL);
}

float V_Kelemen(float LoH) {
  return 0.25 / (LoH * LoH);
}

float V_Neubelt(float NoV, float NoL) {
  // Neubelt and Pettineo 2013, "Crafting a Next-gen Material Pipeline for The Order: 1886"
  return 1.0 / (4.0 * (NoL + NoV - NoL * NoV));
}

float D_GGX(float NdotH, float m2) {
  float f = (NdotH * m2 - NdotH) * NdotH + 1;
  return m2 / (f * f);
}

float D_Charlie(float NoH, float roughness) {
  // Estevez and Kulla 2017, "Production Friendly Microfacet Sheen BRDF"
  float invAlpha  = 1 / roughness;
  float cos2h = NoH * NoH;
  float sin2h = max(1 - cos2h, 0.0078125); // 2^(-14/2), so sin2h^2 > 0 in fp16
  return (2 + invAlpha) * pow(sin2h, invAlpha * 0.5) / 2;
}

float DiffuseBurley(float LdotH, float NdotV, float NdotL, float roughness){
  float FD90 = 0.5 + 2.0 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL;
}

float DiffuseBurley_Custom(float LdotH, float NdotV, float NdotL, float roughness){
  float FD90 = 0.5 + 1.2 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL;
}

float DiffuseDisney(float LdotH, float NdotV, float NdotL, float roughness) {
  float energyBias = lerp(0, 0.5, roughness);
  float energyFactor = lerp(1, 1 / 1.51, roughness);
  float FD90 = energyBias + 2.0 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL * energyFactor;
}

#include "ambientSpecular.hlsl"

Texture2D<float3> txBDRFMap : register(t25);

float computeSpecularAO(float NoV, float ao, float roughness) {
  return saturate(pow(NoV + ao, exp2(-16.0 * roughness - 1)) - 1 + ao);
}

LightingOutput calculateLighting(float3 posC, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, 
    LightingParamsPBR P, bool clearCoatMode = false){
  float3 N = normalW;
  float3 V = -toCamera;
  float3 L = -ksLightDirection.xyz;
  float3 H = normalize(L + V);

  float NdotV = abs(dot(N, V)) + 1e-5f;
	float NdotL = saturate(dot(L, N));
	float LdotH = saturate(dot(L, H));
	float HdotV = saturate(dot(H, V));
	float NdotH = saturate(dot(H, N));
  float HdotX = dot(H, tangentW);
  float HdotY = dot(H, bitangentW);
  float3 BRDF = txBDRFMap.Sample(samLinearClamp, float2(NdotV, 1 - sqrt(P.roughness)));

  LightingOutput result;
  P.roughness = clamp(P.roughness, 0.0001, 1);
  P.occlusion = saturate(P.occlusion);

  #ifdef USE_CLOTH_SHADING
    float3 f0 = P.f0;
    float3 diffColor = P.txDiffuseValue.rgb;
    float3 diffSpecMult = ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE * P.shadow;
  #else
    P.reflectance = lerp(P.reflectance, 1, P.metalness);
    float3 f0 = clearCoatMode ? P.reflectance : lerp(lerp(0, 1, P.reflectance), P.txDiffuseValue.rgb, P.metalness);
    float3 diffColor = lerp(lerp(P.txDiffuseValue.rgb, 0, P.reflectance), 0, P.metalness);
    float3 diffSpecMult = ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE * NdotL * P.shadow;
  #endif

  // float diffModel = DiffuseBurley(LdotH, NdotV, NdotL, P.roughness);
  float diffModel = DiffuseBurley_Custom(LdotH, NdotV, NdotL, P.roughness);
  // float diffModel = DiffuseDisney(LdotH, NdotV, NdotL, P.roughness);
  // float diffModel = 1; // Lambert

  #ifndef ALLOW_PBR_EXTRAS_BASE
    diffModel = 1;
    #ifdef USE_CLEARCOAT 
      if (clearCoatMode){
        return (LightingOutput)0;
      }
    #endif
  #endif

  result.diffuse = diffModel * diffSpecMult * diffColor;
  float3 indirectDiffuse = diffColor * calculateAmbient(P.reflDir, 1) * BRIGHTNESS_FIX_DIFFUSE * P.occlusion;
  float3 emissive = calculateEmissive(posC, P.txDiffuseValue.rgb, P.emissiveMult);

  #ifdef USE_CLEARCOAT
    if (!clearCoatMode){
      f0 = lerp(f0, pow((1 - 5 * sqrt(f0)) / (5 - sqrt(f0)), 2), pbClearCoatIntensity);
    }
  #endif

  P.reflectionOcclusion *= saturate(50.0 * dot(f0, 0.33));

  #ifdef USE_CLOTH_SHADING

    float Vis = V_Neubelt(NdotV, NdotL);
    float D = D_Charlie(NdotH, P.roughness);
    if (dot(P.subsurfaceColor, 1) > 0.001){
      result.diffuse *= saturate((NdotL + 0.5) / 2.25);
      result.diffuse *= saturate(P.subsurfaceColor + NdotL);
      indirectDiffuse *= saturate(P.subsurfaceColor + NdotV);
    } else {
      result.diffuse *= NdotL;
    }

    result.diffuse += indirectDiffuse;
    result.diffuse += emissive;

    float3 Fr = (D * Vis) * f0 / PI;
    result.specular = max(Fr * diffSpecMult * NdotL, 0) * P.reflectionOcclusion * BRIGHTNESS_FIX_SPECULAR;

    result.F = BRDF.z * f0 * P.reflectionOcclusion;
    result.F *= computeSpecularAO(NdotV, P.reflectionOcclusion, P.roughness);
    
  #else

    result.diffuse += indirectDiffuse;
    result.diffuse += emissive;

    float3 F = F_Schlick(f0, 1, HdotV);
    float Vis = clearCoatMode ? V_Kelemen(LdotH) : V_SmithGGXCorrelated(NdotV, NdotL, P.roughness);
    float D = D_GGX(NdotH, P.roughness);

    float3 Fr = (D * Vis) * (clearCoatMode ? 1 : F) / PI;
    float3 energyCompensation = 1 + f0 * (1 / BRDF.y - 1);
    Fr *= energyCompensation;
    result.specular = max(Fr * diffSpecMult, 0) * P.reflectionOcclusion * BRIGHTNESS_FIX_SPECULAR;
    result.F = lerp(BRDF.x, BRDF.y, f0);
    result.F *= computeSpecularAO(NdotV, P.reflectionOcclusion, P.roughness);
    
    float horizon = saturate(1 + dot(normalize(reflect(toCamera, normalW)), normalW));
    result.F *= horizon * horizon;

  #endif

  #ifndef ALLOW_PBR_EXTRAS_BASE
    result.specular = 0;
    result.F = 0;
  #endif

  return result;
}

float getReflBlur(float roughtness){
  return pow(saturate(roughtness), 0.1);
}