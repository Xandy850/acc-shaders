#define SUPPORTS_NORMALS_AO
#include "common/bending.hlsl"
#include "include_new/base/_include_vs.fx"
#include "common/tessellation.hlsl"

BENDING_FX_IMPL_NONE
#define gMinTessDistance 5
#define gMaxTessDistance 0.5

#define gMinTessFactor 1
#ifdef ALLOW_PARALLAX
  #define gMaxTessFactor 5
#else
  #define gMaxTessFactor 1
#endif

VS_OUT main(VS_IN vin) {
  VS_OUT vout;

  vout.NormalW = normalize(vin.NormalL);
  PREPARE_TANGENT;
  vout.PosC = vin.PosL.xyz;
  vout.Tex = vin.Tex;
  PREPARE_AO(vout.Ao);
  
  #ifdef MODE_KUNOS
    vout.TessFactor = 1;
  #else
    float4 posV, posW;
    toScreenSpace(vin.PosL, posW, posV);
    float d = length(posW.xyz - ksCameraPosition.xyz) * extLODDistanceMult;
    float tess = saturate((gMinTessDistance - d) / (gMinTessDistance - gMaxTessDistance));
    // vout.TessFactor = gMinTessFactor + tess * (gMaxTessFactor - gMinTessFactor);
    vout.TessFactor = 5;
  #endif
  return vout;
}
