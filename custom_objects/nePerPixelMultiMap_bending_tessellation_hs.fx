#define SUPPORTS_NORMALS_AO
#include "common/bending.hlsl"
#include "include_new/base/_flags.fx"
#include "common/tessellation.hlsl"

PatchTess PatchHS(InputPatch<VS_OUT, 3> patch, uint patchID : SV_PrimitiveID) {
  PatchTess pt;
  pt.EdgeTess[0] = 0.5 * (patch[1].TessFactor + patch[2].TessFactor);
  pt.EdgeTess[1] = 0.5 * (patch[2].TessFactor + patch[0].TessFactor);
  pt.EdgeTess[2] = 0.5 * (patch[0].TessFactor + patch[1].TessFactor);
  pt.InsideTess = pt.EdgeTess[0];
  return pt;
}

[domain("tri")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("PatchHS")]
HS_OUT main(InputPatch<VS_OUT, 3> p, uint i : SV_OutputControlPointID, uint patchId : SV_PrimitiveID) {
	HS_OUT hout;
	hout.PosC = p[i].PosC;
	hout.NormalW = p[i].NormalW;
  #ifndef NO_NORMALMAPS
    hout.TangentW = p[i].TangentW;
    hout.BitangentW = p[i].BitangentW;
  #endif
  #ifdef ALLOW_PERVERTEX_AO
    hout.Ao = p[i].Ao;
  #endif
	hout.Tex = p[i].Tex;
	return hout;
}