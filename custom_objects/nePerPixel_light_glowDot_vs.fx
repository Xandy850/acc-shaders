// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/distantGlow.hlsl"

#define PixelScale max(extScreenSize.z, extScreenSize.w)

PS_IN_PerPixelExtra1 main(VS_IN vin) {
  #ifdef MODE_KUNOS
    DISCARD_VERTEX(PS_IN_PerPixelExtra1);
  #else
    float3 posC = vin.PosL.xyz - ksCameraPosition.xyz;
    float distance = length(posC);
    float3 look = normalize(posC);
    float3 side = normalize(cross(float3(0, 1, 0), posC));
    float3 up = -normalize(cross(side, posC));

    float ambientK = extGlowBrightness;
    float fovK = tan(3.141592 / 180 * ksFOV / 2.4);
    float pixelRadius = mul(mul(vin.PosL, ksView), ksProjection).w * PixelScale;
    float distanceK = max(pixelRadius * fovK, 0);

    float3 fogPosC = normalize(posC) * min(pow(length(posC) / 1000, 1.6) * 1000, length(posC));
    float fogK = pow(calculateFogImpl(0, 0, fogPosC), 2);
    float2 angle = vsLoadAltTex(vin.TangentPacked);

    float multSize = 1 + 4 * fogK;
    #ifdef INCREASE_GLOW_DOT
      multSize = 20 + 20 * fogK;
    #endif

    vin.PosL.xyz += (up * angle.y + side * angle.x) * distanceK * extDotRadiusMult 
      * ambientK * multSize;
    // vin.PosL.xyz += (up * angle.y + side * angle.x);
    vin.PosL.xyz -= look;

    float3 normalW = vin.NormalL.xyz;
    float normalLength = length(normalW);
    normalW /= normalLength;
    
    float normalConcentration = pow(saturate(normalLength), 2);
    vin.NormalL.xyz = 0;

    GENERIC_PIECE(PS_IN_PerPixelExtra1);
    vout.Fog = calculateFogImpl(posV, posW, fogPosC);

    float NdotV = pow(saturate(dot(-look, normalW)), 0.25);
    vout.NormalW.xy = angle;
    vout.NormalW.z = lerp(1, NdotV, normalConcentration);

    float multExtra = smoothstep(4, 24, ksFOV) * saturate(distance / 80 - 0.3);
    #ifdef INCREASE_GLOW_DOT
      multExtra = 1 * saturate(distance / 40 - 0.3);
    #endif

    vout.Extra = vout.NormalW.z * vout.NormalW.z * ambientK
      * multExtra * (1 - fogK)
      * saturate(2 - distance * extMaxDistanceHalfInv);

    if (vout.Extra.x <= 0.001){
      vout.PosH = 10;
    }

    return vout;
  #endif
}
