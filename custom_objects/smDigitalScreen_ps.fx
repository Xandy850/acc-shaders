#define CARPAINT_SIMPLE
#define SUPPORTS_AO

#define INPUT_DIFFUSE_K 0.01
#define INPUT_AMBIENT_K 0.01
#define INPUT_SPECULAR_K 0.2
#define INPUT_SPECULAR_EXP 40

#include "include_new/base/_include_ps.fx"
#include "common/digitalScreen.hlsl"

RESULT_TYPE main(PS_IN_SmDigitalScreen pin) {
  READ_VECTORS
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  DigitalScreenData D = digitalScreenInit(pin.Tex, pin.DigitalScreenPos);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 screenFix = digitalScreenCalculate(D, pin.PosC + ksCameraPosition.xyz, 
    pin.Tex, normalW, toCamera, txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, 1, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = screenFix;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
  R.useBias = true;
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, withReflection.a);
}
