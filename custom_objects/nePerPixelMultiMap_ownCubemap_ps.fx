#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define SAMPLE_REFLECTION_FN sampleReflectionDoubleCube
float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, float4 extraParam);

#include "include_new/base/_include_ps.fx"

TextureCube txCubeSecond : register(t21);

cbuffer cbCubeSecondTransform : register(b12) {  
  float4x4 extCubeSecondTransform;
}

float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, float4 extraParam){
  float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
  float3 reflDir2 = mul(reflDir, (float3x3)extCubeSecondTransform);
  float3 reflCube2Dir = float3(-reflDir2.x, reflDir2.y, reflDir2.z);

  float3 first;
  float4 second;

  if (useBias){
    float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur + level * saturate(2 - blur)).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2 + level * saturate(2 - blur));
  } else {
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2);
  }

  second.rgb *= ksAmbientColor_sky.rgb * perObjAmbientMultiplier + ksLightColor.rgb * extraParam.w + extraParam.rgb;
  return lerp(first.rgb, second.rgb, second.a);
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  float3 lighting = L.calculate();
  
  float3 lightingFX = LIGHTINGFX_GET;
  lighting.rgb = LIGHTINGFX_APPLY(lighting, lightingFX);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  R.reflectionSampleParam = float4(lightingFX / max(ksDiffuse * txDiffuseValue.rgb, 0.0001), shadow);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  R.resultPower = 0;
  R.resultColor = 0;
  RETURN(withReflection, 1);
}
