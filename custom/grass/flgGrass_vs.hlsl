StructuredBuffer<FoliageFinVertex> particleBuffer : register(t0);
Texture2D<float2> txDeformation : register(t1);

#define MAX_WIND_SPEED_2 4

float2 windOffset2(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED_2);
		float windIntensity = saturate(extWindSpeed / MAX_WIND_SPEED_2);
		float randOffset = lerp(frac(dot(pos.xz, 193.193)) * 3.14, 0, windIntensity);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17 + randOffset) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13 + randOffset);
    return extWindVel * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
  #endif
}

float2 windOffsetPrev2(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeedPrev / MAX_WIND_SPEED_2);
		float windIntensity = saturate(extWindSpeed / MAX_WIND_SPEED_2);
		float randOffset = lerp(frac(dot(pos.xz, 193.193)) * 3.14, 0, windIntensity);
    float wave = sin(extWindWavePrev * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17 + randOffset) 
      * sin(extWindWavePrev * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13 + randOffset);
    return extWindVelPrev * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
  #endif
}


void applyWind(inout float3 pos, float3 up, float heightHalf, bool prev, float phaseOffset){
	float totalHeight = heightHalf * 2;
	float2 windXZ = (prev ? windOffsetPrev2(pos.xyz + phaseOffset, 1, 2.2) : windOffset2(pos.xyz + phaseOffset, 1, 2.2)) * 0.5;
	float y = saturate(length(windXZ));
	float verticalOffset = (1 - sqrt(1 - y * y)) * totalHeight;
	float3 wind = float3(windXZ.x, 0, windXZ.y) * totalHeight;
	pos += wind - up * (dot(wind, up) - verticalOffset);
}

float getBentK(float Py, float bentPow, float bentStrength){
	return pow(abs(Py), bentPow) * bentStrength;
}