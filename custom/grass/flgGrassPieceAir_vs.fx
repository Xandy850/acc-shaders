#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Air main(uint id: SV_VertexID) {
  VS_Air vout;
  float2 ver = BILLBOARD[id];
  float4 posW = mul(float4(-ver.x * 1, -1, ver.y * 1, 1), gTransform);
  float4 posH = mul(posW, gViewProj);
  vout.PosH = posH;
  return vout;
}
