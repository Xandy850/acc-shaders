#include "include/samplers.hlsl"
#include "include/common.hlsl"
#include "flgGrass_configurations.hlsl"

#define RAND rand(seed, uv)
#define RAND_INIT(x) float2 uv = x;

cbuffer cbCSBuffer : register(b11) {
  float3 gCameraPos;
  float gMapDepthSize;

  float3 gCameraDir;
  float gMapStep;

  float2 gMapPointA;
  float2 gMapPointB;

  float2 gMapOrigin;
  uint gTripleFrustum;
  uint gDebugPasses;

  float2 gSpawnPointA;
  float2 gSpawnPointB;

  float gMapOriginY;
  float gMapResolution;
  float gRandomSeed;
  float gPassID;

  float2 gDeformationPointA;
  float2 gDeformationPointB;

  float4 gViewFrustum1;
  float4 gViewFrustum2;
  float4 gViewFrustum3;
  float4 gViewFrustum4;

  float3 gLightDirection;
  float gFarPass;

  float2 gAirPointA;
  float2 gAirPointB;

  float2 gTexGrid;
  float2 gTexGridInv;

  PackedGrassParams gp[PACKED_GRASS_PARAMS_COUNT];

  uint4 gTexGroupsCount;
  float4 gTexGroupsChanceTotal;
  CustomTexCSGroup gTexGroups[CUSTOM_TEX_GROUPS];
};

int getGroup(float4 groupChances, float randValue){  
  float groupRand = randValue * (1 + dot(groupChances, 1));
  if ((groupRand -= 1) < 0) return -1;
  if ((groupRand -= groupChances.x) < 0) return 0;
  if ((groupRand -= groupChances.y) < 0) return 1;
  if ((groupRand -= groupChances.z) < 0) return 2;
  return 3;
}

// float3 forceFields(float3 pos) {
//   float3 ret = 0;
//   for (uint i = 0; i < FORCE_FIELDS; i++) {
//     float3 diff = pos - gForceFields[i].pos;
//     float dist = dot2(diff);
//     float distK = saturate(1 - dist * gForceFields[i].radiusSqrInv);
//     float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult;
//     float3 forceVelSync = (gForceFields[i].velocity - 0) * abs(gForceFields[i].velocity);
//     ret += distK * distK * (forcePushAway + forceVelSync);
//   }
//   return ret;
// }

Texture2D txNoise : register(t0);
Texture2D<float> txDepthGrass : register(t1);
Texture2D txColor : register(t2);
Texture2D txNormalAO : register(t3);
Texture2D<float2> txDeformation : register(t4);
Texture2D txAdjustments : register(t5);
Texture2D txMaterialParams : register(t6);
Texture2D<float2> txAir : register(t7);
AppendStructuredBuffer<FoliageFinVertex> buResult: register(u0);

float getSaturationK(float3 color){
  float ret;  
  float div = max(color.r, color.b);
  ret = saturate(color.g / max(color.r, color.b));
  ret = pow(ret, 2);
  ret *= saturate(2.025 * color.g / (color.r + color.b) - 1);
  ret *= saturate(color.g * 24);
  ret *= saturate(20 - color.g * div * 200);
  ret *= 1.2;
  return ret;
}

inline float rand(inout float seed, in float2 uv) {
	float result = frac(sin(seed * dot(uv, float2(12.9898, 78.233))) * 43758.5453);
	seed += 1.0;
	return result;
}

struct SamplingParams {
  float2 mapUV;
  float4 bilinearFactors;
  bool isOutside;
};

SamplingParams getSamplingParams(float2 posXZ){
  SamplingParams ret;
  float2 mapRel = (posXZ - gMapPointB) / (gMapPointA - gMapPointB);
  ret.mapUV = mapRel;
  ret.bilinearFactors = bilinearFactors(ret.mapUV * gMapResolution - 0.5);
  // ret.isOutside = min(mapRel.x, mapRel.y) < 0 || max(mapRel.x, mapRel.y) > 1;
  ret.isOutside = false;
  return ret;
}

GrassParams getGrassParams(SamplingParams S){
  return getGrassParams(gp, txAdjustments.SampleLevel(samLinearClamp, S.mapUV, 0));
}

float3 getAir(float2 posXZ){
  float2 airUV = (posXZ - gAirPointB) / (gAirPointA - gAirPointB);

  float2 val = 0;
  float count = 0;
  [unroll] for (int x = -1; x <= 1; x++){
    [unroll] for (int y = -1; y <= 1; y++){
      if (abs(x) + abs(y) == 2) continue;
      val += txAir.SampleLevel(samLinearBorder0, airUV, 2, int2(x, y)) * 1.4;
      count++;
    }
  }

  return float3(val.x, 0, val.y) / count;
}

float3 getDeformation(float2 posXZ){
  float2 deformationUV = (posXZ - gDeformationPointB) / (gDeformationPointA - gDeformationPointB);
  float2 deformationEdges = saturate((0.5 - abs(deformationUV - 0.5)) * 10);
  float deformationEdge = deformationEdges.x * deformationEdges.y;
  // if (deformationEdge == 0) return 0;
  int R = 2;
  float2 e = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0);
  float2 a = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, -R));
  float2 b = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, R));
  float2 c = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, -R));
  float2 d = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, R));
  float2 av = (a + b + c + d + e) / 5;
  float2 mx = max(max(a, b), max(c, d));
  float2 ix = min(min(a, b), min(c, d));
  return float3(e.x, av.y * av.y, av.x) * deformationEdge;
  // return max(max(a, b), max(c, d));
  // float4 g = txDeformation.GatherRed(samBorder, deformationUV, int2(0, 0));
  // return max(max(g.x, g.y), max(g.z, g.w));
}

float sampleDepthY(float baseY, float2 mapUV, float2 offset){
  float r = txDepthGrass.SampleLevel(samPointClamp, mapUV + offset, 0);
  return r == 1 ? baseY : gMapOriginY - r * gMapDepthSize;
}

float estimateSingleness(SamplingParams S, float3 toCamera, float distance){
  float ret = 1 - pow(txColor.SampleLevel(samLinearClamp, S.mapUV, 4).a, 4) * 0.8;
  // ret = lerp(ret, 1, saturate(distance / 50 + pow(abs(toCamera.y), 3)));
  return ret;
}

float calculateDepth(SamplingParams S, bool useLowestPoint, out float3 normal, out float ao){
  float4 depthG = txDepthGrass.GatherRed(samLinearClamp, S.mapUV, int2(0, 0));
  float depthRaw = dot(depthG.wzxy, S.bilinearFactors);

  float depthMin = min(min(depthG.x, depthG.y), min(depthG.z, depthG.w));
  float depthMax = max(max(depthG.x, depthG.y), max(depthG.z, depthG.w));
  depthRaw = useLowestPoint ? depthMax : depthRaw;
  // depthRaw = txDepthGrass.SampleLevel(samLinearClamp, S.mapUV, 0);

  float ret = gMapOriginY - depthRaw * gMapDepthSize;
  float4 txNormalAOValue = txNormalAO.SampleLevel(samLinearClamp, S.mapUV, 0);
  normal = normalize(txNormalAOValue.xyz * 2 - 1);
  ao = txNormalAOValue.w;

  if ((depthMax - depthMin) > 0.0008){
  // if ((depthMax - depthMin) > 0.003){
    normal.y = 0;
  }

  // {
  //   float3 point0 = float3(0.0, depthG.w * gMapDepthSize, 0.0);
  //   float3 point1 = float3(0.25, depthG.z * gMapDepthSize, 0.0);
  //   float3 point2 = float3(0.0, depthG.x * gMapDepthSize, 0.25);
  //   float3 point3 = float3(0.25, depthG.y * gMapDepthSize, 0.25);
  //   normal = -normalize(cross(point1 - point0, point2 - point0));
  // }

  // {
  //   float M = 0;
  //   float R = 10;
  //   float U = R / (gMapPointA.x - gMapPointB.x);
  //   float3 point0 = float3(-R, sampleDepthY(ret, S.mapUV, float2(-U, -U)), -R);
  //   float3 point1 = float3(R, sampleDepthY(ret, S.mapUV, float2(U, -U)), -R);
  //   float3 point2 = float3(-R, sampleDepthY(ret, S.mapUV, float2(-U, U)), R);
  //   float3 point3 = float3(R, sampleDepthY(ret, S.mapUV, float2(U, U)), R);
  //   normal = -normalize(cross(point1 - point0, point2 - point0));
  // }

  return ret;
}

bool isVisible(float3 pos){
  // return true;
  if (gTripleFrustum){
    return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
      || dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5;
  } else {
    return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
      && dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5
      && dot(gViewFrustum3.xyz, pos) - gViewFrustum3.w > -0.5
      && dot(gViewFrustum4.xyz, pos) - gViewFrustum4.w > -0.5;
  }
}

float calculateSpecular(float3 normalW, float3 toCamera, float specularEXP){
  float specularBase = saturate(dot(normalize(toCamera - gLightDirection), normalW));
  return pow(specularBase, specularEXP) * saturate(dot(normalW, -gLightDirection) * 4);
}