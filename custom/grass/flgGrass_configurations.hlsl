struct PackedGrassParams {
  float4 base;
  float4 x;
  float4 y;
  float4 z;
  float4 w;
};

#ifdef GRASS_PARAMS_MASK_ONLY
  #define PACKED_GRASS_PARAMS_COUNT 1
#else
  #define PACKED_GRASS_PARAMS_COUNT 3
#endif

void getValue(out float x, out float y, out float z, out float w, PackedGrassParams gp, float4 t){
  float4 ret = gp.base;
  ret = lerp(ret, gp.x, t.x);
  ret = lerp(ret, gp.y, t.y);
  ret = lerp(ret, gp.z, t.z);
  ret = lerp(ret, gp.w, t.w);
  // ret += (gp.x - gp.base) * t.x;
  // ret += (gp.y - gp.base) * t.y;
  // ret += (gp.z - gp.base) * t.z;
  // ret += (gp.w - gp.base) * t.w;
  x = ret.x;
  y = ret.y;
  z = ret.z;
  w = ret.w;
}

#define _GP_READ1(N, P0, P1, P2, P3) getValue(ret.P0, ret.P1, ret.P2, ret.P3, gp[N], t);
#define _GP_READ4(N, P) getValue(ret.P.x, ret.P.y, ret.P.z, ret.P.w, gp[N], t);

struct GrassParams {
	float mask_mainThreshold;
	float mask_redThreshold;
	float mask_minLuminance;
	float mask_maxLuminance;
  #ifndef GRASS_PARAMS_MASK_ONLY
    float shape_size;
    float shape_tidy;
    float shape_cut;
    float shape_width;
    float4 groupChances;
  #endif
};

GrassParams getGrassParams(PackedGrassParams gp[PACKED_GRASS_PARAMS_COUNT], float4 t){
  GrassParams ret;
  _GP_READ1(0, mask_mainThreshold, mask_redThreshold, mask_minLuminance, mask_maxLuminance);
  #ifndef GRASS_PARAMS_MASK_ONLY
    _GP_READ1(1, shape_size, shape_tidy, shape_cut, shape_width);
    _GP_READ4(2, groupChances);
  #endif
	// ret.mask_mainThreshold = 0;
	// ret.mask_redThreshold = 0;
	// ret.mask_minLuminance = 0;
	// ret.mask_maxLuminance = 1;
	// ret.shape_size = 2;
	// ret.shape_tidy = 0;
	// ret.shape_cut = 0;
	// ret.shape_width = 1;
  // ret.groupChances = 0.0001;
  return ret;
}

float colorThreshold(float3 color, float colorThreshold, float colorThresholdRed, float minLuminance, float maxLuminance) {
  float3 n = normalize(color);
  float v = max(color.r, max(color.g, color.b));
  float ret = saturate((n.g + n.r * colorThresholdRed - max(n.b, (n.r + n.b) / 2)) * 10 - colorThreshold);
  ret = min(ret, saturate((v - minLuminance) * 20));
  ret = min(ret, saturate((maxLuminance - v) * 20));
  return ret;  
}
