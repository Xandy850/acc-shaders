#define ALPHATEST_THRESHOLD 0.8
#define A2C_SHARPENED_SIMPLE

#define MODE_MAIN_FX

#define INPUT_DIFFUSE_K 1
#define INPUT_AMBIENT_K 1
#define INPUT_SPECULAR_K 0.14
#define INPUT_SPECULAR_EXP 3
#define INPUT_EMISSIVE3 0
#define NO_AMBIENT_MULT
#define IS_TRACK_MATERIAL 1

#define NO_CARPAINT
// #define NO_GBUFFER
// #define NO_SSAO
#define NO_EXTAMBIENT
// #define INCLUDE_GRASS_CB
#define NO_EXTSPECULAR
#define LIGHTINGFX_GRASS

#include "include_new/base/_include_ps.fx"
#include "flgGrass.hlsl"

Texture2D txGrass : register(t0);

float calculateSpecular(float3 normalW, float3 toCamera, float specularEXP){
  float specularBase = saturate(dot(normalize(toCamera - ksLightDirection.xyz), normalW));
  return pow(specularBase, specularEXP) * dot(normalW, -ksLightDirection.xyz);
}

#if defined(CREATE_SHADOW_BUFFER) || defined(MODE_GBUFFER_)
float main(PS_IN pin) : SV_TARGET {
#else
PS_OUT main(PS_IN pin) {
#endif

  float4 texValue = txGrass.Sample(samLinear, pin.Tex);
  #if defined(CREATE_SHADOW_BUFFER)
    uint2 pos = uint2(pin.PosH.xy);
    uint2 xy2 = pos.xy % 2;
    if (texValue.a < 0.25 || dot(xy2, 1) == 1) discard;
    return 1;
  #else
    clip(texValue.a - 0.25);
    float4 txDiffuseValue = 1;
    #ifdef MODE_GBUFFER
      READ_VECTORS
      float3 lighting = 0;
      texValue.a = 1;
    #else
      float wetK = extSceneWetness * pin.Occlusion;
      float texAO = lerp(1, texValue.g, pin.Fade);
      float texBacklit = saturate(texValue.g * 10 - 8);
      float texSpec = saturate(texValue.g * 10 - 9);
      float texSat = saturate((texValue.g - max(texValue.r, texValue.b)) * 2);
      float3 baseColor = lerp(pin.Color, texValue.rgb * 0.3, (1 - texSat) * pin.Fade);
      baseColor *= lerp(1, 0.85, wetK);
      // baseColor = float3(texSat, 1 - texSat, 0) * 0.25;

      float3 toCamera = normalize(pin.PosC);
      #ifdef NO_SS_EFFECTS
        float3 lighting = baseColor * (pin.Ambient
          + pin.Lighting) * texAO
          + pin.Backlit * texBacklit;
      #else
        float3 lighting = baseColor * (pin.Ambient * AO_LIGHTING.w
          + pin.Lighting * AO_EXTRA_LIGHTING + GI_LIGHTING * 0.33) * texAO
          + pin.Backlit * texBacklit;
      #endif
      lighting += extSpecularColor.rgb * pin.SpecIntensity * (1 + texSpec * 2);

      float3 localNormal = normalize(pin.LocalSpecNormal);
      lighting += extSpecularColor.rgb * calculateSpecular(localNormal, -toCamera, 10) * 0.04 * pin.LocalSpecShadow;

      float facingToCamera = dot(localNormal, -toCamera);
      float fresnel = lerp(0.08, 0.5, pow(saturate(1 - facingToCamera), 4)) * (pow(pin.PosY, 2) + pin.PosY * wetK) * pin.Fade;
      float3 reflDir = fixReflDir(normalize(reflect(toCamera, localNormal)), pin.PosC, localNormal, 0);
      lighting += fresnel * getReflectionAt(reflDir, -toCamera, 6, false, 0);
    #endif

    RETURN_BASE(lighting, texValue.a);
  #endif
}