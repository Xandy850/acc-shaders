struct FoliageFinVertex {
  float3 position;
  uint type_flip;

  uint2 color_fade;
  uint2 rotation_width_height;

  float4 up_singleness;
  
  uint2 normal_ao;
  uint2 shadowmult_passid_dif_spec;
  // float2 pad;
};

void FLG_pack(out uint value, float2 vec){ value = f32tof16(vec.x) | (f32tof16(vec.y) << 16); }
void FLG_pack(out uint2 value, float4 vec){ FLG_pack(value.x, vec.xy); FLG_pack(value.y, vec.zw); }
float2 FLG_unpack(uint vec){ return float2(f16tof32(vec), f16tof32(vec >> 16)); }
float4 FLG_unpack(uint2 vec){ return float4(FLG_unpack(vec.x), FLG_unpack(vec.y)); }
void FLG_pack(out float2 value, float2 vec){ value = vec; }
void FLG_pack(out float4 value, float4 vec){ value = vec; }
float2 FLG_unpack(float2 vec){ return vec; }
float4 FLG_unpack(float4 vec){ return vec; }

#define CUSTOM_TEX_GROUPS 4
#define CUSTOM_TEX_ITEMS 8

struct CustomTexVSPiece {
  uint2 start_size;
  float thickness;
  float pad;
};

struct CustomTexCSPiece {
  float2 sizeMult;
  float chance;
  float pad;
};

struct CustomTexCSGroup {
  CustomTexCSPiece pieces[CUSTOM_TEX_ITEMS];
};

#if defined(TARGET_VS) || defined(TARGET_PS)
  cbuffer cbVPSBuffer : register(b11) {
    float2 gDeformationPointA;
    float2 gDeformationPointB;
    float2 gTexGrid;
    float2 gTexGridInv;
    CustomTexVSPiece gTexPieces[CUSTOM_TEX_GROUPS * CUSTOM_TEX_ITEMS];
    // CustomTexGroup gTexGroups[4];
  };

  struct PS_IN {
    float4 PosH : SV_POSITION;
    
    #if defined(MODE_GBUFFER) || defined(CREATE_SHADOW_BUFFER)
      float2 Tex : TEXCOORD2;
    #else
      centroid float2 Tex : TEXCOORD2;
    #endif

    #ifndef CREATE_SHADOW_BUFFER
      float3 PosC : TEXCOORD1;

      #ifndef MODE_GBUFFER
        centroid float3 Ambient : TEXCOORD8;
        centroid float3 Color : TEXCOORD9;
        centroid float SpecIntensity : TEXCOORD10;
        centroid float3 Backlit : TEXCOORD5;
        centroid float Fade : TEXCOORD0;
        centroid float3 Lighting : TEXCOORD4;
        centroid float Fog : TEXCOORD6;
        centroid float3 LocalSpecNormal : TEXCOORD11;
        centroid float LocalSpecShadow : TEXCOORD12;
        centroid float PosY : TEXCOORD13;
        centroid float Occlusion : TEXCOORD14;
      #else
        float3 NormalW : TEXCOORD0;
        float Fog : TEXCOORD6;
      #endif

      MOTION_BUFFER
    #endif
  };

  struct PS_IN_shadow {
    float4 PosH : SV_POSITION;
  };
#endif
