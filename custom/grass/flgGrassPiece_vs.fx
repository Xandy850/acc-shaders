#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;

  float2 ver = BILLBOARD[id];
  float4 posW = mul(float4(-ver.x * 1, -1, ver.y * 1, 1), gTransform);
  float4 posH = mul(posW, gViewProj);
  
  vout.PosH = posH;
  vout.Tex = ver;

  float distance = posW.y - getY(posW.xz);
  vout.Opacity = (1 - saturate(distance * 2 - 0.1)) * gOpacity;
  return vout;
}
