#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

// #define SHADOWS_FILTER_SIZE 1

#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

// #define LIGHTING_SAFE
#ifndef MODE_MAIN_NOFX
	#define AO_EXTRA_LIGHTING 1
	#define GI_LIGHTING 0
	// #define LIGHTINGFX_NOSPECULAR
	#define LIGHTINGFX_SPECULAR_EXP 24
	#define LIGHTINGFX_SPECULAR_COLOR bitLightingFade * 0.5
	#define LIGHTINGFX_KSDIFFUSE 1
	// #define LIGHTINGFX_SIMPLEST
	#define LIGHTINGFX_SIMPLEST_NORMAL
	#define LIGHTINGFX_SHADOW_GRASS_OFFSET
	// #define NO_EXTSPECULAR
	#ifndef SHADER_MIRROR
		#define LIGHTINGFX_FIND_MAIN
		// #define LIGHTINGFX_TWO_NORMALS
	#endif
	#define POS_CAMERA posCPerVertexLighting
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

#include "include_new/base/common_ps.fx"
#include "include_new/ext_functions/wind.fx"
#include "include/ssgi_vs.hlsl"
#include "flgGrass.hlsl"
#include "flgGrass_vs.hlsl"

// float calculateSpecular(float3 normalW, float3 toCameraW, float specularEXP){
//   float specularBase = saturate(dot(normalize(toCameraW - ksLightDirection.xyz), normalW));
//   return pow(specularBase, specularEXP) * extSpecularColor.g / max(ksLightColor.g, 0.01);
// }

float calculateSpecular(float3 normalW, float3 toCamera, float specularEXP){
  float specularBase = saturate(dot(normalize(toCamera - ksLightDirection.xyz), normalW));
  return pow(specularBase, specularEXP) * saturate(dot(normalW, -ksLightDirection.xyz) * 4);
}

float getDeformationAO(float2 posXZ){
  float2 deformationUV = (posXZ - gDeformationPointB) / (gDeformationPointA - gDeformationPointB);
  float2 deformationEdges = saturate((0.5 - abs(deformationUV - 0.5)) * 10);
  float deformationEdge = deformationEdges.x * deformationEdges.y;
  int R = 2;
  float2 a = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, -R));
  float2 b = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, R));
  float2 c = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, -R));
  float2 d = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, R));
  float2 av = saturate((a + b + c + d) / 4 * 1.6 - 0.6);
	// return gDeformationPointB.x == 0 ? 100 : 0;
  return av.y * deformationEdge;
}

static const float2 BILLBOARD[] = {
	float2(-1, 0),
	float2(1, 0),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, 0),
	float2(1, 1),
};

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	PS_IN vout;

	// #ifdef SIMPLE_SHAPE
	// 	uint vertexID = fakeIndex % 3;
	// 	uint instanceID = fakeIndex / 3;
	// #else
	// 	uint vertexID = fakeIndex % 15;
	// 	uint instanceID = fakeIndex / 15;
	// #endif

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	FoliageFinVertex piece = particleBuffer[instanceID];

	// UNPACK
	float2 type_repeatk = FLG_unpack(piece.type_flip);
	float4 color_fade = FLG_unpack(piece.color_fade);
	float4 rotation_width_height = FLG_unpack(piece.rotation_width_height);
	float4 normal_ao = FLG_unpack(piece.normal_ao);
	float4 up_singleness = FLG_unpack(piece.up_singleness);
	float4 cut_passid_dif_spec = FLG_unpack(piece.shadowmult_passid_dif_spec);

	// PIECE_VERTICAL STUFF
	float3 up = up_singleness.xyz;
	// up = float3(0, -1, 0);
	float2 sideXZ = rotation_width_height.xy;
	float3 side = normalize(cross(float3(sideXZ.x, 0, sideXZ.y), normal_ao.xyz));
	float3 bent = normalize(cross(side, normal_ao.xyz));

	float2 P = BILLBOARD[vertexID];
	// float squashedMult = saturate(1 - type_repeatk.x * type_repeatk.x);
	// squashedMult = 0;

	// float bentHeight = rotation_width_height.w * sqrt(squashedMult);
	// float cutK = saturate(bentHeight * cut_passid_dif_spec.x - 2);
	// if (P.y == 1 && cutK > 0) {
	// 	P.y = lerp(P.y, 0.67, cutK);
	// 	P.x = lerp(P.x, 0.8, cutK);
	// }

	float affectedByWind = P.y * normal_ao.w * 0.5;
	float windPhaseOffset = type_repeatk.x / 2;

	// float affectedByWind = P.y * (0.5 + abs(type_repeatk.x)) * squashedMult;
	// float bentK = getBentK(P.y, type_flip.y, type_repeatk.x);
	rotation_width_height.z *= type_repeatk.y;

	float3 origPosition = piece.position;
	piece.position -= rotation_width_height.w * up * P.y;
	piece.position += side * rotation_width_height.z * P.x;
	// piece.position += bent * bentK * rotation_width_height.w;

	float3 posC = origPosition - ksCameraPosition.xyz;
	float distance = length(posC);
	float3 toCamera = posC / distance;
	// piece.position.y += P.x * saturate(abs(dot(side, toCamera)) * 4 - 3) * rotation_width_height.z / 2 * sign(toCamera.y);

	#ifdef CREATE_SHADOW_BUFFER
		[branch]
		if (distance > 30){
			vout = (PS_IN)0;
			vout.PosH = -1;
			return vout;
		}
	#endif
	
	#ifdef CREATE_SHADOW_BUFFER
		piece.position.y -= rotation_width_height.z * saturate(distance / 15 - 1);
	#endif
	float4 posW = float4(piece.position, 1);

	float2 baseTex = float2(0.5 + 0.5 * P.x, 1 - P.y);
	int pieceID = int(round(cut_passid_dif_spec.y));
	if (pieceID < 0){
		vout.Tex = float2((baseTex.x * type_repeatk.y + type_repeatk.x) * gTexGridInv.x, baseTex.y);
		vout.Tex.y = lerp(vout.Tex.y, 1, cut_passid_dif_spec.x * frac(type_repeatk.x  * gTexGridInv.x + vout.Tex.x * 0.3)) * gTexGridInv.y;
	} else {
		CustomTexVSPiece texPiece = gTexPieces[pieceID];
		float4 start_size = FLG_unpack(texPiece.start_size);
		vout.Tex = start_size.xy + start_size.zw * baseTex;
		// float stableK = 1 / max(1, texPiece.sizeMult.x * 0.25 + pow(texPiece.sizeMult.y, 2));
		cut_passid_dif_spec.w *= texPiece.thickness;
		affectedByWind *= texPiece.thickness;
	}
	
	#ifndef CLOSE_MODE
		float4 origPosW = posW;
		applyWind(posW.xyz, up, saturate(rotation_width_height.w * -up.y - 0.05) * affectedByWind, false, windPhaseOffset);
	#endif

	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);


	// if (1){
	// 	vout = (PS_IN)0;
	// 	vout.PosH = -1;
	// 	return vout;
	// }

	#ifndef CREATE_SHADOW_BUFFER
		vout.PosC = posC;
		vout.Fog = calculateFog(posV);

		#ifndef MODE_GBUFFER
			vout.Color = color_fade.xyz;

			float4 posWPerVertexLighting = float4((posW.xyz + origPosition) / 2 + float3(0, 0.08, 0), 1);
			float4 posWShadow = posWPerVertexLighting;
			#ifndef CLOSE_MODE
				posWShadow.xyz += ksLightDirection.xyz * -0.05;
			#endif
			// posWShadow.xyz -= ksLightDirection.xyz * lerp(2.5, 0.25, saturate(0.1 - ksLightDirection.y));
			// posWShadow.xyz -= ksLightDirection.xyz * 0.1;
			float4 tex0 = mul(posWShadow, ksShadowMatrix0);
			float4 tex1 = mul(posWShadow, ksShadowMatrix1);
			float4 tex2 = mul(posWShadow, ksShadowMatrix2);
			float vout_Shadow = getShadowBiasMult(posC, float3(0, 1, 0), tex0, tex1, tex2, 0, 1);
			// vout_Shadow = P.y;
			// vout_Shadow = min(vout_Shadow, cut_passid_dif_spec.x);

			// float selfShadowK = saturate(type_repeatk.y);
			// float shadowAdj = affectedByWind * 0.5 + 0.5;
			// shadowAdj = lerp(shadowAdj, 1, saturate(distance / 50 + pow(abs(toCamera.y), 3)));
			// float aoValue = normal_ao.w * lerp(shadowAdj, 1, selfShadowK);
			// vout_Shadow *= lerp(shadowAdj, 1, selfShadowK);

			float aoValue = normal_ao.w;

			float deformationAO = getDeformationAO(posW.xz);
		  aoValue *= 1 - deformationAO;
			vout_Shadow = min(vout_Shadow, 1 - deformationAO * P.y);

			float3 bitNormal = bent;
			if (dot(bitNormal, toCamera) > 0) bitNormal = -bitNormal;

			float bitLightingFade = saturate(color_fade.w);// * (1 - 0.8 * saturate(cut_passid_dif_spec.y / 4));
			float advFade = lerp(pow(saturate(1 - distance / 30), 1.4), 0, up_singleness.w);
			float advFadeFurther = lerp(saturate(1.2 - distance / 60), 0, up_singleness.w);
			float bitShadow1 = bitLightingFade * vout_Shadow * saturate(0.5 + P.y * 3);
			float bitShadow2 = bitLightingFade * vout_Shadow * saturate(P.y * 3);

			float3 backlitColor = max(lerp(dot(vout.Color, 0.25), vout.Color, 3), 0) * 0.5;
			float directionMult = abs(dot(ksLightDirection.xyz, bent));
			vout.SpecIntensity = cut_passid_dif_spec.w * bitShadow1 * directionMult;
			// vout.SpecIntensity = cut_passid_dif_spec.w;
			// vout.SpecIntensity += calculateSpecular(bitNormal, -toCamera, 20) * bitShadow2 * 0.08;
			vout.Backlit = extSpecularColor.rgb * pow(saturate(dot(-toCamera, ksLightDirection.xyz)), 12) * bitShadow2 * backlitColor * pow(directionMult, 4);
			// vout.SpecIntensity = 0;
			// vout.Backlit = 0;
			// vout_Shadow *= cut_passid_dif_spec.z;

			// float bitLdotH = saturate(dot(ksLightDirection.xyz, -bitNormal));
			float planeLdotH = saturate(dot(ksLightDirection.xyz, -normal_ao.xyz));
			vout.Lighting = vout_Shadow * ksLightColor.rgb * planeLdotH;
			vout.Fade = color_fade.w * advFadeFurther;
			vout.LocalSpecNormal = bitNormal + normal_ao.xyz * (P.y - 0.5) * 4;
			vout.LocalSpecShadow = bitShadow2 * frac(abs(rotation_width_height.x) * 9217);
			vout.PosY = P.y;
			// aoValue *= lerp(1, saturate(P.y * 2), bitLightingFade * 0.5);

			vout.Ambient = getAmbientBaseAt(normal_ao.xyz, aoValue);
			vout.Color = lerp(dot(vout.Color, 0.25), vout.Color, 1 + P.y * advFade);
			vout.Occlusion = aoValue;
			// vout.Color = lerp(vout.Color, float3(0.04, 0.08, 0.02), P.y * advFade);

			#ifndef MODE_MAIN_NOFX
				float4 txDiffuseValue = 1;
				float3 normalW = normal_ao.xyz;
				// float3 normalW0 = normal_ao.xyz;
				// float3 normalW1 = bitNormal;
				float normalMix = bitLightingFade * 0.3;
				float3 posCPerVertexLighting = posWPerVertexLighting.xyz - ksCameraPosition.xyz;
				LFX_MainLight mainLight;
				float3 lightingFX = 0;
				LIGHTINGFX(lightingFX);
				#ifndef SHADER_MIRROR
					float lightingFocus = saturate(mainLight.power / max(dot(vout.Lighting, 1), 1));
					vout.Backlit += 4 * lightingFocus * backlitColor * pow(directionMult, 4) * vout.Lighting 
						* saturate(dot(toCamera, mainLight.dir)) * P.y * advFade;
				#endif
				vout.Lighting += lightingFX;// * lerp(1, P.y, advFade);
				// vout.Lighting += getSSGI(vout.PosH);
				vout.Lighting *= cut_passid_dif_spec.z;// * lerp(1, P.y, advFade * 0.5);
			#endif
		#else
			if (color_fade.w < 0.99 || distance > 30){
				vout = (PS_IN)0;
				vout.PosH = -1;
				return vout;
			}

			vout.NormalW = normal_ao.xyz;
			posW = origPosW;
			applyWind(posW.xyz, up, saturate(rotation_width_height.w * -up.y - 0.05) * affectedByWind, true, windPhaseOffset);
			GENERIC_PIECE_WORLD(posW);
		#endif
	#endif

	return vout;
}