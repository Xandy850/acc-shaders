#include "flgGrassPiece.hlsl"

Texture2D<float2> txDiffuse : register(t0);

float2 main(VS_Move pin) : SV_TARGET {
  float2 v = txDiffuse.Sample(samLinearSimple, pin.Tex);
  // v.y = 0;
  // if (v.x < 0.96) {
  //   v.x = saturate(v.x - pin.ReduceBy);
  // }

  // float drag = 1.0 / (1.0 + gFrameTime * 12);
  // v = lerp(0, v, drag);
  if (gFrameTime) v -= sign(v) * min(max(gFrameTime * 1.2, 0.01), abs(v));
  return v;
}