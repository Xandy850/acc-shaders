#include "include/common.hlsl"

struct CustomArea {
  float4 color;

  float2 center;
  IN_RANGE(float, radius);

  float aspectRatio;
  float rotSin;
  float rotCos;
  float opacity;
};

#define MAX_AREAS 8

cbuffer cbPiece : register(b11) {
  float4x4 gWorld;
  float4x4 gViewProj;
  float4 gColor;
  float2 gTexOffset;
  float2 gTexScale;
  uint gUseTexture;
  uint gAreasCount;
  float2 pad;
  float4 gMaskBase;
  float4 gMaskR;
  float4 gMaskG;
  float4 gMaskB;
  float4 gMaskA;
  CustomArea gAreas[MAX_AREAS];
};

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN_Solid {
  float4 PosH : SV_POSITION;
  float2 PosW : POSITION;
  float2 Tex : TEXCOORD0;
};

float4 applyTexture(float4 tex){
  return saturate(gMaskBase + tex.r * gMaskR + tex.g * gMaskG + tex.b * gMaskB + tex.a * gMaskA);
}

float4 applyExtras(float4 ret, float2 posXZ){  
  for (uint i = 0U; i < gAreasCount; i++){
    CustomArea A = gAreas[i];
    float l = length(mul(float2x2(A.rotCos, -A.rotSin, A.rotSin, A.rotCos), posXZ - A.center) * float2(1, A.aspectRatio));
    float c = smoothstep(0, 1, IN_RANGE_SAT(A.radius, l));
    ret = saturate(lerp(ret, A.color, c * A.opacity));
  }
  return saturate(ret);
}
