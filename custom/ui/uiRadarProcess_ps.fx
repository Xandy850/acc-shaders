#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float4 gExtras;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"
float4 samplePoisson(float2 uv, float level, float radius){
  float4 ret = 0;
  #define PUDDLES_DISK_SIZE 32
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    ret += txDiffuse.SampleLevel(samLinearSimple, uv + offset, level);
  }
  ret /= PUDDLES_DISK_SIZE;
  return ret;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float s = -gExtras.x * 3.1415 * 2;
  // float2 ra = float2(sin(s), cos(s));
  float2 no = normalize(pin.Tex * 2 - 1);
  float an = atan2(no.x, no.y) / 3.1415 * 0.5 + 0.5;
  float dis = saturate(length(pin.Tex * 2 - 1));
  float hit = frac(1 + an - gExtras.x);
  if (hit > 0.95) hit = 1 - (hit * 20 - 19);
  else hit /= 0.95;
  // return float4(hit.xxx, 1);
  // float hit = length(ra - no) < 0.1 ? 1 : 0;

  // float4 base = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, lerp(1.5, 2.5, dis));
  float4 base = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 1.5);
  base.rgb /= max(0.1, base.a);
  base.a = saturate(remap(base.a, 0.25, 0.75, 0, 1));

  // float4 glow = samplePoisson(pin.Tex, 5.5, 0.05);
  float4 glow = samplePoisson(pin.Tex, 4.5, 0.05);
  glow.rgb /= max(0.001, glow.a);
  // glow.a = pow(glow.a, 2);

  float blurred = samplePoisson(pin.Tex, 1, 0.005).a;
  float contour = saturate((txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 1).a - blurred) * 6);
  // return saturate(contour * 20);
  // return glow;

  // float hit

  if (gExtras.y){
    base.a *= lerp(lerp(0.2, 1, max(saturate((1 - dis) * 1.5), hit)), 1, contour);
  }

  float4 ret = base;// + glow * hit * saturate(dis * 3 - 1) * (1 - base.a);
  // ret.a *= saturate(smoothstep(1, 0.8, dis));
  ret.a *= saturate(remap(dis, 0.8, 1, 1, 0));
  // ret.rgb /= ret.a;
  // ret.a *= ret.a;
  return ret;
}