#define USE_PS_FOG
#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixelPosL pin) : SV_TARGET {
  float2 uv = (1 - pin.PosL.xy) * float2(1.4, 4) + float2(0.4, 0.1);
  if (floor(uv.x) != 1) return 0;
  if (floor(uv.y) != -1) return 0;

  float4 tx = txDiffuse.Sample(samLinear, uv);
  // tx.a = 1;
  // tx.xy = frac(uv);
  tx.rgb *= 3;
  return tx;
}
