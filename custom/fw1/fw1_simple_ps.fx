#include "include/samplers.hlsl"
Texture2D<float> tex0 : register(t10);

struct PSIn {
	float4 Position : SV_Position;
	float4 GlyphColor : COLOR;
	float2 TexCoord : TEXCOORD;
};

float4 main(PSIn Input) : SV_Target {
	float a = tex0.Sample(samLinear, Input.TexCoord);	
	if(a == 0.0f)
		discard;	
	return (a * Input.GlyphColor.a) * float4(Input.GlyphColor.rgb, 1.0f);
}