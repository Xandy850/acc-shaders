#include "include/common.hlsl"

Texture2D<float> txDepth : register(t17);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float ret = 1;
  [unroll] for (int x = -1; x <= 2; x++){
    [unroll] for (int y = -1; y <= 2; y++){
      ret = min(ret, txDepth.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y)));
    }
  }
  return ret;
  // return txDepth.SampleLevel(samLinearClamp, pin.Tex, 0);
  // return frac(pin.Tex.x * 10) > 0.5;
}