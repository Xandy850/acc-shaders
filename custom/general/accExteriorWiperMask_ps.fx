#include "include/common.hlsl"

Texture2D<float> txNow : register(t0);
Texture2D<float> txPrevious : register(t1);
Texture2D<float> txWiperMask : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float sampleNow(float2 uv){
  float ret = 1;
  float cou = 0;
  [unroll] for (int y = -2; y <= 2; y++)
  [unroll] for (int x = -2; x <= 2; x++){
    if (abs(x) + abs(y) == 4) continue;
    float val = txNow.SampleLevel(samLinearBorder1, uv, 1, int2(x, y));
    ret = min(ret, val > 0.8);
  }
  return ret;
}

float samplePrev(float2 uv){
  float bas = txPrevious.SampleLevel(samLinearSimple, uv, 0);
  return bas;

  float ret = 1;
  float cou = 0;
  [unroll] for (int y = -2; y <= 2; y++)
  [unroll] for (int x = -2; x <= 2; x++){
    if (abs(x) + abs(y) == 4) continue;
    float val = txPrevious.SampleLevel(samLinearSimple, uv, 0, int2(x, y));
    ret = min(ret, val);
    if (val < 0.9) cou++;
  }
  return cou > 10 ? ret : bas;
}

float main(VS_Copy pin) : SV_TARGET {
  return sampleNow(pin.Tex)
   * saturate(samplePrev(pin.Tex) + 0.01)
   + txWiperMask.SampleLevel(samLinearSimple, pin.Tex, 0);
}