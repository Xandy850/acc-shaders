Texture2DMS<float> inDepth : register(t0); 
Texture2DMS<float4> inNormals : register(t1); 
// Texture2DMS<float4> inReflections : register(t2); 
RWTexture2D<float> outDepth : register(u0);
RWTexture2D<float4> outNormals : register(u1);
// RWTexture2D<float4> outReflections : register(u2);
 
[numthreads(16, 16, 1)]
void main(uint3 dispatchThreadId : SV_DispatchThreadID)
{ 
  uint2 dim;
  uint sampleCount;
 
  inDepth.GetDimensions(dim.x, dim.y, sampleCount); 

  if (dispatchThreadId.x > dim.x || dispatchThreadId.y > dim.y){
    return;
  }
 
  float retDepth = 1;
  float4 retNormals = 0;
  // float4 retReflections = 0;
  for (uint i = 0; i < sampleCount; ++i){
    float loaded = inDepth.Load(dispatchThreadId.xy, i).r;
    [flatten]
    if (loaded < retDepth){
      retDepth = loaded;
      retNormals = inNormals.Load(dispatchThreadId.xy, i);
    }
    // retReflections += inReflections.Load(dispatchThreadId.xy, i);
  }

  outDepth[dispatchThreadId.xy] = retDepth; 
  outNormals[dispatchThreadId.xy] = retNormals; 
  // outReflections[dispatchThreadId.xy] = retReflections / float(sampleCount); 
} 