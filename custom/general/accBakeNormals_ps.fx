struct PS_IN_Solid {
  float4 PosH : SV_POSITION;
  float3 NormalL : COLOR;
};

float4 main(PS_IN_Solid pin) : SV_TARGET {
  return float4(normalize(pin.NormalL) * 0.5 + 0.5, 1);
}