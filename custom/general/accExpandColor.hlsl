#include "include/common.hlsl"

Texture2D txColor : register(TEX_SLOT);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 result = txColor.SampleLevel(samPointClamp, pin.Tex, 0);
  [unroll]
  for (int x = -1 - RADIUS; x <= RADIUS; x++){
    [flatten]
    if (x == 0) continue;
    float4 v = txColor.SampleLevel(samPointClamp, pin.Tex, 0, OFFSET(x));
    if (result.a < 0.1){
      result = v;
    }
    // result = max(result, v);
  }
  return result;
}