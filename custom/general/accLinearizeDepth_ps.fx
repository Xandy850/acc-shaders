cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

cbuffer cbData : register(b10){
  float4x4 viewProj;
  float4x4 viewProjInv;
  float3 eyePos;
  float _pad0;
  float4 gValues;
  float2 gNearFar;
  float gNearPlane;
  float gFarPlane;
}

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D<float> txDepth : register(t2);

#define g_xFrame_MainCamera_ZFarP gFarPlane
#define g_xFrame_MainCamera_ZNearP gNearPlane

inline float getLinearDepth(float c)
{
	float z_b = c;
    float z_n = 2.0 * z_b - 1.0;
	//float lin = 2.0 * g_xFrame_MainCamera_ZNearP * g_xFrame_MainCamera_ZFarP / (g_xFrame_MainCamera_ZFarP + g_xFrame_MainCamera_ZNearP - z_n * (g_xFrame_MainCamera_ZFarP - g_xFrame_MainCamera_ZNearP));
	float lin = 2.0 * g_xFrame_MainCamera_ZFarP * g_xFrame_MainCamera_ZNearP / (g_xFrame_MainCamera_ZNearP + g_xFrame_MainCamera_ZFarP - z_n * (g_xFrame_MainCamera_ZFarP - g_xFrame_MainCamera_ZNearP));
	return lin;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float recip = 1.0f / max(0.0001f, g_xFrame_MainCamera_ZFarP);
  return getLinearDepth(txDepth.Sample(samLinearSimple, pin.Tex)); // * recip;
}