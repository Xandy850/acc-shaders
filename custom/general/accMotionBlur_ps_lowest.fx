#include "accSS.hlsl"

float2 sampleMotion(float2 uv){
  return txMotion.SampleLevel(samLinearClamp, uv, 0);
}

#ifndef BLUR_STEPS
  #define BLUR_STEPS 2
#endif

float4 main(VS_Copy pin) : SV_TARGET {
  float4 base = txColor.SampleLevel(samPoint, pin.Tex, 0);
  float3 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz;

  float2 baseMotion = sampleMotion(pin.Tex);
  float baseMotionValue = length(baseMotion);
  float blurLevel = saturate(length(baseMotion * 30)) * 2;

  [branch]
  if (blurLevel <= 0.05) return base;

  float4 result = float4(base.xyz, 1) * 0.1;
  float2 offsetMult = baseMotion * 0.4 * motionBlurMult / BLUR_STEPS;
  float2 uvBase = pin.Tex + offsetMult * (random.x - 0.5) * gMotionBlurNoiseMult;
  [unroll] 
  for (int i = -BLUR_STEPS; i <= BLUR_STEPS; i++){
    if (i == 0) continue;
    float2 uvOffset = offsetMult * (float)i;
    float motionDifference = abs(length(sampleMotion(uvBase + uvOffset * 1.5)) - baseMotionValue);
    float sampleWeight = saturate(blurLevel - motionDifference * 400);
    result += float4(txColor.SampleLevel(samLinearClamp, uvBase + uvOffset, 0).xyz, 1) * sampleWeight;
  }
  result /= result.w;
  return result;
}