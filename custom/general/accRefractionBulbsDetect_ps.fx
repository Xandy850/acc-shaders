#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"

Texture2D txInnerNormals : register(t0);
Texture2D txInnerEmissive : register(t1);
Texture2D txSurfaceShot : register(t2);
Texture2D txInnerColor : register(t3);
Texture2D<float> txDepth : register(t4);
Texture2D txBulbs : register(t5);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float hasBulbsMap;
  float useCustomBulbs;
  float useColoredBulbs;
  float lightLeakingK; // 0.001
  float4 customBulbs[4];
}

#define BULB(I) (dot2((pin.Tex - customBulbs[I].xy) * float2(1, customBulbs[I].w)) < customBulbs[I].z ? 1 : 0)

float4 main(VS_Copy pin) : SV_TARGET {
  float4 valInnerNormals = txInnerNormals.SampleLevel(samLinear, pin.Tex, 0);
  float4 valInnerEmissive = txInnerEmissive.SampleLevel(samLinear, pin.Tex, 0);
  float4 valInnerColor = txInnerColor.SampleLevel(samLinear, pin.Tex, 0);
  float valDepth = txDepth.SampleLevel(samLinear, pin.Tex, 0);

  int O = 5;
  bool closeToEdge = txInnerNormals.SampleLevel(samLinear, pin.Tex, 0, int2(O, O)).a == 0
    || txInnerNormals.SampleLevel(samLinear, pin.Tex, 0, int2(O, -O)).a == 0
    || txInnerNormals.SampleLevel(samLinear, pin.Tex, 0, int2(-O, O)).a == 0
    || txInnerNormals.SampleLevel(samLinear, pin.Tex, 0, int2(-O, -O)).a == 0;

  float d = abs(ddx(valInnerNormals.x)) + abs(ddy(valInnerNormals.x))
    + abs(ddx(valInnerNormals.y)) + abs(ddy(valInnerNormals.y)) 
    + abs(ddx(valInnerNormals.z)) + abs(ddy(valInnerNormals.z));

  float bulbs = closeToEdge ? 0 : (txSurfaceShot.SampleLevel(samLinear, pin.Tex, 4).a > 0.95 ? 1 : 0) * saturate(pow(d * 10, 8));
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    if (abs(x) + abs(y) != 2) continue;
    if (txDepth.SampleLevel(samLinear, pin.Tex, 0, int2(x, y) * 5) < valDepth - 0.03){
      bulbs = 0;
    }
  }

  if (hasBulbsMap) {
    bulbs = txBulbs.SampleLevel(samLinear, pin.Tex, 0).a;
  }
    
  [branch]
  if (useCustomBulbs && useColoredBulbs){
    float4 ret = float4(BULB(0), BULB(1), BULB(2), BULB(3));

    // pin.Tex.x -= valInnerNormals.x * 0.3;
    // pin.Tex.y -= valInnerNormals.y * 0.3;

    float vals[4];
    float total = 0.00001;
    for (int i = 0; i < 4; i++){
      float2 pos = pin.Tex;
      float2 dif = customBulbs[i].xy - pos;
      float2 step = dif / 10;
      float targetDepth = txDepth.SampleLevel(samLinear, customBulbs[i].xy, 0);
      float depthMult = 0.2 + 0.8 * saturate((targetDepth - valDepth) * 20);
      depthMult = 1;
      for (int j = 1; j < 10; j++){
        pos += step;
        float stepDepth = txDepth.SampleLevel(samLinear, pos, 0);
        depthMult *= saturate((stepDepth - min(valDepth - lightLeakingK, targetDepth)) * 100 + 1);
        if (stepDepth > 0.99) depthMult = 0;
      }

      float w = customBulbs[i].z / max(customBulbs[i].w, 0.0001) / pow(dot2(dif), 2);
      vals[i] = depthMult * w;
      total += w;
    }

    // float d0 = dot2(pin.Tex - customBulbs[0].xy);
    // float d1 = dot2(pin.Tex - customBulbs[1].xy);
    // float d2 = dot2(pin.Tex - customBulbs[2].xy);
    // float d3 = dot2(pin.Tex - customBulbs[3].xy);
    // float dm = min(min(d0, d1), min(d2, d3));
    // if (d0 == dm) ret.x += 0.1;
    // if (d1 == dm) ret.y += 0.1;
    // if (d2 == dm) ret.z += 0.1;
    // if (d3 == dm) ret.w += 0.1;

    ret += float4(vals[0], vals[1], vals[2], vals[3]) / total / 10;
    return ret;
  }

  if (useCustomBulbs) {
    bulbs = dot(float4(BULB(0), BULB(1), BULB(2), BULB(3)), 1) ? 1 : 0;
  }
  
  if (useColoredBulbs){
    return valInnerEmissive * lerp(0.1, 1, bulbs);
  }
  
  return float4(max(valInnerEmissive.xyz - valInnerColor.xyz, 0), bulbs);
}