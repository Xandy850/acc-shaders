#define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"

#define RADIUS 1
#define VERT_MULTIPLIER 10
#define SAMPLE_THRESHOLD 10.2

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);

  float3 normal = ssGetNormal(pin.Tex);
  // return depth;
  // return normal.y;

  float3 random = normalize(txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz);
  float occlusion = 0.0;
  float radius = min(RADIUS, length(origin) / 8);
  radius = RADIUS;

  float3 giValue = 0;

  for (int i = 0; i < 24; i += 6) {
    float3 rn = reflect(gSamplesKernel[i].xyz, random);
    float3 dirL = rn * sign(dot(rn, normal)) * 3;

    float2 newUv = ssGetUv(origin + dirL * radius).xy;
    float samDepth = ssGetDepth(newUv, true);
    float3 samW = ssGetPos(newUv, samDepth);

    // float3 random3 = reflect(gSamplesKernel[i].xyz, random);
    // float2 uvOffset = normalize(random3.xy) * 0.4 * random3.z * float2(1, 16.0/9);
    // float2 newUv = pin.Tex + uvOffset;
    // float samDepth = ssGetDepth(newUv, true);
    // float3 samW = ssGetPos(newUv, samDepth);
    float3 worldOffset = samW - origin;

    float3 dir = normalize(worldOffset);
    float3 samN = ssGetNormal(newUv, true);
    float3 samColor = txColor.SampleLevel(samLinearBorder0, newUv, 4).rgb;
    float quality = 1
      * saturate(-dot(samN, dir) * 10 + 2)
      * saturate(-dot(samN, normal) * 10 + 2)
      // * saturate(dot(dir, normal) * 10 - 2)
      * saturate(1 - dot(worldOffset, worldOffset) / (3 * 3));
    giValue += samColor * quality * dot(samColor, float3(0.4, 0.4, 0.2));

    if (samDepth < depth){
      occlusion++;
    }
  }

  // return float4(normal, 1);

  float4 baseColor = 0;
  baseColor.rgb += (baseColor.rgb + 0.1) * saturate(giValue / 5) * 5;
  return baseColor;
  // return (1.0 - saturate(pow(occlusion / SAMPLE_THRESHOLD, 1.8))) * txColor.Sample(samPointClamp, pin.Tex);
}