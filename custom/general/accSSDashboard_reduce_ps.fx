#define MIPMAPPED_GBUFFER 1
#include "accSS.hlsl"
#include "accSSDashboard.hlsl"
#include "include/poisson.hlsl"

// float4 main(VS_Copy pin) : SV_TARGET {
//   float3 samColor = txColor.SampleLevel(samLinearBorder0, pin.Tex, 1).rgb;
//   if (luminance(samColor) < 1){
//     samColor = 0; 
//   }
//   return float4(samColor / 3, 1);
// }

struct PS_Out {
  float4 color : SV_TARGET0;
  float4 pos : SV_TARGET1;
  float4 normal : SV_TARGET2;
};

float3 lumaBasedReinhardToneMapping(float3 color) {
	float luma = max(1, dot(color, 0.33));
	float toneMappedLuma = luma / (1 + luma);
	color *= toneMappedLuma / luma;
	return color;
}

#define POISSON_SIZE 16
#define R 2
PS_Out main(VS_Copy pin) {
  float3 totalColor = 0;
  float3 totalPos = 0;
  float3 totalNormal = 0;
  float totalWeight = 0;

  // if (length(posW) > 1.3) totalColor = 0;

  float2 random = normalize(txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xy);
  for (int i = 0; i < POISSON_SIZE; i++){
    float2 offset = SAMPLE_POISSON(POISSON_SIZE, i);
    // offset = reflect(offset, random);

    float2 uv = pin.Tex + offset * gSize.zw * 16;
    float3 value = txColor.SampleLevel(samLinearClamp, uv, 2).rgb;
    float weight = saturate(dashLuminance(value) - 1);

    float samDepth = ssGetDepth(uv, true);
    float3 posW = ssGetPos(uv, samDepth);
    float3 normalW = ssGetNormal(uv);

    // totalPos += posW * weight;
    totalColor += value * weight;
    totalPos += posW * weight;
    totalNormal += normalW * weight;
    totalWeight += weight;
    // totalNormal = 1;
  }

  // [unroll]
  // for (int x = -R; x <= R; x++)
  // [unroll]
  // for (int y = -R; y <= R; y++){
  //   if (R > 0 && abs(x) + abs(y) == R + R) continue;
  //   float3 value = txColor.SampleLevel(samLinearClamp, pin.Tex, 1.5, int2(x, y) * 2).rgb;
  //   float weight = saturate(dashLuminance(value) - 0.5);
  //   totalColor += value * weight;
  //   totalWeight += weight;
  // }

  totalPos /= max(totalWeight, 0.01);
  if (dot2(totalPos) > 1.3 * 1.3 || totalWeight < 0.01){
    return (PS_Out)0;
  }

  PS_Out ret;
  ret.color = float4(lumaBasedReinhardToneMapping(totalColor / max(totalWeight, 1)), 1);
  // ret.color.xyz = totalNormal;
  // ret.color.xyz = frac(txDepth.SampleLevel(samLinearSimple, pin.Tex, 0) * 10);
  // ret.color = txColor.SampleLevel(samLinearClamp, pin.Tex, 0);
  // ret.pos = float4(totalPos / max(totalWeight, 0.1), 1);
  ret.pos = float4(totalPos, 1);
  ret.normal = float4(totalNormal, 1);
  // ret.normal = 1;
  // ret.pos = float4(totalUV / max(totalWeight, 0.01), 1, 1);

  // ret.color = 1;
  // ret.pos = 0.9;

  return ret;
}