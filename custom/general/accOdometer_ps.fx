#include "include/samplers.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b8) {
  float scale;
  float offset;
  float2 pad0;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return lerp(float4(bgColor.rgb / bgColor.a, bgColor.a), 1, txDiffuse.Sample(samLinearSimple, pin.Tex).x);
  return float4(1, 1, 1, txDiffuse.Sample(samLinearBorder0, pin.Tex * float2(1, scale) + float2(0, offset)).x);
}