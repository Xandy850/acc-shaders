#include "include/samplers.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float sampleBlurred(float2 uv, float level, int R){
  float r = 0;
  float w = 0;
  [unroll] for (int x = -R; x <= R; x++)
  [unroll] for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    r += saturate(dot(txDiffuse.SampleLevel(samLinearBorder0, uv, level, int2(x, y)), 1));
    w += 1;
  }
  return r / w;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return float4(pin.Tex, 0, 1);
  float tex4 = sampleBlurred(pin.Tex, 2.2, 2);
  float tex0 = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0).a;
  float outline = pow(saturate(tex4 - tex0), 2.2);

  bool2 checkerFlag = frac(pin.PosH.xy / 64) > 0.5;
  float checkerPattern = all(checkerFlag) || all(!checkerFlag);
  checkerPattern = frac((pin.PosH.x + pin.PosH.y) / 8) > 0.5;

  float checker = checkerPattern * saturate(tex0 * 3);
  return float4(0, 20, 20, outline);
  return float4(0, 20, 20, saturate(checker * 0.03 + outline));
  // return 0;
}