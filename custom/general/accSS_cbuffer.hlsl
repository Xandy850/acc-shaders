#define SAMPLE_COUNT 24
cbuffer cbData : register(b10){
  float4x4 viewProj;
  float4x4 viewProjInv;
  float4x4 viewProjPrev;
  float4 gSamplesKernel[SAMPLE_COUNT];  
  float4 gSize; 

  float ssaoRadius;
  float ssaoOpacity;
  float ssaoExp;
  float motionBlurMult;

  uint sampleCount;
  float sampleCountInv;
  float fogBlurDistanceInv;
  float ksGameTimeS;

  float ss_ksFogBlend;
  float ss_ksFogLinear;
  float ss_extFogConstantPiece;
  float ss_extFogExp;

  float ssgiResolutionDiv;
  float gMapOriginY;
  float gMapResolutionInv;
  float gMapDepthSize;

  float2 gMapPointA;
  float2 gMapPointB;

  float2 gMapBlendOffset;
  float gMotionBlurNoiseMult;
  float gSSGIIntensity;

  float gTrackLightIntensity;
  float gTrackHeatFactor;
  float2 gWindOffset;
}