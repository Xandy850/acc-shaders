SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samColorGrading : register(s15) {
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
  AddressW = CLAMP;
};

Texture2D txDiffuse : register(t0);
Texture3D txColorGrading : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 value = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  return float4(txColorGrading.SampleLevel(samColorGrading, saturate(value.rbg), 0).rbg, value.a);
}