#define MIPMAPPED_GBUFFER 1
#include "accSS.hlsl"

float2 findMax(Texture2D<float2> tex, float2 uv, int2 dir){
  float2 res = 0;
  [unroll]
  for (int i = -8; i <= 7; i++){
    float2 v = tex.SampleLevel(samLinearBorder0, uv, 0, dir * i);
    [flatten]
    if (dot2(v) > dot2(res)) res = v;
  }
  return res;
}

float2 main(VS_Copy pin) : SV_TARGET {
  #ifdef BLURV
    return findMax(txMotionL, pin.Tex, int2(0, 1));
  #else
    return findMax(txMotionLR, pin.Tex, int2(1, 0));
  #endif
}