SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2DMS<float4> txDiffuse : register(t0); 

#include "accSS_cbuffer.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 resValue = 0;
  for (uint i = 0; i < sampleCount; ++i){
    resValue += txDiffuse.Load(pin.PosH.xy, i);
  }
  return resValue * sampleCountInv;
}