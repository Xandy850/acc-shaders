SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2DMS<float> txDepth : register(t0); 
Texture2DMS<float4> txNormals : register(t1); 

#include "accSS_cbuffer.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct PS_Out {
  float depth : SV_TARGET0;
  float4 normals : SV_TARGET1;
};

PS_Out main(VS_Copy pin) {
  float retDepth = 1;
  float4 retNormals = 0;
  for (uint i = 0; i < sampleCount; ++i){
    float loaded = txDepth.Load(pin.PosH.xy, i).r;
    [flatten]
    if (loaded < retDepth){
      retDepth = loaded;
      retNormals = txNormals.Load(pin.PosH.xy, i);
    }
  }

  PS_Out result;
  result.depth = retDepth; 
  result.normals = retNormals; 
  return result;
}