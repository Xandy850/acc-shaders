#include "include/common.hlsl"
#include "include/samplers.hlsl"

TextureCube txFirst : register(t0);
TextureCube<float> txSecond : register(t1);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4x4 transformFirst;
  float4x4 transformSecond;
  float3 ambientColor;
  float pad;
}

float sampleCubeSingle(float3 p){
  return pow(saturate(txSecond.SampleBias(samLinear, p, 2).r), 0.5);
}

float sampleCubeSmooth(float3 p){
  const static float3 gridSamplingDisk[8] = {
    { 1, 1, 1 },
    { 1, 1, -1 },
    { 1, -1, 1 },
    { 1, -1, -1 },
    { -1, 1, 1 },
    { -1, 1, -1 },
    { -1, -1, 1 },
    { -1, -1, -1 },
  };
  float r = 0;
  for (int i = 0; i < 8; i++) {
    r += sampleCubeSingle(p + gridSamplingDisk[i] / 256);
  }
  return r / 8;
}

// cbuffer cbLighting : register(b2) {  
//   float4 ksLightDirection;
//   float4 ksAmbientColor_sky;
// }

float4 main(VS_Copy pin) : SV_TARGET {
  float3 vec = float3(1 - pin.Tex * 2, -1);
  float3 vecFirst = mul(vec, (float3x3)transformFirst);
  float3 vecSecond = mul(vec, (float3x3)transformSecond);
  return lerp(float4(ambientColor, 1), txFirst.Sample(samLinear, vecFirst), sampleCubeSingle(vecSecond));
}