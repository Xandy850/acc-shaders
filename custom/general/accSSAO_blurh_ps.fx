#ifndef SAMPLE_BLUR_TEX
#define SAMPLE_BLUR_TEX(UV) float4(BLUR_TEX.SampleLevel(samLinear, UV, 0).x, 0, 0, 0)
#endif

// #define MIPMAPPED_GBUFFER 4
#define BLUR_TEX txSsaoFirstStep
Texture2D<float> txSsaoFirstStep : register(t0);

#include "accSS.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  // return txSsaoFirstStep.SampleLevel(samPoint, pin.Tex, 0);
  return ppBlur(pin.Tex, float2(1, 0));
}