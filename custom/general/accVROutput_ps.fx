#include "include/common.hlsl"

Texture2D txEye0 : register(t0);
Texture2D txEye1 : register(t1);
Texture2D txEyeBlurred0 : register(t2);
Texture2D txEyeBlurred1 : register(t3);

cbuffer cbData : register(b10) {
  float2 screenResolution;
  float zoom;
  float lensDistortion;

  float lensDarkening;
  float lensAberration;
  float blurStrength;
  float blurBrightness;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct Remapped {
  float2 uv;
  float darkening;
  float distance;
};

float2 fixRatio(float2 uv, float2 screenSize){
  float ratio = screenSize.y / screenSize.x;
  if (ratio > 1) {
    uv.y = uv.y * ratio - (ratio - 1) / 2;
  } else {
    ratio = 1 / ratio;
    uv.x = uv.x * ratio - (ratio - 1) / 2;
  }
  return uv;
}

float2 fakeLens(float2 uv){
  float2 offset = uv * 2 - 1;
  offset *= lerp(lensDistortion, 1, pow(length(offset), 4)) * zoom;
  return offset * 0.5 + 0.5;
}

Remapped getUV(float2 val){
  val = fixRatio(val, screenResolution * float2(0.5, 1));

  Remapped R;
  R.uv = fakeLens(val);

  float2 offset = (val * 2 - 1) / 1.04;
  float flatDist = max(abs(offset.x), abs(offset.y));
  float cornerK = saturate(remap(length(offset) - flatDist, 0, 0.6, 0, 1));
  float cornerDistance = lerp(flatDist, length(offset), cornerK);
  R.darkening = lerp(lensDarkening, 1, pow(saturate(remap(cornerDistance, 0.92, 0.8, 0, 1)), 0.25));
  R.distance = cornerDistance;
  return R;
}

float3 getValue(Remapped R, Texture2D tex){
  float3 col = float3(
    tex.SampleLevel(samLinearBorder0, R.uv, 0).r,
    tex.SampleLevel(samLinearBorder0, R.uv + float2(R.distance * 0.003 * lensAberration, 0), 0).g,
    tex.SampleLevel(samLinearBorder0, R.uv + float2(R.distance * 0.006 * lensAberration, 0), 0).b);
  return col * R.darkening;
}

float3 singleView(float2 uv){
  float2 uvMain = fixRatio(uv, screenResolution);
  uvMain = uvMain * zoom - (zoom - 1) / 2;
  
  float2 offset = uvMain * 2 - 1;
  if (any(abs(offset) > 1)){
    float dist = length(max(abs(offset - float2(0, 0.01)) - 0.99, 0));
    float2 vinp = 0.2 * float2(1, screenResolution.x / screenResolution.y);
    float ving = length(max(abs(uv * 2 - 1) - 1 + vinp * 0.7, 0) / vinp);
    float3 texBlurred = blurBrightness * lerp(txEyeBlurred0.SampleLevel(samLinear, uv, blurStrength).rgb, txEyeBlurred1.SampleLevel(samLinear, uv, blurStrength).rgb, uv.x);
    return texBlurred * lerp(0.4, 1, pow(saturate(dist * 10), 0.5)) * pow(saturate(1 - ving), 0.35);
  } else {
    return (txEye0).SampleLevel(samLinearBorder0, uvMain, 0).xyz;
  }
}

float3 singleStereoView(float2 uv){
  float2 uvMain = fixRatio(uv, screenResolution);
  uvMain = uvMain * zoom - (zoom - 1) / 2;

  float3 colorMain = float3(
    txEye0.SampleLevel(samLinearBorder0, uvMain, 0).r, 
    txEye1.SampleLevel(samLinearBorder0, uvMain, 0).gb);
  
  float2 offset = uvMain * 2 - 1;
  if (any(abs(offset) > 1)){
    float dist = length(max(abs(offset - float2(0, 0.01)) - 0.99, 0));
    float2 vinp = 0.2 * float2(1, screenResolution.x / screenResolution.y);
    float ving = length(max(abs(uv * 2 - 1) - 1 + vinp * 0.7, 0) / vinp);
    float3 texBlurred = blurBrightness * lerp(txEyeBlurred0.SampleLevel(samLinear, uv, blurStrength).rgb, txEyeBlurred1.SampleLevel(samLinear, uv, blurStrength).rgb, uv.x);
    return texBlurred * lerp(0.4, 1, pow(saturate(dist * 10), 0.5)) * pow(saturate(1 - ving), 0.35);
  } else {
    return colorMain;
  }
}

float3 doubleView(float2 uv){
  Remapped R = getUV(frac(uv * float2(2, 1)));
  if (uv.x < 0.5){
    return getValue(R, txEye0);
  } else {
    return getValue(R, txEye1);
  }
}

float4 main(VS_Copy pin) : SV_TARGET {
  #if defined(USE_DOUBLE_VIEW)
    return float4(doubleView(pin.Tex), 1);
  #elif defined(USE_SINGLE_VIEW)
    return float4(singleView(pin.Tex), 1);
  #else
    return float4(singleStereoView(pin.Tex), 1);
  #endif
}