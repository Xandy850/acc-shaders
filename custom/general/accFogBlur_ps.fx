#include "accSS.hlsl"
#include "include_new/base/cbuffers_ps.fx"

Texture2D<float> txMirageMask : register(t3);
Texture2D<float> txHeatMask : register(t4);

float calculateFogNewFn(float3 posC, float3 posN){
  #ifdef USE_PS_FOG
    return 0;
  #else
    return ss_ksFogBlend * pow(saturate(ss_extFogConstantPiece * (1.0 - exp(-posC.y / ss_ksFogLinear)) / posN.y), ss_extFogExp);
  #endif
}

float calculateFogNewFn(float3 posC){
  return calculateFogNewFn(posC, normalize(posC));
}

SamplerState samColor : register(s13) {
  Filter = LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
};

float4 sampleNoise(float2 uv){
	float textureResolution = 32;
	uv = uv * textureResolution + 0.5;
	float2 i = floor(uv);
	float2 f = frac(uv);
	uv = i + f * f * (3 - 2 * f);
	uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

#define R 2
float sampleHeatMask(float2 uv) {
  float currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    currentShot += txHeatMask.SampleLevel(samLinearClamp, uv, 3, int2(x, y));
    ++totalWeight;
  }
  return currentShot / totalWeight;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = 1;
  {
    [unroll] for (int x = -2; x <= 2; x+=2)
    [unroll] for (int y = -2; y <= 2; y+=2){
      if (abs(x) + abs(y) == 4) continue;
      float2 offset = float2(x, y);
      float2 newUV = pin.Tex + gSize.zw * float2(x, y);
      depth = min(depth, ssGetDepth(newUV));
    }
  }

  float3 originC = ssGetPos(pin.Tex, 10);
  float3 dirC = normalize(originC);

  float3 origin = ssGetPos(pin.Tex, depth);
  float distance = length(origin);
  float3 dir = origin / distance;
  float beneath = saturate(0.5 - dir.y * 18);
  float beneath2 = saturate(2 - origin.y / 100);
  beneath = max(beneath, beneath2);

  float fog = max(
    pow(calculateFogNewFn(origin), 4), 
    saturate(distance * fogBlurDistanceInv - 0.5)) * beneath;

  #ifdef USE_HEATING
    float3 heatPoint = dir * 200 + ksCameraPosition.xyz + float3(0, -ksGameTimeS * 0.4, 0);
    heatPoint += float3(gWindOffset.x, 0, gWindOffset.y);

    float3 dirUp = float3(0, 1, 0);
    float3 dirSide = normalize(float3(dir.z, 0, -dir.x)); 

    float noisePosY = dot(heatPoint, dirUp);
    float4 noise0 = sampleNoise(float2(heatPoint.x, noisePosY) * 0.4);
    float4 noise1 = sampleNoise(float2(heatPoint.z, noisePosY) * 0.4);
    float4 noise = lerp(noise0, noise1, abs(dirSide.z));

    float heatK = saturate(distance / 100 - 1) * noise.z
      * lerp(1, 0.5, txMirageMask.SampleLevel(samLinearSimple, pin.Tex, 0))
      * (1 - sampleHeatMask(pin.Tex));
    pin.Tex.xy += (noise.xy * 2 - 1) * gTrackHeatFactor * 0.000025 / max(0.01, fwidth(noisePosY)) * heatK;
    // return float4(noise.xyz, 1);
    // return sampleHeatMask(pin.Tex);
    // return heatK;
    fog = max(fog, heatK * (1 + noise.z));
  #endif

  float3 bColor = txColor.SampleLevel(samLinear, pin.Tex, 0).rgb;
  float3 vColor = bColor;
  float3 mColor = bColor;

  [branch]
  if (fog <= 0.1){
    return float4(vColor, 1);
  }

  {
    // [unroll] for (int x = -2; x <= 2; x+=2)
    // [unroll] for (int y = -2; y <= 2; y+=2){
    //   if (x == 0 && y == 0 || abs(x) + abs(y) == 4) continue;
    //   float2 newUV = pin.Tex + gSize.zw * float2(x, y) * fog;
    //   float3 c = txColor.SampleLevel(samColor, newUV, 0).rgb;
    //   vColor += c;
    //   mColor = max(mColor, c);
    // }

    static float2 poissonDisk[10] = {
      float2(-0.2027472f, -0.7174203f),
      float2(-0.4839617f, -0.1232477f),
      float2(0.4924171f, -0.06338801f),
      float2(-0.6403998f, 0.6834511f),
      float2(-0.8817205f, -0.4650014f),
      float2(0.04554421f, 0.1661989f),
      float2(0.1042245f, 0.9336259f),
      float2(0.6152743f, 0.6344957f),
      float2(0.5085323f, -0.7106467f),
      float2(-0.9731231f, 0.1328296f)
    };
    
    for (int i = 0; i < 10; i++){
      float2 offset = poissonDisk[i];
      // float2 randomDirection = reflect(poissonDisk[i], random);
      float2 newUV = pin.Tex + gSize.zw * offset * fog * 2;
      // float3 c = txColorToBlur.SampleLevel(samColor, newUV, 0).rgb;
      float3 c = txColor.SampleLevel(samColor, newUV, 0).rgb;
      vColor += c;
      mColor = max(mColor, c);
    }
    
    // [unroll] for (int x = -2; x <= 2; x+=2)
    // [unroll] for (int y = -2; y <= 2; y+=2){
    //   if (x == 0 && y == 0 || abs(x) + abs(y) == 4) continue;
    //   float2 newUV = pin.Tex + gSize.zw * float2(x, y) * fog;
    //   float3 c = txColor.SampleLevel(samColor, newUV, 0).rgb;
    //   vColor += c;
    //   mColor = max(mColor, c);
    // }
  }

  // return float4(vColor / 21, 1);
  return float4(vColor / 11, 1);
  // return float4(lerp(vColor / 21, bColor, saturate(dot(mColor, 0.33) - 0.5)) , 1);
  // return float4(lerp(vColor / 5, bColor, saturate(dot(mColor, 0.33) - 0.5)) , 1);
}