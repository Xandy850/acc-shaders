#include "include/samplers.hlsl"

Texture2D<float> txBuildings : register(t0);
Texture2D<float> txRoads : register(t1);
Texture2D<float> txWalls : register(t2);
Texture2D<float> txWater : register(t3);
Texture2D<float> txGrass : register(t4);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

float2 sampleLayer(Texture2D<float> layer, float2 uv){
  float i = 0;
  float a = 0;
  float t = 0;
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    float v = layer.SampleLevel(samPoint, uv, 0, int2(x, y));
    if (v == 0) i++;
    if (v == 1) a++;
  }

  return saturate(float2(a, i > 2 ? 1 : 0));
}

void contrib(inout float4 color, Texture2D<float> layer, float2 uv, float3 layerColor, float wallDarkening){
  float2 sampled = sampleLayer(layer, uv);
  float4 res = float4(layerColor * (1 - sampled.y * wallDarkening), sampled.x);
  color = lerp(color, res, res.a);
}

cbuffer cbData : register(b10) {
  float4 gWaterColor;
  float4 gWallsColor;
  float4 gGrassColor;
  float4 gRoadsColor;
  float4 gBuildingsColor;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;
  contrib(ret, txWater, pin.Tex, gWaterColor.xyz, gWaterColor.w);
  contrib(ret, txWalls, pin.Tex, gWallsColor.xyz, gWallsColor.w);
  contrib(ret, txGrass, pin.Tex, gGrassColor.xyz, gGrassColor.w);
  contrib(ret, txRoads, pin.Tex, gRoadsColor.xyz, gRoadsColor.w);
  contrib(ret, txBuildings, pin.Tex, gBuildingsColor.xyz, gBuildingsColor.w);
  return ret;
}