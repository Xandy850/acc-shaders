struct PS_INPUT {
  float4 pos : SV_POSITION;
  float4 col : COLOR0;
  float2 uv  : TEXCOORD0;
  float4 mad : COLOR1;
};

sampler sampler0;
Texture2D texture0;
            
float4 main(PS_INPUT pin) : SV_Target {
  float4 in_col = texture0.SampleBias(sampler0, pin.uv, -2);
  in_col.rgb = in_col.rgb * pin.mad.x + pin.mad.y;
  in_col.a = saturate(in_col.a * pin.mad.z + pin.mad.w);
  float4 out_col = pin.col * in_col; 
  return saturate(out_col); 
}
