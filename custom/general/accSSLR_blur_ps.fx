#include "accSS.hlsl"
#define USE_POISSON

Texture2D txPreviousMap : register(t14);

#ifdef USE_POISSON

  #include "include/poisson.hlsl"
  float4 main(VS_Copy pin) : SV_TARGET {
    float4 ret = 0;
    float tot = 0;
    #define PUDDLES_DISK_SIZE 16
    for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.007;
      float2 sampleUV = pin.Tex + offset;
      float4 v = txPreviousMap.SampleLevel(samPoint, sampleUV, 0);
      float w = 1;
      ret += v * w;
      tot += w;
    }
    return saturate(ret / tot);
  }

#else

  #define R 2
  float4 main(VS_Copy pin) : SV_TARGET {
    float4 currentShot = 0;
    float totalWeight = 0;
    [unroll]
    for (int x = -R; x <= R; x++)
    [unroll]
    for (int y = -R; y <= R; y++){
      if (abs(x) + abs(y) == R + R) continue;
      // float weight = 1 / (1 + length(float2(x, y)));
      float weight = 1;
      currentShot += txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y) * 2) * weight;
      totalWeight += weight;
    }
    return currentShot / totalWeight;
  }

#endif
