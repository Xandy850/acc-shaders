#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float mask = 1 - txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0).a;
  float checkerPattern = frac((pin.PosH.x + pin.PosH.y) / 12) > 0.5;
  return float4(lerp(0.4, 0.6, checkerPattern.xxx), mask * 0.3);
}