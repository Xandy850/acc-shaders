struct PS_INPUT {
  float4 pos : SV_POSITION;
  float4 col : COLOR0;
  float2 uv  : TEXCOORD0;
  float4 mad : COLOR1;
};

sampler sampler0;
Texture2D texture0;

#include "include/poisson.hlsl"
float4 sampleBlurred(float2 uv, float2 scale){
  float4 intense = 0;
  float4 ret = 0;
  float tot = 0;
  #define PUDDLES_DISK_SIZE 8
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * scale;
    float2 sampleUV = uv + offset;
    float4 v = texture0.SampleLevel(sampler0, sampleUV, 0);
    // if (v.a < 0.1) continue;
    float w = v.a;
    if (v.a > intense.a) intense = v;
    ret += v * w;
    tot += w;
  }
  // return intense;
  return ret / max(tot, 0.0001);
}
            
float4 main(PS_INPUT pin) : SV_Target {
  float4 in_col = texture0.Sample(sampler0, pin.uv);
  // float4 in_col = sampleBlurred(pin.uv, float2(ddx(pin.uv.x), ddy(pin.uv.y))).a;
  in_col.rgb = in_col.rgb * pin.mad.x + pin.mad.y;
  in_col.a = saturate(in_col.a * pin.mad.z + pin.mad.w);
  float4 out_col = pin.col * in_col; 
  float4 ret = saturate(out_col);
  // ret.rgb = 0;
  // ret.a = max(ret.a, outline);
  return ret; 
}
