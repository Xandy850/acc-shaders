#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"

#define TXDEPTH_DEFINED
#define TXNOISE_DEFINED
#define CBCAMERA_DEFINED

Texture2D txColor : register(t0);
#ifdef MIPMAPPED_GBUFFER
  Texture2D txNormals : register(t11);
  Texture2D<float> txDepth : register(t12);
  Texture2D<float2> txMotionLR : register(t19);
#else
  Texture2D txNormals : register(t1);
  Texture2D<float> txDepth : register(t2);
  #define MIPMAPPED_GBUFFER 0
#endif
Texture2D txNoise : register(t20);
Texture2D<float2> txMotion : register(t9);
Texture2D<float2> txMotionL : register(t13);

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
  float4 extScreenSize;
}

float linearize(float depth){
  return 2.0 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2.0 * depth - 1.0) * (ksFarPlane - ksNearPlane));
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float3 ssGetUv(float3 position, bool prevFrame = false){
  float4 p = mul(float4(position, 1), prevFrame ? viewProjPrev : viewProj);
  return p.xyz / p.w;
}

float ssGetDepth(float2 uv, bool lowRes = false){
  return txDepth.SampleLevel(samPointClamp, uv, lowRes ? MIPMAPPED_GBUFFER : 0).x;
}

float3 ssGetNormal(float2 uv, bool lowRes = false){
  return normalize(ssNormalDecode(
    txNormals.SampleLevel(samPointClamp, uv, lowRes ? MIPMAPPED_GBUFFER : 0).xyz));
}

// float3 ssGetNormal(float2 uv, out float reflValue, bool lowRes = false){
//   float4 tx = txNormals.SampleLevel(samPointClamp, uv, lowRes ? MIPMAPPED_GBUFFER : 0);
//   reflValue = tx.z;
//   return normalize(ssNormalDecode(tx.xyz));
// }

#ifdef BLUR_TEX
  #define _DepthAnalysis true
  #define _DepthAnalysisFactor 0.1
  #define _NormalAnalysis true

  float3 ssGetNormal2(float2 uv, bool lowRes = false){
    return ssNormalDecode(txNormals.SampleLevel(samPointClamp, uv, 
      lowRes ? MIPMAPPED_GBUFFER : 0).xyz);
  }

  #define BLUR_RADIUS 4
  #define GET_WEIGHT(x) blurWeights[x]

  static const float blurWeights[] = {
    0.03773688735139936,
    0.07585222587605243,
    0.12489394056057546,
    0.16845549864750073,
    0.18612289512894423,
    0.16845549864750073,
    0.12489394056057546,
    0.07585222587605243,
    0.03773688735139936,
  };

  #ifndef SAMPLE_BLUR_TEX
  #define SAMPLE_BLUR_TEX(UV) BLUR_TEX.SampleLevel(samLinear, UV, 0)
  #endif

  float4 ppBlur(const float2 UV, const float2 dir){
    float4 finalColor = SAMPLE_BLUR_TEX(UV) * GET_WEIGHT(BLUR_RADIUS);
    float lDepthC = _DepthAnalysis ? linearize(ssGetDepth(UV)) : 0;
    float3 normalC = ssGetNormal2(UV);
    float totalAdditionalWeight = GET_WEIGHT(BLUR_RADIUS);

    for (int i = 1; i <= BLUR_RADIUS; i++){
      float2 UVL = UV - 3.7 * gSize.zw * dir * i;
      float2 UVR = UV + 3.7 * gSize.zw * dir * i;

      float depthFactorR = 1.0f;
      float depthFactorL = 1.0f;
      float normalFactorL = 1.0f;
      float normalFactorR = 1.0f;

      if (_DepthAnalysis)	{
        float lDepthR = linearize(ssGetDepth(UVR));
        float lDepthL = linearize(ssGetDepth(UVL));

        depthFactorR = saturate(1.0f / (abs(lDepthR - lDepthC) / _DepthAnalysisFactor));
        depthFactorL = saturate(1.0f / (abs(lDepthL - lDepthC) / _DepthAnalysisFactor));
      }

      if (_NormalAnalysis)	{
        float3 normalR = ssGetNormal2(UVR);
        float3 normalL = ssGetNormal2(UVL);

        normalFactorL = saturate(dot(normalC, normalL));
        normalFactorR = saturate(dot(normalC, normalR));
      }

      float cwR = GET_WEIGHT(BLUR_RADIUS + i) * depthFactorR * normalFactorR;
      float cwL = GET_WEIGHT(BLUR_RADIUS - i) * depthFactorL * normalFactorL;

      finalColor += SAMPLE_BLUR_TEX(UVR) * cwR;
      finalColor += SAMPLE_BLUR_TEX(UVL) * cwL;

      totalAdditionalWeight += cwR;
      totalAdditionalWeight += cwL;
    }

    return finalColor / totalAdditionalWeight;
  }
#endif