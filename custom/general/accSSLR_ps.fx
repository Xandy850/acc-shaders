#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"

Texture2D txNormals : register(t1);
Texture2D<float> txDepth : register(t2);
Texture2D txReflections : register(t5);

Texture2D txNormalsLR : register(t11);
Texture2D<float> txDepthLR : register(t12);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float3 ssGetUv(float3 position){
  float4 p = mul(float4(position, 1), viewProj);
  return p.xyz / p.w;
}

float ssGetDepth(float2 uv){
  return txDepth.SampleLevel(samLinearClamp, uv, 0).x;
  return txDepth.SampleLevel(samLinearBorder1, uv, 0).x;
}

float ssGetDepthLR(float2 uv){
  return txDepthLR.SampleLevel(samLinearClamp, uv, 1).x;
  return txDepthLR.SampleLevel(samLinearBorder1, uv, 1).x;
}

#ifndef ITERATIONS_SHORT
#define ITERATIONS_SHORT 16
#endif

#ifndef ITERATIONS_LARGE
#define ITERATIONS_LARGE 16
#endif

#ifndef FIX_MULT
#define FIX_MULT 0.5
#endif

#ifndef OFFSET
#define OFFSET 0.05
#endif

#ifndef GLOW_FIX
#define GLOW_FIX 0.2
#endif

#ifndef DISTANCE_THRESHOLD
#define DISTANCE_THRESHOLD 0.01
#endif

float selfHitTest(float2 uv, float2 newUv, float depth, float newDepth){ 
  // return 1;
  float2 uvDist = newUv - uv;
  uvDist *= gSize.xy;
  uvDist = abs(uvDist);
  return saturate(max(uvDist.x, uvDist.y) - 1);
  // return max(saturate(dot(uvDist, uvDist) * 1000000), 1 - pow(saturate(newDepth / depth), 100000));
}

struct RESULT {
  float4 main : SV_TARGET0;
  float dimming : SV_TARGET1;
};

RESULT main(VS_Copy pin) {
  float4 vReflections = txReflections.SampleLevel(samPoint, pin.Tex, 0);
  float4 vNormal = txNormals.SampleLevel(samPoint, pin.Tex, 0);

  float forceMode = 0;
  if (vReflections.w < -0.001){
    // Silly way of storing SSLR_FORCE flag
    forceMode = 1;
    vReflections.w = abs(vReflections.w);
  }

  #ifndef SSLR_TRACE_EVERYTHING
    [branch]
    if (vReflections.w * abs(dot(vNormal, 1)) < 0.001) {
      return (RESULT)0;
    }
  #endif

  float depth = ssGetDepth(pin.Tex);
  float3 position = ssGetPos(pin.Tex, depth);
  float3 normal = normalize(ssNormalDecode(vNormal.xyz));

  float dist = length(position);
  float3 viewDir = position / dist;
  float3 reflectDir = normalize(reflect(viewDir, normal));

  // float curvedGroundFixK = saturate(dot(reflectDir, viewDir) - 0.99) 
  //   * saturate(normal.y - 0.99) * 100 * 100;
  // normal = lerp(normal, float3(0, 1, 0), curvedGroundFixK * saturate(normal.y * 1000 - 998));
  // reflectDir = normalize(reflect(viewDir, normal));

  float3 calculatedPosition, newPosition;
  float3 newUv = 0;
  float actualL, newL;
  float newDepth;

  float DK0 = pow(saturate(dist / 4 - 1), 4);
  float DK1 = pow(saturate(dist - 1), 4);

  float RdotV = dot(viewDir, reflectDir);
  float NdotV = dot(viewDir, normal);

  float N = saturate(RdotV * 30 - 29); 
  #ifdef USE_LR_DEPTH
    float L = lerp(0.2, 0.8, DK0);
  #else
    float L = lerp(0.1, 0.2, DK0);
  #endif
  L = min(L, dist / 200);
  L = max(L, dist / 100 - 1);
  // if (forceMode) L /= max(pow(1 - N, 4), 0.01);

  float S = lerp(0.1, 1, DK1);

  [loop]
  for (int j0 = 0; j0 < ITERATIONS_SHORT; j0++) {
    calculatedPosition = position + reflectDir * L;

    newUv = ssGetUv(calculatedPosition);
    #ifdef USE_LR_DEPTH
      newDepth = ssGetDepthLR(newUv.xy);
    #else
      newDepth = ssGetDepth(newUv.xy);
    #endif
    newPosition = ssGetPos(newUv.xy, newDepth);

    #define USE_NEW_CODE
    #ifdef USE_NEW_CODE
      actualL = length(calculatedPosition - newPosition);
      [branch]
      if (newDepth < newUv.z 
        // || actualL < (j0 ? L * 0.5 : L * 0.8)) break;
        || actualL < (j0 ? L * 0.25 : L * 0.4)) break;
    #else
      actualL = length(position - newPosition);
      [branch]
      if (newDepth < newUv.z 
        || abs(actualL - L) < (j0 ? L * 0.5 : L * 0.8)) break;
    #endif

    // L = L * 1.4 + dist / 1;
    L *= 1.4;
  }

  [unroll]
  for (int j1 = 0; j1 < ITERATIONS_LARGE; j1++) {
    calculatedPosition = position + reflectDir * L;

    newUv = ssGetUv(calculatedPosition);
    #ifdef USE_LR_DEPTH
      newDepth = j1 < ITERATIONS_LARGE ? ssGetDepthLR(newUv.xy) : ssGetDepth(newUv.xy);
    #else
      newDepth = ssGetDepth(newUv.xy);
    #endif
    newPosition = ssGetPos(newUv.xy, newDepth);
    newL = length(position - newPosition);

    // if (j1 == 0){
    //   newL = L + min(actualL - L, OFFSET + L * GLOW_FIX + (newDepth < depth ? 1.0 : 0.0));
    //   L = L * (1 - FIX_MULT) + newL * FIX_MULT;
    // } else {
    //   L = newL;
    // }

    if (j1 != ITERATIONS_LARGE - 1){
      L += (newL - L) * (j1 < ITERATIONS_LARGE / 3 ? S : 1);
    }
  }

  calculatedPosition = position + reflectDir * L;
  float miss = length(calculatedPosition - newPosition);
  // newUv = ssGetUv(calculatedPosition);

  float fresnel = saturate(6 * pow(1 + NdotV, 2));
  fresnel = max(fresnel, 1 - pow(saturate(newDepth / depth), 10000));

  float newDist = length(newPosition);  
  float threshold = dist / lerp(100, 10, DK0);
  float thresholdL = dist / 3;
  float thresholdD = dist / 100;

  float qDist = 1 - saturate(miss / threshold - 1);
  float qDistL = 1 - saturate(miss / thresholdL - 1);
  float qDistF = saturate(max(2 - dist / 3, 1.5 - miss / dist));
  float qDistD = 1 - saturate(miss / thresholdD - 1);
  float qDot = dot(normalize(newPosition - position), reflectDir);
  float qDir = saturate(qDot * 10 - 9);
  qDir *= saturate(newDist - dist - 1);
  float qRough = saturate(normal.y * 10 - 9);
  // qRough *= saturate(newDist - dist - 1);
  // qRough *= qDistL;
  // qRough *= saturate(newDist > dist);

  // forceMode = 0;
  float quality = forceMode * qDistF * (1 - pow(dot(normal, viewDir), 2));
  quality = max(quality, qDist);
  quality = max(quality, qDir * qDistL);
  quality = max(quality, qRough * qDistL);
  // quality *= saturate(newDist / dist * 100 - 1);
  quality *= saturate(2 - miss / 200);

  if (newDist < 1.3 && dist > 2){
    // quality = 0;
  }

  // quality = 1;
  // quality = qDistL;
  // quality *= saturate(forceMode + selfHitTest(pin.Tex, newUv.xy, depth, newDepth));
  // quality *= saturate(newDist / dist * 4 - 1);

  #ifdef NO_DIMMING
    float dimQuality = 0;
  #else
    float dimQuality = saturate(2 - L * lerp(1, 0.25, saturate(normal.y)));
    dimQuality = 0.5 + 0.5 * dimQuality;
    quality *= lerp(qDistD, 1, saturate(dimQuality + forceMode));
  #endif

  if (newDepth == 1) quality = 0;
  // quality = 1;

  float edgeK = forceMode ? 0.05 : 0.0001;
  float edge = saturate((min(newUv.x, 1 - newUv.x) + edgeK) / edgeK) 
    * saturate((min(newUv.y, 1 - newUv.y) + edgeK) / edgeK);

  L = saturate(pow(saturate(L / lerp(0.3, 10, DK1)), 1));

  RESULT R;
  R.main = float4(newUv.xy, L, fresnel * quality * edge);
  R.dimming = max(saturate(1 - qDistD) * dimQuality, saturate(miss / 50 - 1));
  return R;
}