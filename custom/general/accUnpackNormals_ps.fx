#include "accSS.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(ssGetNormal(pin.Tex), 1);
}