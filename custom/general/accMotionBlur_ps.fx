#define MIPMAPPED_GBUFFER 1
#include "accSS.hlsl"
// #define NO_MAX_STEP
Texture2D<float2> txExtra : register(t3);

float cone(float T, float l_V){
  return saturate(1.0 - T / l_V);
}

float cylinder(float T, float l_V){
  return 1.0 - smoothstep(0.95 * l_V, 1.05 * l_V, T);
}

float sdc(float a, float b, float t){
  return saturate(1 - (b - a) / t);
}

#define JITTER 4
#define VEL_MULT motionBlurMult
#define MAX_VEL 0.004
// #define NO_MAX_STEP

#ifndef BLUR_STEPS
  #define BLUR_STEPS 12
#endif

float2 fixVel(float2 vel){  
  if (length(vel) > MAX_VEL) vel *= MAX_VEL / length(vel);
  return vel;
  // return vel * 2.5;
}

float2 sampleMotion(float2 uv, bool forceHQ = false){
  if (!forceHQ){
    return txMotionLR.SampleLevel(samLinearBorder0, uv, 0) * VEL_MULT;
  }
  return txMotion.SampleLevel(samLinearBorder0, uv, 0) * VEL_MULT;
}

#define _MaxBlurRadius 16

float SampleWeight(float2 d_n, float l_v_c, float l_v_max, float z_p, float T, float2 S_uv, float w_A) {
  float3 input = float3(sampleMotion(S_uv), linearize(ssGetDepth(S_uv, false)));

  float2 v_S = input.xy;
  float l_v_S = max(length(v_S), 0.0001);

  float z_S = input.z;

  // float f = sdc(z_p, z_S, lerp(200, 0.2, l_v_c / MAX_VEL));
  float f = sdc(z_p, z_S + (1 - saturate(l_v_c / l_v_max)), 0.1);
  float b = sdc(z_S, z_p, 0.1);

  float w_B = abs(dot(v_S / l_v_S, d_n));

  float allowed = saturate(length(input.xy) * 20);
  float weight = 0.0;
  weight += f * cone(T, l_v_S) * w_B;
  weight += b * cone(T, l_v_c) * w_A;
  weight += cylinder(T, min(l_v_S, l_v_c)) * max(w_A, w_B) * 2;

  // return 1;
  return saturate(weight) * allowed; // * (temp.z < z_p ? 1 : 0);
}

float2 neighbourMax(float2 uv){
  float2 nVel = 0;
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    float2 nVelCand = txMotionL.SampleLevel(samPointClamp, uv, 0, int2(x, y)).xy;
    if (dot(nVelCand, nVelCand) > dot(nVel, nVel)) nVel = nVelCand;
  }
  return nVel;
}

float2 rnmix(float2 a, float2 b, float p) {
    return normalize(lerp(a, b, saturate(p)));
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 base = txColor.SampleLevel(samPoint, pin.Tex, 0);
  float3 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz;
  // float3 random = 0;
  // return frac(linearize(ssGetDepth(pin.Tex, false)));

  float dK = 1; // saturate((base.g - 0.4) * 1.6);

  // float2 v_c = fixVel(txMotion.SampleLevel(samPoint, pin.Tex, 0).xy * VEL_MULT);
  float2 v_c = sampleMotion(pin.Tex, true);
  v_c *= dK;

  // return 0.5 + txMotionL.SampleLevel(samPoint, pin.Tex, 0).xyxy * 10;
  // return float4(0.5 + txMotionLR.SampleLevel(samPoint, pin.Tex, 0).xy * 100, 0, 1);

  float2 v_c_n = normalize(v_c);
  float l_v_c = max(length(v_c), 0.0001);

  #ifndef NO_MAX_STEP 
    float2 v_max = fixVel(neighbourMax(pin.Tex + random.xy * 0.01) * VEL_MULT);
    v_max *= dK;

    float2 v_max_n = normalize(v_max);
    float l_v_max = length(v_max);
    float maxSpeed = max(length(v_c), l_v_max);
  #else
    float2 v_max_n = v_c_n;
    float l_v_max = l_v_c;
    float maxSpeed = length(v_c);
  #endif

  [branch]
  if (maxSpeed < 0.001) return base;
  float mipLevel = saturate(maxSpeed / MAX_VEL / 4);

  float z_p = linearize(ssGetDepth(pin.Tex));
  float2 w_p = v_max_n.yx * float2(-1, 1);
  if (dot(w_p, v_c) < 0) w_p = -w_p;

  float2 w_c = rnmix(w_p, v_c_n, (l_v_c - 0.5) / 1.5);

  float sampleCount = BLUR_STEPS * 2;
  float totalWeight = 0.5 * sampleCount / (max(l_v_c / MAX_VEL, 0.5) * 60);
  float3 result = base.rgb * totalWeight;

  float t = -1 + JITTER * (random.z - 0.5) / (JITTER + sampleCount) * gMotionBlurNoiseMult;

  #ifndef NO_MAX_STEP 
    float dt = 4 / sampleCount;
  #else
    float dt = 2 / sampleCount;
  #endif

  float w_A1 = saturate(dot(w_c, v_c_n));
  float w_A2 = saturate(dot(w_c, v_max_n));// * saturate(length(v_c) * 10);

  #if !defined(NO_MAX_STEP) && !defined(NO_UNROLL)
    [unroll]
  #endif
  for (int c = 0; c < BLUR_STEPS; c++){
    {
      float2 S_uv = pin.Tex + t * v_c;
      float weight = SampleWeight(v_c_n, l_v_c, 0.001, z_p, abs(t * l_v_max), S_uv, w_A1);
      result += txColor.SampleLevel(samLinearClamp, S_uv, mipLevel).rgb * weight;
      totalWeight += weight;
      t += dt;
    }

    #ifndef NO_MAX_STEP 
    {
      float2 S_uv = pin.Tex + t * v_max;
      float weight = SampleWeight(v_max_n, l_v_c, l_v_max, z_p, abs(t * l_v_max), S_uv, w_A2);
      result += txColor.SampleLevel(samLinearClamp, S_uv, mipLevel).rgb * weight;
      totalWeight += weight;
      t += dt;
    }
    #endif
  }

  // return float4(v_max * 100 + 0.5, 0, 1);
  // return base * 0.01 + float4(v_c * 100 + 0.5, 0, 1);

  return float4(result / totalWeight, 1);
}