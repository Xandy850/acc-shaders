#include "include/common.hlsl"

Texture2D<float> txInput : register(t0);
Texture2D<float2> txBoundaries : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// float main(VS_Copy pin) : SV_TARGET {
//   float c = 0, t = 0;
//   [unroll]
//   for (int i = -2; i <= 2; i++){
//     float w = 1. / (1. + abs(i));
//     c += txInput.SampleLevel(samLinear, pin.Tex, 0, int2(0, i)) * w;
//     t += w;
//   }
//   return c / t;
// }

float2 minmax(float2 a, float v){
  a.x = min(a.x, v);
  a.y = max(a.y, v);
  return a;
}

float2 sampleBase(float2 uv){
  const int R = 3;
  float2 r = txInput.SampleLevel(samLinearClamp, uv, 0);
  r = minmax(r, txInput.SampleLevel(samLinearClamp, uv, 0, int2(-R, -R)));
  r = minmax(r, txInput.SampleLevel(samLinearClamp, uv, 0, int2(R, -R)));
  r = minmax(r, txInput.SampleLevel(samLinearClamp, uv, 0, int2(R, R)));
  r = minmax(r, txInput.SampleLevel(samLinearClamp, uv, 0, int2(-R, R)));
  return r;
}

// #include "include/poisson.hlsl"
// float4 main(VS_Copy pin) : SV_TARGET {
//   float2 base = txBoundaries.SampleLevel(samLinear, pin.Tex, 2);
//   float dif = base.y - base.x;
//   float distance = saturate(dif * 5 - 1);
//   float ret = 0;
//   float tot = 0;
//   float range = 0.002 + dif * 0.5;
//   #define DISK_SIZE 32
//   for (uint i = 0; i < DISK_SIZE; ++i) {
//     float2 offset = SAMPLE_POISSON(DISK_SIZE, i) * range;
//     float2 sampleUV = pin.Tex + offset;
//     float v = txInput.SampleLevel(samLinearClamp, sampleUV, 0);
//     float w = 1;
//     ret += v * w;
//     tot += w;
//   }
//   return ret / tot;
// }

#ifdef REDUCED_BLUR
  #define R 1
#else
  #define R 2
#endif

float main(VS_Copy pin) : SV_TARGET {
  // return txInput.SampleLevel(samLinearClamp, pin.Tex, 0);

  float currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (R && abs(x) + abs(y) == R + R) continue;
    float weight = 1 / (1 + length(float2(x, y)));
    currentShot += txInput.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y)) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}
