#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_ps.fx"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 getBlurred(Texture2D tex, SamplerState samplerState, float2 uv, float blurMult){
  float4 b = 0;
  [unroll]
  for (int y = -2; y <= 2; y++)
  [unroll]
  for (int x = -2; x <= 2; x++){
    if (abs(x) == 2 && abs(y) == 2) continue;
    b += tex.SampleLevel(samplerState, uv, blurMult, int2(x, y));
  }
  b /= 21;
  return b;
}

float calculateFogNewFn(float3 posC, float3 posN){
  #ifdef USE_PS_FOG
    return 0;
  #else
    return ksFogBlend * pow(saturate(extFogConstantPiece * (1.0 - exp(-posC.y / ksFogLinear)) / posN.y), extFogExp);
  #endif
}

float calculateFogNewFn(float3 posC){
  return calculateFogNewFn(posC, normalize(posC));
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return float4(pin.Tex, 0, 1);
  float4 v = 0;
  float fogAdj = calculateFogNewFn(float3(400, -200, 0));
  v += txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 2.5 + fogAdj * 1.4) * (0.2 + fogAdj * 0.5);
  v += txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 7) * 0.5;
  v.rgb *= v.a;

  return float4(v.rgb * 1000 * 5, 0.0001);
  // return 0;
}