#include "include/common.hlsl"
#include "include/poisson.hlsl"

#ifdef RESOLVE_MSAA
  Texture2DMS<float4> txDiffuse : register(t0); 
#else
  Texture2D txDiffuse : register(t0);
#endif
Texture2D<float> txMask : register(t1);
TextureCube txCube : register(t10);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
  float4 extScreenSize;
}

cbuffer cbData : register(b10) {
  float4x4 gViewProjInv;
  float gUseRainMask;
  float2 gScreenSize;
}

float3 ssGetPos(float2 uv, float depth){
  // float4 p = mul(float4(uv.xy, depth, 1), ksMVPInverse);
  float4 p = mul(float4(uv.xy, depth, 1), gViewProjInv);
  return p.xyz / p.w;
}

float sampleMask(float2 uv){
  POISSON_AVG(float, result, txMask, samLinearClamp, uv, 0.005, 8);
  return saturate(result * 10 - 9);
}

float4 main(VS_Copy pin) : SV_TARGET {
  #ifdef RESOLVE_MSAA
    float4 direct = txDiffuse.Load(int2(pin.Tex * gScreenSize), 0);
  #else
    float4 direct = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  #endif
  direct.rgb = direct.rgb / (1 + dot(direct.rgb, 0.33) * 0.02);

  [branch]
  if (gUseRainMask){
    // float3 pos = -normalize(ssGetPos(pin.Tex * float2(2, -2) + float2(-1, 1), 1000));
    float3 pos = -normalize(ssGetPos(pin.Tex, 1000));
    pos.x = -pos.x;

    float mask = sampleMask(pin.Tex);
    float4 fallback = txCube.SampleLevel(samLinearSimple, pos, 0);
    return lerp(fallback, direct / (1 + dot(direct, 0.33) * 0.02), mask);
  } else {
    return direct;
  }
}