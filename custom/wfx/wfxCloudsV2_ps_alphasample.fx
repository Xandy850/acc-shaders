#define COVER_SUN
#define USE_SHADOW_OPACITY
#define RETURN_ALPHA_AS_COLOR
#define USE_ALT_CBUFFER
#define CLOUD_DETAIL_BIAS 2
#include "wfxCloudsV2_ps.fx"
