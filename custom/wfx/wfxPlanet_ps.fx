#define PS
#include "wfx_common.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float3 lightDir, float3 lightColor){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, 0.6));
  float3 result = frontlitK * 0.5;

  float specularBase = saturate(dot(normalize(lightDir - toCloud), normal));
  result += pow(specularBase, 10.0);

  return result * lightColor;
}

float4 main(PS_IN_Sky pin) : SV_TARGET {
  // return float4(4, 4, 0, 1);
  float2 offset = pin.Tex * 2 - 1;
  float3 toCamera = normalize(pin.PosW);
  float litUp = 1;
  float3 color = colorOpacity.rgb;
  float alpha = saturate(colorOpacity.a * litUp) * saturate((1 - length(offset)) * 100);
  // return withFogImpl(color, toCamera * ksFarPlane, toCamera, pin.LightH.x, 1, alpha);
  return withFogImpl(color, toCamera * ksFarPlane, toCamera, pin.LightH.x, 1, alpha);
}
