#define PS
#define USE_PS_FOG
#define USE_BACKLIT_FOG
#define CLOUDS
#include "wfx_common.hlsl"
#include "wfx_common_clouds.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float alpha, float3 lightDir, float3 lightColor, bool includeSpecular){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, frontlitDiffConcentration));
  float3 result = frontlitK * frontlitMult * colorOpacity.rgb;

  float backlitDot = saturate(dot(toCloud, lightDir));
  float backlitK = pow(backlitDot, abs(backlitExp) * (2 - alpha));
  result += backlitK * backlitMult 
    * (1 - pow(saturate(alpha), backlitOpacityExp) * backlitOpacityMult);

  if (includeSpecular){
    float specularBase = saturate(dot(normalize(lightDir - toCloud), normal)) 
      * backlitDot;
    result += pow(specularBase, specularExp) * specularPower;
  }

  return result * lightColor;
}

float4 main(PS_IN_CloudFX pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float2 txNm = txDiffuseValue.xy * 2 - 1;
  float3 toCamera = normalize(pin.PosC);
  float3 toSide = normalize(pin.ToSide);
  float3 toUp = normalize(pin.ToUp);

  float edge = saturate(length(txNm));
  float notEdge = sqrt(1 - edge * edge);
  float3 normal = -toCamera * notEdge + toUp * txNm.y + toSide * txNm.x;

  // float3 light = calculateLight(toCamera, normal, txDiffuseValue, -ksLightDirection.xyz, ksLightColor.xyz, true);
  float3 light = calculateLight(toCamera, normal, txDiffuseValue.a, lightDirection, lightColor, true);

  float noiseMult = edge * edge;
  float alpha = (txDiffuseValue.a - cutoff) * colorOpacity.a;
  [branch]
  if (useNoise){
    alpha = saturate(alpha 
      - txVariation.Sample(samLinearSimple, pin.Tex + noiseOffset).x * cloudNoiseMult * noiseMult);
  } else {
    alpha = saturate(alpha);
  }

  clip(alpha - 0.0001);
  return withFogImpl((ambientColor + txDiffuseValue.z * colorOpacity.rgb + light) * brightnessMult, 
    toCamera * ksFarPlane, toCamera, pin.Fog, fogOpacity, alpha);
}
