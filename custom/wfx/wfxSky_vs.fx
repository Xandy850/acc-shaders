#define USE_PS_FOG
#include "../../recreated/include_new/base/_include_vs.fx"
#include "wfx_common_sky.hlsl"

PS_IN_Sky main(VS_IN vin) {
  PS_IN_Sky vout;
  float3 posN = normalize(vin.PosL.xyz);
  float3 posC = posN * ksFarPlane * 0.99;
  float3 posW = posC + ksCameraPosition.xyz;
  float4 posV = mul(float4(posW, 1), ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosW = posC;
  vout.NormalL = 0;
  vout.Tex = 0;
  vout.LightH = 0;
  return vout;
}
