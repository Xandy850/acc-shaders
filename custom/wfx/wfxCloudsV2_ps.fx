#define USE_PS_FOG
#define USE_BACKLIT_FOG
#define CLOUDS
#include "wfx_common_clouds_v2.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float edgeTilt,
    float alpha, float3 lightDir, float3 lightColor, bool includeSpecular){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, frontlitDiffConcentration * lerp(0.4, 1, alpha)));
  float3 result = frontlitK * frontlitMult;

  float alpha2 = alpha * alpha;
  float backlitDot = saturate(dot(toCloud, lightDir));
  float backlitK = pow(backlitDot, abs(backlitExp) * (2 - alpha2 * 1.5));
  result += backlitK * backlitMult 
    * saturate(pow(saturate(1 - alpha), backlitOpacityExp) + 1 - backlitOpacityMult);

  float contourK = pow(saturate(edgeTilt), contourExp * alpha) * saturate(-normal.y);
  result += contourK * ambientColor * shadowOpacity;

  if (includeSpecular){
    float specularBase = saturate(dot(normalize(lightDir - toCloud), normal)) 
      * backlitDot;
    result += pow(specularBase, specularExp) * specularPower;
  }

  return result * lightColor;
}

float4 main(PS_IN_CloudFX pin) : SV_TARGET {
  if (pin.Tex.y < 0.005 || pin.Tex.y > 0.995
      || pin.Tex.x < 0.005 || pin.Tex.x > 0.995){
    // return float4(3, 0, 0, 1);
  }

  bool isHorizontal = backlitExp < 0;
  float3 toCamera = normalize(pin.PosC);
  #ifndef USE_ALT_CBUFFER
    float3 toSide = normalize(pin.ToSide);
    float3 toUp = normalize(pin.ToUp);
    float3 toForward = -toCamera;
  #endif

  float2 uv = noiseOffset;
  if (!isHorizontal) {
    pin.Tex.y += pow(abs(pin.Tex.x * 2 - 1), 2) * 0.3 * abs(toCamera.y);
    float2 angleV = normalize(toCamera.xz);
    float angle = atan2(angleV.y, angleV.x) / (2 * M_PI);
    uv.x -= 3 * angle / scaleProc.x;
  } else {
    #ifndef USE_ALT_CBUFFER
      toSide = -normalize(sizeWidth.xyz);
      toUp = -normalize(sizeHeight.xyz);
      toForward = normalize(cross(toSide, toUp));
    #endif
  }

  if (sizeWidth.w) uv.x *= -1;
  uv += pin.Tex - 0.5;

  float4 txMaskValue = txDiffuse.Sample(samLinear, texStart + texSize * pin.Tex);
  txMaskValue.xy = txMaskValue.xy * 2 - 1;
  float sharpness = saturate(txMaskValue.z * procSharpnessMult);
  
  float4 txCloudValue = sampleCloud(float3(uv * scaleProc, shapeShiftingProc)); 
  txCloudValue.xy = txCloudValue.xy * 2 - 1;

  float4 txDetailValue = sampleCloud(float3(uv * scaleProc * 5.3, 0.5 + shapeShiftingProc)); 
  txDetailValue.xy = txDetailValue.xy * 2 - 1;

  #ifdef EXTRA_FIDELITY
    [branch]
    if (extraFidelity > 0) {
      float detailMult = extraFidelity * saturate(txMaskValue.a * 2) * saturate(2 - txMaskValue.a * 2);
      txCloudValue.xy = lerp(txCloudValue.xy, txDetailValue.xy, detailMult);
      txCloudValue.zw = saturate(txCloudValue.zw * lerp(1, 1.4 * txDetailValue.zw, detailMult));
    }
  #endif
  
  float4 txCombinedValue = txCloudValue * txMaskValue;
  float th0 = lerp(1, mapProc.x - sharpness * 0.1, txMaskValue.a * (1 - cutoff));
  float th1 = lerp(1.05, lerp(mapProc.y, mapProc.x, sharpness) - sharpness * 0.1, txMaskValue.a * (1 - cutoff));
  txCombinedValue.a = saturate(remap(txCloudValue.a, th0, th1, 0, 1));

  float2 txNm = txCloudValue.xy * normalScaleProc.x + txMaskValue.xy * normalScaleProc.y;
  float edgeTilt = length(txNm);
  float edge = saturate(edgeTilt);
  float notEdge = sqrt(1 - edge * edge);
  if (sizeWidth.w) txNm.x *= -1;

  #ifndef USE_ALT_CBUFFER
    float3 normal = toForward * notEdge + toUp * txNm.y + toSide * txNm.x;
    // normal = normalize(normal);
  #endif

  float MASK = txDiffuse.Sample(samLinear, pin.Tex).a;
  float CLOUD = saturate(txCombinedValue.a) * colorOpacity.a;
  clip(CLOUD - 0.001);
  
  #ifndef USE_ALT_CBUFFER
    float3 light = calculateLight(toCamera, normal, edgeTilt, CLOUD, lightDirection, lightColor, true);
    float3 finalColor = (ambientColor * saturate(lerp(1, normal.y, ambientConcentration)) 
      + colorOpacity.rgb * saturate(-normal.y)
      // + pow(saturate(1 + dot(normal, toCamera)), 20) * lightColor
      + light) * brightnessMult;
    // finalColor = light;
    // finalColor = pow(1 - notEdge, 2);
  #else
    float3 finalColor = 0;
  #endif

  #ifdef COVER_SUN
    finalColor = 0;
    #ifdef USE_SHADOW_OPACITY
      CLOUD = saturate(sqrt(CLOUD) * shadowOpacity);
    #else
      CLOUD = saturate(CLOUD * 3);
    #endif

    #ifdef SHADOW_DITHERING
      float alpha = CLOUD;
      clip(alpha - 0.1);
      uint2 pos = uint2(pin.PosH.xy);
      uint2 xy2 = pos.xy % 2;
      uint2 xy3 = pos.xy % 3;
      alpha *= 0.85;
      if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0) discard;
      if (alpha < 0.8 && xy3.x == 2 && xy3.y == 1) discard;
      if (alpha < 0.7 && xy3.x == 1 && xy3.y == 2) discard;
      if (alpha < 0.6 && xy3.x == 1 && xy3.y == 0) discard;
      if (alpha < 0.5 && xy3.x == 1 && xy3.y == 1) discard;
      if (alpha < 0.4 && xy3.x == 0 && xy3.y == 2) discard;
      if (alpha < 0.3 && xy3.x == 2 && xy3.y == 0) discard;
      if (alpha < 0.2 && xy3.x == 0 && xy3.y == 1) discard;
      return 1;
    #endif

    #ifdef RETURN_ALPHA_AS_COLOR
      return float4(CLOUD.xxx, 1);
    #else
      return float4(0, 0, 0, CLOUD);
    #endif
  #else
    #ifndef FOR_REFLECTIONS
      float backlitDot = saturate(dot(toCamera, lightDirection));
      float backlitK = saturate(pow(backlitDot, CLOUD * 80 + 80) * 1.1 - 0.05);
      backlitK = smoothstep(0, 1, backlitK);
      finalColor += lightColor * backlitK * lerp(0.5, 0, CLOUD);
      CLOUD = saturate(CLOUD * (1 + CLOUD * backlitK));
    #endif
    return withFogImpl(finalColor, 
      toCamera * ksFarPlane, toCamera, pin.Fog, fogOpacity, CLOUD);
  #endif
}
