#define VS
#define USE_PS_FOG
#define CLOUDS
#include "wfx_common.hlsl"

float3 getPosW_(uint id, out float4 posH, out float2 tex, float mult = 1){
  float2 offset = float2(id & 1, id >> 1);
  float4 width = loadVector(sizeWidth);
  float4 height = loadVector(sizeHeight);
  float3 position2 = float3(0, 100, 0);
  // float4 posW = float4(position2 + offset.x * width.xyz + offset.y * height.xyz, 1);
  // if (mult > 1){
  //   posW.xyz += width.xyz * (mult - 1) * (offset.x ? 1 : -1);
  //   posW.xyz += height.xyz * (mult - 1) * (offset.y ? 1 : -1);
  // }
  float4 posW = float4(position2 + offset.x * float3(100, 0, 0) + offset.y * float3(0, 100, 0), 1);
  float4 posV = mul(posW, ksView);
  posH = mul(posV, ksProjection);
  tex = offset;
  if (width.w == 1) tex.x = 1.0 - tex.x;
  if (height.w == 1) tex.y = 1.0 - tex.y;
  return posW.xyz - ksCameraPosition.xyz;
}

PS_IN_CloudFX main(uint id: SV_VertexID) {
  PS_IN_CloudFX vout;
  vout.PosC = getPosW(id, vout.PosH, vout.Tex);
  vout.Fog = calculateFogNew(vout.PosC);
  vout.ToSide = cross(-vout.PosC, float3(0, 1, 0));
  vout.ToUp = cross(vout.PosC, vout.ToSide);
  // vout.PosH = -1;
  return vout;
}
