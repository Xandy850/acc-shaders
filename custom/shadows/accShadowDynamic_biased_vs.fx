#include "accShadowDynamic.hlsl"

PS_IN main(VS_IN_ac vin) {
  return voutFill(vin.PosL.xyz, vin.Tex, 0.5);
}

