struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
  float2 DistanceClip : TEXCOORD2;
};

#include "include_new/base/_include_ps.fx"
#include "include/common.hlsl"

float main(PS_IN pin) : SV_TARGET {
  float alpha = txDiffuse.Sample(samLinear, pin.Tex).a;
  clip(min(alpha - ksAlphaRef, pin.DistanceClip.y));
  return pin.DistanceClip.x;
}

