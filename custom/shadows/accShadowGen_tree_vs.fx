#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"
#include "include_new/ext_functions/wind.fx"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posH = mul(mul(posL, ksWorld), ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windFromTex(vin.Tex), 1) * 2;
  vout.PosH = toScreenSpaceAlt(vin.PosL);
  vout.Tex = vin.Tex;
  return vout;
}

