struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
  float2 DistanceClip : TEXCOORD2;
};

float main(PS_IN pin) : SV_TARGET {
  // return pin.PosH.z + 0.0005;
  clip(pin.DistanceClip.y);
  return pin.DistanceClip.x;
}

