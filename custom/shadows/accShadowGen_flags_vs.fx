#define INCLUDE_FLAGS_CB

#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"
#include "flagsFX.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posH = mul(mul(posL, ksWorld), ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;

  float flagWidth = length(vin.NormalL) - 1000.0;
  if (abs(flagWidth) > 20) flagWidth = 0;
  float2 aoHeight = FFX_loadVector(asuint(vin.TangentPacked.x));

  float3 dir;
  customWave(vin.Tex, vin.PosL.xyz, vin.NormalL, dir, float2(flagWidth, aoHeight.y), vin.TangentPacked.y, vin.TangentPacked.z,
    extWindVel, extWindSpeed, extWindWave);

  vout.PosH = toScreenSpaceAlt(vin.PosL);
  vout.Tex = vin.Tex;
  return vout;
}

