void __pack(out uint value, float2 vec){ value = f32tof16(vec.x) | (f32tof16(vec.y) << 16); }
void __pack(out uint2 value, float4 vec){ __pack(value.x, vec.xy); __pack(value.y, vec.zw); }
void __unpk(uint value, out float2 vec){ vec = float2(f16tof32(value), f16tof32(value >> 16)); }
void __unpk(uint2 value, out float4 vec){ __unpk(value.x, vec.xy); __unpk(value.y, vec.zw); }

void __pack(out uint value, uint vec){ value = vec; }
void __pack(out uint2 value, uint2 vec){ value = vec; }
void __pack(out uint3 value, uint3 vec){ value = vec; }
void __pack(out uint4 value, uint4 vec){ value = vec; }
void __pack(out float value, float vec){ value = vec; }
void __pack(out float2 value, float2 vec){ value = vec; }
void __pack(out float3 value, float3 vec){ value = vec; }
void __pack(out float4 value, float4 vec){ value = vec; }
void __unpk(uint value, out uint vec){ vec = value; }
void __unpk(uint2 value, out uint2 vec){ vec = value; }
void __unpk(uint3 value, out uint3 vec){ vec = value; }
void __unpk(uint4 value, out uint4 vec){ vec = value; }
void __unpk(float value, out float vec){ vec = value; }
void __unpk(float2 value, out float2 vec){ vec = value; }
void __unpk(float3 value, out float3 vec){ vec = value; }
void __unpk(float4 value, out float4 vec){ vec = value; }

void __pack(out uint value, float x, float y){ __pack(value, float2(x, y)); }
void __pack(out uint2 value, float3 vec){ __pack(value, float4(vec, 0)); }
void __pack(out uint2 value, float3 vec, float w){ __pack(value, float4(vec, w)); }
void __pack(out uint2 value, float2 vec, float z, float w){ __pack(value, float4(vec, z, w)); }
void __pack(out uint2 value, float2 vec1, float2 vec2){ __pack(value, float4(vec1, vec2)); }
void __pack(out uint2 value, float x, float y, float z, float w){ __pack(value, float4(x, y, z, w)); }

void __pack(out float2 value, float x, float y){ __pack(value, float2(x, y)); }
void __pack(out float4 value, float3 vec, float w){ __pack(value, float4(vec, w)); }
void __pack(out float4 value, float2 vec, float z, float w){ __pack(value, float4(vec, z, w)); }
void __pack(out float4 value, float x, float y, float z, float w){ __pack(value, float4(x, y, z, w)); }

void __unpk(uint value, out float x, out float y){ float2 R; __unpk(value, R); x = R.x; y = R.y; }
void __unpk(uint2 value, out float3 vec){ float4 R; __unpk(value, R); vec = R.xyz; }
void __unpk(uint2 value, out float3 vec, out float w){ float4 R; __unpk(value, R); vec = R.xyz; w = R.w; }
void __unpk(uint2 value, out float2 vec, out float z, out float w){ float4 R; __unpk(value, R); vec = R.xy; z = R.z; w = R.w; }
void __unpk(uint2 value, out float2 vec1, out float2 vec2){ float4 R; __unpk(value, R); vec1 = R.xy; vec2 = R.zw; }
void __unpk(uint2 value, out float x, out float y, out float z, out float w){ float4 R; __unpk(value, R); x = R.x; y = R.y; z = R.z; w = R.w; }

void __unpk(float2 value, out float x, out float y){ float2 R; __unpk(value, R); x = R.x; y = R.y; }
void __unpk(float4 value, out float3 vec, out float w){ float4 R; __unpk(value, R); vec = R.xyz; w = R.w; }
void __unpk(float4 value, out float2 vec, out float z, out float w){ float4 R; __unpk(value, R); vec = R.xy; z = R.z; w = R.w; }
void __unpk(float4 value, out float2 vec1, out float2 vec2){ float4 R; __unpk(value, R); vec1 = R.xy; vec2 = R.zw; }
void __unpk(float4 value, out float x, out float y, out float z, out float w){ float4 R; __unpk(value, R); x = R.x; y = R.y; z = R.z; w = R.w; }

