#ifdef DEPTH_COLLISIONS
  Texture2D<float> txDepth : register(t17);
  Texture2D txNormals : register(t18);

	cbuffer cbDepthData : register(b10) {
		float3 de_CameraPosition;
		float de_CameraFOV;

		uint2 de_Resolution;
		float de_ZFarP;
		float de_ZNearP;

		float4x4 de_VP;
		float4x4 de_VPInv;
	};

  float getLinearDepth(float depth) {
    return 2.0 * de_ZNearP * de_ZFarP / (de_ZFarP + de_ZNearP - (2.0 * depth - 1.0) * (de_ZFarP - de_ZNearP));
  }

  float3 ssGetPos(float2 uv, float depth){
    float4 p = mul(float4(uv.xy, depth, 1), de_VPInv);
    return p.xyz / p.w;
  }

  float3 ssGetUV(float3 position){
    float4 p = mul(float4(position, 1), de_VP);
    return p.xyz / p.w;
  }
#endif

float collisionCheck(inout float3 pos, inout float3 velocity, float4 collisionPlane, float particleHeight, 
    float bounceK, float dragContact){
  float planeDistance = distanceToPlane(collisionPlane, pos);
  if (planeDistance < particleHeight){
    velocity -= collisionPlane.xyz * dot(velocity, collisionPlane.xyz) * bounceK;
    velocity *= dragContact;
    pos += collisionPlane.xyz * (-planeDistance + particleHeight + 0.001);
  }
  return planeDistance;
}