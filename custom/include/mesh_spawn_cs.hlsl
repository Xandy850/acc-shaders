float3 unpackNormal(float v){
	uint v_u = asuint(v);
	float3 ret;
	ret.x = (float)((v_u >> 0) & 0x000000FF);
	ret.y = (float)((v_u >> 8) & 0x000000FF);
	ret.z = (float)((v_u >> 16) & 0x000000FF);
	return ret / 255 * 2 - 1;
}

struct MeshPoint {
  float3 pos;
  float3 normal;
};

MeshPoint randomMeshPoint(ByteAddressBuffer vertices, StructuredBuffer<uint> indices, uint indicesSize, RAND_DECL){
	uint tri = (uint)((indicesSize / 3) * RAND);
	float f = RAND;
	float g = RAND;
	[flatten]
	if (f + g > 1) {
		f = 1 - f;
		g = 1 - g;
	}

	uint i0 = indices[tri * 3];
	uint i1 = indices[tri * 3 + 1];
	uint i2 = indices[tri * 3 + 2];

	float4 v0 = asfloat(vertices.Load4(i0 * 16));
	float4 v1 = asfloat(vertices.Load4(i1 * 16));
	float4 v2 = asfloat(vertices.Load4(i2 * 16));

	float3 n0 = unpackNormal(v0.w);
	float3 n1 = unpackNormal(v1.w);
	float3 n2 = unpackNormal(v2.w);

  MeshPoint ret;
	ret.pos = v0.xyz + f * (v1.xyz - v0.xyz) + g * (v2.xyz - v0.xyz);
	ret.normal = n0 + f * (n1 - n0) + g * (n2 - n0);
  return ret;
}

MeshPoint randomMeshPoint(ByteAddressBuffer vertices, StructuredBuffer<uint> indices, uint indicesSize, float4x4 transform, RAND_DECL){
	MeshPoint ret = randomMeshPoint(vertices, indices, indicesSize, RAND_ARGS);
  ret.pos = mul(transform, float4(ret.pos, 1)).xyz;
	ret.normal = normalize(mul((float3x3)transform, ret.normal));
  return ret;
}