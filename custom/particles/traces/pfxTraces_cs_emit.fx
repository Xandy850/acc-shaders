#include "pfxTraces.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL){
	particle.posA = E.emitterPos;
	particle.posB = E.emitterPosPrev;
	particle.posM = (particle.posA + particle.posB) / 2;

	particle.life = 1;
	particle.size = E.glow;
}
