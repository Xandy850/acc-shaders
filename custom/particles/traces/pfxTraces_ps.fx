#include "pfxTraces.hlsl"
#include "include_new/ext_functions/depth_map.fx"

float4 main(PS_IN pin) : SV_TARGET {
  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    float depthZ = getDepth(pin.PosH);
    float depthC = linearize(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * (1.05 - pin.PosH.z * pin.PosH.z) * 3e5);
    softK *= softK;
  #endif

  float2 tex = pin.Tex;
  tex.x = saturate(abs(tex.x) - pin.RoundK) * sign(tex.x) / max(1 - pin.RoundK, 0.001);
  clip(1 - dot(tex, tex));

  float3 color = xColor * max(pin.Brightness, 0.1);
  return float4(color, softK * saturate(pin.Brightness * 10));
}