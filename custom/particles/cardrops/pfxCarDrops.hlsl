// #define PARTICLES_PACKING
#define PARTICLES_MESH_SPAWN
#define PARTICLES_WITHOUT_NORMALS
#define STENCIL_VALUE 0.2
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float3 velocity;
	float size;
	float4 collisionPlane;
};

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		uint meshID;
		uint meshIndexCount;
		float velocityVariance;

		float3 direction;
		float noCarCollisions;

		float3 velocity;
		float pad2;
		
		float4x4 transform;
		float4 collisionPlane;
	};

	#define FORCE_FIELDS 4

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
    uint gCarCollision;
		float2 gPad0;    

		ForceField gForceFields[FORCE_FIELDS];
		float4x4 gShadowTransform;
    float4x4 gCarCollisionTransform;
	}

	Texture2D<float> txCarCollision : register(t2);
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN

		float3 NormalBase : POSITION1;
		float3 LightVal : COLOR5;
		float3 LightDir : POSITION3;
		float LightFocus : POSITION4;
		float BlurK : POSITION5;
		// float AlphaMult : POSITION6;
	};

	cbuffer cbPieces : register(CBUFFER_DRAW_SLOT) {
		float3 gCameraVelocity;
		float gUseColor;
	}
#endif

#include "particles/common/include_impl.hlsl"
#include "rainUtils.hlsl"
