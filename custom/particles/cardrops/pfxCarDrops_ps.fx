#define PARAM_BLUR pin.BlurK
#define PARAM_USE_COLOR gUseColor
#include "pfxCarDrops.hlsl"

PS_OUT main(PS_IN pin) {
  clip(1 - dot2(pin.Tex));

  float3 toCamera = normalize(pin.PosC);
  float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

  float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
  float3 normal = normalize(pin.NormalBase + -toCamera * facingShare);
  float fresnel = saturate(pow(1 + dot(toCamera, normal), 2));

  float3 refraction = lerp(toCamera, -normal, 0.5);
  float3 reflection = reflect(toCamera, normal);
  float3 reflectionColor = sampleEnv(reflection, 3, 0);
  float3 refractionColor = sampleEnv(refraction, 3, 0);
  float3 refractionBlurred = lerp(sampleEnv(toCamera, 4, 1), sampleEnv(float3(0, 1, 0), 4, 0), 0.1);

  if (1){
    float3 side = normalize(cross(float3(0, 1, 0), toCamera));
    float2 fakeRefr = float2(dot(side, normal), normal.y);
    float2 ssUV = pin.PosH.xy * extScreenSize.zw;
    refractionColor = txPrevFrame.SampleBias(samLinearClamp, ssUV + fakeRefr * 0.4 * lerp(0.4, 0.2, facingShare), -1.5).rgb;
  }

  refractionColor = lerp(refractionColor, refractionBlurred, pin.BlurK);

  float3 lighting = lerp(refractionColor, reflectionColor, fresnel);
  lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
    lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus));

  float blur = abs(pin.Tex.y) * pin.BlurK;
  float alpha = 1;
  alpha *= lerp(1, saturate(remap(abs(pin.Tex.y), 0.2, 0.8, 1, 0)) * 0.75, pin.BlurK) * softK;

  // lighting = float3(4, 0, 0);
  // alpha = 1;
  // alpha *= pin.AlphaMult;

  RETURN_BASE(lighting, alpha);
}