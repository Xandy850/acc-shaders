const sh = path.basename(filename, `.js`);
add(`${sh}_cs_simulate.fx`, { saveAs: `${sh}_cs_simulate_depth.fxo`, defines: { 'DEPTH_COLLISIONS': 1 } });
add(`${sh}_vs.fx`, { saveAs: `${sh}_vs_simple.fxo`, defines: { 'MODE_SIMPLE': 1 } });
add(`${sh}_ps.fx`, { saveAs: `${sh}_ps_simple.fxo`, defines: { 'MODE_SIMPLE': 1 } });
// add(`${sh}_vs.fx`, { saveAs: `${sh}_vs_shadow.fxo`, defines: { 'MODE_SHADOW': 1 } });
// add(`${sh}_ps.fx`, { saveAs: `${sh}_ps_shadow.fxo`, defines: { 'MODE_SHADOW': 1 } });
// add(`${sh}_vs.fx`, { saveAs: `${sh}_vs_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
// add(`${sh}_ps.fx`, { saveAs: `${sh}_ps_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
// add(`${sh}_ps.fx`, { saveAs: `${sh}_ps_gbuffer_refl.fxo`, defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1 } });