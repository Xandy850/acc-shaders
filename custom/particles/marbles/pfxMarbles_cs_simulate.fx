#include "pfxMarbles.hlsl"

void forceFields(float3 pos, inout float3 velocity) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot(diff, diff);
    float distK = saturate(1 - dist * gForceFields[i].radiusSqrInv);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult * 30;
    float3 forceVelSync = (gForceFields[i].velocity - velocity) * abs(gForceFields[i].velocity) * 0.4;
    velocity += distK * distK * (forcePushAway + forceVelSync) * gFrameTime * float3(1, 0.3, 1);
  }
}

void process(inout Particle particle, uint3 DTid) {
  #ifdef DEPTH_COLLISIONS
    particle.life -= particle.life < 0.5 ? 1 : DTid.x > gALS2 ? 100 : DTid.x > gALS1 ? 1 : 0;
  #else
    particle.life -= particle.life < 0.5 ? 1 : DTid.x > gALS2 ? 100 : DTid.x > gALS1 ? 2 : 1;
  #endif

	float bounceK = 1.2;
	float particleHeight = particle.size * 0.67 - 0.005;
  float dragContact = 1.0 / (1.0 + gFrameTime * lerp(20, 200, saturate(1 - length(particle.velocity) / 2)));
	if (length(particle.velocity) > 1){
		particle.pos += particle.velocity * gFrameTime;
		particle.life -= max(-particle.velocity.y * gFrameTime - 0.01, 0);
	}
	particle.velocity.y += gGravity;
	float distanceToGround = collisionCheck(particle.pos, particle.velocity, particle.collisionPlane1, 
		particleHeight, bounceK, dragContact);
	forceFields(particle.pos, particle.velocity);

  #ifdef DEPTH_COLLISIONS
    float3 posC = particle.pos - de_CameraPosition;
    float depthCalc = length(posC);
    float2 ssUV = ssGetUV(posC).xy;

    [branch]
    if (depthCalc < 200 && gFrameTime > 0 && ssUV.x > 0 && ssUV.x < 1 && ssUV.y > 0 && ssUV.y < 1) {
      float3 p0 = ssGetPos(ssUV, txDepth.SampleLevel(samPointClamp, ssUV, 0));
      float3 surfaceNormal = ssNormalDecode(txNormals.SampleLevel(samPointClamp, ssUV, 0).xyz);
      float distanceDepth = dot(posC - p0, surfaceNormal);
      float falledDown = -distanceDepth;
			float behind = length(depthCalc) - length(p0);

      [branch]
			if (behind < 0){
				float3 candidate = posC - float3(0, 0.12, 0);
				float2 ssUV2 = ssGetUV(candidate).xy;
				float3 p1 = ssGetPos(ssUV2, txDepth.SampleLevel(samPointClamp, ssUV2, 0));
				if (length(p1) > length(depthCalc)){
					particle.collisionPlane1.xyz = float3(0, 1, 0);
					particle.collisionPlane1.w = particle.pos.y - 0.12;
				}
			} else if (behind < 0.1 + lerp(0, depthCalc / 200, saturate(surfaceNormal.y * 2 - 0.5))) {
        particle.velocity -= surfaceNormal * dot(particle.velocity, surfaceNormal) * bounceK;
        particle.velocity *= dragContact;
				particle.pos.y += behind * (posC / depthCalc).y;
			}
    }
  #endif
}
