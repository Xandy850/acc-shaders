#include "pfxMirageDisruption.hlsl"

void process(inout Particle particle, uint3 DTid) {
  particle.life -= gFrameTime / 4;
}
