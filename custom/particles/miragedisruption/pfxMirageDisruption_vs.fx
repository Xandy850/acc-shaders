#include "pfxMirageDisruption.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
  float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = side * quadPos.x + up * quadPos.y;

	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset * float3(5, offset.y > 0 ? 7.2 : 0.8, 5) * lerp(0.5, 1, saturate(particle.intensity * 1.5 - 0.5));
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	float3 posC = posW.xyz - ksCameraPosition.xyz;
	float dist = dot(posC, posC);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Intensity = saturate((1 - particle.life) * 6) * saturate(particle.life * 2) * saturate((dist - 40000) / 100) * saturate(particle.intensity * 1.5);
	// vout.Tex.x = BILLBOARD[vertexID].x;
	// vout.Tex.y = saturate((1 - particle.life) * 20) * pow(saturate(particle.life), 2) * particle.intensity;
	// vout.Tex.y = particle.intensity;

	// uint id = fakeIndex % 4;
  // float2 v = float2(id % 2, id / 2) * 2;
  // vout.Tex = float2(v.x, 1 - v.y);
  // vout.PosH = float4(v * 2 - 1, 0, 1);

	return vout;
}