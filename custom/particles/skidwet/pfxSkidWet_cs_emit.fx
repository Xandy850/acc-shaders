#include "pfxSkidWet.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = E.emitterPos;
	particle.dpos0 = E.dpos0;
	particle.dpos1 = E.dpos1;
	particle.dpos2 = E.dpos2;
	particle.life = E.life;
	particle.intensity = E.intensity;
}
