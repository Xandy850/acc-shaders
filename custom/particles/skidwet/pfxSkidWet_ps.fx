#include "pfxSkidWet.hlsl"

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);
  float3 lighting = float3(pin.Tex.y * saturate((1 - abs(pin.Tex.x)) * 2), 0, 0);
  float alpha = 1;
  RETURN_BASE(lighting, alpha);
}