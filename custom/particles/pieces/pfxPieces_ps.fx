#include "pfxPieces.hlsl"

#ifdef MODE_SIMPLIFIED_FX
  #define SHADOW 1
  #define SPECULAR 0
  #define USE_REFLECTIONS 0
#elif defined(MODE_GBUFFER)
  #define SHADOW 1
  #define SPECULAR 0
  #define USE_REFLECTIONS pin.UseReflections
#else
  #define SHADOW pin.Shadow
  #define SPECULAR pin.Specular
  #define USE_REFLECTIONS pin.UseReflections
#endif

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);
  float4 txPiecesValue = txMain.Sample(samLinear, pin.Tex);

  bool isGlass = dot(pin.Color, 1) > 30;
  if (isGlass){
    pin.Color = 0.1;
  }

  if (!pin.UseTexture){
    float chunkK = saturate(pin.Color.z / 0.4 - 1);
    float4 txNoiseValue = txNoise.Sample(samLinear, pin.Tex / lerp(20, 15, chunkK) + pin.Color.xy + pin.NormalW.x / 20);
    float noiseVal = lerp(0.6 + 0.8 * txNoiseValue.x, 0.4 + 1.2 * txNoiseValue.x, chunkK);
    txPiecesValue = float4(pin.Tex * 0.5 + 0.5, 1, noiseVal - dot(pin.Tex, pin.Tex) > 0 ? 1 : 0);
    pin.Color = pin.Color.z * (0.8 + 0.4 * txNoiseValue.y);
    if (pin.Color.z > 2){
      pin.Color = (gDustColorA + gDustColorB * txNoiseValue.y) / 6;
    }
  }

  txPiecesValue.xyz = 2 * txPiecesValue.xyz - 1;
  txPiecesValue.xy *= -1;
  txPiecesValue.xy = mul(rotate2d(pin.SinCos.x, pin.SinCos.y), txPiecesValue.xy);

	#ifndef MODE_SIMPLIFIED_FX
    float3 normalW = normalize(txPiecesValue.x * pin.TangentW
      + txPiecesValue.y * pin.BitangentW
      + txPiecesValue.z * pin.NormalW);
  #else
    float3 normalW = pin.NormalW;
    pin.Color *= 0.7 + 0.3 * txPiecesValue.y;
  #endif

  float fresnel = 0;
  float3 reflColor = 0;

  [branch]
  if (USE_REFLECTIONS){
    float facingToCamera = dot(normalW, -toCamera);
    fresnel = (0.03 + 0.17 * pow(saturate(1 - facingToCamera), 5)) * (isGlass ? 5 : 1);
    float3 reflDir = fixReflDir(normalize(reflect(toCamera, normalW)), pin.PosC, normalW, 0);
    reflColor = fresnel * getReflectionAt(reflDir, -toCamera, 0, false /* can’t use bias in a branch */, 0);
  }

	#ifdef MODE_GBUFFER
    float3 lighting = 0;
  #else
    txPiecesValue.rgb = pin.Color * 0.08;
    
    float3 ambient = getAmbientBaseAt(normalW, AO_LIGHTING.w);
    float litFront = saturate(dot(normalW, -ksLightDirection.xyz) * 0.5 + 0.5);
    float litBack = pow(saturate(-dot(normalW, -ksLightDirection.xyz)), 4) * 0.67;
    float3 diffuse = AO_LIGHTING.w * (ksLightColor.rgb * SHADOW * (litFront + litBack) + pin.Lighting);
    float3 lighting = txPiecesValue.rgb * (ambient + diffuse + GI_LIGHTING);

    if (SPECULAR){
      lighting += txPiecesValue.rgb * reflectanceModel(-toCamera, -ksLightDirection.xyz, normalW, 40)
        * 4 * SHADOW * extSpecularColor;
      lighting += txPiecesValue.rgb * reflectanceModel(-toCamera, -ksLightDirection.xyz, normalW, 600)
        * 80 * SHADOW * extSpecularColor;
    }

    lighting += reflColor;
  #endif

  #ifdef MODE_GBUFFER
    ReflParams R = (ReflParams)0;
    R.resultPower = fresnel;
    R.resultColor = reflColor;
  #endif

  // lighting = 1;
  // txPiecesValue.a = 1;
  // clip(txPiecesValue.a - 0.1);

  clip(txPiecesValue.a - 0.1);
  RETURN(lighting, txPiecesValue.a);

  // float4 c = txPieces.Sample(samLinearSimple, pin.Tex);
  // c.rgb *= pin.Color;
  // // c.a = 1;
  // return c;

  // float2 tex = pin.Tex;
  // clip(1 - dot(tex, tex));
  // return float4(0, 0, 0, 1);
  // return float4(4, pin.Debug * 4, 1, 1);
  // tex.x = saturate(abs(tex.x) - pin.RoundK) * sign(tex.x) / max(1 - pin.RoundK, 0.001);

  // float3 color = xColor * max(pin.Brightness, 0.1);
  // return float4(color, softK * saturate(pin.Brightness * 10));
}