#include "pfxPieces.hlsl"

Texture2D txGrass : register(t20);
Texture2D txColor1 : register(t4);
Texture2D txColor2 : register(t5);
Texture2D txColor3 : register(t6);
Texture2D txColor4 : register(t7);

ByteAddressBuffer meshVertices : register(t8);
StructuredBuffer<uint> meshIndices : register(t9);
// globallycoherent RWByteAddressBuffer collectedGrassColors : register(u5);
Texture2D collectedGrassColorsPrev : register(t10);
RWTexture2D<float4> collectedGrassColors : register(u5);

bool alignWithMesh(inout float3 pos, inout float3 velocity, EmittingSpot E, ByteAddressBuffer vertices, StructuredBuffer<uint> indices, RAND_DECL){
	MeshPoint M = randomMeshPoint(vertices, indices, E.meshIndexCount, E.transform, RAND_ARGS);
	if (E.type == SRCTYPE_MESH_TEST){
		pos = M.pos;
		velocity = lerp(M.normal, float3(0, 1, 0), E.emitterVel1.y) * E.emitterVel1.x;
		return true;
	}
	if (dot2(M.pos - pos) < E.meshThresholdSqr && dot(M.normal, normalize(velocity)) > 0){
		pos = M.pos;
		velocity = normalize(M.normal + normalize(velocity) * 2) * max(length(velocity), 1);
		return true;
	}
	return false;
}

void alignWithMesh(inout float3 pos, inout float3 velocity, EmittingSpot E, RAND_DECL){
	[branch]
	if (E.meshID == 1){
		alignWithMesh(pos, velocity, E, meshVertices, meshIndices, RAND_ARGS)
		|| alignWithMesh(pos, velocity, E, meshVertices, meshIndices, RAND_ARGS);
	}
}

void syncColor(inout float3 color, EmittingSpot E, RAND_DECL){
	float2 uv = float2(0.25 + RAND * 0.5, 0.25 + RAND * 0.5);
	if (E.textureID == 1){
		color = txColor1.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 2){
		color = txColor2.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 3){
		color = txColor3.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 4){
		color = txColor4.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else {
		color = 0.04;
	}
	color = clamp(color, 0, 0.4) * 10;
	if (RAND > 0.3 || dot(color, 1) == 0){
		color = 0.2;
	}
	color += float3(RAND, RAND, RAND) * 0.1;
	color += RAND * 0.4;
	if (RAND > 0.95){
		color = 100;
	}
}

void emit(inout Particle particle, EmittingSpot E, RAND_DECL){	
	float3 emitterPos = E.emitterPos1;
	float3 emitterVel = E.emitterVel1;

	if (E.emitterExtraPoints >= 1 && RAND > 0.5){
		emitterPos = E.emitterPos2;
		emitterVel = E.emitterVel2;
	}

	if (E.emitterExtraPoints >= 2 && RAND > 0.667){
		emitterPos = E.emitterPos3;
		emitterVel = E.emitterVel3;
	}

	particle.pos = emitterPos;
	particle.velocity = emitterVel * (0.5 + RAND * 0.25) 
		+ normalize((float3(RAND, RAND, RAND) - 0.5)) * RAND * length(emitterVel) * E.randomMult;
	particle.flipUV = RAND > 0.5;
	particle.collisionPlane1 = E.emitterPlane1;
	// particle.collisionPlane1.w += 1;
	particle.collisionPlane2 = E.emitterPlane2;

	float3 grassColor = float3(1.2, 1.6, 0.8);
	float sizeMult = 1;
	bool dustPiece = false;
	bool noGrass = gMapPointA.x == gMapPointB.x;
	if (E.type == SRCTYPE_OUTSIDE){
		float2 mapUV = (emitterPos.xz - gMapPointB) / (gMapPointA - gMapPointB);
		float4 grass = noGrass 
			? float4(E.grassFallbackColor / 6 + float3(RAND, RAND, RAND) / 35, E.isSurfaceGrassFallback) 
			: txGrass.SampleLevel(samLinearClamp, mapUV, 0);
		if (grass.a > 0.7) {
			particle.type = GEN_OUTSIDE_ADJ(E.typeGrassChance * grass.a);
			grassColor = 10 * grass.rgb / grass.a;
		} else {
			particle.type = GEN_OUTSIDE_NOGRASS;
		}
		dustPiece = true;
	} else if (E.type == SRCTYPE_COLLISION_CAR) {
		particle.type = GEN_COLLISION_CAR;
	} else if (E.type == SRCTYPE_COLLISION_TRACK) {
		particle.type = GEN_COLLISION_TRACK;
	} else if (E.type == SRCTYPE_MESH_TEST) {
		particle.type = GEN_COLLISION_CAR;
	} else if (E.type == SRCTYPE_LAWNMOWER) {
		// return;
		float2 mapUV = (emitterPos.xz - gMapPointB) / (gMapPointA - gMapPointB);
		float4 grass = noGrass 
			? float4(E.grassFallbackColor / 6 + float3(RAND, RAND, RAND) / 35, E.isSurfaceGrassFallback) 
			: txGrass.SampleLevel(samLinearClamp, mapUV, 0);
		if (grass.a > 0.7) {
			particle.type = GEN_OUTSIDE_GRASS;
			grassColor = 10 * grass.rgb / grass.a;
		} else {
			return;
		}
		sizeMult = 0.8;
		if (RAND > 0.95){
			uint sampleID = uint(RAND * COLLECTED_GRASS_SAMPLES_SIZE);
			collectedGrassColors[uint2(sampleID, E.meshIndexCount)] = float4(grassColor.rgb / 10, 1);
		}
	} else if (E.type == SRCTYPE_LAWNMOWER_LEAK){
		particle.type = GEN_OUTSIDE_GRASS;
		uint sampleID = uint(RAND * COLLECTED_GRASS_SAMPLES_SIZE);
		float4 sampleColor = collectedGrassColorsPrev[uint2(sampleID, E.meshIndexCount)] * 10;
		if (sampleColor.a < 0.9) return;
		grassColor = sampleColor.rgb;
		sizeMult = 0.6;
	}

	float3 posSpread = (float3(RAND, RAND, RAND) - 0.5) * E.posSpread;
	if (E.emitterPlane2.w > -1e8){
		particle.pos += posSpread - E.emitterPlane2.xyz * dot(posSpread, E.emitterPlane2.xyz);
	}

	if (IS_CHUNK(particle)){
		particle.color = float3(RAND, RAND, dustPiece ? 10 : E.type == SRCTYPE_OUTSIDE ? 0.6 : 0.4);
		particle.life = 6 + 6 * pow(RAND, 2);
		particle.halfSize = 0.015 + pow(RAND, 4) * 0.02;
		particle.spinSpeed = RAND * 800 - 400;

		if (E.type == SRCTYPE_COLLISION_TRACK){
			particle.halfSize *= 2;	
			particle.color = float3(RAND, RAND, 0.9);
		}
	} else if (IS_GRASS(particle)) {
		particle.halfSize = (0.08 + pow(RAND, 4) * 0.24) * 0.33;
		particle.life = 12 + 12 * pow(RAND, 2);
		particle.color = grassColor;
		particle.velocity *= 0.8;
		particle.spinSpeed = RAND * 100 - 50;
		particle.angle = 3.14;
	} else if (IS_SOIL(particle)) {
		particle.halfSize = (0.03 + pow(RAND, 4) * 0.12) * 0.75;
		particle.life = 12 + 12 * pow(RAND, 2);
		particle.color = E.soilColor;
		particle.spinSpeed = RAND * 40 - 20;
	} else /* if (IS_SHARD(particle)) */ {
		particle.halfSize = (0.04 + pow(RAND, 4) * 0.08) * 0.75;
		particle.life = 16 + 16 * pow(RAND, 2);
		syncColor(particle.color, E, RAND_ARGS);
		alignWithMesh(particle.pos, particle.velocity, E, RAND_ARGS);
		particle.spinSpeed = RAND * 800 - 400;
	}

	particle.halfSize *= sizeMult;
	particle.posSmooth = particle.pos;
	particle.velocitySmooth = particle.velocity;
}
