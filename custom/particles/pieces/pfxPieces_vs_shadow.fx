#include "pfxPieces.hlsl"

PS_IN_shadow main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	// float3 velocity = mul(particle.velocity, (float3x3)ksView);
	// float speed = length(velocity) + 0.01;
	// float quadDot = dot(quadPos, velocity);
	// quadPos += velocity * quadDot / speed / 2;

	[branch]
	if (particle.landedK > 0.8){
		PS_IN_shadow vout = (PS_IN_shadow)0;
		vout.PosH = -100;
		return vout;
	}

	float angle = particle.angle;
	float angleSin = sin(angle);
	float angleCos = cos(angle);
	quadPos.xy = mul(rotate2d(angleSin, angleCos), quadPos.xy);
	
	quadPos *= particle.pad2;
	particle.posSmooth += ksLightDirection.xyz * (0.25 + 0.2 * saturate(1 - particle.lifePassed)) * particle.halfSize * 4;

	PS_IN_shadow vout;
	vout.PosH = float4(particle.posSmooth, 1);

  float3 side = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
  float3 up = normalize(cross(ksLightDirection.xyz, side));

	float3 sideFlat = normalize(float3(side.x, 0, side.z));
	float3 upFlat = normalize(float3(up.x, 0, up.z));
	sideFlat = normalize(cross(float3(0, 1, 0), upFlat));

  side = lerp(side, sideFlat, saturate(-ksLightDirection.y * 2));
  up = lerp(up, upFlat, saturate(-ksLightDirection.y * 2));
  side = normalize(side);
  up = normalize(up);

	vout.PosH.xyz += side * quadPos.x;
	vout.PosH.xyz += up * quadPos.y;

	// vout.PosY = vout.PosH.y;
	// vout.GroundY = particle.groundY;
	vout.PosH = mul(vout.PosH, ksView);
	vout.PosH = mul(vout.PosH, ksProjection);
	vout.Opacity = 1 - particle.landedK;

	if (IS_CHUNK(particle)){
		vout.UseTexture = 0;
		vout.Tex = BILLBOARD[vertexID].xy;
		vout.Color = particle.color;
	} else {
		vout.UseTexture = 1;
		vout.Tex = BILLBOARD[vertexID].xy * 0.5 + 0.5;
		vout.Tex.y = (vout.Tex.y + particle.type) / TYPE_COUNT;
		if (particle.flipUV > 0.5) vout.Tex.x = 1 - vout.Tex.x;
		vout.Color = 0;
	}
	
	// vout.TexMovement = vout.Tex * 0.6 + particle.texMovement;
  // float spreadK = saturate(particle.size / 2.2 - 0.6);
	// vout.Opacity = pow(saturate(particle.life * particle.opacityK), 0.6) * (1 - spreadK);
  // vout.Opacity *= saturate(particle.thickness * particle.thickness * 1.8);
	// if (vout.Opacity < 0.1) vout.PosH = 10;
	// vout.Offset2 = uint2(particle.random * 6, frac(particle.life * 100) * 6);
	return vout;
}