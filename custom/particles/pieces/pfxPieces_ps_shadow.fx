#include "pfxPieces.hlsl"

float4 main(PS_IN_shadow pin) : SV_TARGET {
  float4 txPiecesValue = txMain.Sample(samLinearSimple, pin.Tex);
  if (!pin.UseTexture){
    txPiecesValue = float4(1, 1, 1, 1 - dot2(pin.Tex) > 0 ? 1 : 0);
  }

  float alpha = pin.Opacity * txPiecesValue.a;
  uint2 pos = uint2(pin.PosH.xy);
  uint2 xy2 = pos.xy % 2;
  if (alpha < 0.7 && xy2.x == 0 && xy2.y == 0) discard;
  if (alpha < 0.5 && xy2.x == 1 && xy2.y == 1) discard;
  if (alpha < 0.3 && xy2.x == 1 && xy2.y == 0) discard;
  if (alpha < 0.1) discard;

  return 1;
}