#include "pfxFireworks_smoke.hlsl"

Texture2D txSparkingGlow : register(t0);
Texture2D txSmokeNoise : register(t4);

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);

  float3 lighting = pin.Color;
  float4 tex = txSmokeNoise.Sample(samLinearSimple, pin.Tex * pin.TexScale + pin.TexOffset);
  float alpha = (saturate(1 - dot2(pin.Tex)) - tex.a) * pin.Opacity;
  lighting = pin.Color * (saturate(tex.y) * ksAmbientColor_sky.rgb * 0.4
    + saturate(dot(pin.OffsetW, -ksLightDirection.xyz)) * ksLightColor.rgb * 0.2);

  float4 glow = txSparkingGlow.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw + (tex.xy - 0.5) * 0.3, 5.5)
    + txSparkingGlow.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw + (tex.xy - 0.5) * 0.1, 2.5) * 0.1;

  // alpha = 1;
  lighting += glow.xyz * 1;

  RETURN_BASE(lighting, saturate(alpha));
}