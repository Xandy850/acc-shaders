#include "pfxSmoke.hlsl"

RWStructuredBuffer<Particle> particleBuffer : register(u0);
RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);
RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);
RWStructuredBuffer<uint> deadBuffer : register(u3);
RWByteAddressBuffer counterBuffer : register(u4);
RWStructuredBuffer<float> distanceBuffer : register(u6);
StructuredBuffer<Wheel> wheels : register(t1);
StructuredBuffer<Car> cars : register(t2);

float getPlaneDistance(float3 plane_nm, float plane_dist, float3 p) {
  return dot(plane_nm, p) - plane_dist;
}

// void forceFields(float maxSize, float dt, inout Particle particle) {
//   for (uint i = 0; i < FORCE_FIELDS; i++) {
//     float dtMult = dt * saturate(particle.lifeOut - xForceFields[i].lifeOffset);
//     float3 dist = particle.position - xForceFields[i].pos;
//     float distSqr = dot(dist, dist);
//     float radius = xForceFields[i].radiusSqr + particle.size * particle.size;
//     if (distSqr < radius) {
//       float3 dir = normalize(dist);
//       dir.y /= 4;
//       float forceMult = xForceFields[i].forceMult * clamp(radius / distSqr, 0, 12) * dtMult;
//       particle.velocity += dir * forceMult;
//       particle.size += forceMult * xForceFields[i].sizeMult;
//       particle.size = min(particle.size, maxSize);
//       particle.moved = max(particle.moved, dtMult > 0 ? 1 : 0);
//     }
//   }
// }

void forceFields(float maxSize, inout Particle particle) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float dtMult = gFrameTime * saturate(particle.lifeOut - 0.35);
    float3 diff = particle.position - xForceFields[i].pos;
    float dist = dot(diff, diff) / 4;
    float distK = saturate(1 - dist * xForceFields[i].radiusSqrInv);
    float3 forcePushAway = normalize(diff) * xForceFields[i].forceMult * 10;
    float3 forceVelSync = (xForceFields[i].velocity - particle.velocity) * abs(xForceFields[i].velocity) * 0.5;
    float3 forceSum = distK * distK * (forcePushAway + forceVelSync) * dtMult;
    float force = length(forceSum);
    forceSum.y = abs(forceSum.y) + force / 10;
    particle.velocity += forceSum / 4;
    // particle.size += force / 200;
    // particle.size = min(particle.size, maxSize);
  }
}

void planeCollisionCheck(CollisionPlane plane, float3 position, inout float3 delta, float size){
  float3 fwd = float3(plane.fwd_x, plane.fwd_y, plane.fwd_z);
  float3 p = position - plane.pos;
  float d = dot(p, plane.up);
  delta += plane.up * max(size - abs(d), 0) * sign(d) * all(abs(float2(dot(p, fwd), dot(p, plane.side))) < 1);
}

void planesCollisionsCheck(float3 position, inout float3 delta, float size){
  for (uint i = 0; i < COLLISION_PLANES; i++) {
    planeCollisionCheck(xCollisionPlanes[i], position, delta, size);
  }
}

void carCollisionsCheck(Car car, float3 position, inout float3 delta, float size){
  for (int i = 0; i < 3; i++) {
    float3 sphereDif = position - car.bs[i].xyz;
    float dist = length(sphereDif);
    float sphereDist = dist - size;
    if (sphereDist < car.bs[i].w) {
      delta += sphereDif * (car.bs[i].w - sphereDist) / dist;
    }
  }
}

void collisionsCheck(inout Particle particle, bool checkCars){
  float3 delta = 0;
  float size = particle.size * 0.85;
  planesCollisionsCheck(particle.position, delta, size);

  [branch]
  if (checkCars) {
    carCollisionsCheck(cars[checkCarsIDs.x], particle.position, delta, size);
    if (checkCarsIDs.y) {
      carCollisionsCheck(cars[checkCarsIDs.y], particle.position, delta, size);
      if (checkCarsIDs.z) {
        carCollisionsCheck(cars[checkCarsIDs.z], particle.position, delta, size);
        if (checkCarsIDs.w) carCollisionsCheck(cars[checkCarsIDs.w], particle.position, delta, size);
      }
    }
  }

  particle.position += delta;
  particle.velocity += delta * 2;
  particle.life -= saturate(dot2(delta));
}

#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1

float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

[numthreads(THREADCOUNT_SIMULATION, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID, uint Gid : SV_GroupIndex) {
  uint aliveCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT);
  GroupMemoryBarrierWithGroupSync();

  if (DTid.x < aliveCount) {
    const float dt = gFrameTime / 1;

    const float drag = 1.0 / (1.0 + dt * 2);
    const float dragInside = 1.0 / (1.0 + dt * 4);

    uint particleIndex = aliveBuffer_CURRENT[DTid.x];
    Particle particle = particleBuffer[particleIndex];

    if (particleIndex > 1) {
      float3 dprev1 = particleBuffer[particleIndex - 1].position - particle.position;
      float3 dprev2 = particleBuffer[particleIndex - 2].position - particle.position;
      float threshold = particle.size * particle.lifeOut * 1.5;
      if (min(dot2(dprev1), dot2(dprev2) * 1.5) < threshold * threshold) {
        particle.life -= dt * 4;
      }
    }

    if (particle.life > 0 && dt < 1) {
		  Wheel wheel = wheels[particle.wheelID];
		  Car car = cars[particle.wheelID / 4];

      float groundK = pow(saturate((particle.position.y - particle.groundY) * 4), 1);
      float opacityMult = 1;
      
      if (particle.stuckPos != 0) {
        float stuckRadius = wheel.radius + 0.0;
	    	particle.stuckSpeed = lerp(wheel.angularSpeed, particle.stuckSpeed, dragInside);
        particle.stuckSpeed = min(abs(particle.stuckSpeed), wheel.stuckMaxSpeed) * sign(particle.stuckSpeed);
        particle.stuckPos += particle.stuckSpeed * dt;

        float leadingSide = saturate(dot(wheel.outside, normalize(wheel.velocity))) * saturate(car.carSpeed / 12 - 0.5);
        float3 posBase = wheel.pos 
          - wheel.dir * stuckRadius * sin(particle.stuckPos)
          - wheel.up * stuckRadius * cos(particle.stuckPos);
        particle.position = posBase + lerp(0.35, 0.15, leadingSide) * wheel.stuckOffset * wheel.width * wheel.outside;
        particle.stuckSide += particle.stuckDrift * dt;

        float3 newVelocityBase = (
            - wheel.dir * cos(particle.stuckPos)
            + wheel.up * sin(particle.stuckPos)
          ) * wheel.radius * saturate(1 - particle.stuckDrift / 10);   
        particle.velocity = min(abs(particle.stuckSpeed), 20) * sign(particle.stuckSpeed) * newVelocityBase;

        float input = particle.stuckPos - particle.random * 0.2;
        float offset = frac(input / M_TAU);
        float isBlocked = offset > wheel.blockStart && offset < wheel.blockEnd;
        opacityMult = isBlocked ? pow(saturate(10 * abs(0.5 - offset) - 0.5), 1) : 1;
        particle.isOut = (particle.stuckSide > 1.0 || abs(particle.stuckSpeed) < particle.growK) && (!isBlocked);
        if (offset > wheel.flyoffStart
            && offset < wheel.flyoffEnd
            && abs(input) > particle.random * wheel.flyoffDelay * particle.growK
            || particle.isOut
        ) { 
          particle.velocity = wheel.outside * (min(particle.stuckDrift, 2) * wheel.width) * saturate(offset * 20 - 1) * saturate(length(wheel.velocity) / 10 - 0.2)
            + (particle.isOut ? 1 : 1 + particle.random3) * min(abs(particle.stuckSpeed), 20) * sign(particle.stuckSpeed) * newVelocityBase
            + wheel.velocity * (particle.isOut ? 0.83 : 0.33) * (0.5 + particle.random2);
          particle.neutralSpeedMult = saturate(length(wheel.velocity) / 10 + 0.1);

          if (particle.isOut) {
            particle.velocity = lerp(particle.velocity,
              wheel.velocity * dot(normalize(wheel.velocity), wheel.outside),
              saturate(leadingSide * 2));
          }
          
          // particle.velocity = min(particle.stuckSpeed, 20) * newVelocityBase;

          particle.stuckPos = 0;
          particle.stuckDrift = 0;
          // if (particle.random < 0.93 + gDensityReductionMult * saturate(float(aliveCount) / 200) && aliveCount > 30) {
          if (particle.random < 0.8 && aliveCount > 30) {
            particle.life = min(particle.random * particle.random * min(particle.stuckSide < 1.0 ? 1 : 2.4, particle.life), particle.stuckSide < 1.0 ? 0.4 : 1.6);
            particle.opacityK = 4 / particle.life;
          }

          if (particle.dustMix <= 1) {
            particle.velocity -= dot(particle.velocity, wheel.up) * wheel.up * 0.8;
          } else {
            particle.velocity += wheel.up * min(abs(particle.stuckSpeed), 20) * (0.5 + particle.random2) * 0.15;
          }
          // particle.velocity = wheel.velocity * 2;
          particle.position -= particle.velocity * dt;

          if (particle.isOut) {
            particle.pushedAgainst = leadingSide;
            // particle.life = 0;
          }
        }
      } else {
        particle.position += particle.velocity * dt;
        if (particle.dustMix <= 1) {
          particle.position.y = max(particle.position.y, particle.groundY);
        }

        float speed = length(particle.velocity);
        if (speed > 30) {
          particle.velocity *= 30 / speed;
          speed = 30;
        }

        float unstuckK = saturate((particle.position.y - particle.groundY) * 5 - 0.1);
        float pushedK = particle.pushedAgainst * saturate(car.carSpeed / 20 - 0.5);
        float maxSize = 4 + (1 + particle.thickness) * (0.75 + 0.5 * groundK) * particle.random3;
        maxSize *= 1 + 2 * saturate((particle.lifeOut) / 12);
        maxSize *= lerp(0.2 + 0.4 * particle.random2, 1, saturate(particle.growK));
        maxSize *= lerp(1, 0.2, saturate(-particle.growK));
        maxSize *= lerp(0.4, 1, unstuckK);
        // maxSize /= 20;
        // maxSize = 3.75;

        particle.size += pow(saturate(1 - particle.size / maxSize), 1) 
          * lerp(1, 0.2, saturate(1 - particle.lifeOut) * saturate(2 - speed / 10))
          * lerp(0.5, 1, saturate(speed / 10 - 0.2)) 
          * (1 - pushedK)
          * (4.0 - (particle.random3 + groundK))
          * lerp(0.5, 1, particle.growK)
          * lerp(1, 0.8, particle.dustMix)
          * dt;
        particle.pushedAgainst = saturate(particle.pushedAgainst - dt * 3);

        float currentSpeed = length(particle.velocity);
        float3 neutralVelocityMult = (1 + groundK * float3(2, 0.5, 2)) * 0.8 * particle.neutralSpeedMult / currentSpeed;
        float3 neutralVelocity = particle.velocity * neutralVelocityMult;
        neutralVelocity = min(abs(neutralVelocity), abs(particle.velocity)) * sign(particle.velocity);
        neutralVelocity *= saturate(1 - particle.stuckDrift);

        float windGroundK = (saturate((particle.position.y - particle.groundY) / 10) * 2
          + saturate((particle.position.y - particle.groundY) / 4 + particle.lifeOut / 40)) / 3;
        neutralVelocity.xz += windOffset(particle.position, windGroundK, 2.5) * unstuckK;
        neutralVelocity.y += particle.stuckDrift;
        neutralVelocity *= saturate(1 - particle.lifeOut / 20);
        neutralVelocity.y -= 2 * saturate(particle.dustMix - 1);
        neutralVelocity *= lerp(0.2, 1, unstuckK);
	    	particle.velocity = lerp(neutralVelocity, particle.velocity, lerp(dragInside, drag, unstuckK));

        particle.lifeOut += dt;

        float pushAwayMult = saturate(particle.lifeOut * 4) * saturate(dt * 400);
        for (int i = 0; i < 3; i++) {
          float3 sphereDif = particle.position - car.bs[i].xyz;
          float sphereDist = length(sphereDif) + particle.size * lerp(-0.25, 0, pushedK);
          if (sphereDist < car.bs[i].w) {
            float3 delta = normalize(sphereDif) * (car.bs[i].w - sphereDist) * (0.2 + particle.growK) * particle.thickness;
            delta.y *= 0.3;
            particle.position += delta * pushAwayMult;
          }
        }

        {
          float3 delta = 0;
          planeCollisionCheck(car.centerPlane, particle.position, delta, car.carWidth + particle.size * 0.35);
          particle.position += delta * pushAwayMult;
        }

        if (wheel.radius >= 0) {
          float3 sphereDif = particle.position - wheel.axisCenter;
          float sphereDist = length(sphereDif);
          float3 carVelSide = normalize(cross(car.carVelocity, wheel.up));
          float suckK = dot(normalize(sphereDif), carVelSide);
          float suckD = saturate(1.5 - length(sphereDif) * 0.5);
          float suck = suckD * saturate(car.carSpeed / 10 - 0.8) * particle.thickness * particle.growK * dt;
          float suckRnd = suck * (0.2 + 0.8 * particle.random2);
          particle.velocity -= carVelSide * suckK * 25 * suckRnd;
          particle.velocity -= wheel.up * abs(suckK) * suckD * 10 * suck;
        }
        
        // TODO: Possibly very buggy?
        forceFields(maxSize, particle);
        collisionsCheck(particle, wheel.radius < 0 && particle.growK >= 0);
      }

      particle.life -= dt;

      float3 vel = mul(particle.velocity, (float3x3)xVP);
      // particle.texMovement.xy += (vel.xy * xFOVK + float2(particle.random2 - 0.5, 0)) * dt 
      //   * (1 - saturate((particle.lifeOut) / 6) * 0.5) * (0.5 + particle.random3 * 0.2);

      float2 baseOffset = vel.xy * xFOVK;
      // float2 extraOffset = float2(0, particle.random2 - 0.5) * lerp(1, 0, saturate(length(vel.xy * xFOVK) * 2)) * 0.4;
      float2 extraOffset = float2(0, abs(vel.z));
      // float2 extraOffset = float2(0, 1) * (length(baseOffset) < 0.1);
      particle.texMovement.xy += (baseOffset * 0.2 + extraOffset * xFOVK * 0.04) * dt;
      // particle.texMovement.xy += baseOffset * dt * 0.4;
      // particle.color = packColor(saturate(float4(vel.xy * 100, 0, 0)));

      // float4 p0 = mul(float4(particle.position, 1), xVP);
      // particle.color = packColor(saturate(float4(p0.xy / p0.w * 0.5 + 0.5, 0, 0)));

      // float4 p0 = mul(float4(particle.position, 1), xVP);
      // float4 p1 = mul(float4(particle.position + particle.velocity * dt, 1), xVP);
      // particle.texMovement.xy += (p1.xy / p1.w - p0.xy / p0.w) * 1;

			float3 eyeVector = particle.position - gCameraPosition;
      float distanceToCamera = length(eyeVector);
      float angle = atan2(eyeVector.x, eyeVector.z);
      particle.texMovementStatic.x = angle / (M_PI * 2);
      particle.texMovementStatic.y = acos(-(eyeVector / distanceToCamera).y) / 4;


      // float4 p0 = mul(float4(particle.position, 1), xVP);
      // float4 p1 = mul(float4(particle.position + normalize(cross(xCameraDir, float3(0, 1, 0))) * particle.size, 1), xVP);
      // float screenSize = length(p1.xy / p1.w - p0.xy / p0.w) * lerp(6, 0, pow(saturate(particle.random2), 4));
      // float screenSize2 = length(p1.xy / p1.w - p0.xy / p0.w) * 2;

      // particle.fillRateOpacityFix = 1 + saturate(screenSize2 * 10 - 9) * 4;
      // particle.opacity = 1 - saturate(screenSize * 10 - 9);
      // particle.fillRateOpacityFix = 1;
      particle.opacity = opacityMult;
      particle.opacity *= saturate(particle.stuckPos == 0 ? 1 : abs(particle.stuckPos) * 2 - 0.7);

			float distSQ = distanceToCamera + particle.size / 2;
			distanceBuffer[particleIndex] = 1 - saturate(distSQ / 10000);
      particleBuffer[particleIndex] = particle;

      uint newAliveIndex;
      counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 1, newAliveIndex);
      aliveBuffer_NEW[newAliveIndex] = particleIndex;
    } else {
      uint deadIndex;
      counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, 1, deadIndex);
      deadBuffer[deadIndex] = particleIndex;
    }
  }
}
