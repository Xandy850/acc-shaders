#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D txSmokeNoise : register(t4);

RESULT_TYPE main(PS_IN_fakeshadow pin) {
  float alpha = pin.Opacity * smoothstep(1, 0.4, length(pin.Tex));
  clip(alpha - 0.001);
  float3 nothing = 0;
  RETURN_BASE(nothing, alpha);
}