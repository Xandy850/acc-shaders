#ifndef GBUFFER_NORMALS_REFLECTION
	#define USE_BACKLIT_FOG
#endif

struct PS_IN {
	float4 PosH : SV_POSITION;
	
	float2 Tex : TEXCOORD1;

	#ifndef NO_SHADOWS
		float Occlusion : EXT_OUTSIDE;
		float Shadow : EXT_SHADOW;
	#endif

	nointerpolation float2 Movement : EXT1;
	nointerpolation float Opacity : EXT2;
	// nointerpolation float Life : EXT8;
	// nointerpolation float OpacityFix : EXT2_FIX;
	nointerpolation uint Color : COLOR1;

	nointerpolation float Size : EXT4;
	nointerpolation float DustMix : EXT5;
	nointerpolation float Thickness : EXT7;
	nointerpolation float StuckK : EXT8;

	float3 PosC : POSITION;

	#ifndef SHADER_MIRROR
		float3 ToSide : DIR0;
		float3 ToUp : DIR1;
		float StuckOcclusion : EXT3;
		float3 LightingDir : DIR2;
	#endif

	float Fog : TEXCOORD6;
	float3 Lighting : COLOR0;

	#ifdef SHADER_SOLID
		nointerpolation uint2 Offset2 : EXT9;
	#endif
};

struct PS_IN_mirror {
	float4 PosH : SV_POSITION;

	float2 Tex : TEXCOORD1;
	float Fog : TEXCOORD6;

	nointerpolation float Opacity : EXT0;
	nointerpolation float2 Movement : EXT1;
	nointerpolation float Size : EXT2;
	nointerpolation float SizeK : EXT3;
	nointerpolation float TexXMult : EXT4;
	nointerpolation float Thickness : EXT5;

	float3 PosC : POSITION;
	float3 ToSide : DIR0;
	float3 ToUp : DIR1;
	float3 Lighting : COLOR;
};

struct PS_IN_shadow {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD1;
	float2 TexMovement : EXT0;
	float PosY : EXT1;
	nointerpolation float Opacity : EXT2;
	nointerpolation float GroundY : EXT3;
	nointerpolation uint2 Offset2 : EXT4;
};

struct PS_IN_fakeshadow {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD1;
	float Fog : TEXCOORD2;
	float Opacity : TEXCOORD3;
};

cbuffer cbSparks : register(b6) {
	float vAmbient; // 0.4
	float vDiffuse; // 0.4
	float vDiffuseConcentration; // 0.4
	float vBacklitPower; // 2

	float vBacklitExponent; // 30
	float vUvMult1; // 0.1
	float vUvMult2; // 0.25
	float vDynamicLightingMult; // 3
}