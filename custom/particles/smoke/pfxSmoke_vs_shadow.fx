#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "pfxSmoke_draw.hlsl"

static const float3 BILLBOARD[] = {
	float3(-1, -1, 0),	// 0
	float3(1, -1, 0),	// 1
	float3(-1, 1, 0),	// 2
	float3(-1, 1, 0),	// 3
	float3(1, -1, 0),	// 4
	float3(1, 1, 0),	// 5
};

StructuredBuffer<Particle> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);

PS_IN_shadow main(uint fakeIndex : SV_VERTEXID) {
	PS_IN_shadow vout = (PS_IN_shadow)0;

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	Particle particle = particleBuffer[aliveList[instanceID]];
	particle.size = min(particle.size, 3.5);
	particle.opacity *= unpackColor(particle.color).a;

	vout.Opacity = 1;
	if (particle.growK < 0 && particle.size < 1.2){
		vout.Opacity *= 0.5 * saturate(remap(particle.size, 0.8, 1.2, 1, 0));
		particle.thickness = max(particle.thickness, 0.4);
	}

	[branch]
	if (particle.phase > 1 || particle.opacity <= 0 
			|| particle.lifeOut < 0.05
			|| particle.thickness <= 0.01
			|| particle.random2 > 0.7){
		vout.PosH = 10;
		return vout;
	}

	float3 quadPos = BILLBOARD[vertexID];
	quadPos *= particle.size * 1.6;

	particle.position.y += max(particle.size / 8 - 0.25, 0);
	vout.PosH = float4(particle.position, 1);
  vout.PosH.xyz += ksLightDirection.xyz * (saturate(particle.size / 4 - 0.5) + 0.1);

  float3 side = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
  float3 up = normalize(cross(ksLightDirection.xyz, side));

	float3 sideFlat = normalize(float3(side.x, 0, side.z));
	float3 upFlat = normalize(float3(up.x, 0, up.z));
	sideFlat = normalize(cross(float3(0, 1, 0), upFlat));

  side = lerp(side, sideFlat, saturate(-ksLightDirection.y * 2));
  up = lerp(up, upFlat, saturate(-ksLightDirection.y * 2));
  side = normalize(side);
  up = normalize(up);

	vout.PosH.xyz += side * quadPos.x;
	vout.PosH.xyz += up * quadPos.y;

	vout.PosY = (vout.PosH.y - particle.groundY) * max(10, 2 / particle.size);
	vout.PosH = mul(vout.PosH, ksView);
	vout.PosH = mul(vout.PosH, ksProjection);

	vout.Tex = BILLBOARD[vertexID].xy;
	vout.TexMovement = vout.Tex * 0.6 + particle.texMovement;

  float spreadK = saturate(particle.size / 2.2 - 0.6);
	vout.Opacity *= particle.opacity * saturate(particle.life * particle.opacityK) * (1 - spreadK);
  vout.Opacity *= min(vout.Opacity, particle.thickness);
	if (vout.Opacity < 0.1) vout.PosH = 10;

	vout.Offset2 = uint2(particle.random * 60, particle.random3 * 60);
	return vout;
}