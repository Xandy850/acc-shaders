#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "pfxSmoke_draw.hlsl"

static const float3 BILLBOARD[] = {
	float3(-1, -1, 0),	// 0
	float3(1, -1, 0),	// 1
	float3(-1, 1, 0),	// 2
	float3(-1, 1, 0),	// 3
	float3(1, -1, 0),	// 4
	float3(1, 1, 0),	// 5
};

StructuredBuffer<Particle> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);

cbuffer cbSparks : register(b6) {
}

PS_IN_fakeshadow main(uint fakeIndex : SV_VERTEXID) {
	PS_IN_fakeshadow vout = (PS_IN_fakeshadow)0;

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	Particle particle = particleBuffer[aliveList[instanceID]];
	particle.size = min(particle.size, 3.5);
	particle.opacity *= unpackColor(particle.color).a;

	[branch]
	if (particle.phase > 1 || particle.opacity <= 0 
			|| particle.lifeOut < 0.05
			|| particle.size < 0.1
			|| particle.thickness <= 0.01
			|| particle.position.y - particle.groundY > 1
			|| particle.random2 > 0.1){
		vout.PosH = 10;
		return vout;
	}

	float sizeK = saturate(particle.size - 1);
  float spreadK = saturate(particle.size / 2 - 0.5);
	vout.Opacity = saturate(particle.life * particle.opacityK / 4);
  vout.Opacity *= 1 - spreadK * 0.5;
  vout.Opacity *= saturate(particle.lifeOut * 10);
  vout.Opacity = min(vout.Opacity, particle.thickness);
  vout.Opacity *= 1 - saturate(particle.position.y - particle.groundY);
  vout.Opacity *= particle.opacity * saturate(remap(particle.size, 0.1, 0.2, 0, 1));

	float3 quadPos = BILLBOARD[vertexID];
	quadPos *= particle.size * lerp(3, 1, sizeK);

	vout.PosH = float4(particle.position, 1);
	vout.Fog = calculateFogNew(particle.position - ksCameraPosition.xyz);
  vout.PosH.xyz += ksLightDirection.xyz * (saturate(particle.size / 4 - 0.5) + 0.1);

	float3 side = float3(-1, 0, 0);
	float3 up = float3(0, 0, -1);

	vout.PosH.xyz += side * quadPos.x;
	vout.PosH.xyz += up * quadPos.y;
	vout.PosH.y = particle.groundY;

	float3 posC = vout.PosH.xyz - ksCameraPosition.xyz;
	vout.PosH.xyz -= normalize(posC) * 0.2;

	vout.PosH = mul(vout.PosH, ksView);
	vout.PosH = mul(vout.PosH, ksProjection);
	vout.Tex = BILLBOARD[vertexID].xy;

	if (vout.Opacity < 0.1) vout.PosH = 10;
	vout.Opacity *= 0.4;

	return vout;
}