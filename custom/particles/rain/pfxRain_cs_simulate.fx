#include "pfxRain.hlsl"

// void forceFields(float3 pos, inout float3 velocity) {
//   for (uint i = 0; i < FORCE_FIELDS; i++) {
//     float3 diff = pos - gForceFields[i].pos;
//     float dist = dot(diff, diff);
//     float distK = saturate(1 - dist * gForceFields[i].radiusSqrInv);
//     float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult * 30;
//     float3 forceVelSync = (gForceFields[i].velocity - velocity) * abs(gForceFields[i].velocity) * 0.4;
//     velocity += distK * distK * (forcePushAway + forceVelSync) * gFrameTime * float3(1, 0.3, 1);
//   }
// }

void process(inout Particle particle, uint3 DTid) {
  particle.pos += particle.velocity * gFrameTime;
  particle.life -= gFrameTime;
  
  if (!withinPrevAABB(particle.pos, gPosPrev, gSize)){
    particle.life = -1;
  }
}
