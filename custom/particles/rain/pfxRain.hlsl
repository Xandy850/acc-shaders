// #define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_EMIT_2D 
#define PASS_INDEX 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float size;

	float random;
	float3 pad;
};

struct Particle_packed {
	float3 pos;
	float collisionPlane1w;

	uint2 velocity_size;
	uint2 collisionPlane1xyz_life;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.collisionPlane1w, _U.collisionPlane1.w);\
	FN(_P.velocity_size, _U.velocity, _U.size);\
	FN(_P.collisionPlane1xyz_life, _U.collisionPlane1.xyz, _U.life);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 pos;
		
		float3 posPrev;
		float pad0;

		float3 size;
		float pad1;
	};

	// #define FORCE_FIELDS 4

	// struct ForceField {
	// 	float3 pos;
	// 	float radiusSqrInv;
	// 	float3 velocity;
	// 	float forceMult;
	// };

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float3 gPosPrev;
		float gGravity;

		float3 gSize;
		uint _pad1;

		// ForceField gForceFields[FORCE_FIELDS];
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float AmbientMult : COLOR0;
		float CenterK : COLOR1;
		float RandomK : COLOR2;
		float StretchedK : COLOR3;
		float3 DirUp : POSITION1;
		float3 DirSide : POSITION2;
	};
#endif

bool3 withinPrevAABB3(float3 pos, float3 posPrev, float3 size){
	float3 dist = abs(pos - posPrev);
	return dist <= size * 0.5 + 0.01;
}

bool withinPrevAABB(float3 pos, float3 posPrev, float3 size){
	return all(withinPrevAABB3(pos, posPrev, size));
}

#include "particles/common/include_impl.hlsl"
