#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

#define PARTICLE_LIFESPAN 7
#define PARTICLE_SCALE 1.3
#define MIN_WIPERS_SPEED 0.001

struct Particle {
	float2 pos;
	float2 velocity;
	float life;
	float size;
	float rand;
	float squished;
	float sizeMult;  // for squished: posSmooth.x
	float sticky;    // for squished: posSmooth.y
	float moved;
	float splashMult;
	float wiperLag;
};

struct Particle_packed {
	float2 pos;
	float2 velocity;
	float life;
	float size;
	float rand;
	float squished;
	float sizeMult;  // for squished: posSmooth.x
	float sticky;    // for squished: posSmooth.y
	float moved;
	uint splashMult_wiperLag;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.velocity, _U.velocity);\
	FN(_P.life, _U.life);\
	FN(_P.size, _U.size);\
	FN(_P.rand, _U.rand);\
	FN(_P.squished, _U.squished);\
	FN(_P.sizeMult, _U.sizeMult);\
	FN(_P.sticky, _U.sticky);\
	FN(_P.moved, _U.moved);\
	FN(_P.splashMult_wiperLag, _U.splashMult, _U.wiperLag);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float density;
		float speedDensity;
		float pad1;
		float3 speedDirection;
		float pad2;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float3 gVelocity;
		float gPad0;

		float3 gVelocitySide0;
		float gScale;
		
		float3 gVelocitySide1;
		uint gALS1;

		float3 gRainDir;
		uint gALS2;
		
		float3 gCarVelocity;
		float gFlyOff;

		float gCarLift;
		float gActualScale;
		float2 gPad1;

		float3 gCollisionDir;
		float gCollisionIntensity;
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		#ifdef DIRT_MODE
			float Value : TEXCOORD2;
		#else
			float2 Velocity : TEXCOORD2;
			float Splash : TEXCOORD3;
			float NoiseOffset : TEXCOORD4;
			float Intensity : TEXCOORD5;
			float SizeMult : TEXCOORD6;
		#endif
	};

	cbuffer cbDraw : register(CBUFFER_DRAW_SLOT) {
		float gScale;
		float gDropsSize;
		float2 pad1;
	};
#endif

#include "particles/common/include_impl.hlsl"
