#include "pfxSparks.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float life = fadingBase + saturate(particle.life * fadingLifeLeftK);
	float size = particle.size * life * sizeMultiplier;// * (1 + particle.brightness * 10);
	float2 extraOffset = 0;

	#ifdef MODE_LIGHTS
		float radius = saturate(particle.lifePassedOutside * 3);
		radius += saturate(length(particle.velocity) / 30 - 0.2);
		radius *= saturate(particle.life * 2);
		extraOffset = quadPos.xy * radius;

		[branch]
		if (particle.lightStrength < 1.9){
			PS_IN vout = (PS_IN)0;
			vout.PosH = -1;
			return vout;
		}
	#endif

	quadPos *= size;

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

  float3 side = normalize(cross(billboardAxis, particle.velocity));
  float3 up = normalize(cross(billboardAxis, side));

	float3 offset = side * quadPos.x + up * quadPos.y;

	float4 posW = float4(particle.pos, 1);
	float3 velocity = particle.velocity;
	float3 velocityDir = normalize(velocity);
	float stretchMult = particle.motionMult * pow(saturate(length(velocity) / 2), 10) * 2;
	float ratio = lerp(1 / (stretchMult * length(velocity) + 1), 1, abs(dot(billboardAxis, velocityDir)));
	#ifdef MODE_GBUFFER
		offset *= 2;
	#endif

	#ifndef MODE_LIGHTS
		posW.xyz += offset * (0.8 + 0.5 * ratio);
		posW.xyz += stretchMult * size * velocity * dot(velocityDir, normalize(offset));
	#endif

	float4 posV = mul(posW, ksView);
	posV.xy += extraOffset;
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Ratio = ratio;

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		// vout.Brightness = (brightnessBase + particle.brightness * brightnessExtra) * (0.4 + 0.6 * life);
		vout.Brightness = (brightnessBase + particle.brightness * brightnessExtra) * (1 + 2 * pow(saturate(1 - particle.life), 2));
		vout.Color = particle.color;
		#ifdef MODE_LIGHTS
			vout.GlowExtra = radius;
			vout.Brightness *= saturate(particle.life * 2);
		#else
			vout.GlowExtra = 0.7 + stretchMult * saturate(-dot(billboardAxis, velocityDir));
		#endif
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
		#endif
	#endif

	return vout;
}