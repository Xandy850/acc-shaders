#include "pfxSparks.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {	
	particle.pos = E.emitterPosBase;
	{
		float offset = pow(RAND, 2);
		float angle = RAND * M_TAU;
		particle.pos += E.emitterPosOffset0 * sin(angle) * offset;
		particle.pos += E.emitterPosOffset1 * cos(angle) * offset;
	}

	particle.color = packColor(saturate(unpackColor(E.color) + float4((float3(RAND, RAND, RAND) - 0.5) * 0.1, 0)));
	particle.carIndex = E.carIndex;
	particle.randomValue = RAND;
	particle.carVelocity = carFloors[particle.carIndex].velocity;
	// particle.lightStrength = RAND < E.lightChanceMult * (2 + saturate(E.emitCount / 2000 - 0.1) * 4) / E.emitCount ? 1 : 0;
	// particle.lightStrength = RAND < E.lightChanceMult / min(E.emitCount, 100) ? 1 : 0;
	particle.lightStrength = RAND < E.lightChanceMult / 2 ? -1 : -1;
	particle.lightStrength = RAND;
	particle.randomMotion = 0;
	particle.spawnDensity = 0;

	particle.velocity = E.emitterDirection0 * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND);
	{
		float offset = pow(RAND, 2) * min(E.emitterSpeed, 20) * E.emitterSpreadMult;
		float angle = RAND * M_TAU;
		particle.velocity += normalize(E.emitterPosOffset0) * sin(angle) * offset;
		particle.velocity += normalize(cross(E.emitterPosOffset0, E.emitterDirection0)) * cos(angle) * offset * E.emitterSpreadMult2;
	}

	// particle.velocity += normalize(E.emitterPosOffset0) * (RAND - 0.5) * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND) * RAND * E.emitterSpreadMult;
	// particle.velocity += normalize(cross(E.emitterPosOffset0, E.emitterDirection0)) * (RAND - 0.5) * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND) * RAND * E.emitterSpreadMult * 0.1;

	float power = pow(RAND, 4);
	particle.life = E.xParticleLifeSpan + power * E.xParticleLifeSpanRandomness * (0.5 + pow(RAND, 10) + 10 * pow(RAND, 100));
	particle.brightness = power;
	particle.size = 1;
	particle.collisionPlane1 = E.collisionPlane1;
	particle.collisionPlane2 = E.collisionPlane2;

	particle.carIndex = 0;
}
