#include "accSmaa_common.hlsl"

VS_Smaa_step1 main(uint id: SV_VertexID) {
  VS_Smaa_step1 vout;
  vout.Tex = float2((float)(id / 2) * 2, 1 - (float)(id % 2) * 2);
  vout.PosH = float4(
    (float)(id / 2) * 4 - 1,
    (float)(id % 2) * 4 - 1, 0, 1);
  vout.Offset[0] = mad(SMAA_RT_METRICS.xyxy, float4(-1.0, 0.0, 0.0, -1.0), vout.Tex.xyxy);
  vout.Offset[1] = mad(SMAA_RT_METRICS.xyxy, float4(1.0, 0.0, 0.0, 1.0), vout.Tex.xyxy);
  vout.Offset[2] = mad(SMAA_RT_METRICS.xyxy, float4(-2.0, 0.0, 0.0, -2.0), vout.Tex.xyxy);
  return vout;
}
