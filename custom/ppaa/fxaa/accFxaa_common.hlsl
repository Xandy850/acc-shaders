#include "FXAA.hlsl"

cbuffer cbData : register(b10) {
  float4 gScreenSize;
}

SamplerState samLinearSimple : register(s7) {
  Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  FxaaTex tex = { samLinearSimple, txDiffuse };
  float3 aaImage = FxaaPixelShader(pin.Tex, tex, gScreenSize.xy);
  return float4(aaImage, 1.0);
}