Object.entries({
  default_medium: 12, 
  default_high: 13, 
  default_ultra: 15,
  lessdither_medium: 23, 
  lessdither_high: 26, 
  lessdither_ultra: 29,
}).map(([qualityKey, qualityValue]) => Object.entries({
  luma: 0,
  green: 1
}).map(([greenAsLumaKey, greenAsLumaValue]) => ({
  source: `fxaa3.hlsl`,
  target: FX.Target.PS,
  saveAs: `accFxaa3_${qualityKey}_${greenAsLumaKey}.fxo`,
  defines: {
    'FXAA_QUALITY__PRESET': qualityValue,
    'FXAA_GREEN_AS_LUMA': greenAsLumaValue
  }
}))).flat(Infinity)