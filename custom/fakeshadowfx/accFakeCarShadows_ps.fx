#define SHADOWS_CASCADES_TRANSITION
#define EXTENDED_PEROBJECT_CB
#ifndef MODE_GBUFFER
  #define USE_BACKLIT_FOG
#endif
#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true

#include "include_new/base/_flags.fx"
#include "include_new/base/cbuffers_common.fx"
#include "include_new/base/structs_ps.fx"
#include "include_new/base/samplers_ps.fx"
#include "include_new/base/textures_ps.fx"
#include "include_new/base/utils_ps_fog.fx"
#include "include_new/base/utils_ps_gbuff.fx"
#include "include_new/ext_shadows/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

#ifdef ALLOW_LIGHTINGFX
	#define AO_EXTRA_LIGHTING 1
	#define LIGHTINGFX_SPECULAR_EXP 1
	#define LIGHTINGFX_SPECULAR_COLOR 0
	#define LIGHTINGFX_KSDIFFUSE 1
  #define LIGHTINGFX_NOSPECULAR
  #define LIGHTINGFX_SIMPLEST
	#define POS_CAMERA posC
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

#define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
#define AO_LIGHTING 1
#include "accFakeCarShadows.hlsl"
  
float adjustFocusRaw(float shadow, float focus){
  float sep = 0.38;
  float exp = 1 + focus * 3;
  float ret = shadow > sep ? sep + pow(saturate((shadow - sep) / (1 - sep)), (1 + 1 / exp) / 2) * (1 - sep) : pow(shadow / sep, exp) * sep;
  return ret * lerp(1, 1.2, focus);
}
  
float adjustFocus(float shadow, float focus){
  focus = lerp(saturate(focus), 1, concentration);
  float sep = 0.38;
  float exp = 1 + focus * 3;
  float ret = shadow > sep ? sep + pow(saturate((shadow - sep) / (1 - sep)), (1 + 1 / exp) / 2) * (1 - sep) : pow(shadow / sep, exp) * sep;
  return ret * lerp(1, 1.2, focus);
}

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float getBetterDepth(float2 uv){
  float value = txDepth.SampleLevel(samPointClamp, uv, 0);
  value = max(value, txDepth.SampleLevel(samPointClamp, uv, 0, int2(-2, 0)));
  value = max(value, txDepth.SampleLevel(samPointClamp, uv, 0, int2(2, 0)));
  value = max(value, txDepth.SampleLevel(samPointClamp, uv, 0, int2(0, -2)));
  value = max(value, txDepth.SampleLevel(samPointClamp, uv, 0, int2(0, 2)));
  return value ? value : 1e9;
}

RESULT_TYPE main(PS_IN_FakeCarShadows pin) {
  float3 posC = pin.PosC;
  float3 toCamera = normalize(pin.PosC);
  float3 nothing = 0;

  float2 uv = pin.Tex;
  float softK = 1.0;
  float blurBaseL = blurBase;

  #ifdef WITH_REPROJECTION
    float2 ssUV = pin.PosH.xy * extScreenSize.zw;
    posC = ssGetPos(ssUV, getBetterDepth(ssUV));
    float3 posW = ksCameraPosition.xyz + ssGetPos(ssUV, getBetterDepth(ssUV));
    float2 shadowUV = mul(float4(posW, 1), shadowMatrix).xy;
    float pointAboveShadowPlane = distanceToPlane(shadowPlane, posW);
    pin.PosC = posC;
    uv = shadowUV * float2(0.5, -0.5) + float2(0.5, 0.5);
    softK = saturate(1 - pointAboveShadowPlane * 5);
    blurBaseL += saturate(-pointAboveShadowPlane * 1.1 - 0.1);

    pin.ShadowTex0 = mul(float4(posW, 1), ksShadowMatrix0);
    pin.ShadowTex1 = mul(float4(posW, 1), ksShadowMatrix1);
    pin.ShadowTex2 = mul(float4(posW, 1), ksShadowMatrix2);
  #endif

  #define POISSON_DISK_SIZE 4
  const float2 poissonDisk[POISSON_DISK_SIZE] = {
    float2(-0.8772404f, -0.4460778f),
    float2(0.3047062f, -0.7027774f),
    float2(-0.1694454f, 0.208617f),
    float2(0.6959092f, 0.545491f)
  };

  float blurLevel = blurBaseL + dot(blurExtra * uv, 1);
  #ifdef SIMPLE_MODE
    float value = saturate(txDiffuse.SampleBias(samLinearBorder0, uv, blurLevel * 4).x);
  #else
    float value = 0;
    float2 blurOffset = pow(saturate(blurLevel), 0.75) * float2(0.28, 0.1);
    for (float i = 0; i < POISSON_DISK_SIZE; i++) {
      float2 uvOffset = poissonDisk[i];
      value += txDiffuse.SampleBias(samLinearBorder0, 
        uv * (1 + blurOffset * 2) + (uvOffset - 1) * blurOffset, blurLevel * 4).x;
    }
    value = saturate(value / POISSON_DISK_SIZE);
  #endif

  float luminanceMultiplier = 1;
  float shadowFactor = 1;
  
  #ifndef SIMPLE_MODE
    [branch]
    if (shadowFactor > 0) {
      float shadow = getShadow(pin.PosC, float3(0, 1, 0), SHADOWS_COORDS, 1);
      float ambientValue = max(dot(ksAmbientColor_sky, 1), 2);
      float lightValue = max(dot(ksLightColor, 1), 0);

      float3 normalW = float3(0, 1, 0);
      float3 lighting = 0;
      #ifdef ALLOW_LIGHTINGFX
        LIGHTINGFX(lighting);
      #endif

      luminanceMultiplier = lerp(1, ambientValue / max(0.01, shadow * lightValue + dot(lighting, gDynamicLightsFactor /* 0.02 */) + ambientValue), shadowFactor);
      value = pow(value, 2 - pow(luminanceMultiplier, 4));
      // result *= lerp(neonFix, 1, saturate(ksAmbientColor_sky.g - 0.2));
    }
  #endif
    
  [branch]
  if (useGammaAdjustment) {
    value = adjustFocus(value, saturate(remap(carVaoValue, 0.2, 0.8, 1, 0)));
  }

  float2 centerDif = uv * 2 - 1;
  centerDif.y = sign(centerDif.y) * max(abs(centerDif.y) * 2 - 1, 0);
  float edgeFalloff = saturate(4 - length(centerDif) * 4);
  value = min(value, edgeFalloff);

  float result = value * (1 - transparency) * (1 - blurLevel) * softK * 1.2;
  // clip(result - 0.01);

  result *= luminanceMultiplier;
  result *= lerp(neonFix, 1, smoothstep(0, 1, ksAmbientColor_sky.g * 0.5 - 0.2));
  // result = 1 - neonFix;

  // if (wheelMode){
  //   nothing.r = 1;
  //   result += 0.8;
  // }

  // result = value;
  // result = max(result, 0.8);
  // #ifdef WITH_REPROJECTION
  //   nothing.r = 3;
  // #else
  //   nothing.b = 3;
  // #endif

  // result = 1;
  // nothing = float3(3, 0, 0);

  // RETURN_BASE(lighting, 1);
  RETURN_BASE(nothing, saturate(result));
}
