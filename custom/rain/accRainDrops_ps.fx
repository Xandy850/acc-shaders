#define PARAM_BLUR gStretching
#define PARAM_USE_COLOR gUseColor
#include "accRainDrops.hlsl"

PS_OUT main(PS_IN pin) {
  INTERIOR_TEST;
  float3 toCamera = normalize(pin.PosC);
  float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

  float fresnel = 0;
  float lightMult = 1;
  #ifdef USE_FAST_WAY

    // lightMult = 1 - abs(pin.Tex.y * 2 - 1);

    float alpha = saturate(remap(abs(pin.Tex.y), 0.7, 1, 1, 0));
    alpha *= saturate(remap(length(pin.Tex), 0.5, 1, 1, 0)) * softK;

    #ifdef USE_COLOR_BUFFER
      float3 lighting = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, alpha).rgb;
    #else
      float3 lighting = sampleEnv(toCamera, 4 + alpha, 1);
    #endif

    pin.Tex.y = saturate(remap(abs(pin.Tex.y), lerp(0.9, 0.8, abs(pin.Tex.x)), 1, 0, 1)) * sign(pin.Tex.y);
    float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
    float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
      + pin.DirSide * pin.Tex.x
      + -toCamera * facingShare);

  #else

    float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.07 + pin.NoiseOffset, 0);
    pin.Tex *= 1 + noise.xy * 0.1;

    float sharpness = lerp(0.95, 0, abs(pin.Tex.y) * gStretching);
    float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
    // if (pin.Tex.y < 0) pin.Tex.y *= lerp(1, 1.4, abs(pin.Tex.y));
    float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
      + pin.DirSide * pin.Tex.x
      + -toCamera * facingShare);

    float fresnelBase = saturate(pow((1 + dot(toCamera, normal)) * lerp(1, 2, gStretching), 3));
    fresnel = max(fresnelBase, saturate(abs(pin.Tex.y) * 1.3) * gStretching);

    float alpha = saturate(remap(length(pin.Tex), sharpness, 1, 1, 0)) * softK;
    float3 refraction = lerp(toCamera, -normal, 0.5);
    float3 reflection = reflect(toCamera, normal);
    float3 reflectionColor = sampleEnv(reflection, lerp(3, 5, gStretching), 0);
    float3 refractionColor = sampleEnv(refraction, lerp(1, 3, gStretching), 0);

    if (PARAM_USE_COLOR){
      float3 side = normalize(cross(float3(0, 1, 0), toCamera));
      float2 fakeRefr = float2(dot(side, normal), normal.y);
      float2 ssUV = pin.PosH.xy * extScreenSize.zw;
      refractionColor = txPrevFrame.SampleBias(samLinearClamp, ssUV + fakeRefr * lerp(0.4, 0.2, facingShare), -1.5).rgb;
    }
    
    float3 lighting = lerp(refractionColor, reflectionColor, fresnel);
    pin.Opacity = 1;

  #endif

  lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
    lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus))
    * lightMult;
  alpha *= pin.Opacity * interiorMult;

  #ifdef USE_FAST_WAY
    lighting += ksAmbientColor_sky.rgb * 0.02;
    alpha *= lerp(0.4, 0.8, extSceneRain) * 0.5;
  #endif

  RETURN_BASE(lighting, alpha);
}