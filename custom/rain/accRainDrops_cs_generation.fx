#include "accRainDrops.hlsl"

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(basePosXZ);
  if (gDensity < RAND) return;
  // if (0.1 < RAND) return;

  float3 randomDir = rainDropRandomDir(float2(RAND, RAND));
  float3 dir = rainDropSummaryDir(randomDir);
  float3 pos = rainDropPos(basePosXZ, randomDir, RAND + RAND * 0.1);

  float dist = length(pos - gPos);
  float distCamera = length(pos - gCameraPosition);
  float fade = saturate(remap(dist, 16, 12, 0, 1))
    * (gClipNearby ? saturate(remap(distCamera, 0.8, 1.2, 0, 1)) : 1);
  if (!fade || !isVisible(pos)) return;

  float distanceToGround = pos.y - groundPos(pos).y;
  if (distanceToGround < -0.13) return;

  RainDrop drop;
  drop.pos = pos;
  drop.fade = fade;
  drop.dir = dir + gCameraVelocity * TIME_MULT / -30;
  drop.size = 0.13;
  buResultDrops.Append(drop);
}