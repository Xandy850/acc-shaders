#include "accRainWindscreen.hlsl"

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
}

struct PS_IN_wipers {
  float4 PosH : SV_POSITION;
};

PS_IN_wipers main(VS_IN_ac vin, uint instanceID : SV_InstanceID) {
  PS_IN_wipers vout;
  float4 posW = mul(vin.PosL, gWorld);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);
  return vout;
}
