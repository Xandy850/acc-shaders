#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 1
float4 sampleBlur(float2 uv, float level){
  float4 currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    float weight = 1;
    currentShot += txDiffuse.SampleLevel(samLinearClamp, uv, level, int2(x, y)) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}

float3 getNT(float2 uv, float level, float4 rnd){
  float4 prevB = level == 0 ? txDiffuse.SampleLevel(samLinearClamp, uv, 0) : sampleBlur(uv, level);
  float thickness = (prevB.x + prevB.y) * (level ? 1 : 4)
    + (prevB.z * lerp(0.1, 0.8, rnd.x * rnd.y))
      // * saturate(remap(prevB.y, 0.2, 0.1, 0, 1)) 
      // * saturate(remap(prevB.y, 0.2, 0.3, 0, 1)) 
      // * saturate(remap(prevB.y, 0.5, 0.3, 0, 1)) 
      // * (level ? 0.5 : 0.3);  
      * (level ? 0.5 : 1);  
  float dx = ddx(thickness);
  float dy = ddy(thickness);
  float2 normal = float2(dx, -dy) * 20;
  return float3(normal, thickness);
}

float4 main_(VS_Copy pin) : SV_TARGET {
  float4 rnd = txNoise.SampleLevel(samLinear, pin.Tex, 0);
  float3 nb = getNT(pin.Tex, 0, rnd);
  float3 nt = getNT(pin.Tex, 1.2, rnd);
  nt += getNT(pin.Tex + float2(-0.005, 0), 1.6, rnd);
  nt += getNT(pin.Tex + float2(0.005, 0), 2.2, rnd);
  nt += getNT(pin.Tex + float2(0, -0.005), 1.6, rnd);
  nt += getNT(pin.Tex + float2(0, 0.005), 2.2, rnd);
  nt /= 5;
  return float4(nt.xy * float2(-0.5, -0.5) + 0.5, 0, saturate(nb.z * 2));
}

float get(float2 uv, float level, float4 rnd){
  float4 prevB = txDiffuse.SampleLevel(samLinearClamp, uv, level);
  return (prevB.x + prevB.y) * (level ? 1 : 4)
    + (prevB.z * lerp(0.1, 0.8, rnd.x * rnd.y)) * (level ? 0.5 : 1);  
}

float2 getN(float2 uv, float radius, float level, float4 rnd){
  float tb = get(uv, level, rnd);
  float tl = get(uv - float2(radius, 0), level, rnd);
  float tt = get(uv - float2(0, radius), level, rnd);
  return -float2(tl - tb, tb - tt);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 rnd = txNoise.SampleLevel(samLinear, pin.Tex, 0);
  float t0 = get(pin.Tex, 0, rnd);
  float mx = get(pin.Tex, 3.5, rnd);
  // float2 nt = lerp(getN(pin.Tex, 0.0003, 1, rnd) * 300, getN(pin.Tex, 0.008, 3, rnd) * 10, saturate(mx * 5 - 1));
  // nt = getN(pin.Tex, 0.008, 3, rnd) * 2;
  // nt = getN(pin.Tex, 0.004, 2.5, rnd) * 4;
  // nt = getN(pin.Tex, 0.003, 1.5, rnd) * 4 + getN(pin.Tex, 0.012, 3, rnd) * 2;
  float2 nt = getN(pin.Tex, 0.002, 1.5, rnd) + getN(pin.Tex, 0.006, 3, rnd);
  return float4(nt.xy * 1.2 * float2(-0.5, -0.5) + 0.5, saturate(mx * 40), saturate(t0 * 2));
}