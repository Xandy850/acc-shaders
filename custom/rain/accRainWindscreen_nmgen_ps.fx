#include "accRainWindscreen.hlsl"

struct PS_OUT {  
  float4 dx : SV_Target;
  float4 dy : SV_Target1;
  float4 pos : SV_Target2;
  float4 occlusion : SV_Target3;
};

struct LocalArea {
  float3 posParam;
  float mult;
  float3 radiusInv;
  float addition;
  float value;
  // float isAdditive;
  float3 pad;

  void calculate(inout float ret, float3 pos, bool signedValue){
    float v = saturate(saturate(length(pos * radiusInv + posParam)) * mult + addition);
    ret = signedValue ? clamp(lerp(value, ret, v), -1, 1) : saturate(lerp(value, ret, v));
  }
};

#define OCCLUSION_COUNT 16
#define BORDER_COUNT 8
#define LIFT_COUNT 8

cbuffer cbData : register(b11) {
  LocalArea gOcclusion[OCCLUSION_COUNT];
  LocalArea gOpen[BORDER_COUNT];
  LocalArea gLift[LIFT_COUNT];
}

float sampleOcclusion(float3 pos){
  float ret = 1;
  for (uint i = 0; i < OCCLUSION_COUNT; ++i){
    gOcclusion[i].calculate(ret, pos, false);
  }
  return ret;
}

float sampleOpen(float3 pos){
  float ret = 1;
  for (uint i = 0; i < BORDER_COUNT; ++i){
    gOpen[i].calculate(ret, pos, false);
  }
  return ret;
}

float sampleLift(float3 pos){
  float ret = 0;
  for (uint i = 0; i < LIFT_COUNT; ++i){
    gLift[i].calculate(ret, pos, true);
  }
  return ret;
}

PS_OUT main(PS_IN_project pin) {
  float3 dx = float3(ddx(pin.PosL.x), ddx(pin.PosL.y), ddx(pin.PosL.z));
  float3 dy = float3(ddy(pin.PosL.x), ddy(pin.PosL.y), ddy(pin.PosL.z));

  PS_OUT ret;
  ret.dx = float4(dx * gValue.x, 1);
  ret.dy = float4(dy * gValue.x, gValue.y /* mirror */);
  // ret.dx.xyz = -0.5;
  // ret.dy.xyz = 0.5;
  ret.pos = float4(pin.PosL.xyz, 1);
  ret.occlusion = float4(sampleOcclusion(pin.PosL.xyz), gValue.z * sampleOpen(pin.PosL.xyz), sampleLift(pin.PosL.xyz) * 0.5 + 0.5, 1);
  return ret;
}