#include "accRainWindscreen.hlsl"

float4 sampleSharpen(float2 uv){  
  // float kernel[9] = { 
  //   0.0, -1.0, 0.0,
  //   -1.0, 5.0, -1.0, 
  //   0.0, -1.0, 0.0
  // };
  // int2 offset[9] = { 
  //   int2(-1.0, -1.0), int2( 0.0, -1.0), int2( 1.0, -1.0),
  //   int2(-1.0,  0.0), int2( 0.0,  0.0), int2( 1.0,  0.0),
  //   int2(-1.0,  1.0), int2( 0.0,  1.0), int2( 1.0,  1.0)
  // };
  // float4 result = 0;
  // [unroll]
  // for (int i = 0; i < 9; i++){
  //   result += txInput.SampleLevel(samLinearBorder0, uv, 0, offset[i]) * kernel[i];
  // }
  // return result;

  float4 base = txInput.SampleLevel(samLinearBorder0, uv, 0);
  float4 blurred = 0
    + txInput.SampleLevel(samLinearBorder0, uv + (1.0/2048.0) * float2(0.5, 0.5), 0)
    + txInput.SampleLevel(samLinearBorder0, uv + (1.0/2048.0) * float2(0.5, -0.5), 0)
    + txInput.SampleLevel(samLinearBorder0, uv + (1.0/2048.0) * float2(-0.5, 0.5), 0)
    + txInput.SampleLevel(samLinearBorder0, uv + (1.0/2048.0) * float2(-0.5, -0.5), 0);
  float4 delta = base - blurred / 4;
  return base + 0.36 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.18;
}

#include "include/poisson.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  // return 0;

  // float4 ret = txInput.SampleLevel(samLinear, pin.Tex, 0);
  // // ret.a -= lerp(0.005, 0, saturate(length(ret.xy)));
  // ret.a -= 0.005;
  // #define PUDDLES_DISK_SIZE 32
  // for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
  //   float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i);
  //   float4 v = txInput.SampleLevel(samPoint, pin.Tex + offset * 0.005, 0);
  //   if (length(v.xy - offset) < 0.1){
  //     ret = v;
  //   }
  // }
  // return ret;
  
  // float2 velocity = txVelocity.SampleLevel(samLinear, pin.Tex, 3.5);
  float2 velocity = txVelocity.SampleLevel(samLinear, pin.Tex, 0);
  float4 ret = txInput.SampleLevel(samLinear, pin.Tex + velocity * 0.002, 0);
  ret.a = saturate(ret.a * 1.08 - 0.04); 
  ret.a -= lerp(0.005, 0, saturate(length(velocity)));
  return ret;
}