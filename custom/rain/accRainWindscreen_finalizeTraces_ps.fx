#include "accRainWindscreen.hlsl"

Texture2D<float4> txPrevious : register(t1);
Texture2D txWipers : register(t2);
Texture2D<float> txWipersDirt : register(t3);
Texture2D<float> txDirt : register(t4);
Texture2D txWipersPrev : register(t5);
Texture2D txSplashes : register(t9);

cbuffer cbData : register(b10) {
  float gTime;
}

float sampleWiperDirt(float wiperPos){
  float ret = 0;
  float count = 0;
  const uint samples = 4;
  for (uint i = 0; i < samples; ++i){
    ret += txWipersDirt.SampleLevel(samLinearClamp, float2(wiperPos + ((float)i / samples - 0.5) * 0.02, 0), 0);
    count++;
  }
  return ret / count;
}

// #define R 2
#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  // POISSON_AVG_LEVEL(float4, blur0, txWipers, samLinearClamp, pin.Tex, 0.002, 8, 0);
  POISSON_AVG_LEVEL(float4, layerWipersBlurred, txWipers, samLinearClamp, pin.Tex, 0.01, 5, 2.5);
  // float4 layerWipersBlurred = txWipers.SampleLevel(samLinear, pin.Tex, 3.5);

  float4 layerBase = txInput.SampleLevel(samLinear, pin.Tex, 0);
  float4 layerBaseBlurred = txInput.SampleLevel(samLinear, pin.Tex, 3.5);
  float4 layerWipers = txWipersPrev.SampleLevel(samLinear, pin.Tex, 0);
  float4 layerWipersNext = txWipers.SampleLevel(samLinear, pin.Tex, 0);
  float4 layerWipersPoint = txWipersPrev.SampleLevel(samPoint, pin.Tex, 0);
  float4 layerPrevious = txPrevious.Load(int3(pin.PosH.xy, 0));
  float4 layerSplashes = txSplashes.SampleLevel(samPoint, pin.Tex, 0);
  float layerDirt = txDirt.SampleLevel(samLinear, pin.Tex, 0);

  // float4 noise = txNoise.SampleLevel(samLinear, pin.PosH.xy / 32, 0);
  float4 noise = txNoise.SampleLevel(samLinear, float2(layerWipers.z * 1.3, layerWipers.z * 0.7 + gTime * 0.1), 0);

  float waterRandom = saturate((noise.x + noise.y) * lerp(0.5, 1, saturate(remap(noise.z, 0.3, 0.7, 0, 1))));
  float waterDirt = sampleWiperDirt(layerWipersPoint.z);
  float waterTrace = layerDirt * 2;
  float waterEdge = saturate(sqrt(layerWipersBlurred.a) - layerPrevious.y * 10) * saturate(layerBaseBlurred.a * 10 - 0.1);
  // float waterRegular = saturate(layerBaseBlurred.a * 10 - 0.5) * 1;

  float waterTotal = 0;
  waterTotal += lerp(0.1, 0.5, waterRandom) * waterDirt * saturate(layerWipers.a * 10) * 0.7;
  waterTotal += waterTrace * 0.3;
  // waterTotal += waterEdge;
  // waterTotal += 0.5;
  // waterTotal = lerp(0.1, 0.5, waterRandom) * waterDirt * saturate(layerWipers.a * 10) * 2;
  // waterTotal = waterTrace * 0.5;

  float condensationTotal = layerSplashes.x * saturate(remap(layerSplashes.w, 0.5, 0.51, 1, 0));
  float handTotal = layerSplashes.y * saturate(remap(layerSplashes.w, 0.5, 0.51, 1, 0));

  float accumulative = max(waterTotal, layerPrevious.x - gFadeSlow - layerWipers.a
    + waterEdge * gFadeFast * saturate((waterEdge - layerPrevious.x) * 10) * 4
    // + waterRegular * gFadeFast * saturate((waterRegular - layerPrevious.x) * 2)
    );
  // float accumulative = max(waterTotal, layerPrevious.x - gFadeNormal);
  float2 wiperVelocity = (layerWipers.xy * 2 - 1) * 3.6;
  if (dot2(wiperVelocity) < MIN_WIPERS_SPEED){
    // layerWipers.a = 0;
  }

  // accumulative = waterRegular;

  float condensationCurrent = layerPrevious.z - gFadeExtraSlow 
    + condensationTotal * gFadeFast * max(saturate(condensationTotal * 2) - saturate(layerPrevious.z * 2 - 1), 0);
  condensationCurrent = min(condensationCurrent, 0.95);
  condensationCurrent = min(condensationCurrent, 1 - handTotal);
  // condensationCurrent = 0.95;
    
  return float4(accumulative,
    max(max(layerWipers.a, layerWipers.z), layerPrevious.y - gFadeExtraSlow) * 1,
    // max(condensationTotal, layerPrevious.z - gFadeExtraSlow),
    condensationCurrent,
    // min(layerPrevious.z - gFadeExtraSlow + condensationTotal, 1 - handTotal),
    // handTotal,
    lerp(1, 0, layerWipersBlurred.w));
}