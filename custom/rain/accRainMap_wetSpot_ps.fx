#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  clip(1 - length(pin.Tex));
  return 1;
}