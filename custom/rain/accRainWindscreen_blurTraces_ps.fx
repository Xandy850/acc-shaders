#include "include/common.hlsl"

Texture2D<float4> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;
  float tot = 0;

  #define PUDDLES_DISK_SIZE 7
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.002;
    ret += txDiffuse.SampleLevel(samLinearClamp, pin.Tex + offset, 0);
    tot += 1;
  }

  float4 base = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
  ret = (ret + base) / (tot + 1);
  tot += 1;

  return float4(ret.x, base.y, ret.z, base.w);
}
