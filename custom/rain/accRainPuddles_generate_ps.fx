#include "include/common.hlsl"
Texture2D<float> txHR : register(t0);
Texture2D<float> txLR : register(t1);
Texture2D<float> txWireframe : register(t2);
Texture2D<float> txDetailedMask : register(t3);
Texture2D<float> txPuddles : register(t26);
Texture2D<float> txNoise : register(t20);

cbuffer cbPiece : register(b11) {
  float4x4 gViewProj;
  float2 gArea0;
  float2 gArea1;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};
  
#include "include/poisson.hlsl"

float sampleWir(float2 uv){
  float ret = 0;
  #define R 1
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    ret = max(ret, txWireframe.SampleLevel(samLinearSimple, uv, 1, int2(x, y) * 1));
  }
  return saturate(ret * 4);
}

float detailedMask(float2 uv){
  POISSON_AVG(float, v, txDetailedMask, samLinearSimple, uv, 0.003, 12);

  // v = txDetailedMask.SampleLevel(samLinearSimple, uv, 0);
  return 1 - v;
}

float sampleDifference(float2 uv, float2 uvOrig, float4 posH){
  float wireframeAmount = sampleWir(uv) * detailedMask(uv);
  float fallbackValue = txPuddles.SampleLevel(samLinearSimple, lerp(gArea0, gArea1, uvOrig) * 0.02, 0);
  fallbackValue = pow(saturate(remap(fallbackValue, 0.5, 0.7, 0, 0.25)), 1.6);

  float4 noiseLR = txNoise.SampleLevel(samLinear, uv * 1, 0) - 0.5;
  // uv += noise.xy * 0.002;

  float local;
  // local = min(txLR.SampleLevel(samLinearSimple, uv, 0.5), txHR.SampleLevel(samLinearSimple, uv, 0));
  // local = txLR.SampleLevel(samLinearSimple, uv, 0);
  local = txHR.SampleLevel(samLinearSimple, uv, 0);
  // local = lerp(txHR.SampleLevel(samLinearSimple, uv, 0), txLR.SampleLevel(samLinearSimple, uv, 1.5), wireframeAmount);

  float cy = ddy(local) * 1024;
  float cx = ddx(local) * 1024;
  // cx = 0;
  // cy = 0;

  float sum = 0;
  float sampleScale = 0.005 * (1 + noiseLR.x);

  #define RAIN_DISK_SIZE 32
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    float2 dir = SAMPLE_POISSON(RAIN_DISK_SIZE, i);
    // float2 offset = normalize(dir) * sampleScale;
    float2 offset = (dir) * sampleScale;
    float refHeight = local + dot(1, offset * float2(cx, cy));

    float nearby;
    nearby = txHR.SampleLevel(samLinearClamp, uv + offset, 0);
    // nearby = txHR.SampleLevel(samPointClamp, uv + offset, 0);

    float dif = refHeight - nearby;
    sum += abs(dif) > 0.1 ? 0 : dif;
  }

  sum = abs(sum) > 0.5 ? 0.5 : saturate(sum * 0.5e5 / RAIN_DISK_SIZE);
  sum = lerp(sum, fallbackValue, wireframeAmount);
  return sum;
}

float main(VS_Copy pin) : SV_TARGET {
  // float paddingAdj = 0.52;
  // float2 uvAdj = (pin.Tex * 2 - 1) * (0.5 / paddingAdj) * 0.5 + 0.5;
  float2 uvAdj = pin.Tex;
  return sampleDifference(uvAdj, pin.Tex, pin.PosH);
}