#include "../../custom_objects/common/bending.hlsl"
#include "accRainWindscreen.hlsl"

BENDING_FX_IMPL

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
}

cbuffer cbData : register(b10) {
  float gInstanceMult;
  float gDistanceThreshold;
  float gResolutionStep;
  float gVelocityMult;
}

cbuffer cbDataObject : register(b11) {
  float4x4 gWorldNow;
  float4x4 gWorldPrev;
}

struct PS_IN_wipers {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosL : TEXCOORD0;
  noperspective float3 NormalL : TEXCOORD1;
};

PS_IN_wipers main(VS_IN_ac vin, uint instanceID : SV_InstanceID) {
  PS_IN_wipers vout;

  float4 posW0 = mul(vin.PosL, gWorldNow);
  #ifdef WITH_BENDING
    alterPosBending(vin.PosL.xyz, false, posW0.xyz);
  #endif

  float4 posW1 = mul(vin.PosL, gWorldPrev);
  #ifdef WITH_BENDING
    alterPosBending(vin.PosL.xyz, true, posW0.xyz);
  #endif

  float4 posW = lerp(posW0, posW1, (float)instanceID * gInstanceMult);
  float4 posV = mul(posW, ksView);
  float3 velocity = mul(mul(posW0.xyz - posW1.xyz, (float3x3)ksView), (float3x3)ksProjection) * gVelocityMult;
  // float3 velocity = mul(posW0.xyz - posW1.xyz, (float3x3)ksView) * 25;
  vout.PosH = mul(posV, ksProjection);
  // vout.PosH.z = 0;
  // vout.PosH.w = 1;
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = velocity; // TODO: car transform?
  return vout;
}
