RWTexture2D<float4> rwWiperDirt : register(u7);

[numthreads(256, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  rwWiperDirt[threadID.xy] = saturate(rwWiperDirt[threadID.xy]) - 0.02;
  // rwWiperDirt[threadID.xy] = 0;
}