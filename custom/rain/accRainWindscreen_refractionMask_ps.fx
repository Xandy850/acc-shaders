struct PS_IN {
  float4 PosH : SV_POSITION;
};

cbuffer cbCamera : register(b10) {
  float gValue;
}

float4 main(PS_IN pin) : SV_TARGET {
  return gValue;
}

