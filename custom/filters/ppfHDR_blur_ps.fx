#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gStep;
  float3 gPad0;
}

#include "include/poisson.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float R = lerp(0.003, 0.3, pow((gStep - 1) / 8, 1.8));
  float2 random = normalize(txNoise.SampleLevel(samLinear, pin.PosH.xy / 256, 0).xy);
  // float2 random = normalize(txNoise.SampleLevel(samPoint, gStep/32, 0).xy);

  float4 ret = 0;
  float tot = 0;
  #define DISK_SIZE 52
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    offset = reflect(offset, random);

    float2 sampleUV = pin.Tex + offset * float2(1, 16. / 9) * R;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1 / (0.1 + SAMPLE_POISSON_LENGTH(DISK_SIZE, i));
    ret += v * w;
    tot += 1.2;
  }
  return ret / tot;
}