cbuffer cbData : register(b10){
  float gTimeS;
  float gPaused;
  float gBrightnessMult;
  float gPad0;
}

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 rgbToYiq(float3 c){   
  return mul(float3x3(
    0.299, 0.596, 0.211,
    0.587, -0.274, -0.523,
    0.114, -0.322, 0.312), c);
}

float3 yiqToRgb(float3 c){
  return mul(float3x3(
    1, 1, 1,
    0.956, -0.272, -1.106,
    0.621, -0.647, 1.703), c);
}

float rand(float2 co){
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

float3 blur(float2 uv, float d, float o){
  float3 blurred = 0;
  for (int i = -7; i <= 7; i++){
    float2 newUv = uv + float2((1.0 - 1.5 * (o + i)) * d, 0);
    blurred += clamp(txDiffuse.SampleLevel(samLinearSimple, newUv, 0).rgb, 0, 2);
  }  
  return blurred / 15;
}

float waveOffset(float pos, float y){
  return smoothstep(pos - 0.05, pos, y) - smoothstep(pos, pos + 0.05, y);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex;

  const float offsetIntensity = 0.01;
  for (float j = 0.0; j < 5; j++) {
    float d = fmod(gTimeS * j * 0.12, 7);
    float o = sin(1.0 - tan(gTimeS * j * 0.024));
    uv.x += waveOffset(d, uv.y) * 0.005;
  }

  [branch]
  if (gPaused){
    uv += float2(
      (rand(float2(gTimeS, uv.y)) - 0.5) / 240,
      (rand(gTimeS) - 0.5) / 135);
    float noise = rand(float2(floor(uv.y * 240.0) + gTimeS, floor(uv.x * 150.0)));
    if (30 * uv.y - 18.5 > noise && noise > 5 * uv.y - 3.5) {
      return gBrightnessMult;
    }
  }

  float o = rand(uv);
  float lineValue = round(uv.y * 240) / 240;
  uv.x += 0.001 * rand(float2(gTimeS * 0.00001, lineValue));

  float4 baseColor = txDiffuse.SampleLevel(samLinearSimple, uv, 0);
  float4 nearbyColor = txDiffuse.SampleLevel(samLinearSimple, uv - float2(0, 0.05), 0);
  
  float3 colorNoise = (-0.5 + float3(
    rand(float2(lineValue, gTimeS)),
    rand(float2(lineValue, gTimeS + 1.0)),
    rand(float2(lineValue, gTimeS + 2.0)))) * 0.03 * (0.5 + saturate(dot(nearbyColor.rgb - baseColor.rgb, 10)));

  float s = 0.1;
  float e = min(0.3, pow(max(0, cos(uv.y * 4 + 0.3) - 0.75) * (s + 0.5), 3)) * 25;
  uv.x += e * 0.3;
  
  float y = rgbToYiq(blur(uv, 0.0002, o)).r;    
  float i = rgbToYiq(blur(uv, 0.0018, o)).g;
  float q = rgbToYiq(blur(uv, 0.0027, o)).b;
  return float4(yiqToRgb(float3(y, i, q)) * gBrightnessMult + colorNoise, 1);
}