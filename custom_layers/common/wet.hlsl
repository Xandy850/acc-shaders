cbuffer cbMaterial : register(b5) {
  float4x4 extTransform;
  float4x4 extWipersTransform;

  float3 extUp;
  float extUpAligned;

  float3 extLeft;
  float extRainMult;

  float3 extFwd;
  float extSceneRain;

  float3 extVelocity;
  float extRainPad3;

  float4x4 extShadowTransform;
}
