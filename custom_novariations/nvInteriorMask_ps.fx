#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

struct PS_IN_InteriorMask {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
};

#define DISCARD_AREAS_NUM 4
cbuffer cbDiscardAreas : register(b5) {
  float4 area[DISCARD_AREAS_NUM];
}

float main(PS_IN_InteriorMask pin) : SV_TARGET {
  bool d = false;
  [unroll] for (int i = 0; i < DISCARD_AREAS_NUM; i++){
    float3 dif = pin.PosC - area[i].xyz;
    if (dot2(dif) < area[i].w) d = true;
  }
  if (d) discard;
  return 0;
}
