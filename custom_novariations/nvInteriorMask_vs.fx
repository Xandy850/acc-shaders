#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct PS_IN_InteriorMask {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
};

float4 toScreenSpaceAlt(float4 posL, out float4 posW, out float4 posV){
  posW = mul(posL, ksWorld);
  posV = mul(posW, ksView);
  return mul(posV, ksProjection);
}

PS_IN_InteriorMask main(VS_IN vin) {
  PS_IN_InteriorMask vout;
  float4 posW, posV;
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  return vout;
}
