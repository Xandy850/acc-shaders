#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define EMISSIVE_SLOT 5
#include "lightingBounceBack.hlsl"
#include "emissiveMapping.hlsl"

RESULT_TYPE main(PS_IN_NmExtra4 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);
  
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, false);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW);

  float4 emissiveMap;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.txEmissiveValue = getEmissiveValue(txDiffuseValue, pin.Tex, pin.Extra, emissiveMap);
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting(L);
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_MAPSMASK * saturate(dot(emissiveMap, extBounceBackMask)));
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);
}
