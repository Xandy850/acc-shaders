#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixelAT_vs
// alias: ksPerPixel_tilingfix_vs

PS_IN_PerPixel main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixel);
  RAINFX_VERTEX(vout);
  return vout;
}
