#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#define RAINFX_STATIC_OBJECT
#define AO_MIX_INPUT 0
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixel);
  return vout;
}
