#define NO_CARPAINT
#define SUPPORTS_AO

#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = textureSampleVariation(txDiffuse, samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  #if defined ALLOW_EXTRA_FEATURES && ! defined NO_LIGHTING
    if (ksAlphaRef == -193){
      lighting += ksEmissive * saturate(dot(normalize(-toCamera), normalize(normalW)) * 4 - 3) * 12;
    }
  #endif

  RETURN_BASE(lighting, txDiffuseValue.a);
}
