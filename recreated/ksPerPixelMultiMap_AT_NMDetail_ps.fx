#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_AT_NMDETAILS
#define CARPAINT_NMDETAILS
// #define A2C_SHARPENED
#define USE_GETNORMALW
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW);
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_MAPSMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  RAINFX_WATER(withReflection);
  RETURN(withReflection, alpha);
}
