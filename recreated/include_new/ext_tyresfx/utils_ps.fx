#define DIRT_BLUR_LAYERS 3

float2 getTyreCoords(float3 posW){
  float3 dir = normalize(posW - wheelPos);
  float sideDot = dot(dirSide, dir);
  dir = normalize(dir - dirSide * sideDot);

  float upDot = dot(dirUp, dir);
  float forwardDot = dot(dirForward, dir);
  float angle = acos(upDot) / (M_PI * 2.0);
  float onCircle = forwardDot > 0.0 ? angle : 1 - angle;
  return float2(onCircle, sideDot);
}

void getTyreColor(float3 posW, inout float3 normalW, float3 targentW, float3 bitangentW, 
    inout float lightingMult, inout float specMult, inout float specExpMult, inout float reflMult){
  float2 tyreCoords = getTyreCoords(posW);

  float toUp = damageWidthK - abs(tyreCoords.y + damageOffsetK) * 2.0;
  toUp = saturate((toUp * 0.7 - 0.5) * 3.0 + 0.5);

  #if DIRT_BLUR_LAYERS == 1
    float blurLayer = 0.5;
  #else
    float blurLevelAdj = blurLevel * blurLevel;
    float blurLayer = (0.5 / DIRT_BLUR_LAYERS) + blurLevelAdj * (1.0 - 1.0 / DIRT_BLUR_LAYERS);
  #endif

  float2 dmgBoth = txTyresDamage.Sample(samLinearSimple, float2(tyreCoords.x, blurLayer)) * toUp;
  dmgBoth = dmgBoth * dmgBoth;

  float dmg = max(dmgBoth.x, dmgBoth.y);

  [branch]
  if (dmg < 0.001) return;

  float2 uv = float2(tyreCoords.x, tyreCoords.y + 0.5);
  float4 txTyresDamageNmValue = txTyresDamageNm.SampleLevel(samLinear, uv, blurLevel);
  float4 txTyresMarblesNmValue = txTyresMarblesNm.SampleLevel(samLinear, uv, blurLevel);

  // blending two together
  txTyresDamageNmValue = (txTyresDamageNmValue * dmgBoth.x + txTyresMarblesNmValue * dmgBoth.y) / (0.0001 + dmgBoth.x + dmgBoth.y);

  float3 txTyresDamageNmAligned = txTyresDamageNmValue.xyz * 2 - (float3)1;
  txTyresDamageNmAligned.y *= -1;

  float dmgMat = dmg * saturate(abs(txTyresDamageNmAligned.y) * 5);
  specMult *= lerp(1.0, damageSpecSpecExpReflMult.x, dmgMat);
  specExpMult *= lerp(1.0, damageSpecSpecExpReflMult.y, dmgMat);
  reflMult *= lerp(1.0, damageSpecSpecExpReflMult.z, dmgMat);
  lightingMult = 1.0 - (dmgBoth.x * txTyresDamageNmValue.a + dmgBoth.y * txTyresMarblesNmValue.a) * damageOcclusionMult;

  float3 T = cross(dirSide, normalW);
  float3 B = cross(T, normalW);
  float3x3 m = float3x3(B, normalW, T);
  float3 normalDmg = normalize(mul(transpose(m), txTyresDamageNmAligned.xzy));
  normalW = lerp(normalW, normalDmg, dmg * damageNormalsMult);
}

float getTyreColorDirt(float3 posW, inout float3 normalW, float3 targentW, float3 bitangentW, 
    inout float3 color, inout float lightingMult, inout float3 spec_specExp_refl){
  float2 tyreCoords = getTyreCoords(posW);

  #if DIRT_BLUR_LAYERS == 1
    float blurLayer = 0.5;
  #else
    float blurLevelAdj = blurLevel * blurLevel;
    float blurLayer = (0.5 / DIRT_BLUR_LAYERS) + blurLevelAdj * (1.0 - 1.0 / DIRT_BLUR_LAYERS);
  #endif

  float2 dmgBoth = txTyresDamage.SampleLevel(samLinearSimple, float2(tyreCoords.x, blurLayer), 0);
  float dmg = max(dmgBoth.x, dmgBoth.y);
  float toUpDmg = saturate(damageWidthK * pow(saturate(dmg), 0.3) - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.0);
  dmgBoth *= toUpDmg;
  dmg *= toUpDmg;

  float toUpDirtBase = saturate(dirtWidthK - abs(tyreCoords.y) * 4.2);
  float toUpDirt = toUpDirtBase * dirtFade;
  float3 dirtBoth = txTyresDirt.SampleLevel(samLinearSimple, float2(tyreCoords.x, blurLayer), 0).xyz;
  dirtBoth.xy *= toUpDirt;

  // [branch]
  // if (dmg < 0.001 && dot(dirtBoth, 1) < 0.001) return;

  float2 uv = float2(tyreCoords.x, tyreCoords.y + 0.5);
  float4 txTyresDamageNmValue = txTyresDamageNm.SampleLevel(samLinear, uv, blurLevel);
  float4 txTyresMarblesNmValue = txTyresMarblesNm.SampleLevel(samLinear, uv, blurLevel);

  float level = txTyresGrassComb.CalculateLevelOfDetail(samLinearSimple, uv) - 2;
  float uvYDirt = ((uv.y + 2) % 1.0) * 0.5;
  float4 txTyresGrassCombValue = lerp(
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt), level),
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt + 0.5), level), blurLevel);
  float4 txTyresDirtCombValue = lerp(
    txTyresDirtComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt), level),
    txTyresDirtComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt + 0.5), level), blurLevel);

  float dirtK = saturate(dirtBoth.y * txTyresDirtCombValue.a);
  float grassK = saturate((dirtBoth.x - (1 - txTyresGrassCombValue.a)) * 12);
  float ambientMult = normalW.y * 0.25 + 0.75;
  float combinedDirt = saturate(dirtK + grassK);
  color = lerp(color, (dirtColorA + dirtColorB * txTyresDirtCombValue.z) * (1 - grassK * 0.8) * ambientMult, dirtK);
  color = lerp(color, (grassColorA + grassColorB * txTyresGrassCombValue.z) * ambientMult, grassK);

  // blending two together
  txTyresDamageNmValue = (
    txTyresDamageNmValue * dmgBoth.x 
    + txTyresMarblesNmValue * dmgBoth.y
    + float4(txTyresDirtCombValue.xy, 1, 0) * dirtK) / (0.001 + dmgBoth.x + dmgBoth.y + dirtK);

  float3 txTyresDamageNmUnaligned = lerp(txTyresDamageNmValue.xyz, float3(txTyresGrassCombValue.xy, 1), grassK);
  float3 txTyresDamageNmAligned = txTyresDamageNmUnaligned * 2 - (float3)1;
  txTyresDamageNmAligned.y *= -1;

  float dmgMat = dmg * saturate(abs(txTyresDamageNmAligned.y) * 5);
  spec_specExp_refl += spec_specExp_refl * dmgMat * (damageSpecSpecExpReflMult - 1);
  lightingMult = 1.0 - saturate(dot(dmgBoth, float2(txTyresDamageNmValue.a, txTyresMarblesNmValue.a)))
    * damageOcclusionMult * (1 - combinedDirt);

  spec_specExp_refl = lerp(spec_specExp_refl, float3(0.06, 20, 0.05 * ambientMult), dirtK);
  spec_specExp_refl = lerp(spec_specExp_refl, float3(0.12, 40, 0.1 * ambientMult), grassK);

  float3 T = cross(dirSide, normalW);
  float3 B = cross(T, normalW);
  float3x3 m = float3x3(B, normalW, T);
  float3 normalDmg = normalize(mul(transpose(m), txTyresDamageNmAligned.xzy));
  normalW = lerp(normalW, normalDmg, lerp(dmg * damageNormalsMult, 1, combinedDirt));

  float waterUp = saturate(damageWidthK - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.0);
  return lerp(extSceneWetness * perObjAmbientMultiplier, dirtBoth.z, saturate(toUpDirtBase * 4 - 3));
}

#define TYRESFX(x, y, z, w) getTyreColor(pin.PosC, normalW, tangentW, bitangentW, x, y, z, w);
#define TYRESFX_DIRT(c, x, y) float water = getTyreColorDirt(pin.PosC, normalW, tangentW, bitangentW, c, x, y);