// 8 KB:
#define MAX_VALUES_AMOUNT 500

// 12 KB:
#define MAX_PACKEDBVHLIGHT_AMOUNT 375

#define MAX_SHADOWS_AMOUNT 32

// #define BVH_NODE_HALF
#define LIGHTS_COMPACT_HALF

// LI stands for “length inverted”
// S0 stands for “start”, in pair with LI
// DirW is premultiplied by SpotCosLI to save space
// UpW is premultiplied to save space

// common
#define L_PosW                        gLightsData[_lightData + 0].xyz
#define L_RangeS0LI                   asuint(gLightsData[_lightData + 0].w)

// spot, car
#define L_DirW_SpotCosS0_F            gLightsData[_lightData + 1]
#define L_Color_SpecMult_G            asuint(gLightsData[_lightData + 2].xy)

// spot
#define L_DiffConc_SingWav_NoShadw_   asuint(gLightsData[_lightData + 2].zw)

// car
#define L_SpotEdgeOff_SSInt           asuint(gLightsData[_lightData + 2].zw)
#define L_SecSpotCosS0LI_SecTrimS0LI  asuint(gLightsData[_lightData + 3].xy)
#define L_UpW_SecondRangeLI           asuint(gLightsData[_lightData + 3].zw)

// line
#define L_Color_SpecMult_L            asuint(gLightsData[_lightData + 1].xy)
#define L_DiffBA_DistInv              asuint(gLightsData[_lightData + 1].zw)
#define L_DirW_SpotCosS0_L            asuint(gLightsData[_lightData + 2].xy)
#define L_Color2_DiffConc             asuint(gLightsData[_lightData + 2].zw)

#ifndef LIGHTINGFX_KSDIFFUSE
#define LIGHTINGFX_KSDIFFUSE ksDiffuse
#endif

struct sPackedBVHLight {
  float2 mCenter;

  #ifdef BVH_NODE_HALF
    uint2 mHalfSize_MI;
    // 16 bytes

  #else
    float2 mHalfSize;
    int mLightIndex;
    int mMissIndex;
    int mLightType;
    int mShadowIndex;
    // 32 bytes
  #endif
};

#ifdef ALLOW_DYNAMIC_SHADOWS
  Texture2D<float> txShadowsAtlas : register(t13);

  struct sShadowParams {
    float2 mCenter;
    float mSize;
    float mRangeInvExpFactor;
    float mMaxRange;
    float mPad;
    float mThicknessFixMult;
    float mThicknessFixAdd;
    float4x4 mTransform;
  };
#endif

cbuffer cbExtLighting : register(b7) {
  float4 gNoLightAt[2];
  float2 gNoLightEdgeA, gNoLightEdgeB;

  sPackedBVHLight gLightBVHNodeData[MAX_PACKEDBVHLIGHT_AMOUNT];
  float4 gLightsData[MAX_VALUES_AMOUNT];

  #ifdef ALLOW_DYNAMIC_SHADOWS
    sShadowParams gShadowParams[MAX_SHADOWS_AMOUNT];
  #endif
}

#ifdef ALLOW_DYNAMIC_SHADOWS
  #include "include/poisson.hlsl"
  #include "include/samplers.hlsl"

  float lfxDynamicShadow(int index, float3 posW, float distanceToLight, float biasMult){
    sShadowParams params = gShadowParams[index];

    #ifdef LIGHTINGFX_SHADOW_GRASS_OFFSET
      posW.y -= 0.1;
    #endif

    float4 posH = mul(float4(posW, 1), params.mTransform);
    posH.xyz /= posH.w;

    [branch]
    if (all(abs(posH.xy - params.mCenter) < params.mSize) && posH.w > 0){
      // return 0;
      // return txShadowsAtlas.SampleLevel(samLinearSimple, posH.xy, 0) < distanceToLight ? 1 : 0;
      // return txShadowsAtlas.SampleCmpLevelZero(samShadow, posH.xy, posH.z);

      // float2 relative = (posH.xy - params.mCenter) / params.mSize;
      // float2 pos = pow(abs(relative), 0.8);
      // posH.xy = pos * sign(relative) * params.mSize + params.mCenter;

      distanceToLight = min(distanceToLight, params.mMaxRange);
      float occluder = txShadowsAtlas.SampleLevel(samLinearSimple, posH.xy, 0);
      float receiver = exp(0.1 * biasMult - distanceToLight * params.mRangeInvExpFactor);
      return saturate((occluder * receiver) * params.mThicknessFixMult + params.mThicknessFixAdd);

      // #define LFX_DISK_SIZE 8
      // float ret = 0;
      // for (uint i = 0; i < LFX_DISK_SIZE; ++i) {
      //   float2 sampleUV = posH.xy + SAMPLE_POISSON(LFX_DISK_SIZE, i) * 0.0005;
      //   ret += txShadowsAtlas.SampleCmpLevelZero(samShadow, sampleUV, posH.z);
      // }
      // return ret / LFX_DISK_SIZE;
    } else {
      return 1;
    }
  }

  #define ALLOW_DYNAMIC_SHADOWS_ARG int shadowIndex,
  #define ALLOW_DYNAMIC_SHADOWS_ARG_PASS node.mShadowIndex,
#else
  #define ALLOW_DYNAMIC_SHADOWS_ARG 
  #define ALLOW_DYNAMIC_SHADOWS_ARG_PASS
#endif

float getNoLightMultiplier(int index, float2 edge, float3 posW){
  float3 v = posW - gNoLightAt[index].xyz;
  float d = dot(v, v);
  return saturate(d * edge.y - edge.x);
}

float getNoLightMultiplier(float3 posW){
  return saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) * getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
}

float2 LFX_loadVector(uint vec){
  return float2(f16tof32(vec), f16tof32(vec >> 16));
}

float4 LFX_loadVector(uint2 vec){
  return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float4 LFX_loadVector(float4 vec){
  return vec;
}

struct LFX_MainLight {
  #ifdef LIGHTINGFX_FIND_MAIN
    float3 dir;
    float power;

    void update(float3 diffuseAdd, float3 toLight){
      float p = dot(diffuseAdd, 1);
      if (p > power){
        dir = toLight;
        power = p;
      }
    }
  #endif
};

struct LFX_Result {
  float3 diffuse;
  float3 specular;
  #ifdef HAS_SUN_SPECULAR
    float3 sunSpecular;
  #endif
  #ifdef LIGHTINGFX_BOUNCEBACK
    float3 bounceBack;
  #endif
  #ifdef LIGHTINGFX_RAINFX
    float3 rain;
  #endif
  #ifdef LIGHTINGFX_FUR
    float3 fur;
  #endif
};

#ifdef LIGHTINGFX_RAINFX
  #define LIGHTINGFX_INPUT_NORMAL_RAIN ,rainNormal
#else
  #define LIGHTINGFX_INPUT_NORMAL_RAIN
#endif

#ifdef LIGHTINGFX_FUR
  #define LIGHTINGFX_INPUT_FUR_MULT ,furLightingMult
#else
  #define LIGHTINGFX_INPUT_FUR_MULT
#endif

#ifndef LIGHTINGFX_RAIN_EXP
  #define LIGHTINGFX_RAIN_EXP 250
#endif

#ifdef LIGHTINGFX_TWO_NORMALS
  #define LIGHTINGFX_INPUT_NORMAL_INNER normalW0, normalW1, normalMix
#else
  #define LIGHTINGFX_INPUT_NORMAL_INNER normalW
#endif

#ifndef LIGHTINGFX_INPUT_NORMAL
  #define LIGHTINGFX_INPUT_NORMAL LIGHTINGFX_INPUT_NORMAL_INNER LIGHTINGFX_INPUT_NORMAL_RAIN LIGHTINGFX_INPUT_FUR_MULT
#endif

#ifdef LIGHTINGFX_RAINFX
  #define LIGHTINGFX_ARGS_NORMAL_RAIN ,float3 rainNormal
#else
  #define LIGHTINGFX_ARGS_NORMAL_RAIN
#endif

#ifdef LIGHTINGFX_FUR
  #define LIGHTINGFX_ARGS_FUR ,float furLightingMult
#else
  #define LIGHTINGFX_ARGS_FUR
#endif

#ifdef LIGHTINGFX_TWO_NORMALS
  #define LIGHTINGFX_ARGS_NORMAL float3 normalW0, float3 normalW1, float normalMix LIGHTINGFX_ARGS_NORMAL_RAIN LIGHTINGFX_ARGS_FUR
#else
  #define LIGHTINGFX_ARGS_NORMAL float3 normalW LIGHTINGFX_ARGS_NORMAL_RAIN LIGHTINGFX_ARGS_FUR
#endif

#ifndef LIGHTINGFX_BOUNCEBACK_EXP
  #define LIGHTINGFX_BOUNCEBACK_EXP 100
#endif

#include "utils_ps.fx"

void getLight_spot(ALLOW_DYNAMIC_SHADOWS_ARG uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 txDiffuseValue, float specularExp,
    #ifdef HAS_SUN_SPECULAR
      float sunSpecularExp,
    #endif 
    inout LFX_Result R, inout LFX_MainLight ML) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);
  float spotK = getSpotCone(DirW_SpotCosS0.xyz, toLight.xyz, DirW_SpotCosS0.w);

  [branch]
  if (spotK == 0) return;

  float4 DiffConc_SingWav_NoShadw_= LFX_loadVector(L_DiffConc_SingWav_NoShadw_);
  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(DiffConc_SingWav_NoShadw_.z + getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
  #endif

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float LdotN = getDiffuseMultiplier(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz, DiffConc_SingWav_NoShadw_.x);
  float shadow = attenuation * spotK * noLight;

  #ifdef ALLOW_DYNAMIC_SHADOWS
    shadow *= lfxDynamicShadow(shadowIndex, posW, toLight.w, 0);
  #endif

  #ifdef LIGHTINGFX_EXTRA_FOCUSED
    LdotN *= lerp(0.1, 1, getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, 8));
  #endif

  float3 lightColorAdj = Color_SpecMult.rgb * shadow;
  float3 diffColor = lightColorAdj * lerp(txDiffuseValue,
    (float3)normalize2Colors(Color_SpecMult.rgb, txDiffuseValue) * luminance(txDiffuseValue), 
    DiffConc_SingWav_NoShadw_.y);
  #if defined(LIGHTINGFX_GLASS_BACKLIT) && defined(ALLOW_LIGHTINGFX_BACKLIT)
    if (dot(normalW, toLight.xyz) < 0){
      diffColor = lerp(dot(diffColor, 0.33), diffColor, 2) * 2;
    }
  #endif
  float3 diffuseAdd = LIGHTINGFX_KSDIFFUSE * LdotN * diffColor;
  R.diffuse += diffuseAdd;

  #ifdef LIGHTINGFX_RAINFX
    R.rain += getSpecularComponent(rainNormal, posW, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj;
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += lightColorAdj
      * saturate(dot(toLight.xyz, normalize(posW))) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR)
    float3 specBase = lightColorAdj * LdotN * Color_SpecMult.a;
    float3 specAdd = specBase * getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, specularExp);
    diffuseAdd += specAdd;
    R.specular += specAdd;
    #ifdef HAS_SUN_SPECULAR
      float3 sunSpecAdd = specBase * getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, sunSpecularExp);
      diffuseAdd += sunSpecAdd;
      R.sunSpecular += sunSpecAdd;
    #endif
    #ifdef LIGHTINGFX_BOUNCEBACK
      float3 bbAdd = pow(saturate(-dot(normalize(posW), toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
      diffuseAdd += bbAdd;
      R.bounceBack += bbAdd;
    #endif
  #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd, toLight.xyz);
  #endif
}

void getLight_car(ALLOW_DYNAMIC_SHADOWS_ARG uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 txDiffuseValue, float specularExp, 
    #ifdef HAS_SUN_SPECULAR
      float sunSpecularExp,
    #endif
    inout LFX_Result R, inout LFX_MainLight ML) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);
  float4 SecSpotCosS0LI_SecTrimS0LI = LFX_loadVector(L_SecSpotCosS0LI_SecTrimS0LI);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);
  float spotK1 = lerpInv10(cosAngle, SecSpotCosS0LI_SecTrimS0LI.x, SecSpotCosS0LI_SecTrimS0LI.y);

  [branch]
  if (spotK0 == 0 && spotK1 == 0) return;

  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);
  float4 UpW_SecondRangeLI = LFX_loadVector(L_UpW_SecondRangeLI);
  float4 SpotEdgeOff_SSInt = LFX_loadVector(L_SpotEdgeOff_SSInt);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float secondAttenuation = getTrimmedAttenuation(UpW_SecondRangeLI.w, SecSpotCosS0LI_SecTrimS0LI.z, SecSpotCosS0LI_SecTrimS0LI.w, toLight.w);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  #endif

  float LdotN = getDiffuseMultiplier(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz);
  float3 shadow = (spotK0 * (attenuation * getEdge(UpW_SecondRangeLI.xyz, toLight.xyz, SpotEdgeOff_SSInt.xyz))
      + spotK1 * secondAttenuation * SpotEdgeOff_SSInt.w)
    * noLight;

  #ifdef ALLOW_DYNAMIC_SHADOWS
    shadow *= lfxDynamicShadow(shadowIndex, posW, toLight.w, 1 - abs(dot(toLight.xyz, normalW)));
  #endif

  #ifdef LIGHTINGFX_EXTRA_FOCUSED
    LdotN *= lerp(0.1, 1, getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, 8));
  #endif

  float3 lightColorAdj = Color_SpecMult.rgb * shadow;
  float3 diffColor = lightColorAdj * txDiffuseValue;
  float3 diffuseAdd = LIGHTINGFX_KSDIFFUSE * LdotN * diffColor;
  R.diffuse += diffuseAdd;

  #ifdef LIGHTINGFX_RAINFX
    R.rain += getSpecularComponent(rainNormal, posW, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj;
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += lightColorAdj
      * saturate(dot(toLight.xyz, normalize(posW))) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR)
    float3 specBase = lightColorAdj * LdotN * Color_SpecMult.a;
    float3 specAdd = specBase * getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, specularExp);
    diffuseAdd += specAdd;
    R.specular += specAdd;
    #ifdef HAS_SUN_SPECULAR
      float3 sunSpecAdd = specBase * getSpecularComponent(LIGHTINGFX_INPUT_NORMAL_INNER, posW, toLight.xyz, sunSpecularExp);
      diffuseAdd += sunSpecAdd;
      R.sunSpecular += sunSpecAdd;
    #endif
    #ifdef LIGHTINGFX_BOUNCEBACK
      float3 bbAdd = pow(saturate(-dot(normalize(posW), toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
      diffuseAdd += bbAdd;
      R.bounceBack += bbAdd;
    #endif
  #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd, toLight.xyz);
  #endif
}

void getLight_line(uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, const float3 txDiffuseValue, const float specularExp,
    inout LFX_Result R, inout LFX_MainLight ML) {
  float4 DiffBA_DistInv = LFX_loadVector(L_DiffBA_DistInv);
  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_L);
  float4 Color2_DiffConc = LFX_loadVector(L_Color2_DiffConc);
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_L);

  float abValue;
  float3 closestPoint = closestPointOnSegment(L_PosW.xyz, DiffBA_DistInv.xyz, DiffBA_DistInv.w, posW, abValue);
  float4 toLight = normalizeWithDistance(closestPoint - posW);
  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);

  [branch]
  if (attenuation == 0) return;

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);

  float LdotN = getDiffuseMultiplier(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz, Color2_DiffConc.w);
  float shadow = attenuation * spotK0;

  float3 lightColorAdj = (Color_SpecMult.rgb + Color2_DiffConc.rgb * saturate(abValue)) * shadow;
  float3 diffColor = lightColorAdj * txDiffuseValue;
  float3 diffuseAdd = LIGHTINGFX_KSDIFFUSE * LdotN * diffColor;
  R.diffuse += diffuseAdd;

  #ifdef LIGHTINGFX_RAINFX
    R.rain += getSpecularComponent(rainNormal, posW, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj;
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += lightColorAdj
      * saturate(dot(toLight.xyz, normalize(posW))) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR) && !defined(LIGHTINGFX_TWO_NORMALS)
    [branch]
    if (Color_SpecMult.a == 0) return;

    float3 r = reflect(normalize(posW), LIGHTINGFX_INPUT_NORMAL_INNER);

    // First, the closest point to the ray on the segment
    float3 L0 = L_PosW.xyz - posW;
    float3 L1 = L_PosW.xyz + DiffBA_DistInv.xyz - posW;
    float3 Ld = DiffBA_DistInv.xyz;

    float t = dot(r, L0) * dot(r, Ld) - dot(L0, Ld);
    t /= dot(Ld, Ld) - sqr(dot(r, Ld));

    float3 L = (L0 + saturate(t) * Ld);

    float3 centerToRay = dot(L, r) * r - L;
    L = L + centerToRay * saturate((1.2 - Color2_DiffConc.a) / 4 / length(centerToRay));

    float nDotH = getNDotH(LIGHTINGFX_INPUT_NORMAL_INNER, posW, normalize(L));
    float3 specAdd = lightColorAdj * LdotN * calculateSpecularLight(nDotH, specularExp) * Color_SpecMult.a;
    diffuseAdd += specAdd;
    R.specular += specAdd;
    // #ifdef LIGHTINGFX_BOUNCEBACK
    //   float3 bbAdd = pow(saturate(-dot(normalize(posW), toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
    //   diffuseAdd += bbAdd;
    //   R.bounceBack += bbAdd;
    // #endif
  #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd, toLight.xyz);
  #endif
}
 
float3 getDynamicLights(LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 txDiffuseValue, const float specularExp, const float3 specularValue
  #ifdef HAS_SUN_SPECULAR
    , float sunSpecularExp, float3 sunSpecularValue
  #endif
  #ifdef LIGHTINGFX_RAINFX
    , float rainAlpha
  #endif
  #ifdef LIGHTINGFX_BOUNCEBACK
    , float3 bounceBackMult
  #endif
  #ifdef LIGHTINGFX_FIND_MAIN
    , out LFX_MainLight ML
  #endif
    ) {
  #ifdef NO_EXTSPECULAR
  [branch]
  if (ksSpecular >= 0){
  #endif

  #ifdef LIGHTINGFX_TREE
    #ifdef LIGHTINGFX_TWO_NORMALS
      normalW0 = float3(0, 1, 0);
      normalW1 = float3(0, 1, 0);
      normalMix = 0;
    #else
      normalW = float3(0, 1, 0);
    #endif
  #endif

  LFX_Result R = (LFX_Result)0;
  
  #ifndef LIGHTINGFX_FIND_MAIN
    LFX_MainLight ML;
  #endif
  ML = (LFX_MainLight)0;

  float distSqr = dot(posW, posW);
  int index = 0;
  int safeStop = 0;

  [loop]
  while (true) {
    sPackedBVHLight node = gLightBVHNodeData[index];

    #ifdef BVH_NODE_HALF
      float2 halfSize = float2(f16tof32(node.mHalfSize_MI.x), f16tof32(node.mHalfSize_MI.x >> 16));

      [branch]
      if (all(abs(node.mCenter - posW.xz) < halfSize)) {
        [call]
        switch (node.mHalfSize_MI.y & 0xf){
          case 1:
            getLight_point((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, specularExp, diffuse, specular);
            break;
          case 1:
            getLight_spotSingleFrequency((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, txDiffuseValue, 
              specularExp, noLight, diffuse, specular);
            break;
          case 2:
            getLight_spot((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, specularExp, noLight, diffuse, specular);
            break;
          case 3:
            getLight_car((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, specularExp, diffuseCar, specularCar);
            break;
          case 4:
            getLight_spotDiff((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, diffuse);
            break;
          case 5:
            getLight_line((node.mHalfSize_MI.y >> 4) & 0x3fff, LIGHTINGFX_INPUT_NORMAL, posW, specularExp, diffuse, specular);
            break;
        }
        index++;
      } else {
        index = node.mHalfSize_MI.y >> 18;
      }

    #else

      #define LFX_GETLIGHT_ARGS_BASE node.mLightIndex, LIGHTINGFX_INPUT_NORMAL, posW, txDiffuseValue, specularExp
      #ifdef HAS_SUN_SPECULAR
        #define LFX_GETLIGHT_ARGS_EXT LFX_GETLIGHT_ARGS_BASE, sunSpecularExp
      #else
        #define LFX_GETLIGHT_ARGS_EXT LFX_GETLIGHT_ARGS_BASE
      #endif

      [branch]
      if (all(abs(node.mCenter - posW.xz) < node.mHalfSize)) {
        [call]
        switch (node.mLightType){
          case 1:
            getLight_spot(ALLOW_DYNAMIC_SHADOWS_ARG_PASS LFX_GETLIGHT_ARGS_EXT, R, ML);
            break;
          case 2:
            getLight_car(ALLOW_DYNAMIC_SHADOWS_ARG_PASS LFX_GETLIGHT_ARGS_EXT, R, ML);
            break;
          case 3:
            getLight_line(LFX_GETLIGHT_ARGS_BASE, R, ML);
            break;

          /*case 2:
            getLight_spot(node.mLightIndex, LIGHTINGFX_INPUT_NORMAL, posW, specularExp, noLight, diffuse, specular);
            break; 
          case 4:
            getLight_spotDiff(node.mLightIndex, LIGHTINGFX_INPUT_NORMAL, posW, diffuse);
            break;*/
        }
        index++;
      } else {
        index = node.mMissIndex;
      }

    #endif

    if (!index) break;

    #ifdef LIGHTING_SAFE
      if (++safeStop > 100) return float3(4, 0, 4);
    #endif
  }

  #if defined(LIGHTINGFX_SIMPLEST) || defined(LIGHTINGFX_NOSPECULAR)
    return R.diffuse;
  #else
    float3 ret = R.diffuse + R.specular * specularValue;
    #ifdef HAS_SUN_SPECULAR
      ret += R.sunSpecular * sunSpecularValue;
    #endif
    #ifdef LIGHTINGFX_BOUNCEBACK
      ret += R.bounceBack * bounceBackMult;
    #endif
    #ifdef LIGHTINGFX_RAINFX
      ret += R.rain * 2 * rainAlpha;
    #endif
    #ifdef LIGHTINGFX_FUR
      ret += R.fur * furLightingMult;
    #endif
    return ret;
  #endif

  #ifdef NO_EXTSPECULAR
  } else return 0;
  #endif
}

static const float2 extraShadow = float2(1, 1);

#ifndef POS_CAMERA
  #define POS_CAMERA pin.PosC
#endif

#ifdef LIGHTINGFX_NOSPECULAR
  #define LIGHTINGFX_SPECULAR_EXP 1
  #define LIGHTINGFX_SPECULAR_COLOR 0
  #ifdef HAS_SUN_SPECULAR
    #define LIGHTINGFX_SUNSPECULAR_EXP 1
    #define LIGHTINGFX_SUNSPECULAR_COLOR 0
  #endif
#else
  #ifndef LIGHTINGFX_SPECULAR_EXP
    #define LIGHTINGFX_SPECULAR_EXP L.specularExp
  #endif
  #ifndef LIGHTINGFX_SPECULAR_COLOR
    #define LIGHTINGFX_SPECULAR_COLOR (L.specularValue * L.txSpecularValue)
  #endif
  #ifdef HAS_SUN_SPECULAR
    #ifndef LIGHTINGFX_SUNSPECULAR_EXP
      #define LIGHTINGFX_SUNSPECULAR_EXP L.sunSpecularExp
    #endif
    #ifndef LIGHTINGFX_SUNSPECULAR_COLOR
      #define LIGHTINGFX_SUNSPECULAR_COLOR (L.sunSpecularValue * L.txSpecularValue)
    #endif
  #endif
#endif

  #ifndef LIGHTINGFX_TXDIFFUSE_COLOR
    #define LIGHTINGFX_TXDIFFUSE_COLOR L.txDiffuseValue.rgb
  #endif

#ifdef LIGHTINGFX_FIND_MAIN
  #define LFX_ARG_ML ,mainLight
#else
  #define LFX_ARG_ML
#endif
#ifdef LIGHTINGFX_BOUNCEBACK
  #define LFX_ARG_BB ,bounceBackMult
#else
  #define LFX_ARG_BB
#endif
#ifdef LIGHTINGFX_RAINFX
  #define LFX_ARG_RN ,pow(RP.alpha, 2)*RP.occlusion
#else
  #define LFX_ARG_RN
#endif
#ifdef HAS_SUN_SPECULAR
  #define LFX_ARG_SS ,LIGHTINGFX_SUNSPECULAR_EXP,LIGHTINGFX_SUNSPECULAR_COLOR 
#else
  #define LFX_ARG_SS
#endif
#define EXTRA_ARGS LFX_ARG_SS LFX_ARG_RN LFX_ARG_BB LFX_ARG_ML

#ifdef LIGHTING_SPECULAR_EXP
  #error LIGHTING_SPECULAR_EXP is obsolete, use L or LIGHTINGFX_SPECULAR_EXP
#endif
#ifdef LIGHTING_SPECULAR_EXP_MULT
  #error LIGHTING_SPECULAR_EXP_MULT is obsolete, use L or LIGHTINGFX_SPECULAR_EXP
#endif
#ifdef LIGHTING_SPECULAR_POWER
  #error LIGHTING_SPECULAR_POWER is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif
#ifdef LIGHTING_SPECULAR_POWER_MULT
  #error LIGHTING_SPECULAR_POWER_MULT is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif
#ifdef LIGHTING_SPECULAR_COLOR
  #error LIGHTING_SPECULAR_COLOR is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif

#ifndef MODE_GBUFFER
  #ifdef LIGHTINGFX_SIMPLEST
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, 1, 0, 0 EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + extraShadow.x * AO_EXTRA_LIGHTING * (_G))
  #elif defined(LIGHTINGFX_SIMPLEST_NORMAL)
    #define LIGHTINGFX_GET getDynamicLights(LIGHTINGFX_INPUT_NORMAL, POS_CAMERA, 1, \
    LIGHTINGFX_SPECULAR_EXP, LIGHTINGFX_SPECULAR_COLOR EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + extraShadow.x * AO_EXTRA_LIGHTING * (_G))
  #else
    #define LIGHTINGFX_GET getDynamicLights(LIGHTINGFX_INPUT_NORMAL, POS_CAMERA, LIGHTINGFX_TXDIFFUSE_COLOR, \
    LIGHTINGFX_SPECULAR_EXP, LIGHTINGFX_SPECULAR_COLOR EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + extraShadow.x * AO_EXTRA_LIGHTING * (_G) + GI_LIGHTING * LIGHTINGFX_TXDIFFUSE_COLOR * ksDiffuse)
  #endif

  #if defined(ALLOW_PERVERTEX_AO_DEBUG) && defined(SUPPORTS_AO)
    #define LIGHTINGFX(_X) _X.rgb = extVaoMode == 4 ? saturate(pin.Ao) : extVaoMode == 5 ? pin.NormalW : \
      LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
  #else
    #define LIGHTINGFX(_X) _X.rgb = LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
  #endif

  #if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_AO
    #ifdef ALLOW_PERVERTEX_AO_DEBUG
      #define _AO_VAO (extVaoMode == 3 ? 1 : pin.Ao)
    #else
      #define _AO_VAO pin.Ao
    #endif
  #else
    #define _AO_VAO 1
  #endif
#endif