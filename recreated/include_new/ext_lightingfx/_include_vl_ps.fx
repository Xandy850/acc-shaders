#include "utils_ps.fx"

// 8 KB:
#define MAX_VALUES_AMOUNT 500

// 12 KB:
#define MAX_PACKEDBVHLIGHT_AMOUNT 375

// #define BVH_NODE_HALF
#define LIGHTS_COMPACT_HALF

// LI stands for “length inverted”
// S0 stands for “start”, in pair with LI
// DirW is premultiplied by SpotCosLI to save space
// UpW is premultiplied to save space

// common
#define L_PosW                        gLightsData[_lightData + 0].xyz
#define L_RangeS0LI                   asuint(gLightsData[_lightData + 0].w)

// spot, car
#define L_DirW_SpotCosS0_F            gLightsData[_lightData + 1]
#define L_Color_SpecMult_G            asuint(gLightsData[_lightData + 2].xy)

// spot
#define L_DiffConc_SingWav_NoShadw_   asuint(gLightsData[_lightData + 2].zw)

// car
#define L_SpotEdgeOff_SSInt           asuint(gLightsData[_lightData + 2].zw)
#define L_SecSpotCosS0LI_SecTrimS0LI  asuint(gLightsData[_lightData + 3].xy)
#define L_UpW_SecondRangeLI           asuint(gLightsData[_lightData + 3].zw)

// line
#define L_Color_SpecMult_L            asuint(gLightsData[_lightData + 1].xy)
#define L_DiffBA_DistInv              asuint(gLightsData[_lightData + 1].zw)
#define L_DirW_SpotCosS0_L            asuint(gLightsData[_lightData + 2].xy)
#define L_Color2_DiffConc             asuint(gLightsData[_lightData + 2].zw)

struct sPackedBVHLight
{
  float2 mCenter;

  #ifdef BVH_NODE_HALF
    uint2 mHalfSize_MI;
    // 16 bytes

  #else
    float2 mHalfSize;
    int mLightIndex;
    int mMissIndex;
    int mLightType;
    float padding;
    // 32 bytes
  #endif
};

cbuffer cbExtLighting : register(b7) {
  float4 gNoLightAt[2];
  float2 gNoLightEdgeA, gNoLightEdgeB;

  sPackedBVHLight gLightBVHNodeData[MAX_PACKEDBVHLIGHT_AMOUNT];
  float4 gLightsData[MAX_VALUES_AMOUNT];
}

// StructuredBuffer<ExtraLight> gExtraLights : register(t12);

#ifndef LIGHTING_SPECULAR_EXP
#define LIGHTING_SPECULAR_EXP ksSpecularEXP
#endif

#ifndef LIGHTING_SPECULAR_POWER
#define LIGHTING_SPECULAR_POWER ksSpecular
#endif

#ifndef LIGHTINGFX_KSDIFFUSE
#define LIGHTINGFX_KSDIFFUSE ksDiffuse
#endif

float getNoLightMultiplier(int index, float2 edge, float3 posW){
  float3 v = posW - gNoLightAt[index].xyz;
  float d = dot(v, v);
  return saturate(d * edge.y - edge.x);
}

float getNoLightMultiplier(float3 posW){
  return saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) * getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
}

float2 LFX_loadVector(uint vec){
  return float2(f16tof32(vec), f16tof32(vec >> 16));
}

float4 LFX_loadVector(uint2 vec){
  return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float4 LFX_loadVector(float4 vec){
  return vec;
}

struct LFX_MainLight {
  #ifdef LIGHTINGFX_FIND_MAIN
    float3 dir;
    float power;
  #endif
};

void getLight_spot(uint _lightData, float3 normal, float3 posW, const float3 txDiffuseValue, const float specularExp, 
    inout float3 diffuse, inout float3 specular, inout LFX_MainLight mainLight) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);
  float spotK = getSpotCone(DirW_SpotCosS0.xyz, toLight.xyz, DirW_SpotCosS0.w);

  float4 DiffConc_SingWav_NoShadw_= LFX_loadVector(L_DiffConc_SingWav_NoShadw_);
  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float shadow = attenuation * spotK;

  diffuse += Color_SpecMult.rgb * shadow;
}

void getLight_car(uint _lightData, float3 normal, float3 posW, const float3 txDiffuseValue, const float specularExp, 
    inout float3 diffuse, inout float3 specular, inout LFX_MainLight mainLight) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);
  float4 SecSpotCosS0LI_SecTrimS0LI = LFX_loadVector(L_SecSpotCosS0LI_SecTrimS0LI);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);
  float spotK1 = lerpInv10(cosAngle, SecSpotCosS0LI_SecTrimS0LI.x, SecSpotCosS0LI_SecTrimS0LI.y);

  [branch]
  if (spotK0 == 0 && spotK1 == 0) return;

  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);
  float4 UpW_SecondRangeLI = LFX_loadVector(L_UpW_SecondRangeLI);
  float4 SpotEdgeOff_SSInt = LFX_loadVector(L_SpotEdgeOff_SSInt);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float secondAttenuation = getTrimmedAttenuation(UpW_SecondRangeLI.w, SecSpotCosS0LI_SecTrimS0LI.z, SecSpotCosS0LI_SecTrimS0LI.w, toLight.w);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  #endif

  float3 shadow = getDiffuseMultiplier(normal, toLight.xyz) 
    * (spotK0 * (attenuation * getEdge(UpW_SecondRangeLI.xyz, toLight.xyz, SpotEdgeOff_SSInt.xyz))
      + spotK1 * secondAttenuation * SpotEdgeOff_SSInt.w)
    * noLight;

  diffuse += Color_SpecMult.rgb * shadow;
}
 
float3 getDynamicLights(float3 normalW, float3 posW, float3 txDiffuseValue, const float specularExp, const float specularValue
  #ifdef LIGHTINGFX_FIND_MAIN
    , out LFX_MainLight mainLight
  #endif
    ) {
  #ifdef NO_EXTSPECULAR
  [branch]
  if (ksSpecular >= 0){
  #endif

  #ifdef LIGHTINGFX_TREE
    normalW = float3(0, 1, 0);
  #endif

  float3 diffuse = 0.0;
  float3 specular = 0.0;
  float3 diffuseCar = 0.0;
  float3 specularCar = 0.0;
  float3 diffuseFixed = 0.0;
  
  #ifndef LIGHTINGFX_FIND_MAIN
    LFX_MainLight mainLight;
  #endif
  mainLight = (LFX_MainLight)0;

  float distSqr = dot(posW, posW);
  // int index = distSqr <= gEdge0 ? 0 : distSqr <= gEdge1 ? gIndex0 : gIndex1;
  int index = 0;
  int safeStop = 0;

  // some extra lights should work no matter if there is a shadow
  // #ifdef NO_EXTSPECULAR
  //   float noLight = 1;
  //   float noCarLight = 1;
  // #else
  //   float noLight = saturate(getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
  //   float noCarLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  // #endif

  [loop]
  while (true) {
    sPackedBVHLight node = gLightBVHNodeData[index];

    #ifdef BVH_NODE_HALF
      float2 halfSize = float2(f16tof32(node.mHalfSize_MI.x), f16tof32(node.mHalfSize_MI.x >> 16));

      [branch]
      if (all(abs(node.mCenter - posW.xz) <= halfSize)) {
        [call]
        switch (node.mHalfSize_MI.y & 0xf){
          case 1:
            getLight_point((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, specularExp, diffuse, specular);
            break;
          case 1:
            getLight_spotSingleFrequency((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, txDiffuseValue, 
              specularExp, noLight, diffuse, specular);
            break;
          case 2:
            getLight_spot((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, specularExp, noLight, diffuse, specular);
            break;
          case 3:
            getLight_car((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, specularExp, diffuseCar, specularCar);
            break;
          case 4:
            getLight_spotDiff((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, diffuse);
            break;
          case 5:
            getLight_line((node.mHalfSize_MI.y >> 4) & 0x3fff, normalW, posW, specularExp, diffuse, specular);
            break;
        }
        index++;
      } else {
        index = node.mHalfSize_MI.y >> 18;
      }

    #else

      [branch]
      if (all(abs(node.mCenter - posW.xz) <= node.mHalfSize)) {
        [call]
        switch (node.mLightType){
          case 1:
            getLight_spot(node.mLightIndex, normalW, posW, txDiffuseValue, specularExp, diffuse, specular, mainLight);
            break;
          case 2:
            getLight_car(node.mLightIndex, normalW, posW, txDiffuseValue, specularExp, diffuse, specular, mainLight);
            break;

          /*case 2:
            getLight_spot(node.mLightIndex, normalW, posW, specularExp, noLight, diffuse, specular);
            break; 
          case 4:
            getLight_spotDiff(node.mLightIndex, normalW, posW, diffuse);
            break;*/
        }
        index++;
      } else {
        index = node.mMissIndex;
      }

    #endif

    if (!index) break;
    // if (++safeStop > 200) return float3(4, 0, 4);
  }

  #if defined LIGHTINGFX_SIMPLEST || defined LIGHTINGFX_NOSPECULAR
    return diffuse;
  #else
    return diffuse + specular * specularValue;
  #endif

  #ifdef NO_EXTSPECULAR
  } else return 0;
  #endif
}

#ifndef LIGHTING_SPECULAR_POWER_MULT
#define LIGHTING_SPECULAR_POWER_MULT 1.0
#endif

#ifndef LIGHTING_SPECULAR_EXP_MULT
#define LIGHTING_SPECULAR_EXP_MULT 1.0
#endif

static const float2 extraShadow = float2(1, 1);

#ifndef POS_CAMERA
  #define POS_CAMERA pin.PosC
#endif

#ifdef LIGHTINGFX_SIMPLEST
  #ifdef LIGHTINGFX_FIND_MAIN
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, 1, 0, 0, mainLight)
  #else
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, 1, 0, 0)
  #endif
#elif defined(LIGHTINGFX_SIMPLEST_NORMAL)
  #ifdef LIGHTINGFX_FIND_MAIN
    #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, 1, 0, 0, mainLight)
  #else
    #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, 1, 0, 0)
  #endif
#else
  #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, txDiffuseValue.rgb, \
    (LIGHTING_SPECULAR_EXP) * (LIGHTING_SPECULAR_EXP_MULT), \
    (LIGHTING_SPECULAR_POWER) * (LIGHTING_SPECULAR_POWER_MULT))
#endif

#define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + extraShadow.x * AO_EXTRA_LIGHTING * (_G))

#if defined ALLOW_PERVERTEX_AO_DEBUG && defined SUPPORTS_AO
  #define LIGHTINGFX(_X) _X.rgb = extVaoMode == 4 ? saturate(pin.Ao) : extVaoMode == 5 ? pin.NormalW : \
    LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
#else
  #define LIGHTINGFX(_X) _X.rgb = LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
#endif

#if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_AO
  #ifdef ALLOW_PERVERTEX_AO_DEBUG
    #define _AO_VAO (extVaoMode == 3 ? 1 : pin.Ao)
  #else
    #define _AO_VAO pin.Ao
  #endif
#else
  #define _AO_VAO 1
#endif