cbuffer cbExtWindscreen : register(b6) {
	float3 carVelocity;
	float noiseMult;

	float3 rainVelocity;
	float rainAmount;

	float4 rainTime;

	float4x4 rainInvModel;
	float4x4 rainWindscreenMatrix;
	float4x4 rainWiperMatrix;

	float4 rainWiperOffset;

	float rainWiperSpeed;
	uint disableTexture;
	float2 dummy;

	// float3 reflectionPlaneNormal;
	// float reflectionDistance;

	// float3 reflectionPoint0;
	// float3 reflectionDirX;
	// float3 reflectionDirY;
};