#include "include/common.hlsl"

#ifndef INPUT_AMBIENT_K
  #define INPUT_AMBIENT_K ksAmbient
#endif
#ifndef INPUT_DIFFUSE_K
  #define INPUT_DIFFUSE_K ksDiffuse
#endif
#ifndef INPUT_EMISSIVE3
  #define INPUT_EMISSIVE3 ksEmissive
#endif
#ifndef INPUT_SPECULAR_K
  #define INPUT_SPECULAR_K ksSpecular
#endif
#ifndef INPUT_SPECULAR_EXP
  #define INPUT_SPECULAR_EXP ksSpecularEXP
#endif
#ifndef INPUT_NM_OBJECT_SPACE
  #define INPUT_NM_OBJECT_SPACE nmObjectSpace
#endif
#ifndef INPUT_DETAIL_UV_MULT
  #define INPUT_DETAIL_UV_MULT detailUVMultiplier
#endif
#ifndef INPUT_USE_DETAIL
  #define INPUT_USE_DETAIL useDetail
#endif

// Colored specular and reflections

float3 getBaseSpecularColor(float3 txDiffuseValue, float normalizationK){
  return txDiffuseValue / max(txDiffuseValue.r, max(txDiffuseValue.g, max(txDiffuseValue.b, normalizationK + 0.001)));
}

#define GET_SPEC_COLOR lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), extColoredReflection)
#define GET_SPEC_COLOR_MASK_DETAIL lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), \
  extColoredReflection == 2 ? txMapsValue.a : extColoredReflection == 3 ? 1 - txDiffuseValue.a : extColoredReflection)
#define GET_SPEC_COLOR_MASK_DETAIL_EXTRA(X) lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), \
  (extColoredReflection == 2 ? txMapsValue.a : extColoredReflection == 3 ? 1 - txDiffuseValue.a : extColoredReflection) * (X))
  
// Shadows

#define READ_VECTORS\
  float3 toCamera = normalize(pin.PosC);\
  float3 normalW = normalize(pin.NormalW);

#ifdef NO_NORMALMAPS
  #define READ_VECTORS_NM\
    READ_VECTORS\
    float3 tangentW = float3(1, 0, 0);
    float3 bitangentW = float3(1, 0, 0);
#else
  #define READ_VECTORS_NM\
    READ_VECTORS\
    float3 tangentW = normalize(pin.TangentW);\
    float3 bitangentW = normalize(pin.BitangentW);
#endif

#ifdef NO_SHADOWS

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS 0, 0, 0
  #endif

  #define getShadow(posC, normalW, shadowTex0, shadowTex1, shadowTex2, fallbackValue) (extCloudShadowBase * luminance(_AO_VAO) * perObjAmbientMultiplier)
  #define getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackValue) (extCloudShadowBase * luminance(_AO_VAO) * perObjAmbientMultiplier)

#elif !defined(OPTIMIZE_SHADOWS) && !(defined(SUPPORTS_BLUR_SHADOWS) && defined(BLUR_SHADOWS))

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
  #endif

  bool checkCascade(float4 uv){
    return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
  }

  float cascadeShadowStep(float deltaZ, float4 uv, Texture2D<float> tx){
    float result = 0;
    float comparisonValue = uv.z - deltaZ;
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

    [unroll]
    for (int x = -1; x <= 1; x++){
      [unroll]
      for (int y = -1; y <= 1; y++){
        #ifdef MICROOPTIMIZATIONS
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * float2(x, y), comparisonValue);
        #else
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + float2(y * textureSize, x * textureSize), comparisonValue);
        #endif
      }
    }

    return result;
  }

  float getShadowBiasMult(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float biasMultiplier, 
      float fallbackValue){
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;

    if (checkCascade(shadowTex0)){
      return cascadeShadowStep(deltaZBase * bias.x, shadowTex0, txShadow0) / 9;
    } else

    if (checkCascade(shadowTex1)){
      return cascadeShadowStep(deltaZBase * bias.y, shadowTex1, txShadow1) / 9;
    } else

    if (checkCascade(shadowTex2)){
      return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, txShadow2) / 9 
          + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
    } else

    return 1;
  }

  float getShadow(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float fallbackValue){
    #ifdef USE_SHADOW_BIAS_MULT
      float biasMultiplier = shadowBiasMult + 1;
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackValue);
    #elif defined GETSHADOW_BIAS_MULTIPLIER
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, GETSHADOW_BIAS_MULTIPLIER, fallbackValue);
    #else
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, 1, fallbackValue);
    #endif
  }

#endif

// Fix for tiling

float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias, 
    float noiseMult){
  float k = txNoise.Sample(samLinear, noiseMult * uv).x;

  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;
    
  float l = k * 8.0;
  float i = floor(l);
  float f = frac(l);

  float2 offa = sin(float2(3.0, 7.0) * (i + 0.0));
  float2 offb = sin(float2(3.0, 7.0) * (i + 1.0));
  float4 cola = tx.SampleGrad(sam, uv + offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + offb, duvdx, duvdy);

  return lerp(cola, colb, smoothstep(0.2, 0.8, f - 0.1 * dot(cola - colb, 1)));
}

float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias = 1){
  return textureSampleVariation(tx, sam, uv, bias, 0.05);
}

// Normals

#if !defined(NO_CARPAINT) || defined(USE_GETNORMALW)

#ifdef USE_GETNORMALW
  #undef INPUT_NM_OBJECT_SPACE
  #define INPUT_NM_OBJECT_SPACE false
#endif

#ifndef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear
#endif

#ifndef GETNORMALW_OS_SAMPLER
  #define GETNORMALW_OS_SAMPLER samLinearSimple
#endif

#ifdef CARPAINT_DAMAGES

float3 getDamageNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, float damage, 
    out float normalAlpha){
  #ifdef NO_NORMALMAPS
    normalAlpha = 0.0;
    return normalW;
  #else

    #ifndef GETNORMALW_NORMALIZED_INPUT
      normalW = normalize(normalW);
    #endif

    #ifdef GETNORMALW_XYZ_TX
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzyw;
    #else
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyzw;
    #endif

    normalAlpha = txNormalValue.a;

    #ifdef CARPAINT_GLASS
      normalAlpha = damage * txNormalValue.a;
    #endif

    if (INPUT_NM_OBJECT_SPACE){
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned.z *= -1;
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned = normalize(txNormalAligned);
      #ifdef CARPAINT_GLASS
        float3x3 m = float3x3(tangentW, bitangentW, normalW);
      #else
        float3x3 m = float3x3(tangentW, normalW, bitangentW);
      #endif
      return lerp(normalW, normalize(mul(transpose(m), txNormalAligned)), damage);
    }

  #endif
}

#endif

#if defined CARPAINT_NM || defined USE_GETNORMALW || defined CARPAINT_AT

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float alpha){
  #ifdef NO_NORMALMAPS
    #ifdef USE_BLURRED_NM
      alpha = 1.0;
    #else 
      #ifdef GETNORMALW_UV_MULT
        alpha = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv * GETNORMALW_UV_MULT).a;
      #else
        alpha = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv).a;
      #endif
    #endif

    return normalW;
  #else

    #ifdef USE_BLURRED_NM
      alpha = 1;

      float3 T = normalize(tangentW);
      float3 B = normalize(bitangentW);
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
      float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
      float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
      float3 txNormalBlurAligned = txNormalBlurValue.xyz * 2 - (float3)1;
      float3x3 m = float3x3(T, normalW, B);
      float3 normalStatic = normalize(mul(transpose(m), txNormalAligned.xzy));
      float3 normalBlur = normalize(mul(transpose(m), txNormalBlurAligned.xzy));
      return lerp(normalStatic, normalBlur, blurLevel);
    #else 
      if (INPUT_NM_OBJECT_SPACE){
        #ifdef GETNORMALW_UV_MULT
          float4 txNormalValue = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv * GETNORMALW_UV_MULT);
        #else
          float4 txNormalValue = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv);
        #endif
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
        txNormalAligned.z *= -1;
        float3x3 m = (float3x3)ksWorld;
        return normalize(mul(txNormalAligned, m));
      } else {
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
        float3x3 m = float3x3(T, normalW, B);
        return normalize(mul(transpose(m), txNormalAligned.xzy));
      }
    #endif

  #endif
}

#elif defined CARPAINT_NM_UVMULT
#else

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  #ifdef NO_NORMALMAPS
    return normalW;
  #else
    if (objectSpace){
      #ifdef GETNORMALW_XYZ_TX
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #else
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #endif
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      #ifdef GETNORMALW_NORMALIZED_TB
        float3 T = tangentW;
        float3 B = bitangentW;
      #else
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
      #endif

      #ifdef GETNORMALW_XYZ_TX
        float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyz;
      #else
        float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzy;
      #endif

      float3 txNormalAligned = txNormalValue * 2 - (float3)1;
      float3x3 m = float3x3(T, normalW, B);
      return normalize(mul(transpose(m), txNormalAligned.xzy));
    }
  #endif
}

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW){
  #ifdef NO_NORMALMAPS
    return normalW;
  #elif defined CARPAINT_NMDETAILS || defined CARPAINT_SIMPLE
    return getNormalW(uv, normalW, tangentW, bitangentW, false);
  #else
    return getNormalW(uv, normalW, tangentW, bitangentW, INPUT_NM_OBJECT_SPACE);
  #endif
}

#endif

#endif

float3 fixReflDir(float3 dir, float3 pos, float3 normal, float reflBlur, float modifier = 1){
  float3 result;
  #ifdef EXTENDED_PEROBJECT_CB
    float fixAmount = reflectionsTweak;
    fixAmount *= saturate(dot(dir, normalize(pos)) * 2 + 1);
    result = normalize(dir + pos * fixAmount);
  #else 
    result = dir;
  #endif
  return result;
}

float4 getSkyColor(float3 direction){
  float v = saturate(900 / ksFogLinear);
  float3 color;
  if (direction.y > 0) {
    color = lerp(ksHorizonColor.rgb, ksZenithColor.rgb, 1 / rsqrt(direction.y));
    color = lerp(color, ksAmbientColor_sky.r * 0.5, v);
  } else {
    color = lerp(ksHorizonColor.rgb, float3(0.15, 0.125, 0.125), saturate(direction.y * -7));
    color = lerp(color, ksAmbientColor_sky.r * 0.5, v);
  }
  return float4(color, v);
}

#ifdef RAINBOW_REFLECTIONS_K
  #ifndef RAINBOW_REFLECTIONS_MULT
    #define RAINBOW_REFLECTIONS_MULT 1
  #endif

  float3 getReflectionRainbowValue(float3 dir, float blur){
    return txCube.SampleLevel(samLinearSimple, dir, max(2, blur)).rgb;
  }

  float3 getReflectionRainbow(float3 reflDir, float3 look, float blur, float mask){
    #ifdef BLACK_REFLECTIONS
      return 0;
    #else
      float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
      float offset = mask * RAINBOW_REFLECTIONS_K / (abs(dot(reflDir, look)) + 1);
      float3 extraOffset = mask * RAINBOW_REFLECTIONS_K * float3(0, 0.1, 0);
      float c0 = getReflectionRainbowValue(reflCubeDir + look * offset + extraOffset, blur).r;
      float c1 = getReflectionRainbowValue(reflCubeDir, blur).g;
      float c2 = getReflectionRainbowValue(reflCubeDir - look * offset - extraOffset, blur).b;
      return max(lerp(c1, float3(c0, c1, c2), RAINBOW_REFLECTIONS_MULT), 0);
    #endif
  }
#endif

#ifndef SAMPLE_REFLECTION_FN
  #define SAMPLE_REFLECTION_FN sampleReflectionCube
#endif

#ifndef REFL_SAMPLE_PARAM_TYPE
  #define REFL_SAMPLE_PARAM_TYPE float4
  #define REFL_SAMPLE_PARAM_DEFAULT 1
#elif !defined(REFL_SAMPLE_PARAM_DEFAULT)
  #define REFL_SAMPLE_PARAM_DEFAULT (REFL_SAMPLE_PARAM_TYPE)0
#endif

#ifndef REFLECTION_BLUR_MULT
  #define REFLECTION_BLUR_MULT 1
#endif

float3 sampleReflectionCube(float3 reflDir, float blur, bool useBias, REFL_SAMPLE_PARAM_TYPE extraParam){
  #ifdef BLACK_REFLECTIONS
    return 0;
  #else
    float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
    if (useBias){
      float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
      return txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT + level * saturate(2 - blur) * 0.8).rgb;
    } else {
      return txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT).rgb;
    }
  #endif
}

#ifndef NOREFLECTIONS

// Reflections (new)

float3 getReflectionAt(float3 reflDir, float3 look, float blur, bool useBias, REFL_SAMPLE_PARAM_TYPE extraParam){
  #ifdef BLACK_REFLECTIONS
    return 0;
  #elif defined(NO_REFLECTIONS)
    float v = saturate(extUseNewFog ? ksFogBlend : 900 / ksFogLinear);
    float3 color = lerp(
      lerp(ksHorizonColor.rgb, ksZenithColor.rgb, saturate(reflDir.y)) * 1.5, 
      ksFogColor.rgb, v);
    color = lerp(color, float3(0.05, 0.11, 0.08), saturate(reflDir.y * -60 / (blur + 1) + 0.1));
    return color * saturate(perObjAmbientMultiplier * 2);
  #elif defined(FORCE_BLURREST_REFLECTIONS)
    return SAMPLE_REFLECTION_FN(reflDir, 100, false, extraParam);
  #else
    #ifdef RAINBOW_REFLECTIONS_K
      return getReflectionRainbow(reflDir, look, blur, (extraParam).x);
    #else
      return SAMPLE_REFLECTION_FN(reflDir, blur, useBias, extraParam);  
    #endif
  #endif
}

struct ReflParams {
  float fresnelMaxLevel;
  float fresnelC;
  float fresnelEXP;
  float ksSpecularEXP;
  float finalMult;
  float3 metallicFix;

  float coloredReflections;
  float3 coloredReflectionsColor;

  bool useBias;
  bool isCarPaint;
  bool useSkyColor;
  bool useStereoApproach;

  float stereoExtraMult;
  REFL_SAMPLE_PARAM_TYPE reflectionSampleParam;

  bool forceReflectedDir;
  float3 forcedReflectedDir;

  #ifdef ALLOW_RAINFX
    float fresnelMaxLevelDry;
    float fresnelCDry;
    float fresnelEXPDry;
  #endif

  // Out:
  float3 resultColor;
  float resultPower;
  float resultBlur;
};

#ifndef IS_ADDITIVE_VAR
  #ifdef CARPAINT_NM_UVMULT
    #define IS_ADDITIVE_VAR 0
  #elif defined(CARPAINT_GLASS)
    #define IS_ADDITIVE_VAR 1
  #elif defined(NO_CARPAINT)
    #define IS_ADDITIVE_VAR 0
  #else
    #define IS_ADDITIVE_VAR isAdditive
  #endif
#endif

#ifndef GET_REFL_DIR
  #define GET_REFL_DIR(LOOK, SURFACE) normalize(reflect(LOOK, SURFACE))
#endif

float getCPLMult(float3 reflDir){
  #ifdef USE_CPL
    return 1 - saturate(dot(reflDir, extCPLDir) * extCPLFactor);
  #else
    return 1;
  #endif
}

float4 calculateReflection(float4 lighting, float3 toCamera, float3 posC, float3 normalW, inout ReflParams P){
  #ifdef BLACK_REFLECTIONS
    P = (ReflParams)0;
    return lighting;
  #endif

  #ifdef ALLOW_PERVERTEX_AO_DEBUG__NOREFL
    [branch]
    if (extVaoMode == 4){
      P = (ReflParams)0;
      return lighting;
    } else {
  #endif

  float facingToCamera = dot(normalW, -toCamera);

  float input = saturate(1 - facingToCamera);
  #ifdef REFLECTION_FRESNELEXP_BOUND
    float fresnel = pow(input, IS_ADDITIVE_VAR == 0 ? max(P.fresnelEXP, 1) : P.fresnelEXP);
  #else
    float fresnel = pow(input, P.fresnelEXP);
  #endif

  fresnel += P.useStereoApproach
    ? lerp(P.fresnelC, P.fresnelMaxLevel, saturate(2 * P.finalMult - 1)) * P.stereoExtraMult
    : P.fresnelC;
  fresnel = min(fresnel, P.fresnelMaxLevel);

  #ifdef ALLOW_RAINFX
    float fresnelDry = min(pow(input, P.fresnelEXPDry) + P.fresnelCDry, P.fresnelMaxLevelDry);
  #else
    float fresnelDry = fresnel;
  #endif

  float origFinalMult = P.finalMult;
  if (P.useStereoApproach){
    P.finalMult = saturate(2.0 * origFinalMult);
  }

  float reflBlurBase = saturate(1 - P.ksSpecularEXP / (P.isCarPaint && IS_ADDITIVE_VAR == 2 ? 8 : 255));
  float reflBlur = reflBlurBase * 6;
  P.resultBlur = reflBlur;

  float3 reflDir = P.forceReflectedDir ? P.forcedReflectedDir : GET_REFL_DIR(toCamera, normalW);
  float cplMult = getCPLMult(reflDir);
  P.finalMult *= cplMult;

  reflDir = fixReflDir(reflDir, posC, normalW, reflBlurBase);

  #ifdef AMBIENT_TYRES_FIX
    P.finalMult *= pow(saturate(normalW.y + 1), 0.5);
  #endif

  float3 reflColor = (P.useSkyColor 
    ? getSkyColor(reflDir).rgb 
    : getReflectionAt(reflDir, -toCamera, reflBlur, P.useBias, P.reflectionSampleParam)) * P.finalMult;

  #ifdef DIM_REFLECTTIONS_WITH_SAMPLE_PARAM
    reflColor *= (P.reflectionSampleParam).x;
  #endif

  reflColor = lerp(reflColor, reflColor * P.coloredReflectionsColor, P.coloredReflections);

  P.resultPower = fresnel * P.finalMult;
  P.resultPower *= luminance(lerp(1, P.coloredReflectionsColor, P.coloredReflections));
  // P.resultPower += round(P.coloredReflections * 10) * 2; // TODO: SSLR+Metalness
  P.resultColor = reflColor * fresnel;

  float3 finalColor = lighting.rgb;
  if (!(P.useSkyColor || (P.isCarPaint ? IS_ADDITIVE_VAR == 1 : IS_ADDITIVE_VAR))){
    finalColor = max(finalColor - lighting.rgb * P.metallicFix * fresnel 
      * (P.useStereoApproach ? P.finalMult : cplMult), 0);
  }
  finalColor += reflColor * fresnel;
  return float4(finalColor, (P.isCarPaint ? IS_ADDITIVE_VAR == 1 : IS_ADDITIVE_VAR) 
    ? lighting.w : lerp(lighting.w, 1, fresnelDry));
    
  #ifdef ALLOW_PERVERTEX_AO_DEBUG__NOREFL
    }
  #endif
}

float3 calculateReflection(float3 lighting, float3 toCamera, float3 posC, float3 normalW, inout ReflParams P){
  return calculateReflection(float4(lighting, 1), toCamera, posC, normalW, P).rgb;
}

ReflParams getReflParamsZero(){
  ReflParams R = (ReflParams)0;
  R.useBias = false;
  R.finalMult = 1;
  R.metallicFix = 1;
  return R;
}

#ifndef NO_CARPAINT

  ReflParams getReflParams(float3 specColor, float extraMultiplier, float3 txMapsValue){
    ReflParams result = (ReflParams)0;
    result.fresnelEXP = fresnelEXP;
    result.ksSpecularEXP = txMapsValue.y * INPUT_SPECULAR_EXP;
    result.finalMult = txMapsValue.z;
    result.metallicFix = 1;
    result.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
    result.coloredReflections = 1;
    result.coloredReflectionsColor = specColor;
    #ifdef CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE
      result.fresnelMaxLevel = extraMultiplier;
      result.fresnelC = fresnelC;
    #else
      result.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
      result.fresnelC = fresnelC * extraMultiplier;
    #endif    
    return result;
  }

  ReflParams getReflParamsBase(float extraMultiplier = 1, float3 txMapsValue = 1){
    ReflParams result = (ReflParams)0;
    result.fresnelEXP = fresnelEXP;
    result.ksSpecularEXP = txMapsValue.y * INPUT_SPECULAR_EXP;
    result.finalMult = txMapsValue.z;
    result.metallicFix = 1;
    result.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
    #ifdef CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE
      result.fresnelMaxLevel = extraMultiplier;
      result.fresnelC = fresnelC;
    #else
      result.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
      result.fresnelC = fresnelC * extraMultiplier;
    #endif    
    return result;
  }

#endif

#endif

// Lighting

float3 calculateDiffuse(float3 normalW){
  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  return ksLightColor.rgb * INPUT_DIFFUSE_K * lightValue;
}

float getEmissiveMult(){
  #ifdef USE_GLOBAL_EMISSIVE_MULT
    return HAS_FLAG(FLAG_FIXED_EMISSIVE) ? 1 : extEmissiveMult;
  #else
    return 1;
  #endif
}

float3 getAmbientAt(float3 dirW, float4 ambientAoMult = 1){
  #ifndef NO_AMBIENT_MULT
    ambientAoMult.xyz *= perObjAmbientMultiplier;
  #endif
  #if defined(ALLOW_SH_AMBIENT) && !defined(NO_EXTAMBIENT)
    float3 base = getAmbientBaseAt(dirW, ambientAoMult);
    float3 sampled = SAMPLE_REFLECTION_FN(dirW, 100, false, REFL_SAMPLE_PARAM_DEFAULT);
    return lerp(base, 
      max(
        base * extIBLAmbientBaseThreshold, 
        extIBLAmbientBrightness * lerp(dot(sampled, 0.333), sampled, extIBLAmbientSaturation)) * ambientAoMult.xyz, 
      IS_TRACK_MATERIAL ? 0 : extIBLAmbient);
  #else
    return getAmbientBaseAt(dirW, ambientAoMult);
  #endif
}

float3 calculateAmbient(float3 normalW, float4 ambientAoMult){
  return getAmbientAt(normalW, ambientAoMult) * INPUT_AMBIENT_K;
}

float3 calculateEmissive(float3 posC, float3 txDiffuseValue, float3 txEmissiveValue){
  #ifdef DIM_NEARBY_EMISSIVES
    txEmissiveValue *= saturate(dot(posC, posC) / 400 - 0.5);
  #endif
  #ifdef USE_TXEMISSIVE_AS_EMISSIVE
    return txEmissiveValue * getEmissiveMult();
  #else
    return INPUT_EMISSIVE3 * txDiffuseValue * txEmissiveValue * getEmissiveMult();
  #endif
}

float3 calculateEmissiveSpecial(float3 txDiffuseValue, float3 txEmissiveValue){
  #ifdef USE_TXEMISSIVE_AS_EMISSIVE
    return 0;
  #else
    return (extForceEmissive >= 0 ? extForceEmissive : INPUT_EMISSIVE3) 
      * txDiffuseValue * txEmissiveValue * getEmissiveMult();
  #endif
}

// float3 calculateSpecular(float3 normalW, float3 toCameraW, float3 txSpecularValue, float shadow){
//   float specularBase = saturate(dot(normalize(toCameraW - ksLightDirection.xyz), normalW));
//   return pow(specularBase, max(INPUT_SPECULAR_EXP, 1)) * INPUT_SPECULAR_K * shadow * extSpecularColor * txSpecularValue;
// }

// Basic shaders

float3 simplerLighting(float3 normalW, float3 txDiffuseValue, float4 ambientAoMult, float3 txEmissiveValue){
  return saturate(0.6 + 0.4 * normalW.y) * INPUT_AMBIENT_K * ambientAoMult.xyz * txDiffuseValue.rgb
    + calculateEmissiveSpecial(txDiffuseValue, txEmissiveValue);
}

struct LightingParams {
  float3 posC;
  float3 toCamera;
  float3 normalW;
  float shadow;
  float3 txDiffuseValue;
  float3 txSpecularValue;
  float3 txEmissiveValue;
  // float3 txMapsValue;
  float4 ambientAoMult;
  float specularValue;
  float specularExp;
  bool txMapsApplied;

  #ifdef HAS_SUN_SPECULAR
    float sunSpecularValue;
    float sunSpecularExp;
  #endif

  LightingParams fill(float3 posC, float3 toCamera, float3 normalW, float3 txDiffuseValue, float shadow, float4 ambientAoMult){
    this.posC = posC;
    this.toCamera = toCamera;
    this.normalW = normalW;
    this.txDiffuseValue = txDiffuseValue;
    this.txSpecularValue = 1;
    this.txEmissiveValue = 1;
    this.shadow = shadow;
    this.ambientAoMult = ambientAoMult;
    this.specularValue = INPUT_SPECULAR_K;
    this.specularExp = INPUT_SPECULAR_EXP;
    #ifdef HAS_SUN_SPECULAR
      this.sunSpecularValue = sunSpecular;
      this.sunSpecularExp = sunSpecularEXP;
    #endif
    return this;
  }

  LightingParams multAO(float mult){
    this.ambientAoMult *= mult;
    return this;
  }

  LightingParams setSpecularMult(float mult, float expMult){
    #ifdef CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
      #error CALCULATELIGHTING_USESPECULARMULT_AS_VALUE is obsolete 
    #endif
    #ifdef CALCULATELIGHTING_USESPECULAREXPMULT_AS_VALUE
      #error CALCULATELIGHTING_USESPECULAREXPMULT_AS_VALUE is obsolete 
    #endif
  
    specularValue *= mult;
    specularExp *= expMult;
    return this;
  }

  LightingParams applyTxMaps(float3 txMapsValue){    
    txMapsApplied = true;
    specularValue = txMapsValue.x * specularValue;
    specularExp = txMapsValue.y * specularExp + 1;
    #ifdef HAS_SUN_SPECULAR
      sunSpecularValue = (txMapsValue.z * txMapsValue.y) * sunSpecularValue;
      sunSpecularExp = txMapsValue.y * sunSpecularExp + 1;  
    #endif
    return this;
  }

  float3 calculate();
};

LightingParams getLightingParams(float3 posC, float3 toCamera, float3 normalW, float3 txDiffuseValue, float shadow, float4 ambientAoMult){
  LightingParams result = (LightingParams)0;
  result.fill(posC, toCamera, normalW, txDiffuseValue, shadow, ambientAoMult);
  return result;
}

#if defined(NO_CARPAINT) || defined(CARPAINT_SIMPLE) || defined(CARPAINT_NM) || defined(CARPAINT_NM_UVMULT) || defined(CARPAINT_GLASS)

#ifndef SUNLIGHT_DOT_FUNC
  #define SUNLIGHT_DOT_FUNC(x) saturate(x)
#endif

float3 calculateLighting_tyres(float3 normalW, float shadow, float4 ambientAoMult){
  float dotValue = SUNLIGHT_DOT_FUNC(dot(normalW, -ksLightDirection.xyz));
  float3 light = ksLightColor.rgb * INPUT_DIFFUSE_K * dotValue;

  float3 iblAmbientMult = ambientAoMult.xyz;
  #ifndef NO_AMBIENT_MULT
    iblAmbientMult *= perObjAmbientMultiplier;
  #endif

  return light * shadow 
    + calculateAmbient(normalW, ambientAoMult) * saturate(normalW.y * 1.4 + 0.6)
    + pow(saturate(0.5 + 0.5 * normalW.y), 2) * SAMPLE_REFLECTION_FN(normalW, 100, false, REFL_SAMPLE_PARAM_DEFAULT) * iblAmbientMult;
}

float3 calculateLighting_base(float3 normalW, float shadow, float4 ambientAoMult){
  float dotValue = SUNLIGHT_DOT_FUNC(dot(normalW, -ksLightDirection.xyz));
  float3 light = ksLightColor.rgb * INPUT_DIFFUSE_K * dotValue;
  return light * shadow + calculateAmbient(normalW, ambientAoMult);
}

float3 calculateLighting_base(float3 normalW, float3 txDiffuseValue, float shadow, float4 ambientAoMult){
  #if defined AMBIENT_TYRES_FIX && defined EXTENDED_PEROBJECT_CB
    // float dotValue = saturate(dot(normalW, -ksLightDirection.xyz));
    // float3 light = ksLightColor.rgb * INPUT_DIFFUSE_K * dotValue;
    // return light * shadow * txDiffuseValue.rgb 
    //   + calculateAmbient(normalW, ambientAoMult) 
    //     * (saturate(normalW.y * 1.4 + 0.6) * extraAmbientBrightness/2 + txDiffuseValue.rgb * pow(saturate(normalW.y + 1), 0.5));
    return calculateLighting_tyres(normalW, shadow, ambientAoMult) * txDiffuseValue.rgb;
  #else
    return calculateLighting_base(normalW, shadow, ambientAoMult) * txDiffuseValue.rgb;
  #endif
}

float3 calculateLighting_spec(LightingParams L){
  #ifdef CALCULATELIGHTING_USETXSPECULAR_AS_REFLECTANCE_MODEL
    return L.txSpecularValue * L.specularValue * L.shadow * extSpecularColor;
  #endif
  return reflectanceModel(-L.toCamera, -ksLightDirection.xyz, L.normalW, max(L.specularExp, 1)) 
    * L.specularValue * L.shadow * extSpecularColor * L.txSpecularValue;
}

float3 calculateLighting(LightingParams L){
  #ifdef SIMPLEST_LIGHTING
    return L.txDiffuseValue * INPUT_AMBIENT_K;
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  #if defined(SIMPLIFED_SPECULARS) && defined(NO_EXTSPECULAR)
    return calculateLighting_base(L.normalW, L.txDiffuseValue, L.shadow, L.ambientAoMult)
      + calculateEmissive(L.posC, L.txDiffuseValue.rgb, L.txEmissiveValue);
  #else
    return calculateLighting_base(L.normalW, L.txDiffuseValue, L.shadow, L.ambientAoMult) 
      + calculateLighting_spec(L)
      + calculateEmissive(L.posC, L.txDiffuseValue.rgb, L.txEmissiveValue);
  #endif
}

float3 LightingParams::calculate(){
  return calculateLighting(this);
}

// MultiMap shaders

#else

float4 getTxDetailValue(float2 uv, bool sampleBias = false){
  #ifdef TXDETAIL_VARIATION
    [branch]
    if (TXDETAIL_VARIATION) {
      return textureSampleVariation(txDetail, samLinear, uv * INPUT_DETAIL_UV_MULT, 
        sampleBias ? 0.8 : 1, 0.05);
    } else {
  #endif
  return txDetail.SampleBias(samLinear, uv * INPUT_DETAIL_UV_MULT, sampleBias ? -1 : 0);
  #ifdef TXDETAIL_VARIATION
    }
  #endif
}

float considerDetails(float2 uv, inout float4 txDiffuseValue, inout float3 txMapsValue, bool sampleBias = false){
  #ifdef REDUCE_DIFFUSE_AO
    float maxValue = max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z));
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif
  if (!INPUT_USE_DETAIL) return 1;
  float4 txDetailValue = getTxDetailValue(uv, sampleBias);
  txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
  txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, 1 - txDiffuseValue.a);
  return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
}

float considerDetails(float2 uv, inout float4 txDiffuseValue, bool sampleBias = false){
  #ifdef REDUCE_DIFFUSE_AO
    float maxValue = max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z));
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif
  if (!INPUT_USE_DETAIL) return 1;
  float4 txDetailValue = getTxDetailValue(uv, sampleBias);
  txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
  return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
}

#ifdef CARPAINT_NMDETAILS
float considerNmDetails(float2 uv, float3 tangentW, float3 bitangentW,
    inout float4 txDiffuseValue, inout float3 txMapsValue, inout float3 normalW
    #ifdef USE_HILL12_REORIENTATION
    , float3 txNormalValue
    #endif
    #ifdef CARPAINT_NMDETAILS_PBR_MODE
    , bool usePBRFormat, float blurColor, out float3 pbrValues
    #endif
    #ifdef CARPAINT_NMDETAILS_ALPHA
    , out float alpha
    #endif
    ){
  #ifdef REDUCE_DIFFUSE_AO
    float maxValue = max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z));
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif

  #ifdef NO_NORMALMAPS
    #ifdef CARPAINT_NMDETAILS_PBR_MODE
      pbrValues = 1;
    #endif

    #ifdef CARPAINT_NMDETAILS_ALPHA
      alpha = 1;
    #endif

    #ifdef CONSIDERNMDETAILS_LOAD_TX
      txDiffuseValue = txDiffuse.Sample(samLinear, uv);
      txMapsValue = txMaps.Sample(samLinear, uv).xyz;
    #endif

    if (INPUT_USE_DETAIL){
      uv *= INPUT_DETAIL_UV_MULT;
      float4 txDetailValue = txDetail.Sample(samLinear, uv);

      #if !defined(CARPAINT_SKINNED_NM)
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
      #endif
    }

    return 1;
  #else
    if (INPUT_USE_DETAIL){
      uv *= INPUT_DETAIL_UV_MULT;

      #ifndef CARPAINT_NMDETAILS_PBR_MODE
        float blurColor = 0;
      #endif

      #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION) && !defined(NO_MULTIMAP_TEXTURE_VARIATION)
        float4 txNormalDetailValue;
        float4 txDetailValue;
        
        [branch]
        if (ksAlphaRef == -193){
          txNormalDetailValue = textureSampleVariation(txNormalDetail, samLinear, uv, 0.5);
          txDetailValue = textureSampleVariation(txDetail, samLinear, uv, lerp(0.5, 10, blurColor));
        } else {
          txNormalDetailValue = txNormalDetail.Sample(samLinear, uv);
          txDetailValue = txDetail.SampleBias(samLinear, uv, blurColor * 10);
        }
      #else
        #ifdef CARPAINT_NMDETAILS_ALPHA
          float4 txNormalDetailValue = txNormalDetail.Sample(samLinear, uv);
        #else
          float4 txNormalDetailValue = txNormalDetail.Sample(samLinearSimple, uv);
        #endif
        float4 txDetailValue = txDetail.Sample(samLinear, uv);
      #endif

      #ifdef CARPAINT_NMDETAILS_ALPHA
        alpha = txNormalDetailValue.a;
      #endif

      #ifdef CARPAINT_NMDETAILS_PBR_MODE
        if (usePBRFormat){
          pbrValues = float3(txNormalDetailValue.zw, txDetailValue.w);
          txDetailValue.a = 1;
          txNormalDetailValue.z = sqrt(saturate(1 - dot2(txNormalDetailValue.xy * 2 - 1))) * 0.5 + 0.5;
        } else {
          pbrValues = float3(1, 0, 1);
        }
      #endif

      float transparency = 1 - txDiffuseValue.a;
      #ifdef USE_HILL12_REORIENTATION

        float3x3 m = float3x3(tangentW, normalW, bitangentW);
        #ifdef USE_HILL12_REORIENTATION_UDN
          float3 t = txNormalValue.xzy * 2.0 - 1.0;
          float3 u = lerp(float3(0, 0, 1), txNormalDetailValue.xyz * 2.0 - 1.0, detailNormalBlend * transparency);
          normalW = normalize(mul(float3(t.xy + u.xy, t.z), m));
        #else
          float3 t = txNormalValue.xzy * 2 + float3(-1, -1, 0);
          float3 u = lerp(float3(0, 0, 1), txNormalDetailValue.xyz * float3(-2, -2, 2) + float3(1, 1, -1), detailNormalBlend * transparency);
          normalW = normalize(mul(t * dot(t, u) - u * t.z, m));
        #endif

      #else
        float3 txNormalDetailAligned = txNormalDetailValue.xyz * 2 - 1;
        float3x3 m = float3x3(cross(normalW, tangentW), normalW, cross(normalW, bitangentW));
        float3 normalDetailW = normalize(mul(transpose(m), txNormalDetailAligned.xzy));
        normalW = normalize(lerp(normalW, normalDetailW, detailNormalBlend * transparency));
      #endif

      #if !defined CARPAINT_SKINNED_NM
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, transparency);
        txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, transparency);
      #endif

      return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
    } else {
      #ifdef CARPAINT_NMDETAILS_PBR_MODE
        pbrValues = 1;
      #endif
      #ifdef CARPAINT_NMDETAILS_ALPHA
        alpha = 1;
      #endif
      return 1;
    }
  #endif
}
#endif

#define NO_SUNSPEC_MAT (defined(CARPAINT_NMDETAILS) || defined(CARPAINT_SIMPLE_REFL) || defined(CARPAINT_AT) || defined(CARPAINT_SKINNED))

#if !NO_SUNSPEC_MAT
float3 calculateMapsLighting_wSun(LightingParams L){
  if (!L.txMapsApplied) return 0/0;
  
  #ifdef SIMPLEST_LIGHTING
    return L.txDiffuseValue * INPUT_AMBIENT_K;
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  float lightValue = saturate(dot(L.normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * INPUT_DIFFUSE_K * lightValue;
  float3 diffuseAmbient = diffuse * L.shadow + calculateAmbient(L.normalW, L.ambientAoMult);

  float specularBase = saturate(dot(normalize(-L.toCamera - ksLightDirection.xyz), L.normalW));
  #ifdef SIMPLIFED_SPECULARS
    float sunSpecularResult = pow(specularBase, max(L.sunSpecularExp, 1)) * L.shadow;
    float3 specularPart = sunSpecularResult * L.sunSpecularValue * extSpecularColor * L.txSpecularValue;
  #else
    float3 baseSpecularResult = pow(specularBase, max(L.specularExp, 1)) * lerp(1, getBaseSpecularColor(L.txDiffuseValue.xyz, extColoredReflectionNorm), extColoredBaseReflection);
    float3 sunSpecularResult = pow(specularBase, max(L.sunSpecularExp, 1)) * L.txSpecularValue;
    float3 specularPart = (baseSpecularResult * L.specularValue + sunSpecularResult * L.sunSpecularValue) * L.shadow * extSpecularColor;
  #endif

  return diffuseAmbient * L.txDiffuseValue + specularPart * lightValue + calculateEmissive(L.posC, L.txDiffuseValue, L.txEmissiveValue);
}
#endif

float3 calculateMapsLighting_woSun(LightingParams L){
  if (!L.txMapsApplied) return 0/0;

  #ifdef SIMPLEST_LIGHTING
    return L.txDiffuseValue * INPUT_AMBIENT_K;
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  float lightValue = saturate(dot(L.normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * INPUT_DIFFUSE_K * lightValue;
  float3 diffuseAmbient = diffuse * L.shadow + calculateAmbient(L.normalW, L.ambientAoMult);

  float specularBase = saturate(dot(normalize(-L.toCamera - ksLightDirection.xyz), L.normalW));
  float3 specularPart = pow(specularBase, max(L.specularExp, 1)) * L.shadow * L.specularValue * extSpecularColor;  
  return diffuseAmbient * L.txDiffuseValue + specularPart * lightValue * L.txSpecularValue + calculateEmissive(L.posC, L.txDiffuseValue, L.txEmissiveValue);
}

float3 calculateMapsLighting(LightingParams L){
  if (!L.txMapsApplied) return 0/0;

  #ifdef SIMPLEST_LIGHTING
    return L.txDiffuseValue * INPUT_AMBIENT_K;
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  #if NO_SUNSPEC_MAT
    return calculateMapsLighting_woSun(L);
  #else
    return calculateMapsLighting_wSun(L);
  #endif
}

float3 LightingParams::calculate(){
  return calculateMapsLighting(this);
}

#endif

float3 calculateBounceBackLighting(float3 toCamera, float3 normalW,
    float3 txDiffuseValue, float3 txBounceBackValue, float shadow, float expValue){
  float3 lightValue = ksLightColor.rgb * txDiffuseValue * shadow;
  return pow(saturate(dot(toCamera, ksLightDirection.xyz)), expValue) * lightValue * txBounceBackValue;
}

#include "utils_ps_fog.fx"

#if defined(ALLOW_EXTRA_SHADOW) && !defined(NO_SHADOWS)
  float2 getExtraShadow(float3 pos){
    float3 rel = pos - shadowPos;
    float dist = dot(rel, rel);

    float result = saturate(dist * shadowR1 + shadowR2);
    #ifdef EXTRA_SHADOW_DARKER
      return float2(result, lerp(1, result, shadowOpacityDarker));
    #else
      return float2(result, lerp(1, result, shadowOpacity));
    #endif
  }

  #define APPLY_EXTRA_SHADOW \
      float2 extraShadow = getExtraShadow(pin.PosC);

  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE \
      float2 extraShadow = getExtraShadow(pin.PosC);\
      shadow *= extraShadow.x;
#else
  float getExtraShadow(float3 pos){
    return 1;
  }

  #define APPLY_EXTRA_SHADOW float2 extraShadow = 1;
  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE float2 extraShadow = 1;
#endif

// Warning: extra shadow is not applied with G-buffer
#ifdef EXTRA_SHADOW_AFFECTS_REFLECTION
  #define EXTRA_SHADOW_REFLECTION extraShadow.x
#else
  #define EXTRA_SHADOW_REFLECTION 1
#endif

float posBasedVariation(float3 posC){
  return txNoise.SampleLevel(samLinearSimple, (ksCameraPosition.xz + posC.xz) / 1000, 0).x;
}

float3 adjustTextureColor(float3 color, float3 normal, float variation){
  #ifdef NO_ADJUSTING_COLOR
    return color;
  #else
    [branch]
    if (seasonWinter + seasonAutumn > 0){
      float leaf = saturate(seasonWinter * saturate(normal.y) + (color.g * 2 - color.r - color.b) * 20);

      if (seasonAutumn > 0){
        color = lerp(color, float3(
          saturate(color.r * 0.2 + color.g * 1.6), 
          saturate(color.g * (1.3 - variation * variation) + color.r * 0.2), 
          saturate(color.b - 0.4 * color.g)), seasonAutumn * leaf);
      }

      color = lerp(color, saturate(luminance(color) * ((float3(1, 1.1, 1.2) * 1.6 + saturate(seasonWinter * 2 - 1) * 0.4) * 0.85)), saturate(seasonWinter * 2) * leaf);
    }
    return color;
  #endif
}

#define ADJUSTCOLOR(v) v.rgb = adjustTextureColor(v.rgb, normalW, 0.5);
#define ADJUSTCOLOR_PV(v) v.rgb = adjustTextureColor(v.rgb, normalW, posBasedVariation(pin.PosC));

// #define ADJUSTCOLOR(v) v.rgb = v.rgb;
// #define ADJUSTCOLOR_PV(v) v.rgb = v.rgb;