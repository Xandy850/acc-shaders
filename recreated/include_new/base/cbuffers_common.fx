#ifndef _BASE_CBUFFERS_COMMON_HLSL_
#define _BASE_CBUFFERS_COMMON_HLSL_

#include "include/common.hlsl"

#ifndef CB_MATERIAL_EXTRA_0
  #define CB_MATERIAL_EXTRA_0
#endif

#ifndef CB_MATERIAL_EXTRA_1
  #define CB_MATERIAL_EXTRA_1
#endif

#ifndef CB_MATERIAL_EXTRA_2
  #define CB_MATERIAL_EXTRA_2
#endif

#ifndef CB_MATERIAL_EXTRA_3
  #define CB_MATERIAL_EXTRA_3
#endif

#ifndef CB_MATERIAL_EXTRA_4
  #define CB_MATERIAL_EXTRA_4
#endif

#define FLAG_ISTRACKMATERIAL 1
#define FLAG_ISINTERIORMATERIAL 2
#define FLAG_GBUFFERPREMULTIPLIEDALPHA 4
#define FLAG_ALPHATEST 8
#define FLAG_RAINFX_ROUGH 16
#define FLAG_RAINFX_SMOOTH 32
#define FLAG_RAINFX_SOAKING 64
#define FLAG_FIXED_EMISSIVE 128
#define HAS_FLAG(FLAG) ((bool)(extFlags & FLAG))

cbuffer cbMaterial : register(b4) {
  float ksAmbient;
  float ksDiffuse;
  float ksSpecular;
  float ksSpecularEXP;

  #ifdef REDEFINE_KSEMISSIVE
    float3 ksEmissivePad0;
  #else
    float3 ksEmissive;
  #endif
  float ksAlphaRef;

  PARAM_CSP(uint, extFlags);
  PARAM_CSP(float, extExtraSharpLocalReflections);

  CB_MATERIAL_EXTRA_0
  CB_MATERIAL_EXTRA_1
  CB_MATERIAL_EXTRA_2
  CB_MATERIAL_EXTRA_3
  CB_MATERIAL_EXTRA_4
}

#ifdef MODE_KUNOS
  #define extFlags 0
  #define extExtraSharpLocalReflections 0
#endif

#ifndef CBCAMERA_DEFINED
  cbuffer cbCamera : register(b0) {
    float4x4 ksView;
    float4x4 ksProjection;
    float4x4 ksMVPInverse;
    float4 ksCameraPosition;
    float ksNearPlane;
    float ksFarPlane;
    float ksFOV;
    float ksDofFactor;

    #ifndef MODE_KUNOS
      float4 extScreenSize;

      #ifdef CREATE_MOTION_BUFFER
        float4x4 ksViewProjPrev;
      #endif
    #endif
  }
#endif

#ifndef EXCLUDE_PEROBJECT_CB
  cbuffer cbPerObject : register(b1) {
    float4x4 ksWorld;

    #ifndef MODE_KUNOS
      float vaoSecondaryMix;
      float perObjAmbientMultiplier;
      float extMotionStencil;
      float _cbPerObject_pad0;

      #ifdef CREATE_MOTION_BUFFER
        float4x4 ksWorldPrev;
      #endif
    #endif
  }
#endif

cbuffer cbLighting : register(b2) {  
  float4 ksLightDirection;
  float4 ksAmbientColor_sky;
  float4 ksLightColor;
  float4 ksHorizonColor;
  float4 ksZenithColor;
  // 5*16=80

  float ksExposure;
  float ksScreenWidth;
  float ksScreenHeight;
  float ksFogLinear;

  float ksFogBlend;
  float3 ksFogColor;

  float ksCloudCover;
  float ksCloudCutoff;
  float ksCloudColor;
  float ksCloudOffset;

  float ksMinExposure;
  float ksMaxExposure;
  float ksDofFocus;
  float ksDofRange;

  float ksSaturation;
  float ksGameTime;

  #ifndef MODE_KUNOS
    float extSceneRain;
    float extSceneWetness;
    float extSceneWater;
    float extFrameTime;

    float extVaoMode;
    uint extUseNewFog;

    float extFogConstantPiece; 
    float extFogBacklitMult; // 0.5
    float extFogBacklitExp; // 12
    float extFogExp;

    float3 extAdditionalAmbientColor;
    float extPhotoElBoost;
    
    float3 extAdditionalAmbientDir;
    uint extMSAA;

    float3 extBaseAmbient;
    float extLODDistanceMult;

    float3 extSpecularColor;
    float extEmissiveMult;

    float2 extWindVel;
    float extWindSpeed;
    float extWindWave;

    float2 extWindVelPrev;
    float extWindSpeedPrev;
    float extWindWavePrev;

    float extGlowBrightness;
    float extForceEmissive;
    float extCloudShadowBase;
    float extIBLAmbient;

    float extIBLAmbientBaseThreshold;
    float extIBLAmbientBrightness;
    float extIBLAmbientSaturation;
    float extCloudShadowOpacity;
    float4x4 extCloudShadowMatrix;

    float3 extCPLDir;
    float extCPLFactor;

    float3 extDirUp;
    float _lighting_pad0;
  #endif
}

// #define extSceneRain ksMaxExposure
// #define extSceneWetness ksDofRange
// #define extSceneWater ksDofFocus

// #ifndef NO_SHADOWS
cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;
  float4x4 ksShadowMatrix1;
  float4x4 ksShadowMatrix2;
  float3 bias;
  float textureSize;

  #if defined(EXTENDED_PEROBJECT_CB) && !defined(MODE_KUNOS)
    float shadowR1;
    float3 shadowPos;

    float shadowR2;
    float shadowOpacity;
    float shadowOpacityDarker;
    float extraAmbientBrightness;

    float reflectionsTweak;
    // float shSet;
    float extShadowRemap0;
    float extShadowRemap1;
    float _padShadows0;

    // float4 shValues[9];
  #endif
}
// #endif

#ifdef INCLUDE_PARTICLE_CB 
  cbuffer cbParticle : register(b5) {
    float emissiveBlend;
    float minDistance;
    float shaded;
    float boh;
  }
#endif

#if defined(MODE_GRASSFX)
  // #define ksAmbientColor_sky float3(0, 0, 0)
  #define extBaseAmbient float3(0, 0, 0)
#endif

#if defined(MODE_LIGHTMAP)
  // #define ksAmbientColor_sky float3(0, 0, 0)
  #define extBaseAmbient float3(0, 0, 0)
  #define ksSpecular 0
#endif

#ifdef MODE_KUNOS
  #define extBaseAmbient float3(0,0,0)
  #define extAdditionalAmbientColor float3(0,0,0)
  #define extAdditionalAmbientDir float3(0,0,0)
  #define extDirUp float3(0,1,0)
  #define extScreenSize float4(1920,1080,1./1920,1./1080)
  #define perObjAmbientMultiplier 1
  #define extForceEmissive 0
  #define extSpecularColor ksLightColor.xyz
  #define extGlowBrightness 0
  #define extMSAA 1
  #define extPhotoElBoost 0
  #define extSceneRain 0
  #define extSceneWetness 0
  #define extSceneWater 0
#endif

#ifndef AMBIENT_SIMPLE_FN
#define AMBIENT_SIMPLE_FN(x) saturate((x).y * 0.25 + 0.75)
#endif

float3 getAmbientBaseAt(float3 dirW, float4 ambientAoMult){
  #ifdef MICROOPTIMIZATIONS
    float ambientMultiplier = AMBIENT_SIMPLE_FN(dirW);
  #else
    float upMultiplier = dirW.y * 0.5 + 0.5;
    float ambientMultiplier = saturate(upMultiplier * 0.5 + 0.5);
  #endif

  float3 additionalAmbient = extAdditionalAmbientColor * saturate(dot(dirW, extAdditionalAmbientDir));
  return ambientAoMult.w * extBaseAmbient
    + max(0, ((ksAmbientColor_sky.rgb + additionalAmbient) * min(ambientAoMult.xyz, ambientMultiplier)));
}

float3 getAmbientBaseNonDirectional(){
  return extBaseAmbient + (ksAmbientColor_sky.rgb + extAdditionalAmbientColor) * 0.75;
}

#endif