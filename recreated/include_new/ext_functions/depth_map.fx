#ifndef NO_DEPTH_MAP
  #ifndef DEPTH_SLOT
    #define DEPTH_SLOT 17
  #endif

  #ifndef TXDEPTH_DEFINED
    TEXTURE2D_TYPE(txDepth, float, DEPTH_SLOT);
  #endif

  float linearize(float depth){
    return (2 * ksNearPlane) / (ksFarPlane + ksNearPlane - depth * (ksFarPlane - ksNearPlane));
  }

  float linearizeAccurate(float depth){
    return 2 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2 * depth - 1) * (ksFarPlane - ksNearPlane));
  }

  float getDepthRaw(float4 pinPosH){
    float value = txDepth.Load(int3(pinPosH.xy, 0)).x;
    return value ? value : 1e9;
  }

  float getDepth(float4 pinPosH){
    float value = txDepth.Load(int3(pinPosH.xy, 0)).x;
    return value ? linearize(value) : 1e9;
  }

  float getDepthAccurate(float4 pinPosH){
    float v = txDepth.Load(int3(pinPosH.xy, 0)).x;
    return v ? linearizeAccurate(v) : ksFarPlane;
  }

  float getMinDepth(float4 pinPosH){
    float value = max(
      max(
        txDepth.Load(int3(pinPosH.xy, 0), int2(-1, -1)).x,
        txDepth.Load(int3(pinPosH.xy, 0), int2(1, -1)).x),
      max(
        txDepth.Load(int3(pinPosH.xy, 0), int2(-1, 1)).x,
        txDepth.Load(int3(pinPosH.xy, 0), int2(1, 1)).x)
    );
    return linearize(value);
  }
#endif