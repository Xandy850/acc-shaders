#include "include/common.hlsl"
#include "include/samplers.hlsl"

#ifndef SHADOWS_FILTER_SIZE
  #define SHADOWS_FILTER_SIZE 3
#endif

#ifdef TARGET_VS
  #undef USE_DISC_SHADOWS
#endif

static const float SHADOWS_W[3][3] =
{
    { 0.5,1.0,0.5, },
    { 1.0,1.0,1.0, },
    { 0.5,1.0,0.5, }
};

#ifdef USE_DISC_SHADOWS
  #define SHADOW_SAMPLE_ARGS_DECL float deltaZ, float bias, float4 uv, float2 aliasingFix, Texture2D<float> tx
  #define SHADOW_SAMPLE_ARGS_PREP(DZ, BI, UV, TX) (DZ)*(BI), BI, UV, UV##_fwidth, TX
#else
  #define SHADOW_SAMPLE_ARGS_DECL float deltaZ, float4 uv, Texture2D<float> tx
  #define SHADOW_SAMPLE_ARGS_PREP(DZ, BI, UV, TX) (DZ)*(BI), UV, TX
#endif

// from https://github.com/TheRealMJP/Shadows/blob/master/Shadows/Mesh.hlsl
float cascadeShadowStep_optimized(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  const int FS_2 = SHADOWS_FILTER_SIZE / 2;

  float2 tc = uv.xy;

  float shadowMapSize = 1.0 / textureSize.x;
  float shadowMapSizeInv = textureSize.x;

  float4 s = 0.0f;
  float2 stc = (shadowMapSize * tc.xy) + float2(0.5f, 0.5f);
  float2 tcs = floor(stc);
  float2 fc;
  int row;
  int col;
  float w = 0.0f;
  float4 v1[FS_2 + 1];
  float2 v0[FS_2 + 1];

  fc.xy = stc - tcs;
  tc.xy = tcs * shadowMapSizeInv;

  for(row = 0; row < SHADOWS_FILTER_SIZE; ++row)
    for(col = 0; col < SHADOWS_FILTER_SIZE; ++col)
      w += SHADOWS_W[row][col];

  // -- loop over the rows
  [unroll]
  for(row = -FS_2; row <= FS_2; row += 2)
  {
    [unroll]
    for(col = -FS_2; col <= FS_2; col += 2)
    {
      float value = SHADOWS_W[row + FS_2][col + FS_2];

      if(col > -FS_2)
        value += SHADOWS_W[row + FS_2][col + FS_2 - 1];

      if(col < FS_2)
        value += SHADOWS_W[row + FS_2][col + FS_2 + 1];

      if(row > -FS_2) {
        value += SHADOWS_W[row + FS_2 - 1][col + FS_2];

        if(col < FS_2)
          value += SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1];

        if(col > -FS_2)
          value += SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1];
      }

      if(value != 0.0f)
      {
        float sampleDepth = comparisonValue;

        // #if UsePlaneDepthBias_
        //   // Compute offset and apply planar depth bias
        //   float2 offset = float2(col, row) * texelSize;
        //   sampleDepth += dot(offset, receiverPlaneDepthBias);
        // #endif

        v1[(col + FS_2) / 2] = tx.GatherCmp(samShadow, tc.xy,
                               sampleDepth, int2(col, row));
      }
      else
        v1[(col + FS_2) / 2] = 0.0f;

      if(col == -FS_2)
      {
        s.x += (1.0f - fc.y) * (v1[0].w * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].z * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2 + 1.0f])
                   + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[0].x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].y * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2 + 1])
                   +  SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        if(row > -FS_2)
        {
          s.z += (1.0f - fc.y) * (v0[0].x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                       - SHADOWS_W[row + FS_2 - 1][col + FS_2] * fc.x)
                       + v0[0].y * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                       - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1])
                       + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[0].w * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2] * fc.x)
                    + v1[0].z * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      }
      else if(col == FS_2)
      {
        s.x += (1 - fc.y) * (v1[FS_2].w * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                   - SHADOWS_W[row + FS_2][col + FS_2]) + SHADOWS_W[row + FS_2][col + FS_2])
                   + v1[FS_2].z * fc.x * SHADOWS_W[row + FS_2][col + FS_2]);
        s.y += fc.y * (v1[FS_2].x * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                   - SHADOWS_W[row + FS_2][col + FS_2] ) + SHADOWS_W[row + FS_2][col + FS_2])
                   + v1[FS_2].y * fc.x * SHADOWS_W[row + FS_2][col + FS_2]);
        if(row > -FS_2) {
          s.z += (1 - fc.y) * (v0[FS_2].x * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + v0[FS_2].y * fc.x * SHADOWS_W[row + FS_2 - 1][col + FS_2]);
          s.w += fc.y * (v1[FS_2].w * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + v1[FS_2].z * fc.x * SHADOWS_W[row + FS_2 - 1][col + FS_2]);
        }
      }
      else
      {
        s.x += (1 - fc.y) * (v1[(col + FS_2) / 2].w * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 0] ) + SHADOWS_W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].z * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 0]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 1]) + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[(col + FS_2) / 2].x * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2-1]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 0]) + SHADOWS_W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].y * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 0]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 1]) + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        if(row > -FS_2) {
          s.z += (1 - fc.y) * (v0[(col + FS_2) / 2].x * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0])
                      + v0[(col + FS_2) / 2].y * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 0]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[(col + FS_2) / 2].w * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0])
                      + v1[(col + FS_2) / 2].z * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 0]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      }

      if(row != FS_2)
        v0[(col + FS_2) / 2] = v1[(col + FS_2) / 2].xy;
    }
  }

  return saturate((uv.z > 1 ? 1 : 0) + dot(s, 1.0f) / w);
}

#if SHADOWS_FILTER_SIZE == 4
  #define FilterSize_ 7
#elif SHADOWS_FILTER_SIZE == 3
  #define FilterSize_ 5
#elif SHADOWS_FILTER_SIZE == 2
  #define FilterSize_ 3
#else
  #define FilterSize_ 2
#endif

float SampleShadowMap(in float2 base_uv, in float u, in float v, in float2 shadowMapSizeInv,
                      in float comparisonValue, Texture2D<float> tx) {
    float2 uv = base_uv + float2(u, v) * shadowMapSizeInv;
    return tx.SampleCmpLevelZero(samShadow, uv, comparisonValue);
}

float cascadeShadowStep_witness(SHADOW_SAMPLE_ARGS_DECL) {
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  float shadowMapSize = 1.0 / textureSize.x;
  float shadowMapSizeInv = textureSize.x;

  float2 uvAdj = uv.xy * shadowMapSize; // 1 unit - 1 texel
  float2 base_uv;
  base_uv.x = floor(uvAdj.x + 0.5);
  base_uv.y = floor(uvAdj.y + 0.5);

  float s = (uvAdj.x + 0.5 - base_uv.x);
  float t = (uvAdj.y + 0.5 - base_uv.y);

  base_uv -= float2(0.5, 0.5);
  base_uv *= shadowMapSizeInv;

  float sum = 0;
  float mult = saturate(comparisonValue * 40) * saturate((1 - comparisonValue) * 40);

  #if FilterSize_ == 2
    return lerp(1, tx.SampleCmpLevelZero(samShadow, uvAdj.xy, comparisonValue), mult);
  #elif FilterSize_ == 3
    float uw0 = (3 - 2 * s);
    float uw1 = (1 + 2 * s);
    float u0 = (2 - s) / uw0 - 1;
    float u1 = s / uw1 + 1;
    float vw0 = (3 - 2 * t);
    float vw1 = (1 + 2 * t);
    float v0 = (2 - t) / vw0 - 1;
    float v1 = t / vw1 + 1;
    sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, comparisonValue, tx);
    return lerp(1, sum / 16, mult);
  #elif FilterSize_ == 5
    float uw0 = (4 - 3 * s);
    float uw1 = 7;
    float uw2 = (1 + 3 * s);
    float u0 = (3 - 2 * s) / uw0 - 2;
    float u1 = (3 + s) / uw1;
    float u2 = s / uw2 + 2;
    float vw0 = (4 - 3 * t);
    float vw1 = 7;
    float vw2 = (1 + 3 * t);
    float v0 = (3 - 2 * t) / vw0 - 2;
    float v1 = (3 + t) / vw1;
    float v2 = t / vw2 + 2;
    sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, comparisonValue, tx);
    return lerp(1, sum / 144, mult);
  #elif FilterSize_ == 7
    float uw0 = (5 * s - 6);
    float uw1 = (11 * s - 28);
    float uw2 = -(11 * s + 17);
    float uw3 = -(5 * s + 1);
    float u0 = (4 * s - 5) / uw0 - 3;
    float u1 = (4 * s - 16) / uw1 - 1;
    float u2 = -(7 * s + 5) / uw2 + 1;
    float u3 = -s / uw3 + 3;
    float vw0 = (5 * t - 6);
    float vw1 = (11 * t - 28);
    float vw2 = -(11 * t + 17);
    float vw3 = -(5 * t + 1);
    float v0 = (4 * t - 5) / vw0 - 3;
    float v1 = (4 * t - 16) / vw1 - 1;
    float v2 = -(7 * t + 5) / vw2 + 1;
    float v3 = -t / vw3 + 3;
    sum += uw0 * vw0 * SampleShadowMap(base_uv, u0, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(base_uv, u1, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw0 * SampleShadowMap(base_uv, u2, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw3 * vw0 * SampleShadowMap(base_uv, u3, v0, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(base_uv, u0, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(base_uv, u1, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw1 * SampleShadowMap(base_uv, u2, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw3 * vw1 * SampleShadowMap(base_uv, u3, v1, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw2 * SampleShadowMap(base_uv, u0, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw2 * SampleShadowMap(base_uv, u1, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw2 * SampleShadowMap(base_uv, u2, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw3 * vw2 * SampleShadowMap(base_uv, u3, v2, shadowMapSizeInv, comparisonValue, tx);
    sum += uw0 * vw3 * SampleShadowMap(base_uv, u0, v3, shadowMapSizeInv, comparisonValue, tx);
    sum += uw1 * vw3 * SampleShadowMap(base_uv, u1, v3, shadowMapSizeInv, comparisonValue, tx);
    sum += uw2 * vw3 * SampleShadowMap(base_uv, u2, v3, shadowMapSizeInv, comparisonValue, tx);
    sum += uw3 * vw3 * SampleShadowMap(base_uv, u3, v3, shadowMapSizeInv, comparisonValue, tx);
    return lerp(1, sum / 2704, mult);
  #endif
}

#include "include/poisson.hlsl"

#ifdef USE_DISC_SHADOWS
  float cascadeShadowStep_disc(SHADOW_SAMPLE_ARGS_DECL) {
    #define SHADOW_POISSON_DISK_SIZE 8

    float comparisonValue = uv.z - deltaZ;
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
    float2 sampleScale = clamp(aliasingFix * 0.25, textureSize.x * 2, textureSize.x * 10);
    float valueOffset = bias * 200;
    float sum = 0;
    // float2 random = normalize(txNoise.SampleLevel(samPoint, uv.xy * 397 + ksGameTime, 0).xy - 0.5);
    for (uint i = 0; i < SHADOW_POISSON_DISK_SIZE; ++i) {
      float2 sampleDir = SAMPLE_POISSON(SHADOW_POISSON_DISK_SIZE, i);
      // sampleDir = reflect(sampleDir, random);
      float2 sampleOffset = sampleDir * sampleScale;
      sum += tx.SampleCmpLevelZero(samShadow, uv.xy + sampleOffset, comparisonValue - length(sampleOffset) * valueOffset);
    }
    return sum / SHADOW_POISSON_DISK_SIZE;
  }
#endif

// #define USE_LARGE_DISC_SHADOWS
#ifdef USE_LARGE_DISC_SHADOWS
  float cascadeShadowStep_largeDisc(SHADOW_SAMPLE_ARGS_DECL, float2 blurDir) {
    #define SHADOW_POISSON_DISK_SIZE 32

    float comparisonValue = uv.z - deltaZ;
    float mult = saturate(comparisonValue * 40) * saturate((1 - comparisonValue) * 40);
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
    float2 sampleScale = textureSize.x * 10;
    float valueOffset = 0;
    float sum = 0;
    for (uint i = 0; i < SHADOW_POISSON_DISK_SIZE; ++i) {
      float2 sampleDir = SAMPLE_POISSON(SHADOW_POISSON_DISK_SIZE, i);
      float2 sampleOffset = (sampleDir + blurDir * SAMPLE_POISSON_LENGTH(SHADOW_POISSON_DISK_SIZE, i)) * sampleScale;
      sum += tx.SampleCmpLevelZero(samShadow, uv.xy + sampleOffset, 
        comparisonValue - SAMPLE_POISSON_LENGTH(SHADOW_POISSON_DISK_SIZE, i) * 0.003);
      // sum += saturate((tx.SampleLevel(samLinearBorder1, uv.xy + sampleOffset, 0) - comparisonValue));
    }
    return lerp(1, sum / SHADOW_POISSON_DISK_SIZE, mult);
  }
#endif

#ifdef USE_STRETCHED_SHADOWS
  float cascadeShadowStep_stretched(SHADOW_SAMPLE_ARGS_DECL, float2 blurDir) {
    float comparisonValue = uv.z - deltaZ;
    float mult = saturate(comparisonValue * 40) * saturate((1 - comparisonValue) * 40);
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
    float2 sampleScale = textureSize.x * 10;
    float valueOffset = 0;
    float sum = 0;
    for (uint i = 0; i < 10; ++i) {
      sum += tx.SampleCmpLevelZero(samShadow, clamp(uv.xy, 0.0001, 0.9999), comparisonValue);
      uv.xy += blurDir * sampleScale;
      // sum += saturate((tx.SampleLevel(samLinearBorder1, uv.xy + sampleOffset, 0) - comparisonValue));
    }
    return lerp(1, sum / 10, mult);
  }
#endif

#ifdef USE_DISC_SHADOWS
  #define cascadeShadowStep cascadeShadowStep_disc
#elif defined(USE_LARGE_DISC_SHADOWS)
  #define cascadeShadowStep cascadeShadowStep_largeDisc
#elif defined(USE_STRETCHED_SHADOWS)
  #define cascadeShadowStep cascadeShadowStep_stretched
#elif defined(USE_OPTIMIZED_SHADOWS)
  #define cascadeShadowStep cascadeShadowStep_optimized
#else
  #define cascadeShadowStep cascadeShadowStep_witness
#endif
// #define cascadeShadowStep cascadeShadowStep_witness
// #define cascadeShadowStep cascadeShadowStep_disc
// #define cascadeShadowStep cascadeShadowStep_largeDisc
// #define cascadeShadowStep cascadeShadowStep_optimized

bool checkCascade(float4 uv) {
  // return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
  return all(abs(uv.xy) < float2(1.0, 1.0));
}

float cascadeShadowStep_samplecmp(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
  // return tx.SampleCmpLevelZero(samShadow, uv.xy, comparisonValue);

  [unroll]
  for (int x = -1; x <= 1; x += 2) {
    [unroll]
    for (int y = -1; y <= 1; y += 2) {
      result += tx.SampleCmpLevelZero( samShadow, uv.xy, comparisonValue, int2(x, y) );
    }
  }

  return result * 0.25;
}

float cascadeShadowStep_sharper(SHADOW_SAMPLE_ARGS_DECL) {
  float4 result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 0; x += 1) {
    [unroll]
    for (int y = -1; y <= 0; y += 1) {
      result += tx.GatherCmp( samShadow, uv.xy, comparisonValue, int2(x, y) );
    }
  }

  float4 interpolation = bilinearFactors(uv.xy / textureSize.x - 0.5);
  return dot(result.wzxy, interpolation / 4);
}

float cascadeShadowStep_single(SHADOW_SAMPLE_ARGS_DECL) {
  // return tx.Sample(samLinear, uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5)) > 0.01;
  return uv.z >= 1 ? 1 : tx.SampleCmpLevelZero(samShadow, uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), uv.z);

  // float4 result = 0;
  // float comparisonValue = uv.z - deltaZ;
  // uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
  // result += tx.GatherCmp( samShadow, uv.xy, comparisonValue );
  // float4 interpolation = bilinearFactors(uv.xy / textureSize.x - 0.5);
  // return dot(result.wzxy, interpolation);
}

float cascadeShadowStep_orig(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  float2 rotate_at = float2(frac(10000 * uv.x), frac(11000 * uv.y));

  [unroll]
  for (int x = -1; x <= 0; x++) {
    [unroll]
    for (int y = -1; y <= 0; y++) {
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * reflect(float2(x, y), rotate_at), comparisonValue);
    }
  }

  return result;
}

#if SHADOWS_FILTER_SIZE == 1
  #define CASCADESHADOWSTEP cascadeShadowStep_single
#else
  #define CASCADESHADOWSTEP cascadeShadowStep
#endif
// #define CASCADESHADOWSTEP cascadeShadowStep_single

float getShadowBiasMult(float3 posC, float3 normalW, 
    float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, 
    #ifdef USE_STRETCHED_SHADOWS
    float2 blurDir,
    #endif
    float biasMultiplier, float fallbackShadow) {
  float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;

  #ifdef USE_STRETCHED_SHADOWS
    #define ARGS_EXTRA ,blurDir
  #else
    #define ARGS_EXTRA 
  #endif

  #ifdef NO_CLOUD_SHADOW
    float cloudShadowsMult = 1;
  #else
    float2 cloudShadowTex = mul(float4(posC, 1), extCloudShadowMatrix).xy;
    #ifdef TARGET_VS
      float cloudShadowsMult = saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 2.5) * extCloudShadowOpacity);
    #else
      float cloudShadowsMult = saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 1.5) * extCloudShadowOpacity);
    #endif
  #endif

  #ifdef USE_DISC_SHADOWS 
    float2 shadowTex0_fwidth = fwidth(shadowTex0.xy);
    float2 shadowTex1_fwidth = fwidth(shadowTex1.xy);
    float2 shadowTex2_fwidth = fwidth(shadowTex2.xy);
  #endif

  #if defined(SHADOWS_CASCADES_TRANSITION) && !defined(NO_SHADOWS_CASCADES_TRANSITION)
    float3 s = float3(1, 1, fallbackShadow);
    float3 x = float3(
      max(abs(shadowTex0.x), abs(shadowTex0.y)),
      max(abs(shadowTex1.x), abs(shadowTex1.y)),
      max(abs(shadowTex2.x), abs(shadowTex2.y)));
    float3 t0 = saturate(1 - x);

    [branch]
    if (x.z < 1) {
      s.z = lerp(fallbackShadow, CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.z, shadowTex2, txShadow2)), 
        saturate(t0.z * 4 + saturate(2 - x.y)));
    }

    [branch]
    if (x.y < 1) {
      s.y = CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.y, shadowTex1, txShadow1));
    }

    [branch]
    if (x.x < 1) {
      s.x = CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.x, shadowTex0, txShadow0));
    }

    float2 d0 = max(t0.yz - t0.xy, 0.01);
    float ret = (t0.x > 0
      ? lerp(s.y, s.x, saturate(1 / dot2(posC) + t0.x / d0.x))
      : lerp(s.z, s.y, t0.y > 0 ? saturate(t0.y / d0.y) : 0));
    ret = saturate(ret * extShadowRemap0 + extShadowRemap1);
    return ret * cloudShadowsMult;
  #else
    if (checkCascade(shadowTex0)) {
      return cloudShadowsMult * CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.x, shadowTex0, txShadow0) ARGS_EXTRA);
    } else

    if (checkCascade(shadowTex1)) {
      return cloudShadowsMult * CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.y, shadowTex1, txShadow1) ARGS_EXTRA);
    } else

    if (checkCascade(shadowTex2)) {
      return cloudShadowsMult * lerp(CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, bias.z, shadowTex2, txShadow2) ARGS_EXTRA), 
        fallbackShadow, saturate(-(shadowTex2.y + 0.5) * 2));
    } else
  #endif

  return cloudShadowsMult * fallbackShadow;
}

#ifndef USE_STRETCHED_SHADOWS
  float getShadow(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float fallbackShadow) {
    #ifdef USE_SHADOW_BIAS_MULT
      float biasMultiplier = shadowBiasMult + 1;
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackShadow);
    #elif defined GETSHADOW_BIAS_MULTIPLIER
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, GETSHADOW_BIAS_MULTIPLIER, fallbackShadow);
    #else
      return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, 1, fallbackShadow);
    #endif
  }

  float getShadow(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float3 fallbackShadow) {
    return getShadow(posC, normalW, shadowTex0, shadowTex1, shadowTex2, fallbackShadow.g);
  }

  float getShadowBiasMult(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float biasMultiplier, float3 fallbackShadow) {
    return getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackShadow.g);
  }
#endif
