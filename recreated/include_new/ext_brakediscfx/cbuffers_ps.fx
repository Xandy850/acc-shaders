cbuffer cbExtBrakeDisc : register(b6) {
  float3 wheelPos;
  float rimRadius;

  float3 wheelNormal;
  float rimHeight;

  float rimInternalRadius;
  float reflectionPowerMult;
  float simplifyNormalsK;
  float ambientMult;

  float discInternalRadius;
  float discInternalRadiusSharpness;
  float2 extBrakeDiscPadding;
};