#include "include_new/base/_include_vs.fx"

PS_IN_ShadowGetAt main(VS_IN vin) {
  PS_IN_ShadowGetAt vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.Tex = vin.Tex;
  return vout;
}
