#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_Grass main(VS_IN vin) {
  vin.PosL.xz += windOffset(vin.PosL.xyz, windFromTex(vin.Tex), 3.5) / 10;
  GENERIC_PIECE(PS_IN_Grass);
  vout.GrassThing = posW.xz;
  return vout;
}
