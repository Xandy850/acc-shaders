#define NO_CARPAINT
#define SUPPORTS_AO

#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  bool2 checkerFlag = frac(pin.PosH.xy / 64) > 0.5;
  float checkerPattern = all(checkerFlag) || all(!checkerFlag);
  checkerPattern = frac((pin.PosH.x + pin.PosH.y) / 8) > 0.3;

  // txDiffuseValue.rgb = float3(checkCascade(pin.ShadowTex0), checkCascade(pin.ShadowTex1), checkCascade(pin.ShadowTex2)) * (0.75 + 0.25 * normalW);
  // if (checkCascade(pin.ShadowTex0)) txDiffuseValue.rgb = float3(0.5, 0, 0);
  // else if (checkCascade(pin.ShadowTex1)) txDiffuseValue.rgb = float3(0, 0.5, 0);
  // else if (checkCascade(pin.ShadowTex2)) txDiffuseValue.rgb = float3(0, 0, 0.5);
  // else txDiffuseValue.rgb = float3(0, 0, 0);
  // if (pin.ShadowTex0.z >= 1) txDiffuseValue.x *= checkerPattern;
  // if (pin.ShadowTex1.z >= 1) txDiffuseValue.y *= checkerPattern;
  // if (pin.ShadowTex2.z >= 1) txDiffuseValue.z *= checkerPattern;
  // txDiffuseValue.x *= frac((pin.ShadowTex0.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex0.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.y *= frac((pin.ShadowTex1.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex1.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.z *= frac((pin.ShadowTex2.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex2.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.rgb = 0.5;
  // txDiffuseValue.rgb *= saturate(-dot(normalW, ksLightDirection.xyz));

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  #if defined ALLOW_EXTRA_FEATURES && ! defined NO_LIGHTING
    if (ksAlphaRef == -193){
      lighting += ksEmissive * saturate(dot(normalize(-toCamera), normalize(normalW)) * 4 - 3) * 12;
    }
  #endif

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, txDiffuseValue.a);
}
