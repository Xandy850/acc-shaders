#define NO_CARPAINT
#define INCLUDE_MULTILAYER_CB
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {  
  READ_VECTORS
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  float3 posW = pin.PosC + ksCameraPosition.xyz;
  #ifdef USE_MULTIMAP_TEXTURE_VARIATION
    float4 txDetailRValue;
    float4 txDetailGValue;
    float4 txDetailBValue;
    float4 txDetailAValue;

    [branch]
    if (ksAlphaRef == -193){
      txDetailRValue = textureSampleVariation(txDetailR, samLinear, posW.xz * multR);
      txDetailGValue = textureSampleVariation(txDetailG, samLinear, posW.xz * multG);
      txDetailBValue = textureSampleVariation(txDetailB, samLinear, posW.xz * multB);
      txDetailAValue = textureSampleVariation(txDetailA, samLinear, posW.xz * multA);
    } else {
      txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
      txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
      txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
      txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
    }
  #else
    float4 txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
    float4 txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
    float4 txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
    float4 txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.setSpecularMult(txDiffuseValue.a, 1);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, 1);
}
