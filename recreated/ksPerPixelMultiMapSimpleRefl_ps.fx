#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_SIMPLE_REFL
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(AO_REFLECTION, txMapsValue);
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = true;
  RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);  
}
