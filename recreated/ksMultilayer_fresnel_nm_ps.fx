#define NO_CARPAINT
#define INCLUDE_MULTILAYER3_CB
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_ps.fx"

Texture2D<float> txMirageMask : register(t19);

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  #if !defined(NO_NORMALMAPS) && !defined(NO_OPTIONAL_MAPS)
    float2 uv = pin.Tex * detailNMMult;
    // float4 txNormalValue = txDetailNM.Sample(samLinearSimple, uv);
    float4 txNormalValue = textureSampleVariation(txDetailNM, samLinear, uv);
    float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
    float3x3 m = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(m), txNormalAligned.xzy));
  #endif

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  // float2 cloudShadowTex = mul(float4(pin.PosC, 1), extCloudShadowMatrix).xy;
  // float cloudShadowsMult = saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 1.5) * extCloudShadowOpacity);

  float3 posW = pin.PosC + ksCameraPosition.xyz;
  #ifdef USE_MULTIMAP_TEXTURE_VARIATION
    float4 txDetailRValue;
    float4 txDetailGValue;
    float4 txDetailBValue;
    float4 txDetailAValue;

    [branch]
    if (ksAlphaRef == -193){
      txDetailRValue = textureSampleVariation(txDetailR, samLinear, posW.xz * multR);
      txDetailGValue = textureSampleVariation(txDetailG, samLinear, posW.xz * multG);
      txDetailBValue = textureSampleVariation(txDetailB, samLinear, posW.xz * multB);
      txDetailAValue = textureSampleVariation(txDetailA, samLinear, posW.xz * multA);
    } else {
      txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
      txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
      txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
      txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
    }
  #else
    float4 txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
    float4 txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
    float4 txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
    float4 txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue);

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = min(
      tarmacSpecularMultiplier * pow(
        coefficient, 
        fresnelEXP) 
        + fresnelC, 
      fresnelMaxLevel);
  intensity = txDiffuseValue.a * saturate(intensity);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.specularValue = intensity; // ksSpecular is not used, it’s not a mistake
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  #ifdef MODE_GRASSFX
    float damp = extSceneWetness * ((float3)(_AO_VAO)).g;
    L.specularExp = lerp(L.specularExp, 40, damp);
    L.specularValue = lerp(L.specularValue, 0.3, damp);
  #endif

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);

  #if !defined(ALLOW_RAINFX) && (defined(ALLOW_EXTRA_VISUAL_EFFECTS) || defined(MODE_GBUFFER))
    // float mirageMult = 1;
    float3 normalWBase = normalize(pin.NormalW);
    float NdotV = dot(-toCamera, normalWBase);
    float mirageBaseMult = saturate(remap(NdotV, 0.003 * mirageMult, 0.0029 * mirageMult, 0, 1));

    [branch]
    if (mirageBaseMult != 0){
      float distSqr = dot2(pin.PosC);
      float reflIntensity = mirageBaseMult * saturate((distSqr - 40000) / 10000) 
        * (1 - txMirageMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0));
      float3 reflDir = normalize(reflect(toCamera, normalWBase));
      float3 reflColor = getReflectionAt(reflDir, -toCamera, 0, true, 0);
      lighting = lerp(lighting, reflColor, reflIntensity);
      R.resultPower = reflIntensity;
      R.resultColor = reflIntensity * reflColor;
    }
  #endif

  RETURN(lighting, 1);
}
