#define NO_CARPAINT
#define SUPPORTS_AO

#define INPUT_DIFFUSE_K 0.25
#define INPUT_AMBIENT_K 0.45
#define INPUT_EMISSIVE3 0
#define INPUT_SPECULAR_K 0
#define INPUT_SPECULAR_EXP 0

#define LIGHTINGFX_KSDIFFUSE 0.55
#define LIGHTINGFX_NOSPECULAR

#include "include_new/base/_include_ps.fx"

cbuffer cbSingleColor : register(b5) {
  float4 colour;
  float extGlow;
  float3 extPad0;
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float3 lighting = colour.rgb;
  lighting *= AO_REFLECTION;
  lighting *= lerp(1, 16, extGlow * AO_REFLECTION * AO_REFLECTION);
  RETURN_BASE(lighting, colour.a);
}
