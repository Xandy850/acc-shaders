#define NO_CARPAINT
#define SUPPORTS_AO
// #define INPUT_SPECULAR_K 1
// #define INPUT_SPECULAR_EXP 20
  
#include "include/common.hlsl"
#define CB_MATERIAL_EXTRA_0\
  float multR;\
  float multG;\
  float multB;\
  float multA;\
  \
  float multTilingFix;\
  float fresnelC;\
  float fresnelEXP;\
  float fresnelMaxLevel;\
  \
  float magicMult;\
  float tarmacSpecularMultiplier;\
  PARAM_CSP(float, seasonAutumn);\
  PARAM_CSP(float, seasonWinter);
  \
  float tilingFixR;\
  float tilingFixG;\
  float tilingFixB;\
  float tilingFixA;
#include "include_new/base/_include_ps.fx"

#ifndef NO_OPTIONAL_MAPS
  Texture2D txNormalR : register(t21);
  Texture2D txNormalG : register(t22);
  Texture2D txNormalB : register(t23);
  Texture2D txNormalA : register(t25);
#endif

struct TilingParams {
  float f;
  float2 offa;
  float2 offb;
};

float rand(float n){ return frac(sin(n) * 43758.5453123); }
float hash(float2 p){ return rand(p.x * 0.317 + p.y * 0.819); }

float procNoise(float2 p){
  float2 i = floor(p);
  float2 f = frac(p);	
  return lerp(
    lerp(hash(i + float2(0, 0)), hash(i + float2(1, 0)), f.x),
    lerp(hash(i + float2(0, 1)), hash(i + float2(1, 1)), f.x), f.y);
}

TilingParams getTilingParams(float2 uv, float noiseMult) {
  #ifdef MODE_KUNOS
    float noise = procNoise(32 * uv * noiseMult);
  #else
    float noise = txNoise.Sample(samLinear, uv * noiseMult).x;
  #endif
  float l = noise * 8.0;
  float i = floor(l);
  TilingParams ret;
  ret.f = frac(l);
  ret.offa = sin(float2(3.0, 7.0) * (i + 0.0));
  ret.offb = sin(float2(3.0, 7.0) * (i + 1.0));
  return ret;
}

float4 txTilingFix(Texture2D tx, SamplerState sam, float2 uv, TilingParams TP, float bias = 1){
  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;    
  float4 cola = tx.SampleGrad(sam, uv + TP.offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + TP.offb, duvdx, duvdy);
  return lerp(cola, colb, smoothstep(0.2, 0.8, TP.f - 0.1 * dot(cola - colb, 1)));
}

bool useTilingFix(float v){
  // return false;
  #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION)
    return v;
  #else
    return false;
  #endif
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  float4 txDetailRValue;
  float4 txDetailGValue;
  float4 txDetailBValue;
  float4 txDetailAValue;
  #ifndef NO_OPTIONAL_MAPS
    float4 txNormalRValue;
    float4 txNormalGValue;
    float4 txNormalBValue;
    float4 txNormalAValue;
  #endif

  float3 posW = pin.PosC + ksCameraPosition.xyz;
  TilingParams TP = getTilingParams(posW.xz, multTilingFix * 0.05);

  [branch]
  if (useTilingFix(tilingFixR)){
    txDetailRValue = txTilingFix(txDetailR, samLinear, posW.xz * multR, TP);
    #ifndef NO_OPTIONAL_MAPS
      txNormalRValue = txTilingFix(txNormalR, samLinearSimple, posW.xz * multR, TP);
    #endif
  } else {
    txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
    #ifndef NO_OPTIONAL_MAPS
      txNormalRValue = txNormalR.Sample(samLinearSimple, posW.xz * multR);
    #endif
  }

  [branch]
  if (useTilingFix(tilingFixG)){
    txDetailGValue = txTilingFix(txDetailG, samLinear, posW.xz * multG, TP);
    #ifndef NO_OPTIONAL_MAPS
      txNormalGValue = txTilingFix(txNormalG, samLinearSimple, posW.xz * multG, TP);
    #endif
  } else {
    txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
    #ifndef NO_OPTIONAL_MAPS
      txNormalGValue = txNormalG.Sample(samLinearSimple, posW.xz * multG);
    #endif
  }

  [branch]
  if (useTilingFix(tilingFixB)){
    txDetailBValue = txTilingFix(txDetailB, samLinear, posW.xz * multB, TP);
    #ifndef NO_OPTIONAL_MAPS
      txNormalBValue = txTilingFix(txNormalB, samLinearSimple, posW.xz * multB, TP);
    #endif
  } else {
    txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
    #ifndef NO_OPTIONAL_MAPS
      txNormalBValue = txNormalB.Sample(samLinearSimple, posW.xz * multB);
    #endif
  }

  [branch]
  if (useTilingFix(tilingFixA)){
    txDetailAValue = txTilingFix(txDetailA, samLinear, posW.xz * multA, TP);
    #ifndef NO_OPTIONAL_MAPS
      txNormalAValue = txTilingFix(txNormalA, samLinearSimple, posW.xz * multA, TP);
    #endif
  } else {
    txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
    #ifndef NO_OPTIONAL_MAPS
      txNormalAValue = txNormalA.Sample(samLinearSimple, posW.xz * multA);
    #endif
  }

  #if !defined(NO_NORMALMAPS) && !defined(NO_OPTIONAL_MAPS)
    float4 nmCombined = 
        txNormalRValue * txMaskValue.x
      + txNormalGValue * txMaskValue.y
      + txNormalBValue * txMaskValue.z
      + txNormalAValue * txMaskValue.w;
    float3 txNormalAligned = nmCombined.xyz * 2 - (float3)1;
    float3x3 m = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(m), txNormalAligned.xzy));
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = min(
      tarmacSpecularMultiplier * pow(
        coefficient, 
        fresnelEXP) 
        + fresnelC, 
      fresnelMaxLevel);
  intensity = txDiffuseValue.a * saturate(intensity);
  // txDiffuseValue.rgb = float3(txDiffuseValue.a, 1 - txDiffuseValue.a, 0);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.specularValue = intensity; // ksSpecular is not used, it’s not a mistake
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
