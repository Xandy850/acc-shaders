#define CARPAINT_NM
#define GETNORMALW_OS_SAMPLER samLinearSimple
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Uv2 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, 1);
}
