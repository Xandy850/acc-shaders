#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
#define AMBIENT_TYRES_FIX
#define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_DARKER
#define EXTRA_SHADOW_AFFECTS_REFLECTION

#define CB_MATERIAL_EXTRA_4\
	float extGroundOcclusion;

#ifdef MICROOPTIMIZATIONS
  #define GETSHADOW_BIAS_MULTIPLIER 4
#else
  #define GETSHADOW_BIAS_MULTIPLIER 10
#endif

#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW_INC_DIFFUSE

  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float dirtyInv = saturate(1 - dirtyLevel);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.setSpecularMult(dirtyInv * txDiffuseValue.a, 1);
  L.multAO(saturate(extraShadow.y + saturate(-normalW.y)));
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting *= lerp(1, sqrt(saturate(normalW.y + 1)), extGroundOcclusion);

  ReflParams R = getReflParamsBase(dirtyInv * EXTRA_SHADOW_REFLECTION);

  float wetK = extSceneWetness * perObjAmbientMultiplier * EXTRA_SHADOW_REFLECTION * txDiffuseValue.a;
  R.fresnelC = lerp(R.fresnelC, 0.2, wetK);
  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 0.2, wetK);
  R.fresnelEXP = lerp(R.fresnelEXP, 1, wetK);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, 100, wetK);

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  R.resultColor = R.resultPower = 0;
  RETURN(withReflection, 1);
}
