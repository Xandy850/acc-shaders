#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "emissiveMappingExtra.hlsl"

RESULT_TYPE main(PS_IN_NmExtra1 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.txEmissiveValue = getEmissiveValue(txDiffuseValue, pin.Tex, pin.Extra < 0);
  L.applyTxMaps(txMapsValue.xyz);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, emAlphaFromDiffuse ? txDiffuseValue.a : txMapsValue.a);
}
