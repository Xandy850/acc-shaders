#define INCLUDE_PARTICLE_CB
#define EXCLUDE_PEROBJECT_CB
// #define FOG_PARTICLE_VERSION
// #define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_Particle main(VS_IN_Particle vin) {
  PS_IN_Particle vout;

  // float3 toCamera = vin.PosL.xyz - ksCameraPosition.xyz;
  // float wave = sin(ksGameTime / 10000 * (1 + frac(vin.PosL.z*0.01)) + vin.PosL.x*0);
  // vin.PosL.xz += normalize(cross(toCamera, float3(0, 1, 0)).xz) * saturate(pow(abs(wave) * 1.6, 10)) * sign(wave) / 30;

  float4 posW = vin.PosL;
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);

  [branch]
  if (shaded){
    shadows(posW, SHADOWS_COORDS);
    vout.NormalW = cross(float3(0, 1, 0), vout.PosC);
  } else {
    #ifndef NO_SHADOWS
      vout.ShadowTex0 = 0;
      vout.ShadowTex1 = 0;
      vout.ShadowTex2 = 0;
    #endif
    vout.NormalW = 0;
  }

  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_STATIC(vin.PosL);

  vout.Color = vin.Color;
  vout.Extra = -posV.z;
  return vout;
}
